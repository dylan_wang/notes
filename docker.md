### centOS下安装

#### 安装流程

```

#卸载旧版本
$ sudo yum remove docker \
	docker-client \
	docker-client-latest \
	docker-common \
	docker-latest \
	docker-latest-logrotate \
	docker-logrotate \
	docker-selinux \
	docker-engine-selinux \
	docker-engine
#安装必要程序
$ sudo yum install -y yum-utils device-mapper-persistent-data lvm2
#添加镜像
$ sudo yum-config-manager --add-repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
#清楚缓存
$ sudo yum makecache fast
#执行安装
$ sudo yum -y install docker-ce
#启动服务
$ sudo systemctl start docker
#卸载
$ sudo yum remove docker-ce
$ sudo rm -rf /var/lib/docker
```

#### 镜像加速

新版的 Docker 使用 /etc/docker/daemon.json（Linux） 或者 %programdata%\docker\config\daemon.json（Windows） 来配置 Daemon。

请在该配置文件中加入（没有该文件的话，请先建一个）：

```

{
  "registry-mirrors": ["http://hub-mirror.c.163.com"]
}
```

### 镜像管理

#### 查询网络镜像

```

$  docker search httpd
NAME                                    DESCRIPTION                                     STARS               OFFICIAL            AUTOMATED
httpd                                   The Apache HTTP Server Project                  2267                [OK]                
hypriot/rpi-busybox-httpd               Raspberry Pi compatible Docker Image with a …   45                                      
centos/httpd                                                                            22                                      [OK]
centos/httpd-24-centos7                 Platform for running Apache httpd 2.4 or bui…   20                                      
```
#### 抽取网络镜像

`$ docker pull httpd`


#### 查看本地镜像

```

$ docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
ubuntu              13.10               7f020f7bf345        4 years ago         185MB
```

#### 启动镜像

```

#启动镜像并静茹交互模式
$ sudo docker run -t -i ubuntu:13.10 /bin/bash
root@4e4ce204281a:/# ls
bin  boot  dev	etc  home  lib	lib64  media  mnt  opt	proc  root  run  sbin  srv  sys  tmp  usr  var
root@4e4ce204281a:/# 

```

#### 更新镜像

```

#进入镜像并更新系统
$ sudo docker run -t -i ubuntu:13.10 /bin/bash
root@4e4ce204281a:/# ls
bin  boot  dev	etc  home  lib	lib64  media  mnt  opt	proc  root  run  sbin  srv  sys  tmp  usr  var
root@4e4ce204281a:/# apt-get list t
E: Invalid operation list
root@4e4ce204281a:/# apt-get update
Ign http://archive.ubuntu.com saucy InRelease
Ign http://archive.ubuntu.com saucy-updates InRelease
.......
#更新完成退出
root@4e4ce204281a:/# exit
exit
#提交镜像
$ sudo docker commit -m "update" -a "dylan" 4e4ce204281 dylan/ubuntu:1.0.0
sha256:dda89e4c85a9957308e9d11a71062bdb47be15350e217c23944390e847eea87e
#查看本地镜像
$ sudo docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
dylan/ubuntu        1.0.0               dda89e4c85a9        7 seconds ago       185MB
ubuntu              13.10               7f020f7bf345        4 years ago         185MB


```

