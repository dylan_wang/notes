### 设置代理

```

$ git config --global http.proxy http://proxy.neusoft.com:8080/

git config --global http.proxy http://bin.wang.neu:@Wb201901@proxy.neusoft.com:8080/

$ git config --global https.proxy http://proxy.neusoft.com:8080/
```

### 查看代理

```

$ git config --global --get http.proxy
http://proxy.neusoft.com:8080/
$ git config --global --get https.proxy
http://proxy.neusoft.com:8080/
```

### 取消代理

```

$ git config --global --unset http.proxy
$ git config --global --unset https.proxy

```
