# 2.Spring

## 2.1Spring前世今生

​		相信经历过不使用框架开发Web 项目的70后、80后都会有如此感触，如今的程序员开发项目太轻松了，基本只需要关心业务如何实现，通用技术问题只需要集成框架便可。早在2007年，一个基于Java语言的开源框架正式发布，取了一个非常有活力且美好的名字，叫做Spring。它是一个开源的轻量级Java SE（Java标准版本）/Java EE（Java 企业版本）开发应用框架，其目的是用于简化企业级应用程序开发。应用程序是由一组相互协作的对象组成。而在传统应用程序开发中，一个完整的应用是由一组相互协作的对象组成。所以开发一个应用除了要开发业务逻辑之外，最多的是关注如何使这些对象协作来完成所需功能，而且要低耦合、高聚合。业务逻辑开发是不可避免的，那如果有个框架出来帮我们来创建对象及管理这些对象之间的依赖关系。可能有人说了，比如"抽象工厂、工厂方法模式"不也可以帮我们创建对象，"生成器模式"帮我们处理对象间的依赖关系，不也能完成这些功能吗?可是这些又需要我们创建另一些工厂类、生成器类，我们又要而外管理这些类，增加了我们的负担，如果能有种通过配置方式来创建对象，管理对象之间依赖关系，我们不需要通过工厂和生成器来创建及管理对象之间的依赖关系，这样我们是不是减少了许多工作，加速了开发，能节省出很多时间来干其他事。Spring 框架刚出来时主要就是来完成这个功能。
​		Spring 框架除了帮我们管理对象及其依赖关系，还提供像通用日志记录、性能统计、安全控制、异常处理等面向切面的能力，还能帮我管理最头疼的数据库事务，本身提供了一套简单的 JDBC访问实现，提供与第三方数据访问框架集成（如Hibernate、JPA），与各种 Java EE技术整合（如Java Mail、任务调度等等），提供一套自己的Web层框架 Spring MVC、而且还能非常简单的与第三方Web框架集成。从这里我们可以认为 Spring是一个超级粘合大平台，除了自己提供功能外，还提供粘合其他技术和框架的能力，从而使我们可以更自由的选择到底使用什么技术进行开发。而且不管是JANASE（C/S 架构）应用程序还是JAVAEE（B/S架构）应用程序都可以使用这个平台进行开发。如今的Spring 已经不再是一个框架，早已成为了一种生态。SpringBoot的便捷式开发实现了零配置，SpringCloud全家桶，提供了非常方便的解决方案。接下来，让我们来深入探讨 Spring 到底能给我们带来什么?

### 2.1.1一切从Bean开始

​		说到Bean这个概念，还得从 Java的起源说起。早在1996年，Java还只是一个新兴的、初出茅庐的编程语言。人们之所以关注她仅仅是因为，可以使用Java的 Applet来开发Web应用，作为浏览器组件。但开发者们很快就发现这个新兴的语言还能做更多的事情。与之前的所有语言不同，Java让模块化构建复杂的系统成为可能（当时的软件行业虽然在业务上突飞猛进，但当时开发用的是传统的面向过程开发思想，软件的开发效率一直踟蹰不前。伴随着业务复杂性的不断加深，开发也变得越发困难。其实，当时也是OOP思想飞速发展的时期，她在80年代末被提出，成熟于90年代，现今大多数编程语言都已经是面向对象的）。

​		司年12月，Sun公司发布了当时还名不见经传但后来人尽皆知的 JavaBean1.00-A规范。早期的JavaBean规范针对于Java，她定义了软件组件模型。这个规范规定了一整套编码策略，使简单的 Java象不仅可以被重用，而且还可以轻松地构建更为复杂的应用。尽管JavaBean最初是为重用应用组件而设计的，但当时他们却是主要用作构建窗体控件，毕竟在PC时代那才是主流。但相比于当时正如日中天的Delphi、VB和C++，它看起来还是太简易了，以至于无法胜任任何"实际的"工作需要。

​		复杂的应用通常需要事务、安全、分布式等服务的支持，但JavaBean并未直接提供。所以到了1998 年3月，Sun公司发布了EJB1.0规范，该规范把 Java组件的设计理念延伸到了服务器端，并提供了许多必须的企业级服务，但他也不再像早期的JavaBean那么简单了。实际上，除了名字叫EJB Bean 以外，其他的和 JavaBean 关系不大了。

​		尽管现实中有很多系统是基于EJB构建的，但EJB 从来没有实现它最初的设想∶简化开发。EJB的声明式编程模型的确简化了很多基础架构层面的开发，例如事务和安全;但另一方面 EJB在部署描述符配套代码实现等方面变得异常复杂。随着时间的推移，很多开发者对 EJB已经不再抱有幻想，开始寻求更简洁的方法。

​		现在Java 组件开发理念重新回归正轨。新的编程技术AOP和 DI的不断出现，他们为JavaBean提共了之前EJB才能拥有的强大功能。这些技术为POJO提供了类似EJB的声明式编程模型，而没有引入任何EJB的复杂性。当简单的 JavaBean 足以胜任时，人们便不愿编写笨重的 EJB 组件了

​		客观地讲，EJB的发展甚至促进了基于 POJO的编程模型。引入新的理念，最新的 EJB规范相比之前所未有的简化，但对很多开发者而言，这一切的一切都来得太迟了。到了EJB3规范发布时，其他基于POJO的开发架构已经成为事实的标准了，而Spring框架也就是在这样的大环境下出现的。

### 2.1.2Spring的设计初衷

​		Spring 是为解决企业级应用开发的复杂性而设计，她可以做很多事。但归根到底支撑 Spring的仅仅是少许的基本理念，而所有的这些基本理念都能可以追溯到一个最根本的使命∶简化开发。这是一个郑重的承诺，其实许多框架都声称在某些方面做了简化。而Spring则立志于全方面的简化Java开发。对此，她主要采取了4个关键策略∶

1. 基于 POJO的轻量级和最小侵入性编程;
2. 通过依赖注入和面向接口松耦合;
3. 基于切面和惯性进行声明式编程;
4. 通过切面和模板减少样板式代码;

而他主要是通过∶面向Bean（BOP）、依赖注入（DI）以及面向切面（AOP）这三种方式来达成的。

### 2.1.3BOP编程伊始

​		Spring是面向Bean的编程（Bean Oriented Programming， BOP），Bean在Spring中才是真正的主角。Bean在Spring中作用就像Object对OOP的意义一样 Spring中没有Bean也就没有Spring 存在的意义。Spring提供了IOC容器通过配置文件或者注解的方式来管理对象之间的依赖关系。

​		控制反转（其中最常见的实现方式叫做依赖注入（Dependency Injection，DI），还有一种方式叫"依赖查找"（Dependency Lookup，DL），她在C++、Java、PHP以及.NET中都运用。在最早的Spring中是包含有依赖注入方法和依赖查询的，但因为依赖查询使用频率过低，不久就被Spring移除了，所以在Spring中控制反转也被直接称作依赖注入），她的基本概念是∶不创建对象，但是描述创建它们的方式。在代码中不直接与对象和服务连接，但在配置文件中描述哪一个组件需要哪一项服务。容器（在Spring 框架中是 IOC容器）负责将这些联系在一起。

在典型的IOC场景中，容器创建了所有对象，并设置必要的属性将它们连接在一起，决定什么时间调用方法

### 2.1.4依赖注入基本概念

​		Spring 设计的核心org.springframework.beans包（架构核心是 org.springframework.core 包），它的设计目标是与JavaBean组件一起使用。这个包通常不是由用户直接使用，而是由服务器将其用作其他多数功能的底层中介。下一个最高级抽象是 Beanfactory接口，它是工厂设计模式的实现，允许通过名称创建和检索对象。BeanFactory也可以管理对象之间的关系。Beanfactory最底层支持两个对象模型。

1. 单例∶提供了具有特定名称的全局共享实例对象，可以在查询时对其进行检索。Singleton 是默认的也是最常用的对象模型。
2. 原型∶确保每次检索都会创建单独的实例对象。在每个用户都需要自己的对象时，采用原型模式。Bean工厂的概念是 Spring 作为 IOC容器的基础。IOC则将处理事情的责任从应用程序代码转移到框架。

### 2.1.5AOP编程理念

​		面向切面编程，即AOP，是一种编程思想，它允许程序员对横切关注点或横切典型的职责分界线的行为（例如日志和事务管理）进行模块化。AOP的核心构造是方面（切面），它将那些影响多个类的行为封装到可重用的模块中。

​		AOP和IOC是补充性的技术，它们都运用模块化方式解决企业应用程序开发中的复杂问题。在典型的面向对象开发方式中可能要将日志记录语句放在所有方法和Java类中才能实现日志功能。在AOP 方式中，可以反过来将日志服务模块化，并以声明的方式将它们应用到需要日志的组件上。当然，优势就是Java类不需要知道日志服务的存在，也不需要考虑相关的代码。所以，用Spring AOP编写的应用程序代码是松散耦合的。

​		AOP的功能完全集成到了Spring 事务管理、日志和其他各种特性的上下文中。

​		AOP编程的常用场景有 Authentication权限认证）Auto Caching（自动缓存处理）Error Handling （统一错误处理）、Debugging（调试信息输出）、Logging（日志记录）、Transactions（事务处理）等。

## 2.2Spring编程思想

### 2.2.1Spring注解编程

**1、Spring Framework 1.x注解驱动启蒙时代**

​		从Spring Framework 1.2.0版本开始，开始支持Annotation，虽然框架层面均已支持`@managedResource`和`@Transactional`等，但是其主要的还是以XML配置为准。

**2、Spring Framework 2.X 注解驱动过渡时代**

​		Spring Framework 2.0在Annotation支持方面添加了新的成员，`@Required`、数据相关的`@Repository`及AOP相关的`@Aspect`等，但同时提升了XML配置能力，即"可扩展的XML编写（ExtensibleXML authoring）"，当然的，这种扩展能力的出现，无形中为XML的配置增加了筹码

**3、Spring Framework 2.5开始，引入了新的骨架式 Annotation。**

`@Service`

`@Controller`，`@RequestMapping `及`@Modelattribute`

Spring Framework 2.5还支持了JSR-250（Java规范）。

`@Resource`注入

`@PostConstruct`替代`<bean init-method="…./>`

`@PreDestroy`替代`<bean destroy-method="…"/>`
		尽管 Spring Framework 2X时代提供了为数不少的注解，然而编程的手段却不多，最主要的原因在于框架层面仍未"直接"提供驱动注解的 Spring 应用上下文，并且仍需要XML驱动，
`<context∶annotation-config>`和`<context∶component-scan>`

**4、Spring Framework 3.x 注解驱动黄金时代**

​		Spring Framework3x是一个里程碑式的时代，3.0除了提升 Spring模式注解"派生"的层次性，首要任务是替换XML 配置方式，引入配置类注解`@Configuration`，该注解是内建的`@Component`
的派生"注解，遗憾的是，3.0并没有引入`<context∶component-scan>`的注解，而是选择过度方案
——`@lmportResource`和`@lmport`。`ImportResource`负责导入遗留的XML配置文件，`Import`允许导入一个或多个类作为 Spring Bean。

​		3.0引入 AnnotationConfigApplicationContext最为前时代ApplicationContext的替代者。3.1
新引入注解`@ComponentScan`，替换XML的`<context∶component-scan>`，成为全面进入注解驱动时代的一大步。

​		SpringFramework3.x注解提升还体现在以下方面∶

​		定义声明中，`@Bean `允许使用注解`@Role`设置其角色使得 Spring 应用上下文具备条件化 Bean定义的能力方面，`@RequestHeader`，`@CookieVale`和`@RequestPart`出现，使得不必使用Servlet API以及配置属性源抽象PropertySources，奠定了SpringBoot 外部化配置的基础。配套的注解 Caching 和Cacheable极大简化数据缓存的开发。周期异步`@Schedule`及异步web请求 DeferredResult。

**5、Spring Framework 4.x 注解驱动完善时代**

​		3.1开始提供`@Profile`提供了配置化的条件组装，不过这方面还是比较单一的，4.0开始，引入条件化注解`@Conditional`，通过自定义Condition 实现配合，弥补之前版本条件化装配的短板，4.0开始 Profile 反过来通过@conditional 实现。

​		Java8开始对提供`@Repeatable`，Framework4.0巧妙的兼容了JSR-310。根据特性，将`@PropertySource`、`@ComponentScan`提升为可重复使用的注解`@PropertySources`、`@ComponentScans`。

​		4.2开始

​		新增了事件监听器注解`@EventListener`，作为 ApplicationListener 接口编程的第二选择。`@AliasFor`解除注解派生的时候冲突限制

​		在浏览器跨域资源访问方面，引入@CrossOrigin，作为CorsRegistry 替换注解方案。

**6、SpringFramework 5.x 注解驱动成熟时代**

​		SpringFramework5.0作为Spring Boot 2.0的底层，注解驱动的性能提升不是那么明显。在
SpringBoot应用场景中，大量使用`@ComponentScan`扫描，导致Spring模式的注解解析时间耗时越长，面对这个问题，5.0引入`@lIndexed`，为 Spring模式注解添加索引。

### 2.2.2Spring5系统框架

Spring总共大约有20个模块，由1300多个不同的文件构成。而这些组件被分别整合在核心容器（Core Container）、AOP（Aspect Oriented Programming）和设备支持（Instrmentation）、数据访问及集成（Data Access/Integeration）、Web、报文发送（Messaging）、Test，6个模块集合中。以下是 Spring5的模块结构图∶

![Spring Framework架构图](images/Spring Framework架构图.jpg)

​		组成 Spring框架的每个模块集合或者模块都可以单独存在，也可以一个或多个模块联合实现。每个模块的组成和功能如下∶

**核心容器**

​		由 spring-beans、spring-core、spring-context和 spring-expression（Spring Expression Language， SpEL）4个模块组成。
​		spring-core和spring-beans模块是Spring框架的核心模块，包含了控制反转（Inversion of Control， IOC）和依赖注入（Dependency Injection， DIl）。BeanfFactory接口是 Spring框架中的核心接口，它是工厂模式的具体实现。BeanFactory使用控制反转对应用程序的配置和依赖性规范与实际的应用程序代码进行了分离。但BeanFactory容器实例化后并不会自动实例化 Bean，只有当Bean被使用时 BeanFactory 容器才会对该 Bean进行实例化与依赖关系的装配。

​		spring-context 模块构架于核心模块之上，他扩展了BeanfFactory，为她添加了Bean 生命周期控制、框架事件体系以及资源加载透明化等功能。此外该模块还提供了许多企业级支持，如邮件访问、远程访问、任务调度等，ApplicationContext是该模块的核心接口，她的超类是 BeanFactory。与BeanFactory 不同，ApplicationContext容器实例化后会自动对所有的单实例Bean进行实例化与依赖关系的装配，使之处于待用状态。

​		spring-context-support模块是对 Spring lOC 容器的扩展支持，以及 IOC子容器。

​		spring-context-indexer模块是 Spring的类管理组件和Classpath扫描。

​		spring-expression模块是统一表达式语言（EL）的扩展模块，可以查询、管理运行中的对象，同时也方便的可以调用对象方法、操作数组、集合等。它的语法类似于传统EL，但提供了额外的功能，最出色的要数函数调用和简单字符串的模板函数。这种语言的特性是基于Spring产品的需求而设计，他可以非常方便地同 Spring IOC进行交互。

**AOP和设备支持**

​		由 spring-aop、spring-aspects和 spring-instrument 3个模块组成。

​		spring-aop是Spring的另一个核心模块，是AOP主要的实现模块。作为继OOP后，对程序员影响最大的编程思想之一，AOP极大地开拓了人们对于编程的思路。在Spring中，他是以JⅣM的动态代理技术为基础，然后设计出了一系列的AOP横切实现，比如前置通知、返回通知、异常通知等，同时，Pointcut接口来匹配切入点，可以使用现有的切入点来设计横切面，也可以扩展相关方法根据需求进行切入。

​		spring-aspects模块集成自Aspectl框架，主要是为 Spring AOP提供多种AOP实现方法。

​		spring-instrument模块是基于JAVA SE中的"java.lang.instrument"进行设计的，应该算是AOP 的一个支援模块，主要作用是在ⅣM启用时，生成一个代理类，程序员通过代理类在运行时修改类的字节，从而改变一个类的功能，实现 AOP的功能。在分类里，我把他分在了AOP模块下，在Spring 官方文档里对这个地方也有点含糊不清。

**数据访问与集成**

​		由spring-jdbc、spring-tx、spring-orm、spring-jms和 spring-oxm 5个模块组成。

​		spring-jdbc模块是Spring 提供的 JDBC抽象框架的主要实现模块用于简化Spring JDBC操作。是提供 JDBC模板方式、关系数据库对象化方式、SimpleJdbc 方式、事务管理来简化 JDBC编程，
主要实现类是JdbcTemplate、SimpleJdbcTemplate以及NamedParameterJdbcTemplate。

​		spring-tx模块是Spring JDBC事务控制实现模块。使用Spring框架，它对事务做了很好的封装，通过它的AOP配置，可以灵活的配置在任何一层;但是在很多的需求和应用，直接使用 JDBC事务控制还是有其优势的。其实，事务是以业务逻辑为基础的;一个完整的业务应该对应业务层里的一个方法;如果业务操作失败，则整个事务回滚;所以，事务控制是绝对应该放在业务层的;但是，持久层的设计则应该遵循一个很重要的原则∶保证操作的原子性，即持久层里的每个方法都应该是不可以分割的。所以，在使用 Spring JDBC事务控制时，应该注意其特殊性。

​		spring-orm模块是ORM框架支持模块，主要集成 Hibernate，Java Persistence API（JPA）
Java Data Objects（JDO）用于资源管理、数据访问对象（DAO）的实现和事务策略。

​		spring-oxm模块主要提供一个抽象层以支撑 OXM（OXM是Object-to-XML-Mapping 的缩写，它是一个O/M-mapper，将java对象映射成XML 数据，或者将XML数据映射成java对象），例如∶JAXB，Castor，XMLBeans，JiBX 和 XStream等。

​		spring-jms模块（Java Messaging Service）能够发送和接收信息，自Spring Framework 4.1以后，他还提供了对 spring-messaging模块的支撑。

**Web组件**

​		由 spring-web、spring-webmvc、spring-websocket和 spring-webflux4个模块组成

​		spring-web模块为Spring提供了最基础Web支持，主要建立于核心容器之上，通过Servlet或者Listeners 来初始化 IOC容器，也包含一些与Web相关的支持。

​		spring-webmvc模块众所周知是一个的Web-Servlet模块，实现了Spring MVC （model-view-Controller）的Web应用。

​		spring-websocket模块主要是与 Web前端的全双工通讯的协议。

​		spring-webflux是一个新的非堵塞函数式 Reactive Web框架，可以用来建立异步的，非阻塞，事件驱动的服务，并且扩展性非常好。

**通信报文**

​		即 spring-messaging模块，是从Spring4开始新加入的一个模块，主要职责是为 Spring框架集成一些基础的报文传送应用。

**集成测试**

​		即 spring-test模块，主要为测试提供支持的，毕竟在不需要发布（程序）到你的应用服务器或者连接到其他企业设施的情况下能够执行一些集成测试或者其他测试对于任何企业都是非常重要的。

**集成兼容**

​		即spring-framework-bom模块，Bill of Materials.解决Spring的不同模块依赖版本不同问题。

### 2.2.3各模块之间的依赖关系

Spring 官网对 Spring5各模块之间的关系也做了详细说明∶

![Spring Framework模块关系-1](images/Spring Framework模块关系-1.jpg)

​		我本人也对Spring5各模块做了一次系统的总结，描述模块之间的依赖关系，希望能对小伙伴们有所帮助。

![Spring Framework模块关系-2](images/Spring Framework模块关系-2.jpg)

​		接下来的课程中，我们将深入了解Spring的核心模块功能。基本学习顺序为∶从spring-core入手，其次是 spring-beans和 spring-aop，随后是spring-context，再其次是 spring-tx和 spring-orm，最后是 spring-web和其他部分。

### 2.2.4Spring 版本命名规则

![Spring Framework版本命名规则](images/Spring Framework版本命名规则.jpg)

​		从上可以看出，不同的软件版本号风格各异，随着系统的规模越大，依赖的软件越多，如果这些软件没有遵循一套规范的命名风格，容易造成 Dependency Hell。所以当我们发布版本时，版本号的命名需要遵循某种规则，其中 SemanticVersioning 2.00定义了一套简单的规则及条件来约束版本号的配置和增长。本文根据 Semantic Versionning 2.0.0和 Semantic Versioning 300选择性的整理出版本号命名规则指南。

**语义化版本命名通用规则**

​		该规则对版本的迭代顺序命名做了很好的规范，其版本号的格式为 X.Y.Z（又称Major.Minor.Patch），递增的规则为∶

![Spring Framework版本命名规则-2](images/Spring Framework版本命名规则-2.jpg)

详细的使用规则如下∶

​		X，Y，Z 必须为非负整数，且不得包含前导零，必须按数值递增，如1.9.0->1.10.0->1.11.0 0.Y.Z的版本号表明软件处于初始开发阶段，意味着 API 可能不稳定;1.0.0表明版本已有稳定的
API.

​		当 API的兼容性变化时，X必须递增，Y和 Z 同时设置为0;当新增功能（不影响 API的兼容
性）或者 API被标记为 Deprecated 时，Y必须递增，同时 Z设置为0;当进行 bug fix 时，Z必须递增。

​		先行版本号（Pre-release）意味该版本不稳定，可能存在兼容性问题，其格式为∶X.YZ【a-c正整数，如1.0.0.a1，1.0.0.b99，1.0.0.c1000。

​		开发版本号常用于Cl-CD，格式为 X.Y.Zdev【正整数】，如1.0.1.dev4。

​		版本号的排序规则为依次比较主版本号、次版本号和修订号的数值，如1.00<1.0.1<1.1.1<2.00;对于先行版本号和开发版本号，有∶1.00.a100<1.0.0，2.1.0.dev3<2.1.0;当存在字母时，以 ASCl 的排序来比较，如1.0.0.a1< 1.0.0.b1。

​		注意∶版本一经发布，不得修改其内容，任何修改必须在新版本发布!

**商业软件中常见的修饰词**

![Spring Framework版本命名规则-3](images/Spring Framework版本命名规则-3.jpg)

![Spring Framework版本命名规则-4](images/Spring Framework版本命名规则-4.jpg)

**软件版本号使用限定**

​		为了方便理解，版本限定的语法简述为为【范围描述】<版本号描述>范围描述可选必须配和版本描述确定范围，无法独立存在

`<`小于某一版本号

`<=`小于等于某一版本号

`>`大于某一版本号

`>=`大于等于某一版本号

`=`等于某一版本号没有意义和直接写该版本号一样

`~`基于版本号描述的最新补丁版本

`^`基于版本号描述的最新兼容版本

`-`某个范围，他应该出现在两个版本描述中间，实际上语法应为<版本描述>-<版本描述>，写在此处为了统一

严格来讲对`～`，个的表述需要结合具体的包管理工具和版本号规则来确定.但是对于一般使用记住如下原则∶

`^`是确保版本兼容性时默认对次版本号的限定约束

`~`是确保版本兼容性时默认对补丁号的约束

**Spring版本命名规则**

![Spring Framework版本命名规则-5](images/Spring Framework版本命名规则-5.jpg)

![Spring Framework版本命名规则-6](images/Spring Framework版本命名规则-6.jpg)

## 2.3Spring注解编程

### 2.3.1配置组件

![Spring Framework注解-1](images/Spring Framework注解-1.jpg)

引入依赖

```xml
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>com.gupaoedu.vip.spring</groupId>
	<artifactId>gupaoedu-vip-spring-annotation</artifactId>
	<version>1.0</version>
	
	<properties>
        <!-- dependency versions -->
        <servlet.api.version>2.4</servlet.api.version>
    </properties>

    <dependencies>
        <!-- requied start -->
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>servlet-api</artifactId>
            <version>${servlet.api.version}</version>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context</artifactId>
            <version>5.0.2.RELEASE</version>
        </dependency>

        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.8.1</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>RELEASE</version>
        </dependency>
        <!-- requied end -->

    </dependencies>


    <build>
        <finalName>${artifactId}</finalName>
        <resources>
            <resource>
                <directory>${basedir}/src/main/resources</directory>
                <includes>
                    <include>**/*</include>
                </includes>
            </resource>
            <resource>
                <directory>${basedir}/src/main/java</directory>
                <excludes>
                    <exclude>**/*.java</exclude>
                    <exclude>**/*.class</exclude>
                </excludes>
            </resource>
        </resources>
        <plugins>
            <plugin>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>2.3.2</version>
                <configuration>
                    <source>1.6</source>
                    <target>1.6</target>
                    <encoding>UTF-8</encoding>
                    <compilerArguments>
                        <verbose />
                        <bootclasspath>${java.home}/lib/rt.jar</bootclasspath>
                    </compilerArguments>
                </configuration>
            </plugin>

            <plugin>
                <artifactId>maven-resources-plugin</artifactId>
                <version>2.5</version>
                <executions>
                    <execution>
                        <id>copy-resources</id>
                        <!-- here the phase you need -->
                        <phase>validate</phase>
                        <goals>
                            <goal>copy-resources</goal>
                        </goals>
                        <configuration>
                            <encoding>UTF-8</encoding>
                            <outputDirectory>${basedir}/target/classes</outputDirectory>
                            <resources>
                                <resource>
                                    <directory>src/main/resources</directory>
                                    <includes>
                                        <include>**/*.*</include>
                                    </includes>
                                    <filtering>true</filtering>
                                </resource>
                            </resources>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.mortbay.jetty</groupId>
                <artifactId>maven-jetty-plugin</artifactId>
                <version>6.1.26</version>
                <configuration>
                    <webDefaultXml>src/main/resources/webdefault.xml</webDefaultXml>
                    <contextPath>/</contextPath>
                    <connectors>
                        <connector implementation="org.mortbay.jetty.nio.SelectChannelConnector">
                            <port>80</port>
                        </connector>
                    </connectors>
                    <scanIntervalSeconds>0</scanIntervalSeconds>
                    <scanTargetPatterns>
                        <scanTargetPattern>
                            <directory>src/main/webapp</directory>
                            <includes>
                                <include>**/*.xml</include>
                                <include>**/*.properties</include>
                            </includes>
                        </scanTargetPattern>
                    </scanTargetPatterns>
                    <systemProperties>
                        <systemProperty>
                            <name>
                                javax.xml.parsers.DocumentBuilderFactory
                            </name>
                            <value>
                                com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl
                            </value>
                        </systemProperty>
                        <systemProperty>
                            <name>
                                javax.xml.parsers.SAXParserFactory
                            </name>
                            <value>
                                com.sun.org.apache.xerces.internal.jaxp.SAXParserFactoryImpl
                            </value>
                        </systemProperty>
                        <systemProperty>
                            <name>
                                javax.xml.transform.TransformerFactory
                            </name>
                            <value>
                                com.sun.org.apache.xalan.internal.xsltc.trax.TransformerFactoryImpl
                            </value>
                        </systemProperty>
                        <systemProperty>
                            <name>org.eclipse.jetty.util.URI.charset</name>
                            <value>UTF-8</value>
                        </systemProperty>
                    </systemProperties>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-war-plugin</artifactId>
                <version>2.2</version>
                <configuration>
                    <archive>
                        <addMavenDescriptor>false</addMavenDescriptor>
                    </archive>
                    <webResources>
                        <resource>
                            <!-- this is relative to the pom.xml directory -->
                            <directory>src/main/resources/</directory>
                            <targetPath>WEB-INF/classes</targetPath>
                            <includes>
                                <include>**/*.*</include>
                            </includes>
                            <filtering>true</filtering>
                        </resource>
                        <resource>
                            <!-- this is relative to the pom.xml directory -->
                            <directory>src/main/resources</directory>
                            <targetPath>WEB-INF/classes</targetPath>
                            <filtering>true</filtering>
                        </resource>
                    </webResources>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.zeroturnaround</groupId>
                <artifactId>javarebel-maven-plugin</artifactId>
                <executions>
                    <execution>
                        <id>generate-rebel-xml</id>
                        <phase>process-resources</phase>
                        <goals>
                            <goal>generate</goal>
                        </goals>
                    </execution>
                </executions>
                <version>1.0.5</version>
            </plugin>
        </plugins>
        <pluginManagement>
            <plugins>
                <!--This plugin's configuration is used to store Eclipse m2e settings
                    only. It has no influence on the Maven build itself. -->
                <plugin>
                    <groupId>org.eclipse.m2e</groupId>
                    <artifactId>lifecycle-mapping</artifactId>
                    <version>1.0.0</version>
                    <configuration>
                        <lifecycleMappingMetadata>
                            <pluginExecutions>
                                <pluginExecution>
                                    <pluginExecutionFilter>
                                        <groupId>
                                            org.zeroturnaround
                                        </groupId>
                                        <artifactId>
                                            javarebel-maven-plugin
                                        </artifactId>
                                        <versionRange>
                                            [1.0.5,)
                                        </versionRange>
                                        <goals>
                                            <goal>generate</goal>
                                        </goals>
                                    </pluginExecutionFilter>
                                    <action>
                                        <ignore></ignore>
                                    </action>
                                </pluginExecution>
                            </pluginExecutions>
                        </lifecycleMappingMetadata>
                    </configuration>
                </plugin>
            </plugins>
        </pluginManagement>
    </build>

</project>
```



创建Person

```java
public class Person {
    private String name;
    private int age;

    public Person() {
    }

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}

```

#### 2.3.1.1XML配置

配置中引入Person

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd">

    <bean id="person" class="com.gupaoedu.project.entity.Person">
        <property name="name" value="Tom"></property>
        <property name="age" value="18"></property>
    </bean>

</beans>
```

web.xml中加载

```xml
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns="http://java.sun.com/xml/ns/j2ee" xmlns:javaee="http://java.sun.com/xml/ns/javaee"
	xmlns:web="http://java.sun.com/xml/ns/javaee/web-app_2_5.xsd"
	xsi:schemaLocation="http://java.sun.com/xml/ns/j2ee http://java.sun.com/xml/ns/j2ee/web-app_2_4.xsd"
	version="2.4">
	<display-name>Gupao Web Application</display-name>
	<servlet>
		<servlet-name>gpmvc</servlet-name>
		<servlet-class>com.gupaoedu.mvcframework.v2.servlet.GPDispatchServlet</servlet-class>
		<init-param>
			<param-name>contextConfigLocation</param-name>
			<param-value>application.properties</param-value>
		</init-param>

		<load-on-startup>1</load-on-startup>
	</servlet>
	<servlet-mapping>
		<servlet-name>gpmvc</servlet-name>
		<url-pattern>/*</url-pattern>
	</servlet-mapping>
</web-app>
```

直接调用

```java
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {

    public static void main(String[] args) {
        ApplicationContext app = new ClassPathXmlApplicationContext("application.xml");
        Object bean = app.getBean("person");
        System.out.println(bean);
    }

}

```

#### 2.3.1.2`@Configuration`

创建配置类

```java
import com.gupaoedu.project.entity.Person;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MyConfig {

    @Bean
    public Person person1(){
        return new Person("Tom",18);
    }
}
```

测试

```java
import com.gupaoedu.project.entity.Person;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Arrays;

public class MyTest {
    @Test
    public void test() {
        ApplicationContext app = new AnnotationConfigApplicationContext(MyConfig.class);
        Object bean1 = app.getBean("person");
        Object bean2 = app.getBean("person");
        System.out.println(bean1 == bean2);
        
		Object bean3 = app.getBean(Person.class);
        
        String [] beanNames = app.getBeanNamesForType(Person.class);
        System.out.println(Arrays.toString(beanNames));

    }
}
```

`app.getBean("person")`中取的时配置类中的方法名，如果不想要使用方法名可以只用`@Bean("persion")`注解在方法上。

#### 2.3.1.3`@ComponentScan`

扫描bean

```java
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

@Configuration
@ComponentScan(value = "com.gupaoedu.project",
//                includeFilters = {@Filter(type = FilterType.ANNOTATION,value = {Controller.class})},
//                includeFilters = {@Filter(type = FilterType.ASSIGNABLE_TYPE,value = {MyController.class})},
//                  includeFilters = {@Filter(type = FilterType.CUSTOM,value = {GPTypeFilter.class})},
                useDefaultFilters = false)
public class MyConfig {


}
```

`includeFilters`：过滤器，根据指定条件过滤要装入容器的对象，可以自定义过滤规则

自定义过滤规则

```java
import org.springframework.core.io.Resource;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.ClassMetadata;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.MetadataReaderFactory;
import org.springframework.core.type.filter.TypeFilter;

import java.io.IOException;

public class GPTypeFilter implements TypeFilter{

    /**
     *
     * @param metadataReader 获取当前正在操作类的信息
     * @param metadataReaderFactory 获取上下文中所有的信息
     * @return
     * @throws IOException
     */
    @Override
    public boolean match(MetadataReader metadataReader, MetadataReaderFactory metadataReaderFactory) throws IOException {
        //获取当前扫描到的类的注解信息
        AnnotationMetadata annotationMetadata = metadataReader.getAnnotationMetadata();
        //获取当前扫描到的类信息
        ClassMetadata classMetadata = metadataReader.getClassMetadata();
        //获取当前扫描到的类的资源
        Resource resource = metadataReader.getResource();
        String className = classMetadata.getClassName();
        System.out.println("------" + className + "------");
        if(className.contains("er")){
            return true;
        }
        return false;
    }
}
```

测试

```java
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Arrays;

/**
 * Created by Tom.
 */
public class MyTest {


    @Test
    public void test() {
        ApplicationContext app = new AnnotationConfigApplicationContext(MyConfig.class);
        String [] beanNames = app.getBeanDefinitionNames();
        System.out.println(Arrays.toString(beanNames)
                .replaceAll("\\[|\\]","")
                .replaceAll(", ","\n"));
    }
}
```

#### 2.3.1.4`@Scope`

​		对象创建模式

```java
import com.gupaoedu.project.entity.Person;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class MyConfig {

    //prototype 多例
    //singleton 单例
    //request 主要应用于web模块，同一次请求只创建一个实例
    //session 主要应用于web模块，同一个session只创建一个实例
    @Scope("prototype")
    @Bean
    public Person person(){
        //IoC实例化对象的时候，并不是简单地调用我们定义的方法
        return new Person("Tom",18);
    }
}
```

测试

```java
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class MyTest {
    @Test
    public void test() {
        ApplicationContext app = new AnnotationConfigApplicationContext(MyConfig.class);
        Object bean1 = app.getBean("person");
        System.out.println(bean1);

        Object bean2 = app.getBean("person");
        System.out.println(bean2);

        System.out.println(bean1 == bean2);

    }
}
```

#### 2.3.1.5`@Lazy`

​		延迟加载

```java
import com.gupaoedu.project.entity.Person;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MyConfig {

    //懒加载只针对单例Bean起作用
    //默认容器启动时不创建对象，调用对象的功能时才创建
	//    @Lazy
    @Bean
    public Person person(){
        System.out.println("将对象添加到IoC容器中");
        return new Person("Tom",18);
    }
}

```

测试

```java
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class MyTest {
    @Test
    public void test(){
        ApplicationContext app = new AnnotationConfigApplicationContext(MyConfig.class);
        System.out.println("IoC容器创建完成");
        app.getBean("person");
    }
}
```

#### 2.3.1.6`@Conditional`

​		根据自定义条件在不同环境下实现不同的容器注册

```java
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.env.Environment;
import org.springframework.core.type.AnnotatedTypeMetadata;

public class WinCondition implements Condition {
    @Override
    public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
        //从IoC容器中拿到已经实例化的对象
        ConfigurableListableBeanFactory beanFactory = context.getBeanFactory();
        Environment environment = context.getEnvironment();
        String osName = environment.getProperty("os.name");
        if(osName.contains("Windows")){
            return true;
        }
        return false;
    }
}
```

```java
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.env.Environment;
import org.springframework.core.type.AnnotatedTypeMetadata;

public class LinuxCondition implements Condition {
    @Override
    public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
        //从IoC容器中拿到已经实例化的对象
        ConfigurableListableBeanFactory beanFactory = context.getBeanFactory();
        Environment environment = context.getEnvironment();
        String osName = environment.getProperty("os.name");
        System.out.println(osName);
        if(osName.contains("Linux")){
            return true;
        }
        return false;
    }
}

```



```java
import com.gupaoedu.project.entity.Person;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MyConfig {

    @Conditional(WinCondition.class)
    @Bean
    public Person mic(){
        System.out.println("将对象Mic添加到IoC容器中");
        return new Person("Mic",16);
    }

    @Conditional(WinCondition.class)
    @Bean
    public Person tom(){
        System.out.println("将对象Tom添加到IoC容器中");
        return new Person("Tom",18);
    }

    @Conditional(LinuxCondition.class)
    @Bean
    public Person james(){
        System.out.println("将对象James添加到IoC容器中");
        return new Person("James",17);
    }
}
```

```java
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class MyTest {
    @Test
    public void test(){
        ApplicationContext app = new AnnotationConfigApplicationContext(MyConfig.class);
        System.out.println("IoC容器创建完成");

        //假设：
        //如果操作系统是Windows，那么就将James实例化到容器中
        //如果操作系统是Linux，那么就将Tom实例化到容器中
    }
}
```

#### 2.3.1.7`@Import`

​		引入外部资源

```java
public class Member {}
```

```java
public class Company {}
```

```java
public class Cat {}
```

```java
public class User {}
```

```java
public class Monkey {}
```



```java
import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;

public class MyImportSelector implements ImportSelector {
    @Override
    public String[] selectImports(AnnotationMetadata importingClassMetadata) {
        return new String[]{"com.gupaoedu.project.entity.Company",
                            "com.gupaoedu.project.entity.Member"};
    }
}
```

```java
import com.gupaoedu.project.entity.User;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.type.AnnotationMetadata;

public class MyImportBeanDefinitionRegistrar implements ImportBeanDefinitionRegistrar {

    /**
     *
     * @param importingClassMetadata  当前类的注解信息
     * @param registry 完成BeanDefinition的注册
     */
    @Override
    public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {
        //如果声明了company和member，注册User
        boolean company = registry.containsBeanDefinition("com.gupaoedu.project.entity.Company");
        boolean member = registry.containsBeanDefinition("com.gupaoedu.project.entity.Member");

        if(company && member){
            BeanDefinition beanDefinition = new RootBeanDefinition(User.class);
            registry.registerBeanDefinition("user",beanDefinition);
        }
    }
}
```

```java
import com.gupaoedu.project.entity.Monkey;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.lang.Nullable;

/**
 * Created by Tom.
 */
public class MyFactoryBean implements FactoryBean<Monkey> {
    @Nullable
    @Override
    public Monkey getObject() throws Exception {
        return new Monkey();
    }

    @Nullable
    @Override
    public Class<?> getObjectType() {
        return Monkey.class;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }
}

```



```java
import com.gupaoedu.project.entity.Cat;
import com.gupaoedu.project.entity.Company;
import com.gupaoedu.project.entity.Member;
import com.gupaoedu.project.entity.Person;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(value = {Cat.class,MyImportSelector.class,MyImportBeanDefinitionRegistrar.class})
public class MyConfig {

    @Bean
    public Company company(){
        return new Company();
    }

    @Bean
    public Member member(){
        return new Member();
    }

    //给IoC中注册Bean的方式
    //1、@Bean 直接导入单个类
    //2、@ComponentScan 包扫描默认是扫描（@Controller、@Service、@Repository、@Component）
    //3、@Import 快速给容器导入组件Bean
    //      a. @Import 直接传参导入
    //      b. ImportSelector 自定义导入规则
    //      c.ImportBeanDefinitionRegistrar ,使用BeanDefinitionRegistry可以手动注入到IoC容器中
    //4、FactoryBean 把需要注入的对象封装为FactoryBean
    //      a、FactoryBean 负责将Bean注入到容器的Bean
    //      b、BeanFactory 从IoC中获得Bean对象
    @Bean
    public Person person(){
        return new Person("Tom",18);
    }

    @Bean
    public MyFactoryBean monkey(){
        return new MyFactoryBean();
    }
}

```

```java
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Arrays;

/**
 * Created by Tom.
 */
public class MyTest {

    @Test
    public void test(){
        ApplicationContext app = new AnnotationConfigApplicationContext(MyConfig.class);

        //通过FactoryBean注入的值
        System.out.println("============" + app.getBean("monkey").getClass());
        Object monkey1 = app.getBean("monkey");
        Object monkey2 = app.getBean("monkey");
        System.out.println("是否单例：" + monkey1 == monkey2);

        //拿到构建monkey的FactoryBean
        Object monkeyFactoryBean = app.getBean("&monkey");
        System.out.println(monkeyFactoryBean);

        String [] beanNames = app.getBeanDefinitionNames();
        System.out.println(Arrays.toString(beanNames)
                .replaceAll("\\[|\\]","")
                .replaceAll(", ","\n"));
    }

}
```

#### 2.3.1.8生命周期

生命周期

```java
public class Car {
    public Car() {
        System.out.println("调用Car的构造方法");
    }

    public void addOil(){
        System.out.println("行驶前加油");
    }

    public void run(){
        System.out.println("正在开车");
    }

    public void close(){
        System.out.println("停车熄火");
    }
}
```

```java
import com.gupaoedu.project.entity.Car;
import org.springframework.context.annotation.*;

@ComponentScans({
        @ComponentScan("com.gupaoedu.project.entity"),
        @ComponentScan("com.gupaoedu.demo.annotaions.configures.lifecycle")
})
@Configuration
public class MyConfig {

    @Lazy
    @Bean(initMethod = "addOil",destroyMethod = "close")
    public Car car(){
        return new Car();
    }

    //3种方式
    // 1.添加initMethod 和 destroyMethod
    // 2.实现InitializingBean和DisposableBean接口
    // 3.使用@PostConstruct和@PreDestroy注解
}

```

```java
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class MyTest {
    @Test
    public void test(){
        AnnotationConfigApplicationContext app = new AnnotationConfigApplicationContext(MyConfig.class);
        System.out.println("IoC容器创建完成");
//        System.out.println(app.getBean("car"));
        System.out.println(app.getBean("train"));

        System.out.println(app.getBean("airPlane"));
        app.close();
    }
}
```

```java
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

@Component
public class MyBeanPostProcessor implements BeanPostProcessor {

    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("postProcessBeforeInitialization" + beanName + "," + bean);
        return bean;
    }



    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("postProcessAfterInitialization" + beanName + "," + bean);
        return bean;
    }
}
```

```java
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

@Component
public class Train implements InitializingBean,DisposableBean {
    @Override
    public void destroy() throws Exception {
        System.out.println("火车对象销毁");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("火车对象初始化");
    }
}
```

```java
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component
public class AirPlane {

    public AirPlane() {
        System.out.println("调用AirPlane的构造方法");
    }

    @PostConstruct
    public void addOil(){
        System.out.println("飞机飞行前加油");
    }

    public void run(){
        System.out.println("正在空中巡航");
    }

    @PreDestroy
    public void close(){
        System.out.println("飞机落地熄火");
    }
}
```

### 2.3.2赋值注解

![Spring Framework注解-2](images/Spring Framework注解-2.jpg)

#### 2.3.2.1`@Value`

初始化注入值

```java
import org.springframework.beans.factory.annotation.Value;

public class Bird {

    //支持的类型
    //1、基本数据类型
    //3、Spring EL表达式
    //4、通过配置文件赋值
    @Value("鹦鹉")
    private String name;
    @Value("#{8-5}")
    private int age;

    @Value("${bird.color}")
    private String color;


    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Bird() {
    }

    public Bird(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Bird{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", color='" + color + '\'' +
                '}';
    }
}

```

```java
import com.gupaoedu.project.entity.Bird;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan({
        "com.gupaoedu.project.controller",
        "com.gupaoedu.project.service",
        "com.gupaoedu.project.dao"
            })
public class MyConfig {

}
```

```java
import com.gupaoedu.project.controller.MyController;
import com.gupaoedu.project.dao.MyDao;
import com.gupaoedu.project.service.MyService;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.Environment;

import java.util.Arrays;

public class MyTest {
    @Test
    public void test(){
        ApplicationContext app = new AnnotationConfigApplicationContext(MyConfig.class);
        Object controller = app.getBean("myController");
        System.out.println(controller);

        MyService service = app.getBean(MyService.class);
        service.print();

        MyDao dao = app.getBean(MyDao.class);
        System.out.println(dao);
    }
}
```

外部配置注入

```properties
bird.color=Black
```

```java
import com.gupaoedu.project.entity.Bird;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:values.properties")
public class MyConfig {

    @Bean
    public Bird bird(){
        return new Bird();
    }
}
```

```java
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.Environment;

import java.util.Arrays;

public class MyTest {
    @Test
    public void test(){
        ApplicationContext app = new AnnotationConfigApplicationContext(MyConfig.class);

        System.out.println(app.getBean("bird"));

        String [] beanNames = app.getBeanDefinitionNames();
        System.out.println(Arrays.toString(beanNames)
                .replaceAll("\\[|\\]","")
                .replaceAll(", ","\n"));

        Environment evn = app.getEnvironment();

        System.out.println("从环境变量中取值================" + evn.getProperty("bird.color"));
    }
}
```

#### 2.3.2.2`@Autowired`

自动注入扫描到的Bean

```java
import com.gupaoedu.project.entity.Bird;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan({
        "com.gupaoedu.project.controller",
        "com.gupaoedu.project.service",
        "com.gupaoedu.project.dao"
            })
public class MyConfig {

}
```

```java
import com.gupaoedu.project.service.MyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class MyController {
    @Autowired 
    private MyService service;
}
```

```java
import com.gupaoedu.project.dao.MyDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class MyService {
    @Qualifier("dao")
//    @Resource(name="dao")
    @Autowired
    private MyDao myDao;

    public void print(){
        System.out.println(myDao);
    }
}

```

```java
import org.springframework.stereotype.Repository;

@Repository
public class MyDao {
    private String flag = "1";

    public void setFlag(String flag) {
        this.flag = flag;
    }

    @Override
    public String toString() {
        return "MyDao{" +
                "flag='" + flag + '\'' +
                '}';
    }
}
```

#### 2.3.2.3`@Qualifier`

提高注入优先级

```java
import com.gupaoedu.project.dao.MyDao;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
@ComponentScan({
        "com.gupaoedu.project.controller",
        "com.gupaoedu.project.service",
        "com.gupaoedu.project.dao"
            })
public class MyConfig {

    @Bean("dao")
    public MyDao dao(){
        MyDao dao = new MyDao();
        dao.setFlag("2");
        return dao;
    }
}
```

```java
import com.gupaoedu.project.dao.MyDao;
import com.gupaoedu.project.service.MyService;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class MyTest {
    @Test
    public void test(){
        ApplicationContext app = new AnnotationConfigApplicationContext(MyConfig.class);


        MyService service = app.getBean(MyService.class);
        service.print();


    }
}
```

#### 2.3.2.4`@Primary`

提高选择同名Bean时的优先级

```java
import com.gupaoedu.project.dao.MyDao;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
@ComponentScan({
        "com.gupaoedu.project.controller",
        "com.gupaoedu.project.service",
        "com.gupaoedu.project.dao"
            })
public class MyConfig {


    @Primary
    @Bean("myDao")
    public MyDao dao(){
        MyDao dao = new MyDao();
        dao.setFlag("9");
        return dao;
    }


    @Bean("myDao")
    public MyDao myDao(){
        MyDao dao = new MyDao();
        dao.setFlag("3");
        return dao;
    }
}
```

### 2.3.3织入注解

![Spring Framework注解-3](images/Spring Framework注解-3.jpg)

### 2.3.4切面组件

![Spring Framework注解-4](images/Spring Framework注解-4.jpg)

## 2.4Spring核心原理

### 2.4.1Spring容器

​		（Inversion of Control）控制反转∶所谓控制反转，就是把原先我们代码里面需要实现的对象创建、依赖的代码，反转给容器来帮忙实现。那么必然的我们需要创建一个容器，同时需要一种描述来让容器知道需要创建的对象与对象的关系。这个描述最具体表现就是我们所看到的配置文件。

​		DI（Dependency Injection）依赖注入∶就是指对象是被动接受依赖类而不是自己主动去找，换句话说就
是指对象不是从容器中查找它依赖的类，而是在容器实例化对象的时候主动将它依赖的类注入给它。

先从我们自己设计这样一个视角来考虑∶

1、对象和对象的关系怎么表示?
	可以用xml，properties文件等语义化配置文件表示。

2、描述对象关系的文件存放在哪里?
	可能是classpath，filesystem，或者是URL网络资源，servletContext等。
	回到正题，有了配置文件，还需要对配置文件解析。

3、不同的配置文件对对象的描述不一样，如标准的，自定义声明式的，如何统一?
	在内部需要有一个统一的关于对象的定义，所有外部的描述都必须转化成统一的描述定义。

4、如何对不同的配置文件进行解析?
	需要对不同的配置文件语法，采用不同的解析器。

#### 2.4.1.1BeanFactory

​		Spring Bean的创建是典型的工厂模式，这一系列的Bean工厂，也即IOC容器为开发者管理对象间的依赖关系提供了很多便利和基础服务，在Spring 中有许多的IOC容器的实现供用户选择和使用，其相互关系如下∶

<img src="images/Spring Framework容器-1.jpg" alt="Spring Framework容器-1" style="zoom: 33%;" />	

​		其中 BeanFactory 作为最顶层的一个接口类，它定义了IOC容器的基本功能规范，BeanFactory有三个重要的子类∶ListableBeanFactory、HierarchicalBeanFactory和 AutowireCapableBeanFactory。但是从类图中我们可以发现最终的默认实现类是 DefaultListableBeanFactory，它实现了所有的接口。

​		那为何要定义这么多层次的接口呢?查阅这些接口的源码和说明发现，每个接口都有它使用的场合，它主要是为了区分在Spring 内部在操作过程中对象的传递和转化过程时，对对象的数据访问所做的限制。例如 ListableBeanFactory接口表示这些Bean是可列表化的，而 HierarchicalBeanFactory表示的是这些Bean是有继承关系的，也就是每个 Bean有可能有父 Bean。AutowireCapableBeanFactory接口定义 Bean的自动装配规则。这三个接口共同定义了Bean的集合、Bean 之间的关系、以及Bean行为。最基本的 IOC容器接口 BeanFactory，来看一下它的源码∶

```java

```

​		在BeanFactory里只对IOC容器的基本行为作了定义，根本不关心你的 Bean是如何定义怎样加载的。正如我们只关心工厂里得到什么的产品对象，至于工厂是怎么生产这些对象的，这个基本的接口不关心

​		而要知道工厂是如何产生对象的，我们需要看具体的IOC容器实现，Spring 提供了许多IOC容器的 实 现。比 如 GenericApplicationContext，ClasspathXmApplicationContext 等。

​		ApplicationContext是Spring提供的一个高级的IOC容器，它除了能够提供IOC容器的基本功能外，还为用户提供了以下的附加服务。从ApplicationContext接口的实现，我们看出其特点∶

1. 支持信息源，可以实现国际化。（实现 MessageSource接口）
2. 访问资源。（实现 ResourcePatternResolver接口）
3. 支持应用事件。（实现 ApplicationEventPublisher接口）

#### 2.4.1.2BeanDefinition

​		SpringlOC容器管理了我们定义的各种Bean 对象及其相互的关系，Bean对象在 Spring实现中是以 BeanDefinition来描述的，其继承体系如下∶

<img src="images/Spring Framework容器-2.jpg" alt="Spring Framework容器-2" style="zoom:40%;" />

#### 2.4.1.3BeanDefinitionReader

​		Bean的解析过程非常复杂，功能被分的很细，因为这里需要被扩展的地方很多，必须保证有足够的灵舌性，以应对可能的变化。Bean的解析主要就是对Spring配置文件的解析。这个解析过程主要通过BeanDefintionReader来完成，最后看看 Spring中 BeanDefintionReader的类结构图∶

<img src="images/Spring Framework容器-3.jpg" alt="Spring Framework容器-3" style="zoom:40%;" />

​		通过本章内容的分析，我们对 Spring框架体系有了一个基本的宏观了解，希望小伙伴们好好理解，最好在脑海中形成画面，为以后的学习打下良好的铺垫。

### 2.4.2Web IoC容器

​		我们还是从大家最熟悉的DispatcherServlet开始，我们最先想到的还是 DispatcherServlet的init（）方法。我们发现在DispatherServlet中并没有找到 init0方法。但是经过探索，往上追索在其父类HttpServletBean 中找到了我们想要的 init（）方法，如下∶

```java

```

​		在init0方法中，真正完成初始化容器动作的逻辑其实在initServletBean（）方法中，我们继续跟进initServletBean（）中的代码在 FrameworkServlet类中∶

```java

```

在上面的代码中终于看到了我们似曾相识的代码initWebAppplicationContext（），继续跟进

```java

```

​		从上面的代码中可以看出，在 `configAndRefreshWebApplicationContext()`方法中，调用 `refresh()`方法，这个是真正启动 IOC容器的入口，后面会详细介绍。IOC容器初始化以后，最后调用了`DispatcherServlet`的 `onRefresh()`方法，在`onRefresh()`方法中又是直接调用`initStrategies()`方法初始化 SpringMVC的九大组件∶

```java

```

### 2.4.3Xml IoC容器

​		IOC容器的初始化包括BeanDefinition的Resource定位、加载和注册这三个基本的过程。我们以ApplicationContext为例讲解，ApplicationContext系列容器也许是我们最熟悉的，因为Web 项目中使用的XmlWebApplicationContext就属于这个继承体系 还有ClasspathXmlApplicationContext 等，其继承体系如下图所示∶

<img src="images/Spring Framework容器-4.jpg" alt="Spring Framework容器-4" style="zoom:40%;" />

​		ApplicationContext允许上下文嵌套，通过保持父上下文可以维持一个上下文体系。对于Bean的查找可以在这个上下文体系中发生，首先检查当前上下文，其次是父上下文，逐级向上这样为不同的Spring应用提供了一个共享的Bean定义环境。

**1、寻找入口**

​		还有一个我们用的比较多的ClassPathXmlApplicationContext，通过main()方法启动∶

```java
ApplicationContext app = new ClassPathXmlApplicationContext("application.xml");
```

​		先看其构造函数的调用

```java
public ClassPathXmlApplicationContext(String configLocation) throws BeansException {
    this(new String[] {configLocation}, true, null);
}
```

​		实际调用的构造函数为

```java
public ClassPathXmlApplicationContext(
        String[] configLocations, boolean refresh, @Nullable ApplicationContext parent)
        throws BeansException {

    super(parent);
    setConfigLocations(configLocations);
    if (refresh) {
        //重启、刷新、重置
        refresh();
    }
}
```

​		还有像 AnnotationConfigApplicationContext、FileSystemXmlApplicationContext、XmWebApplicationContext等都继承自父容器AbstractApplicationContext主要用到了装饰器模式和策略模式，最终都是调用 refresh()方法。

**2、获得配置路径**

​		通过分析 ClassPathXmlApplicationContext 的源代码可以知道，在创建ClassPathXxmlApplicationContext 容器时，构造方法做以下两项重要工作∶首先，调用父类容器的构造方法（super（parent）方法）为容器设置好 Bean资源加载器。然后，再调用父类 AbstractRefreshableConfigApplicationContext 的setConfiglocation(configLocations)方法设置Bean 配置信息的定位路径。
通过追踪 ClassPathXmlApplicationContext 的继承体系，发现其父类的父类AbstractApplicationContext 中初始化 IOC容器所做的主要源码如下∶

```java
public abstract class AbstractApplicationContext extends DefaultResourceLoader
		implements ConfigurableApplicationContext {
·········

	//静态初始化块，在整个容器创建过程中只执行一次
	static {
		//为了避免应用程序在Weblogic8.1关闭时出现类加载异常加载问题，加载IoC容
		//器关闭事件(ContextClosedEvent)类
		ContextClosedEvent.class.getName();
	}

	public AbstractApplicationContext() {
		this.resourcePatternResolver = getResourcePatternResolver();
	}

	public AbstractApplicationContext(@Nullable ApplicationContext parent) {
		this();
		setParent(parent);
	}
    //获取一个Spring Source的加载器用于读入Spring Bean定义资源文件
	protected ResourcePatternResolver getResourcePatternResolver() {
		//AbstractApplicationContext继承DefaultResourceLoader，因此也是一个资源加载器
		//Spring资源加载器，其getResource(String location)方法用于载入资源
		return new PathMatchingResourcePatternResolver(this);
	}
·········
}
```

AbstractApplicationContext的默认构造方法中有调用 PathMatchingResourcePatternResolver 的构造方法创建 Spring资源加载器∶

```java
public PathMatchingResourcePatternResolver(ResourceLoader resourceLoader) {
    Assert.notNull(resourceLoader, "ResourceLoader must not be null");
    //设置Spring的资源加载器
    this.resourceLoader = resourceLoader;
}
```

​		在设置容器的资源加载器之后，接下来ClassPathXmlApplicationContext执行 setConfigLocations（）方法通过调用其父类AbstractRefreshableConfigApplicationContext的方法进行对 Bean配置信息的定位，该方法的源码如下∶

```java
//处理单个资源文件路径为一个字符串的情况
public void setConfigLocation(String location) {
    //String CONFIG_LOCATION_DELIMITERS = ",; /t/n";
    //即多个资源文件路径之间用” ,; \t\n”分隔，解析成数组形式
    setConfigLocations(StringUtils.tokenizeToStringArray(location, CONFIG_LOCATION_DELIMITERS));
}

//解析Bean定义资源文件的路径，处理多个资源文件字符串数组
public void setConfigLocations(@Nullable String... locations) {
    if (locations != null) {
        Assert.noNullElements(locations, "Config locations must not be null");
        this.configLocations = new String[locations.length];
        for (int i = 0; i < locations.length; i++) {
            // resolvePath为同一个类中将字符串解析为路径的方法
            this.configLocations[i] = resolvePath(locations[i]).trim();
        }
    }
    else {
        this.configLocations = null;
    }
}
```

​		通过这两个方法的源码我们可以看出，我们既可以使用一个字符串来配置多个Spring Bean配置信息也可以使用字符串数组，即下面两种方式都是可以的∶

```java
ClassPathResource res= new ClassPathResource("a.xml,b.xml");
```

​		多个资源文件路径之间可以是用"，;t\n"等分隔。

```java
ClassPathResource res =new ClassPathResource(new String[]{"a.xml","b.xml");
```

​		至此，SpringlOC容器在初始化时将配置的Bean配置信息定位为Spring 封装的Resource。

**3、开始启动**

​		SpringlOC容器对 Bean配置资源的载入是从refresh()函数开始的，refresh()是一个模板方法，规定了IOC 容器的启动流程，有些逻辑要交给其子类去实现。它对 Bean 配置资源进行载入ClassPathXmlApplicationContext通过调用其父类AbstractApplicationContext的 refresh()函数启动整个IOC容器对 Bean定义的载入过程，现在我们来详细看看 refresh()中的逻辑处理∶

```java
@Override
public void refresh() throws BeansException, IllegalStateException {
    synchronized (this.startupShutdownMonitor) {
        // Prepare this context for refreshing.
        //1、调用容器准备刷新的方法，获取容器的当时时间，同时给容器设置同步标识
        prepareRefresh();

        // Tell the subclass to refresh the internal bean factory.
        //2、告诉子类启动refreshBeanFactory()方法，Bean定义资源文件的载入从
        //子类的refreshBeanFactory()方法启动
        ConfigurableListableBeanFactory beanFactory = obtainFreshBeanFactory();

        // Prepare the bean factory for use in this context.
        //3、为BeanFactory配置容器特性，例如类加载器、事件处理器等
        prepareBeanFactory(beanFactory);

        try {
            // Allows post-processing of the bean factory in context subclasses.
            //4、为容器的某些子类指定特殊的BeanPost事件处理器
            postProcessBeanFactory(beanFactory);

            // Invoke factory processors registered as beans in the context.
            //5、调用所有注册的BeanFactoryPostProcessor的Bean
            invokeBeanFactoryPostProcessors(beanFactory);

            // Register bean processors that intercept bean creation.
            //6、为BeanFactory注册BeanPost事件处理器.
            //BeanPostProcessor是Bean后置处理器，用于监听容器触发的事件
            registerBeanPostProcessors(beanFactory);

            // Initialize message source for this context.
            //7、初始化信息源，和国际化相关.
            initMessageSource();

            // Initialize event multicaster for this context.
            //8、初始化容器事件传播器.
            initApplicationEventMulticaster();

            // Initialize other special beans in specific context subclasses.
            //9、调用子类的某些特殊Bean初始化方法
            onRefresh();

            // Check for listener beans and register them.
            //10、为事件传播器注册事件监听器.
            registerListeners();

            // Instantiate all remaining (non-lazy-init) singletons.
            //11、初始化所有剩余的单例Bean
            finishBeanFactoryInitialization(beanFactory);

            // Last step: publish corresponding event.
            //12、初始化容器的生命周期事件处理器，并发布容器的生命周期事件
            finishRefresh();
        }

        catch (BeansException ex) {
            if (logger.isWarnEnabled()) {
                logger.warn("Exception encountered during context initialization - " +
                            "cancelling refresh attempt: " + ex);
            }

            // Destroy already created singletons to avoid dangling resources.
            //13、销毁已创建的Bean
            destroyBeans();

            // Reset 'active' flag.
            //14、取消refresh操作，重置容器的同步标识。
            cancelRefresh(ex);

            // Propagate exception to caller.
            throw ex;
        }

        finally {
            // Reset common introspection caches in Spring's core, since we
            // might not ever need metadata for singleton beans anymore...
            //15、重设公共缓存
            resetCommonCaches();
        }
    }
}
```

​		refresh（）方法主要为IOC容器 Bean的生命周期管理提供条件，Spring IOC容器载入Bean配置信息从其 子 类 容 器 的 refreshBeanFactory() 方 法 启 动，所 以 整 个 refresh() 中`ConfigurableListableBeanFactory beanFactory = obtainFreshBeanFactory();`这句以后代码的都是注册容器的信息源和生命周期事件，我们前面说的载入就是从这句代码开始启动。

​		`refresh()`方法的主要作用是∶在创建IOC容器前，如果已经有容器存在，则需要把已有的容器销毁和关闭，以保证在refresh 之后使用的是新建立起来的IOC容器。它类似于对IOC容器的重启，在新建立好的容器中对容器进行初始化，对 Bean配置资源进行载入。

**4、创建容器**

​		obtainFreshBeanFactory()方法调用子类容器的 AbstractApplicationContext#refreshBeanFactory（）方法，启动容器载入 Bean配置信息的过程，代码如下∶

```java
protected ConfigurableListableBeanFactory obtainFreshBeanFactory() {
    //这里使用了委派设计模式，父类定义了抽象的refreshBeanFactory()方法，具体实现调用子类容器的refreshBeanFactory()方法
    refreshBeanFactory();
    ConfigurableListableBeanFactory beanFactory = getBeanFactory();
    if (logger.isDebugEnabled()) {
        logger.debug("Bean factory for " + getDisplayName() + ": " + beanFactory);
    }
    return beanFactory;
}
```

AbstractApplicationContext类中只抽象定义了refreshBeanFactory（）方法，容器真正调用的是其子类AbstractRefreshableApplicationContext 实现的 refreshBeanFactory（）方法，方法的源码如下∶

```java
@Override
protected final void refreshBeanFactory() throws BeansException {
    //如果已经有容器，销毁容器中的bean，关闭容器
    if (hasBeanFactory()) {
        destroyBeans();
        closeBeanFactory();
    }
    try {
        //创建IOC容器
        DefaultListableBeanFactory beanFactory = createBeanFactory();
        beanFactory.setSerializationId(getId());
        //对IOC容器进行定制化，如设置启动参数，开启注解的自动装配等
        customizeBeanFactory(beanFactory);
        //调用载入Bean定义的方法，主要这里又使用了一个委派模式，
        //在当前类中只定义了抽象的loadBeanDefinitions方法，具体的实现调用子类容器
        loadBeanDefinitions(beanFactory);
        synchronized (this.beanFactoryMonitor) {
            this.beanFactory = beanFactory;
        }
    }
    catch (IOException ex) {
        throw new ApplicationContextException("I/O error parsing bean definition source for " + getDisplayName(), ex);
    }
}
```

​		在这个方法中，先判断 BeanFactory是否存在，如果存在则先销毁 beans并关闭 beanFactory，接着创建 DefaultListableBeanFactory，并调用 loadBeanDefinitions（beanFactory）装载 bean定义。

**5、载入配置路径**

​		AbstractRefreshableApplicationContext 中只定义了抽象的 loadBeanDefinitions 方法，容器真正调用的是其子类AbstractXmlApplicationContext 对该方法的实现，AbstractXmlApplicationContext的主要源码如下∶
loadBeanDefinitions()方法同样是抽象方法，是由其子类实现的，也即在AbstractXmlApplicationContext 中。

```java
//实现父类抽象的载入Bean定义方法
@Override
protected void loadBeanDefinitions(DefaultListableBeanFactory beanFactory) throws BeansException, 		IOException {
    // Create a new XmlBeanDefinitionReader for the given BeanFactory.
    //创建XmlBeanDefinitionReader，即创建Bean读取器，并通过回调设置到容器中去，容  器使用该读取器读取Bean定义资源
    XmlBeanDefinitionReader beanDefinitionReader = new XmlBeanDefinitionReader(beanFactory);

    // Configure the bean definition reader with this context's
    // resource loading environment.
    //为Bean读取器设置Spring资源加载器，AbstractXmlApplicationContext的
    //祖先父类AbstractApplicationContext继承DefaultResourceLoader，因此，容器本身也是一个资源加载器
    beanDefinitionReader.setEnvironment(this.getEnvironment());
    beanDefinitionReader.setResourceLoader(this);
    //为Bean读取器设置SAX xml解析器
    beanDefinitionReader.setEntityResolver(new ResourceEntityResolver(this));

    // Allow a subclass to provide custom initialization of the reader,
    // then proceed with actually loading the bean definitions.
    //当Bean读取器读取Bean定义的Xml资源文件时，启用Xml的校验机制
    initBeanDefinitionReader(beanDefinitionReader);
    //Bean读取器真正实现加载的方法
    loadBeanDefinitions(beanDefinitionReader);
}

protected void initBeanDefinitionReader(XmlBeanDefinitionReader reader) {
    reader.setValidating(this.validating);
}

//Xml Bean读取器加载Bean定义资源
protected void loadBeanDefinitions(XmlBeanDefinitionReader reader) throws BeansException, IOException {
    //获取Bean定义资源的定位
    Resource[] configResources = getConfigResources();
    if (configResources != null) {
        //Xml Bean读取器调用其父类AbstractBeanDefinitionReader读取定位
        //的Bean定义资源
        reader.loadBeanDefinitions(configResources);
    }
    //如果子类中获取的Bean定义资源定位为空，则获取FileSystemXmlApplicationContext构造方法中setConfigLocations方法设置的资源
    String[] configLocations = getConfigLocations();
    if (configLocations != null) {
        //Xml Bean读取器调用其父类AbstractBeanDefinitionReader读取定位
        //的Bean定义资源
        reader.loadBeanDefinitions(configLocations);
    }
}
//这里又使用了一个委托模式，调用子类的获取Bean定义资源定位的方法
//该方法在ClassPathXmlApplicationContext中进行实现，对于我们
//举例分析源码的FileSystemXmlApplicationContext没有使用该方法
@Nullable
protected Resource[] getConfigResources() {
    return null;
}
```

​		以XmlBean读取器的其中一种策略XmlBeanDefinitionReader为例。XmlBeanDefinitionReader调用其父类AbstractBeanDefinitionReader的 reader.loadBeanDefinitions（方法读取Bean配置资源由于我们使用ClassPathXmlApplicationContext作为例子分析，因此 getConfigResources的返回值为null，因此程序执行 reader.loadBeanDefinitions（configLocations）分支。

**6、分配路径处理策略**

​		在XmlBeanDefinitionReader的抽象父类AbstractBeanDefinitionReader 中定义了载入过程。AbstractBeanDefinitionReader的loadBeanDefinitions（）方法源码如下∶

```java
//重载方法，调用下面的loadBeanDefinitions(String, Set<Resource>);方法
@Override
public int loadBeanDefinitions(String location) throws BeanDefinitionStoreException {
    return loadBeanDefinitions(location, null);
}

public int loadBeanDefinitions(String location, @Nullable Set<Resource> actualResources) throws BeanDefinitionStoreException {
    //获取在IoC容器初始化过程中设置的资源加载器
    ResourceLoader resourceLoader = getResourceLoader();
    if (resourceLoader == null) {
        throw new BeanDefinitionStoreException(
            "Cannot import bean definitions from location [" + location + "]: no ResourceLoader available");
    }

    if (resourceLoader instanceof ResourcePatternResolver) {
        // Resource pattern matching available.
        try {
            //将指定位置的Bean定义资源文件解析为Spring IOC容器封装的资源
            //加载多个指定位置的Bean定义资源文件
            Resource[] resources = ((ResourcePatternResolver) resourceLoader).getResources(location);
            //委派调用其子类XmlBeanDefinitionReader的方法，实现加载功能
            int loadCount = loadBeanDefinitions(resources);
            if (actualResources != null) {
                for (Resource resource : resources) {
                    actualResources.add(resource);
                }
            }
            if (logger.isDebugEnabled()) {
                logger.debug("Loaded " + loadCount + " bean definitions from location pattern [" + location + "]");
            }
            return loadCount;
        }
        catch (IOException ex) {
            throw new BeanDefinitionStoreException(
                "Could not resolve bean definition resource pattern [" + location + "]", ex);
        }
    }
    else {
        // Can only load single resources by absolute URL.
        //将指定位置的Bean定义资源文件解析为Spring IOC容器封装的资源
        //加载单个指定位置的Bean定义资源文件
        Resource resource = resourceLoader.getResource(location);
        //委派调用其子类XmlBeanDefinitionReader的方法，实现加载功能
        int loadCount = loadBeanDefinitions(resource);
        if (actualResources != null) {
            actualResources.add(resource);
        }
        if (logger.isDebugEnabled()) {
            logger.debug("Loaded " + loadCount + " bean definitions from location [" + location + "]");
        }
        return loadCount;
    }
}

public int loadBeanDefinitions(Resource... resources) throws BeanDefinitionStoreException {
    Assert.notNull(resources, "Resource array must not be null");
    int counter = 0;
    for (Resource resource : resources) {
        counter += loadBeanDefinitions(resource);
    }
    return counter;
}

//重载方法，调用loadBeanDefinitions(String);
@Override
public int loadBeanDefinitions(String... locations) throws BeanDefinitionStoreException {
    Assert.notNull(locations, "Location array must not be null");
    int counter = 0;
    for (String location : locations) {
        counter += loadBeanDefinitions(location);
    }
    return counter;
}
```

```java
//XmlBeanDefinitionReader加载资源的入口方法
@Override
public int loadBeanDefinitions(Resource resource) throws BeanDefinitionStoreException {
    //将读入的XML资源进行特殊编码处理
    return loadBeanDefinitions(new EncodedResource(resource));
}

/**
	 * Load bean definitions from the specified XML file.
	 * @param encodedResource the resource descriptor for the XML file,
	 * allowing to specify an encoding to use for parsing the file
	 * @return the number of bean definitions found
	 * @throws BeanDefinitionStoreException in case of loading or parsing errors
	 */
//这里是载入XML形式Bean定义资源文件方法
public int loadBeanDefinitions(EncodedResource encodedResource) throws BeanDefinitionStoreException {
    Assert.notNull(encodedResource, "EncodedResource must not be null");
    if (logger.isInfoEnabled()) {
        logger.info("Loading XML bean definitions from " + encodedResource.getResource());
    }

    Set<EncodedResource> currentResources = this.resourcesCurrentlyBeingLoaded.get();
    if (currentResources == null) {
        currentResources = new HashSet<>(4);
        this.resourcesCurrentlyBeingLoaded.set(currentResources);
    }
    if (!currentResources.add(encodedResource)) {
        throw new BeanDefinitionStoreException(
            "Detected cyclic loading of " + encodedResource + " - check your import definitions!");
    }
    try {
        //将资源文件转为InputStream的IO流
        InputStream inputStream = encodedResource.getResource().getInputStream();
        try {
            //从InputStream中得到XML的解析源
            InputSource inputSource = new InputSource(inputStream);
            if (encodedResource.getEncoding() != null) {
                inputSource.setEncoding(encodedResource.getEncoding());
            }
            //这里是具体的读取过程
            return doLoadBeanDefinitions(inputSource, encodedResource.getResource());
        }
        finally {
            //关闭从Resource中得到的IO流
            inputStream.close();
        }
    }
    catch (IOException ex) {
        throw new BeanDefinitionStoreException(
            "IOException parsing XML document from " + encodedResource.getResource(), ex);
    }
    finally {
        currentResources.remove(encodedResource);
        if (currentResources.isEmpty()) {
            this.resourcesCurrentlyBeingLoaded.remove();
        }
    }
}

```



​		AbstractRefreshableConfigApplicationContext 的 loadBeanDefinitions（Resourceresources）方法实际上是调用AbstractBeanDefinitionReader的`loadBeanDefinitions(Resource... resources)`方法。从对AbstractBeanDefinitionReader的 loadBeanDefinitions（）方法源码分析可以看出该方法就做了两件事∶

​		首先，调用资源加载器的获取资源方法 resourceLoader.getResource（location）获取到要加载的资源。

​		其次，真正执行加载功能是其子类XmlBeanDefinitionReader的 loadBeanDefinitions（)方法。在loadBeanDefinitions（）方法中调用了AbstractApplicationContext 的 getResources（）方法，跟进去之后发现 getResources（）方法其实定义在 ResourcePatternResolver中，此时，我们有必要来看一下ResourcePatternResolver的全类图∶

<img src="images/Spring Framework容器-5.jpg" alt="Spring Framework容器-5" style="zoom:40%;" />

​		从上面可以看到Resourceloader与 ApplicationContext的继承关系，可以看出其实际调用的是DefaultResourceloader 中的 getSource()方 法 定 位 Resource，因ClassPathXmlApplicationContext本身就是 DefaultResourcelLoader的实现类，所以此时又回到了ClassPathXmlApplicationContext 中来。

**7、解析配置文件路径**

​		XmlBeanDefinitionReader 通 过调 用ClassPathXmlApplicationContext 的 父 类DefaultResourceloader的getResource（）方法获取要加载的资源，其源码如下：

```java
//获取Resource的具体实现方法
@Override
public Resource getResource(String location) {
    Assert.notNull(location, "Location must not be null");

    for (ProtocolResolver protocolResolver : this.protocolResolvers) {
        Resource resource = protocolResolver.resolve(location, this);
        if (resource != null) {
            return resource;
        }
    }
    //如果是类路径的方式，那需要使用ClassPathResource 来得到bean 文件的资源对象
    if (location.startsWith("/")) {
        return getResourceByPath(location);
    }
    else if (location.startsWith(CLASSPATH_URL_PREFIX)) {
        return new ClassPathResource(location.substring(CLASSPATH_URL_PREFIX.length()), getClassLoader());
    }
    else {
        try {
            // Try to parse the location as a URL...
            // 如果是URL 方式，使用UrlResource 作为bean 文件的资源对象
            URL url = new URL(location);
            return (ResourceUtils.isFileURL(url) ? new FileUrlResource(url) : new UrlResource(url));
        }
        catch (MalformedURLException ex) {
            // No URL -> resolve as resource path.
            //如果既不是classpath标识，又不是URL标识的Resource定位，则调用
            //容器本身的getResourceByPath方法获取Resource
            return getResourceByPath(location);
        }
    }
}
```

​		DefaultResourceLoader提供了getResourceByPath（）方法的实现，就是为了处理既不是 classpath标识，又不是 URL 标识的Resource定位这种情况。

```java
protected Resource getResourceByPath(String path) {
    return new ClassPathContextResource(path, getClassLoader());
}
```

​		在ClassPathResource中完成了对整个路径的解析。这样，就可以从类路径上对IOC配置文件进行加载，当然我们可以按照这个逻辑从任何地方加载，在Spring 中我们看到它提供的各种资源抽象，比如ClassPathResource、URLResource、FileSystemResource等来供我们使用。上面我们看到的是定位Resource的一个过程，而这只是加载过程的一部分。例如 FileSystemXmlApplication容器就重写了
getResourceByPath（）方法∶

```java
@Override
protected Resource getResourceByPath(String path) {
    if (path.startsWith("/")) {
        path = path.substring(1);
    }
    //这里使用文件系统资源对象来定义bean 文件
    return new FileSystemResource(path);
}
```

​		通过子类的覆盖，巧妙地完成了将类路径变为文件路径的转换。

**8、开始读取配置内容**

​		继续回到XmlBeanDefinitionReader的loadBeanDefinitions（Resource…)方法看到代表bean文件的资源定义以后的载入过程。

```java
//XmlBeanDefinitionReader加载资源的入口方法
@Override
public int loadBeanDefinitions(Resource resource) throws BeanDefinitionStoreException {
    //将读入的XML资源进行特殊编码处理
    return loadBeanDefinitions(new EncodedResource(resource));
}

//这里是载入XML形式Bean定义资源文件方法
public int loadBeanDefinitions(EncodedResource encodedResource) throws BeanDefinitionStoreException {
    Assert.notNull(encodedResource, "EncodedResource must not be null");
    if (logger.isInfoEnabled()) {
        logger.info("Loading XML bean definitions from " + encodedResource.getResource());
    }

    Set<EncodedResource> currentResources = this.resourcesCurrentlyBeingLoaded.get();
    if (currentResources == null) {
        currentResources = new HashSet<>(4);
        this.resourcesCurrentlyBeingLoaded.set(currentResources);
    }
    if (!currentResources.add(encodedResource)) {
        throw new BeanDefinitionStoreException(
            "Detected cyclic loading of " + encodedResource + " - check your import definitions!");
    }
    try {
        //将资源文件转为InputStream的IO流
        InputStream inputStream = encodedResource.getResource().getInputStream();
        try {
            //从InputStream中得到XML的解析源
            InputSource inputSource = new InputSource(inputStream);
            if (encodedResource.getEncoding() != null) {
                inputSource.setEncoding(encodedResource.getEncoding());
            }
            //这里是具体的读取过程
            return doLoadBeanDefinitions(inputSource, encodedResource.getResource());
        }
        finally {
            //关闭从Resource中得到的IO流
            inputStream.close();
        }
    }
    catch (IOException ex) {
        throw new BeanDefinitionStoreException(
            "IOException parsing XML document from " + encodedResource.getResource(), ex);
    }
    finally {
        currentResources.remove(encodedResource);
        if (currentResources.isEmpty()) {
            this.resourcesCurrentlyBeingLoaded.remove();
        }
    }
}

//从特定XML文件中实际载入Bean定义资源的方法
protected int doLoadBeanDefinitions(InputSource inputSource, Resource resource)
    throws BeanDefinitionStoreException {
    try {
        //将XML文件转换为DOM对象，解析过程由documentLoader实现
        Document doc = doLoadDocument(inputSource, resource);
        //这里是启动对Bean定义解析的详细过程，该解析过程会用到Spring的Bean配置规则
        return registerBeanDefinitions(doc, resource);
    }
    catch (BeanDefinitionStoreException ex) {
        throw ex;
    }
    catch (SAXParseException ex) {
        throw new XmlBeanDefinitionStoreException(resource.getDescription(),
                                                  "Line " + ex.getLineNumber() + " in XML document from " + resource + " is invalid", ex);
    }
    catch (SAXException ex) {
        throw new XmlBeanDefinitionStoreException(resource.getDescription(),
                                                  "XML document from " + resource + " is invalid", ex);
    }
    catch (ParserConfigurationException ex) {
        throw new BeanDefinitionStoreException(resource.getDescription(),
                                               "Parser configuration exception parsing XML from " + resource, ex);
    }
    catch (IOException ex) {
        throw new BeanDefinitionStoreException(resource.getDescription(),
                                               "IOException parsing XML document from " + resource, ex);
    }
    catch (Throwable ex) {
        throw new BeanDefinitionStoreException(resource.getDescription(),
                                               "Unexpected exception parsing XML document from " + resource, ex);
    }
}
```

​		通过源码分析，载入Bean配置信息的最后一步是将Bean配置信息转换为 Document 对象，该过程由documentLoader（）方法实现。

**9、准备文档对象**

​		DocumentLoader将 Bean配置资源转换成 Document 对象的源码如下∶

XmlBeanDefinitionReader 

```java
protected Document doLoadDocument(InputSource inputSource, Resource resource) throws Exception {
    return this.documentLoader.loadDocument(inputSource, getEntityResolver(), this.errorHandler,
                                            getValidationModeForResource(resource), isNamespaceAware());
}
```

DefaultDocumentLoader 

```java
//使用标准的JAXP将载入的Bean定义资源转换成document对象
@Override
public Document loadDocument(InputSource inputSource, EntityResolver entityResolver,
                             ErrorHandler errorHandler, int validationMode, boolean namespaceAware) throws Exception {

    //创建文件解析器工厂
    DocumentBuilderFactory factory = createDocumentBuilderFactory(validationMode, namespaceAware);
    if (logger.isDebugEnabled()) {
        logger.debug("Using JAXP provider [" + factory.getClass().getName() + "]");
    }
    //创建文档解析器
    DocumentBuilder builder = createDocumentBuilder(factory, entityResolver, errorHandler);
    //解析Spring的Bean定义资源
    return builder.parse(inputSource);
}

protected DocumentBuilderFactory createDocumentBuilderFactory(int validationMode, boolean namespaceAware)
    	throws ParserConfigurationException {

    //创建文档解析工厂
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    factory.setNamespaceAware(namespaceAware);

    //设置解析XML的校验
    if (validationMode != XmlValidationModeDetector.VALIDATION_NONE) {
        factory.setValidating(true);
        if (validationMode == XmlValidationModeDetector.VALIDATION_XSD) {
            // Enforce namespace aware for XSD...
            factory.setNamespaceAware(true);
            try {
                factory.setAttribute(SCHEMA_LANGUAGE_ATTRIBUTE, XSD_SCHEMA_LANGUAGE);
            }
            catch (IllegalArgumentException ex) {
                ParserConfigurationException pcex = new ParserConfigurationException(
                    "Unable to validate using XSD: Your JAXP provider [" + factory +
                    "] does not support XML Schema. Are you running on Java 1.4 with Apache Crimson? " +
                    "Upgrade to Apache Xerces (or Java 1.5) for full XSD support.");
                pcex.initCause(ex);
                throw pcex;
            }
        }
    }

    return factory;
}
```

​		上面的解析过程是调用JavaEE 标准的JAXP标准进行处理。至此Spring IOC容器根据定位的Bean配置信息，将其加载读入并转换成为Document 对象过程完成。接下来我们要继续分析Spring IOC容器将载入的Bean配置信息转换为Document对象之后，是如何将其解析为Spring IOC管理的Bean对象并将其注册到容器中的。

**10、分配解析策略**

​		XmlBeanDefinitionReader类中的doLoadBeanDefinition（）方法是从特定XML文件中实际载入Bean配置资源的方法，该方法在载入Bean配置资源之后将其转换为Document 对象，接下来调用registerBeanDefinitions（）启 动 Spring IOC 容 器 对 Bean 定 义 的 解 析 过 程，registerBeanDefinitions（）方法源码如下∶

```java
//按照Spring的Bean语义要求将Bean定义资源解析并转换为容器内部数据结构
public int registerBeanDefinitions(Document doc, Resource resource) throws BeanDefinitionStoreException {
    //得到BeanDefinitionDocumentReader来对xml格式的BeanDefinition解析
    BeanDefinitionDocumentReader documentReader = createBeanDefinitionDocumentReader();
    //获得容器中注册的Bean数量
    int countBefore = getRegistry().getBeanDefinitionCount();
    //解析过程入口，这里使用了委派模式，BeanDefinitionDocumentReader只是个接口,
    //具体的解析实现过程有实现类DefaultBeanDefinitionDocumentReader完成
    documentReader.registerBeanDefinitions(doc, createReaderContext(resource));
    //统计解析的Bean数量
    return getRegistry().getBeanDefinitionCount() - countBefore;
}
```

​		Bean配置资源的载入解析分为以下两个过程∶

​		首先，通过调用XML解析器将Bean配置信息转换得到Document 对象，但是这些Document 对象并没有按照 Spring的Bean规则进行解析。这一步是载入的过程

​		其次，在完成通用的XML解析之后，按照 Spring Bean的定义规则对 Document 对象进行解析，其过 程是在 接 口 BeanDefinitionDocumentReader 的 实 现类DefaultBeanDefinitionDocumentReader 中实现。

**11、将配置载入内存**

​		BeanDefinitionDocumentReader 接口 通过 registerBeanDefinitions（）方法调用其 实现类DefaultBeanDefinitionDocumentReader 对Document 对象进行解析，解析的代码如下∶

DefaultBeanDefinitionDocumentReader 

```java
//根据Spring DTD对Bean的定义规则解析Bean定义Document对象
	@Override
	public void registerBeanDefinitions(Document doc, XmlReaderContext readerContext) {
		//获得XML描述符
		this.readerContext = readerContext;
		logger.debug("Loading bean definitions");
		//获得Document的根元素
		Element root = doc.getDocumentElement();
		doRegisterBeanDefinitions(root);
	}

protected void doRegisterBeanDefinitions(Element root) {
    //具体的解析过程由BeanDefinitionParserDelegate实现，
    //BeanDefinitionParserDelegate中定义了Spring Bean定义XML文件的各种元素
    BeanDefinitionParserDelegate parent = this.delegate;
    this.delegate = createDelegate(getReaderContext(), root, parent);

    if (this.delegate.isDefaultNamespace(root)) {
        String profileSpec = root.getAttribute(PROFILE_ATTRIBUTE);
        if (StringUtils.hasText(profileSpec)) {
            String[] specifiedProfiles = StringUtils.tokenizeToStringArray(
                profileSpec, BeanDefinitionParserDelegate.MULTI_VALUE_ATTRIBUTE_DELIMITERS);
            if (!getReaderContext().getEnvironment().acceptsProfiles(specifiedProfiles)) {
                if (logger.isInfoEnabled()) {
                    logger.info("Skipped XML bean definition file due to specified profiles [" + profileSpec +
                                "] not matching: " + getReaderContext().getResource());
                }
                return;
            }
        }
    }

    //在解析Bean定义之前，进行自定义的解析，增强解析过程的可扩展性
    preProcessXml(root);
    //从Document的根元素开始进行Bean定义的Document对象
    parseBeanDefinitions(root, this.delegate);
    //在解析Bean定义之后，进行自定义的解析，增加解析过程的可扩展性
    postProcessXml(root);

    this.delegate = parent;
}

//创建BeanDefinitionParserDelegate，用于完成真正的解析过程
protected BeanDefinitionParserDelegate createDelegate(
    XmlReaderContext readerContext, Element root, @Nullable BeanDefinitionParserDelegate parentDelegate) {

    BeanDefinitionParserDelegate delegate = new BeanDefinitionParserDelegate(readerContext);
    //BeanDefinitionParserDelegate初始化Document根元素
    delegate.initDefaults(root, parentDelegate);
    return delegate;
}

//使用Spring的Bean规则从Document的根元素开始进行Bean定义的Document对象
protected void parseBeanDefinitions(Element root, BeanDefinitionParserDelegate delegate) {
    //Bean定义的Document对象使用了Spring默认的XML命名空间
    if (delegate.isDefaultNamespace(root)) {
        //获取Bean定义的Document对象根元素的所有子节点
        NodeList nl = root.getChildNodes();
        for (int i = 0; i < nl.getLength(); i++) {
            Node node = nl.item(i);
            //获得Document节点是XML元素节点
            if (node instanceof Element) {
                Element ele = (Element) node;
                //Bean定义的Document的元素节点使用的是Spring默认的XML命名空间
                if (delegate.isDefaultNamespace(ele)) {
                    //使用Spring的Bean规则解析元素节点
                    parseDefaultElement(ele, delegate);
                }
                else {
                    //没有使用Spring默认的XML命名空间，则使用用户自定义的解//析规则解析元素节点
                    delegate.parseCustomElement(ele);
                }
            }
        }
    }
    else {
        //Document的根节点没有使用Spring默认的命名空间，则使用用户自定义的
        //解析规则解析Document根节点
        delegate.parseCustomElement(root);
    }
}


//使用Spring的Bean规则解析Document元素节点
private void parseDefaultElement(Element ele, BeanDefinitionParserDelegate delegate) {
    //如果元素节点是<Import>导入元素，进行导入解析
    if (delegate.nodeNameEquals(ele, IMPORT_ELEMENT)) {
        importBeanDefinitionResource(ele);
    }
    //如果元素节点是<Alias>别名元素，进行别名解析
    else if (delegate.nodeNameEquals(ele, ALIAS_ELEMENT)) {
        processAliasRegistration(ele);
    }
    //元素节点既不是导入元素，也不是别名元素，即普通的<Bean>元素，
    //按照Spring的Bean规则解析元素
    else if (delegate.nodeNameEquals(ele, BEAN_ELEMENT)) {
        processBeanDefinition(ele, delegate);
    }
    else if (delegate.nodeNameEquals(ele, NESTED_BEANS_ELEMENT)) {
        // recurse
        doRegisterBeanDefinitions(ele);
    }
}

//解析<Import>导入元素，从给定的导入路径加载Bean定义资源到Spring IoC容器中
protected void importBeanDefinitionResource(Element ele) {
    //获取给定的导入元素的location属性
    String location = ele.getAttribute(RESOURCE_ATTRIBUTE);
    //如果导入元素的location属性值为空，则没有导入任何资源，直接返回
    if (!StringUtils.hasText(location)) {
        getReaderContext().error("Resource location must not be empty", ele);
        return;
    }

    // Resolve system properties: e.g. "${user.dir}"
    //使用系统变量值解析location属性值
    location = getReaderContext().getEnvironment().resolveRequiredPlaceholders(location);

    Set<Resource> actualResources = new LinkedHashSet<>(4);

    // Discover whether the location is an absolute or relative URI
    //标识给定的导入元素的location是否是绝对路径
    boolean absoluteLocation = false;
    try {
        absoluteLocation = ResourcePatternUtils.isUrl(location) || ResourceUtils.toURI(location).isAbsolute();
    }
    catch (URISyntaxException ex) {
        // cannot convert to an URI, considering the location relative
        // unless it is the well-known Spring prefix "classpath*:"
        //给定的导入元素的location不是绝对路径
    }

    // Absolute or relative?
    //给定的导入元素的location是绝对路径
    if (absoluteLocation) {
        try {
            //使用资源读入器加载给定路径的Bean定义资源
            int importCount = getReaderContext().getReader().loadBeanDefinitions(location, actualResources);
            if (logger.isDebugEnabled()) {
                logger.debug("Imported " + importCount + " bean definitions from URL location [" + location + "]");
            }
        }
        catch (BeanDefinitionStoreException ex) {
            getReaderContext().error(
                "Failed to import bean definitions from URL location [" + location + "]", ele, ex);
        }
    }
    else {
        // No URL -> considering resource location as relative to the current file.
        //给定的导入元素的location是相对路径
        try {
            int importCount;
            //将给定导入元素的location封装为相对路径资源
            Resource relativeResource = getReaderContext().getResource().createRelative(location);
            //封装的相对路径资源存在
            if (relativeResource.exists()) {
                //使用资源读入器加载Bean定义资源
                importCount = getReaderContext().getReader().loadBeanDefinitions(relativeResource);
                actualResources.add(relativeResource);
            }
            //封装的相对路径资源不存在
            else {
                //获取Spring IOC容器资源读入器的基本路径
                String baseLocation = getReaderContext().getResource().getURL().toString();
                //根据Spring IOC容器资源读入器的基本路径加载给定导入路径的资源
                importCount = getReaderContext().getReader().loadBeanDefinitions(
                    StringUtils.applyRelativePath(baseLocation, location), actualResources);
            }
            if (logger.isDebugEnabled()) {
                logger.debug("Imported " + importCount + " bean definitions from relative location [" + location + "]");
            }
        }
        catch (IOException ex) {
            getReaderContext().error("Failed to resolve current resource location", ele, ex);
        }
        catch (BeanDefinitionStoreException ex) {
            getReaderContext().error("Failed to import bean definitions from relative location [" + location + "]",
                                     ele, ex);
        }
    }
    Resource[] actResArray = actualResources.toArray(new Resource[actualResources.size()]);
    //在解析完<Import>元素之后，发送容器导入其他资源处理完成事件
    getReaderContext().fireImportProcessed(location, actResArray, extractSource(ele));
}

//解析<Alias>别名元素，为Bean向Spring IoC容器注册别名
protected void processAliasRegistration(Element ele) {
    //获取<Alias>别名元素中name的属性值
    String name = ele.getAttribute(NAME_ATTRIBUTE);
    //获取<Alias>别名元素中alias的属性值
    String alias = ele.getAttribute(ALIAS_ATTRIBUTE);
    boolean valid = true;
    //<alias>别名元素的name属性值为空
    if (!StringUtils.hasText(name)) {
        getReaderContext().error("Name must not be empty", ele);
        valid = false;
    }
    //<alias>别名元素的alias属性值为空
    if (!StringUtils.hasText(alias)) {
        getReaderContext().error("Alias must not be empty", ele);
        valid = false;
    }
    if (valid) {
        try {
            //向容器的资源读入器注册别名
            getReaderContext().getRegistry().registerAlias(name, alias);
        }
        catch (Exception ex) {
            getReaderContext().error("Failed to register alias '" + alias +
                                     "' for bean with name '" + name + "'", ele, ex);
        }
        //在解析完<Alias>元素之后，发送容器别名处理完成事件
        getReaderContext().fireAliasRegistered(name, alias, extractSource(ele));
    }
}

//解析Bean定义资源Document对象的普通元素
protected void processBeanDefinition(Element ele, BeanDefinitionParserDelegate delegate) {
    BeanDefinitionHolder bdHolder = delegate.parseBeanDefinitionElement(ele);
    // BeanDefinitionHolder是对BeanDefinition的封装，即Bean定义的封装类
    //对Document对象中<Bean>元素的解析由BeanDefinitionParserDelegate实现
    // BeanDefinitionHolder bdHolder = delegate.parseBeanDefinitionElement(ele);
    if (bdHolder != null) {
        bdHolder = delegate.decorateBeanDefinitionIfRequired(ele, bdHolder);
        try {
            // Register the final decorated instance.
            //向Spring IOC容器注册解析得到的Bean定义，这是Bean定义向IOC容器注册的入口
            BeanDefinitionReaderUtils.registerBeanDefinition(bdHolder, getReaderContext().getRegistry());
        }
        catch (BeanDefinitionStoreException ex) {
            getReaderContext().error("Failed to register bean definition with name '" +
                                     bdHolder.getBeanName() + "'", ele, ex);
        }
        // Send registration event.
        //在完成向Spring IOC容器注册解析得到的Bean定义之后，发送注册事件
        getReaderContext().fireComponentRegistered(new BeanComponentDefinition(bdHolder));
    }
}
```

​		通过上述 Spring IOC容器对载入的 Bean定义 Document 解析可以看出，我们使用Spring时，在Spring 配置文件中可以使用`<import>`元素来导入IOC容器所需要的其他资源，Spring IOC容器在解行时会首先将指定导入的资源加载进容器中。使用`<ailas>`别名时，Spring IOC容器首先将别名元素所定义的别名注册到容器中。

​		对于既不是`<import>`元素，，又不是`<alias>`元素的元素，即Spring配置文件中普通的`<bean>`元素的解析由 BeanDefinitionParserDelegate类的parseBeanDefinitionElement()方法来实现。这个解析的过程非常复杂，我们在 mini版本的时候，就用 properties 文件代替了。

**12、载入`<bean>`元素**

​		Bean配置信息中的`<import>`和`<alias>`元素解析在 DefaultBeanDefinitionDocumentReader 中已经完成，对Bean配置信息中使用最多的`<bean>`元素交由 BeanDefinitionParserDelegate来解析，其解析实现的源码如下∶

BeanDefinitionParserDelegate

```java
//解析<Bean>元素的入口
@Nullable
public BeanDefinitionHolder parseBeanDefinitionElement(Element ele) {
    return parseBeanDefinitionElement(ele, null);
}

//解析Bean定义资源文件中的<Bean>元素，这个方法中主要处理<Bean>元素的id，name和别名属性
@Nullable
public BeanDefinitionHolder parseBeanDefinitionElement(Element ele, @Nullable BeanDefinition containingBean) {
    //获取<Bean>元素中的id属性值
    String id = ele.getAttribute(ID_ATTRIBUTE);
    //获取<Bean>元素中的name属性值
    String nameAttr = ele.getAttribute(NAME_ATTRIBUTE);

    //获取<Bean>元素中的alias属性值
    List<String> aliases = new ArrayList<>();

    //将<Bean>元素中的所有name属性值存放到别名中
    if (StringUtils.hasLength(nameAttr)) {
        String[] nameArr = StringUtils.tokenizeToStringArray(nameAttr, MULTI_VALUE_ATTRIBUTE_DELIMITERS);
        aliases.addAll(Arrays.asList(nameArr));
    }

    String beanName = id;
    //如果<Bean>元素中没有配置id属性时，将别名中的第一个值赋值给beanName
    if (!StringUtils.hasText(beanName) && !aliases.isEmpty()) {
        beanName = aliases.remove(0);
        if (logger.isDebugEnabled()) {
            logger.debug("No XML 'id' specified - using '" + beanName +
                         "' as bean name and " + aliases + " as aliases");
        }
    }

    //检查<Bean>元素所配置的id或者name的唯一性，containingBean标识<Bean>
    //元素中是否包含子<Bean>元素
    if (containingBean == null) {
        //检查<Bean>元素所配置的id、name或者别名是否重复
        checkNameUniqueness(beanName, aliases, ele);
    }

    //详细对<Bean>元素中配置的Bean定义进行解析的地方
    AbstractBeanDefinition beanDefinition = parseBeanDefinitionElement(ele, beanName, containingBean);
    if (beanDefinition != null) {
        if (!StringUtils.hasText(beanName)) {
            try {
                if (containingBean != null) {
                    //如果<Bean>元素中没有配置id、别名或者name，且没有包含子元素
                    //<Bean>元素，为解析的Bean生成一个唯一beanName并注册
                    beanName = BeanDefinitionReaderUtils.generateBeanName(
                        beanDefinition, this.readerContext.getRegistry(), true);
                }
                else {
                    //如果<Bean>元素中没有配置id、别名或者name，且包含了子元素
                    //<Bean>元素，为解析的Bean使用别名向IOC容器注册
                    beanName = this.readerContext.generateBeanName(beanDefinition);
                    // Register an alias for the plain bean class name, if still possible,
                    // if the generator returned the class name plus a suffix.
                    // This is expected for Spring 1.2/2.0 backwards compatibility.
                    //为解析的Bean使用别名注册时，为了向后兼容
                    //Spring1.2/2.0，给别名添加类名后缀
                    String beanClassName = beanDefinition.getBeanClassName();
                    if (beanClassName != null &&
                        beanName.startsWith(beanClassName) && beanName.length() > beanClassName.length() &&
                        !this.readerContext.getRegistry().isBeanNameInUse(beanClassName)) {
                        aliases.add(beanClassName);
                    }
                }
                if (logger.isDebugEnabled()) {
                    logger.debug("Neither XML 'id' nor 'name' specified - " +
                                 "using generated bean name [" + beanName + "]");
                }
            }
            catch (Exception ex) {
                error(ex.getMessage(), ele);
                return null;
            }
        }
        String[] aliasesArray = StringUtils.toStringArray(aliases);
        return new BeanDefinitionHolder(beanDefinition, beanName, aliasesArray);
    }
    //当解析出错时，返回null
    return null;
}

protected void checkNameUniqueness(String beanName, List<String> aliases, Element beanElement) {
    String foundName = null;

    if (StringUtils.hasText(beanName) && this.usedNames.contains(beanName)) {
        foundName = beanName;
    }
    if (foundName == null) {
        foundName = CollectionUtils.findFirstMatch(this.usedNames, aliases);
    }
    if (foundName != null) {
        error("Bean name '" + foundName + "' is already used in this <beans> element", beanElement);
    }

    this.usedNames.add(beanName);
    this.usedNames.addAll(aliases);
}

//详细对<Bean>元素中配置的Bean定义其他属性进行解析
//由于上面的方法中已经对Bean的id、name和别名等属性进行了处理
//该方法中主要处理除这三个以外的其他属性数据
@Nullable
public AbstractBeanDefinition parseBeanDefinitionElement(
    Element ele, String beanName, @Nullable BeanDefinition containingBean) {
    //记录解析的<Bean>
    this.parseState.push(new BeanEntry(beanName));

    //这里只读取<Bean>元素中配置的class名字，然后载入到BeanDefinition中去
    //只是记录配置的class名字，不做实例化，对象的实例化在依赖注入时完成
    String className = null;

    //如果<Bean>元素中配置了parent属性，则获取parent属性的值
    if (ele.hasAttribute(CLASS_ATTRIBUTE)) {
        className = ele.getAttribute(CLASS_ATTRIBUTE).trim();
    }
    String parent = null;
    if (ele.hasAttribute(PARENT_ATTRIBUTE)) {
        parent = ele.getAttribute(PARENT_ATTRIBUTE);
    }

    try {
        //根据<Bean>元素配置的class名称和parent属性值创建BeanDefinition
        //为载入Bean定义信息做准备
        AbstractBeanDefinition bd = createBeanDefinition(className, parent);

        //对当前的<Bean>元素中配置的一些属性进行解析和设置，如配置的单态(singleton)属性等
        parseBeanDefinitionAttributes(ele, beanName, containingBean, bd);
        //为<Bean>元素解析的Bean设置description信息
        bd.setDescription(DomUtils.getChildElementValueByTagName(ele, DESCRIPTION_ELEMENT));

        //对<Bean>元素的meta(元信息)属性解析
        parseMetaElements(ele, bd);
        //对<Bean>元素的lookup-method属性解析
        parseLookupOverrideSubElements(ele, bd.getMethodOverrides());
        //对<Bean>元素的replaced-method属性解析
        parseReplacedMethodSubElements(ele, bd.getMethodOverrides());

        //解析<Bean>元素的构造方法设置
        parseConstructorArgElements(ele, bd);
        //解析<Bean>元素的<property>设置
        parsePropertyElements(ele, bd);
        //解析<Bean>元素的qualifier属性
        parseQualifierElements(ele, bd);

        //为当前解析的Bean设置所需的资源和依赖对象
        bd.setResource(this.readerContext.getResource());
        bd.setSource(extractSource(ele));

        return bd;
    }
    catch (ClassNotFoundException ex) {
        error("Bean class [" + className + "] not found", ele, ex);
    }
    catch (NoClassDefFoundError err) {
        error("Class that bean class [" + className + "] depends on not found", ele, err);
    }
    catch (Throwable ex) {
        error("Unexpected failure during bean definition parsing", ele, ex);
    }
    finally {
        this.parseState.pop();
    }

    //解析<Bean>元素出错时，返回null
    return null;
}

```

​		只要使用过Spring对Spring 配置文件比较熟悉的人通过对上述源码的分析就会明白我们在Spring 配置文件中`<Bean>`元素中配置的属性就是通过该方法解析和设置到Bean中去的。

​		注意∶在解析`<Bean>`元素过程中没有创建和实例化 Bean 对象，只是创建了Bean 对象的定义类BeanDefinition，将`<Bean>`元素中的配置信息设置到BeanDefinition中作为记录，当依赖注入时才使用这些记录信息创建和实例化具体的Bean 对象。
​		上面方法中一些对一些配置如元信息（meta）、qualifier等的解析，我们在 Spring 中配置时使用的也不多，我们在使用Spring的`<Bean>`元素时，配置最多的是`<property>`属性，因此我们下面继续分析源码，了解 Bean 的属性在解析时是如何设置的。

**13、载入`<property>`元素**

​		BeanDefinitionParserDelegate在解析`<Bean>`调用parsePropertyElements()方法解析`<Bean>`元素中的`<property>`属性子元素，解析源码如下∶

BeanDefinitionParserDelegate

```java
//解析<Bean>元素中的<property>子元素
public void parsePropertyElements(Element beanEle, BeanDefinition bd) {
    //获取<Bean>元素中所有的子元素
    NodeList nl = beanEle.getChildNodes();
    for (int i = 0; i < nl.getLength(); i++) {
        Node node = nl.item(i);
        //如果子元素是<property>子元素，则调用解析<property>子元素方法解析
        if (isCandidateElement(node) && nodeNameEquals(node, PROPERTY_ELEMENT)) {
            parsePropertyElement((Element) node, bd);
        }
    }
}

//解析<property>元素
public void parsePropertyElement(Element ele, BeanDefinition bd) {
    //获取<property>元素的名字
    String propertyName = ele.getAttribute(NAME_ATTRIBUTE);
    if (!StringUtils.hasLength(propertyName)) {
        error("Tag 'property' must have a 'name' attribute", ele);
        return;
    }
    this.parseState.push(new PropertyEntry(propertyName));
    try {
        //如果一个Bean中已经有同名的property存在，则不进行解析，直接返回。
        //即如果在同一个Bean中配置同名的property，则只有第一个起作用
        if (bd.getPropertyValues().contains(propertyName)) {
            error("Multiple 'property' definitions for property '" + propertyName + "'", ele);
            return;
        }
        //解析获取property的值
        Object val = parsePropertyValue(ele, bd, propertyName);
        //根据property的名字和值创建property实例
        PropertyValue pv = new PropertyValue(propertyName, val);
        //解析<property>元素中的属性
        parseMetaElements(ele, pv);
        pv.setSource(extractSource(ele));
        bd.getPropertyValues().addPropertyValue(pv);
    }
    finally {
        this.parseState.pop();
    }
}

//解析获取property值
@Nullable
public Object parsePropertyValue(Element ele, BeanDefinition bd, @Nullable String propertyName) {
    String elementName = (propertyName != null) ?
        "<property> element for property '" + propertyName + "'" :
    "<constructor-arg> element";

    // Should only have one child element: ref, value, list, etc.
    //获取<property>的所有子元素，只能是其中一种类型:ref,value,list,etc等
    NodeList nl = ele.getChildNodes();
    Element subElement = null;
    for (int i = 0; i < nl.getLength(); i++) {
        Node node = nl.item(i);
        //子元素不是description和meta属性
        if (node instanceof Element && !nodeNameEquals(node, DESCRIPTION_ELEMENT) &&
            !nodeNameEquals(node, META_ELEMENT)) {
            // Child element is what we're looking for.
            if (subElement != null) {
                error(elementName + " must not contain more than one sub-element", ele);
            }
            else {
                //当前<property>元素包含有子元素
                subElement = (Element) node;
            }
        }
    }

    //判断property的属性值是ref还是value，不允许既是ref又是value
    boolean hasRefAttribute = ele.hasAttribute(REF_ATTRIBUTE);
    boolean hasValueAttribute = ele.hasAttribute(VALUE_ATTRIBUTE);
    if ((hasRefAttribute && hasValueAttribute) ||
        ((hasRefAttribute || hasValueAttribute) && subElement != null)) {
        error(elementName +
              " is only allowed to contain either 'ref' attribute OR 'value' attribute OR sub-element", ele);
    }

    //如果属性是ref，创建一个ref的数据对象RuntimeBeanReference
    //这个对象封装了ref信息
    if (hasRefAttribute) {
        String refName = ele.getAttribute(REF_ATTRIBUTE);
        if (!StringUtils.hasText(refName)) {
            error(elementName + " contains empty 'ref' attribute", ele);
        }
        //一个指向运行时所依赖对象的引用
        RuntimeBeanReference ref = new RuntimeBeanReference(refName);
        //设置这个ref的数据对象是被当前的property对象所引用
        ref.setSource(extractSource(ele));
        return ref;
    }
    //如果属性是value，创建一个value的数据对象TypedStringValue
    //这个对象封装了value信息
    else if (hasValueAttribute) {
        //一个持有String类型值的对象
        TypedStringValue valueHolder = new TypedStringValue(ele.getAttribute(VALUE_ATTRIBUTE));
        //设置这个value数据对象是被当前的property对象所引用
        valueHolder.setSource(extractSource(ele));
        return valueHolder;
    }
    //如果当前<property>元素还有子元素
    else if (subElement != null) {
        //解析<property>的子元素
        return parsePropertySubElement(subElement, bd);
    }
    else {
        // Neither child element nor "ref" or "value" attribute found.
        //propery属性中既不是ref，也不是value属性，解析出错返回null
        error(elementName + " must specify a ref or value", ele);
        return null;
    }
}
```

​		通过对上述源码的分析，我们可以了解在Spring配置文件中，`<Bean>`元素中`<property>`元素的相关配置是如何处理的∶

1. ref被封装为指向依赖对象一个引用。
2. value 配置都会封装成一个字符串类型的对象。
3. ref和 value都通过"解析的数据类型属性值.setSource(extractSource(ele));"方法将属性值/引用与所引用的属性关联起来。

​		在方法的最后对于`<property>`元素的子元素通过parsePropertySubElement （方法解析我们继续分析该方法的源码，了解其解析过程。

**14、载入`<property>`的子元素**

​		在 BeanDefinitionParserDelegate类中的 parsePropertySubElement()方法对`<property>`中的子元素解析，源码如下∶

```java
@Nullable
public Object parsePropertySubElement(Element ele, @Nullable BeanDefinition bd) {
    return parsePropertySubElement(ele, bd, null);
}

//解析<property>元素中ref,value或者集合等子元素
@Nullable
public Object parsePropertySubElement(Element ele, @Nullable BeanDefinition bd, @Nullable String defaultValueType) {
    //如果<property>没有使用Spring默认的命名空间，则使用用户自定义的规则解析内嵌元素
    if (!isDefaultNamespace(ele)) {
        return parseNestedCustomElement(ele, bd);
    }
    //如果子元素是bean，则使用解析<Bean>元素的方法解析
    else if (nodeNameEquals(ele, BEAN_ELEMENT)) {
        BeanDefinitionHolder nestedBd = parseBeanDefinitionElement(ele, bd);
        if (nestedBd != null) {
            nestedBd = decorateBeanDefinitionIfRequired(ele, nestedBd, bd);
        }
        return nestedBd;
    }
    //如果子元素是ref，ref中只能有以下3个属性：bean、local、parent
    else if (nodeNameEquals(ele, REF_ELEMENT)) {
        // A generic reference to any name of any bean.
        //可以不再同一个Spring配置文件中，具体请参考Spring对ref的配置规则
        String refName = ele.getAttribute(BEAN_REF_ATTRIBUTE);
        boolean toParent = false;
        if (!StringUtils.hasLength(refName)) {
            // A reference to the id of another bean in a parent context.
            //获取<property>元素中parent属性值，引用父级容器中的Bean
            refName = ele.getAttribute(PARENT_REF_ATTRIBUTE);
            toParent = true;
            if (!StringUtils.hasLength(refName)) {
                error("'bean' or 'parent' is required for <ref> element", ele);
                return null;
            }
        }
        if (!StringUtils.hasText(refName)) {
            error("<ref> element contains empty target attribute", ele);
            return null;
        }
        //创建ref类型数据，指向被引用的对象
        RuntimeBeanReference ref = new RuntimeBeanReference(refName, toParent);
        //设置引用类型值是被当前子元素所引用
        ref.setSource(extractSource(ele));
        return ref;
    }
    //如果子元素是<idref>，使用解析ref元素的方法解析
    else if (nodeNameEquals(ele, IDREF_ELEMENT)) {
        return parseIdRefElement(ele);
    }
    //如果子元素是<value>，使用解析value元素的方法解析
    else if (nodeNameEquals(ele, VALUE_ELEMENT)) {
        return parseValueElement(ele, defaultValueType);
    }
    //如果子元素是null，为<property>设置一个封装null值的字符串数据
    else if (nodeNameEquals(ele, NULL_ELEMENT)) {
        // It's a distinguished null value. Let's wrap it in a TypedStringValue
        // object in order to preserve the source location.
        TypedStringValue nullHolder = new TypedStringValue(null);
        nullHolder.setSource(extractSource(ele));
        return nullHolder;
    }
    //如果子元素是<array>，使用解析array集合子元素的方法解析
    else if (nodeNameEquals(ele, ARRAY_ELEMENT)) {
        return parseArrayElement(ele, bd);
    }
    //如果子元素是<list>，使用解析list集合子元素的方法解析
    else if (nodeNameEquals(ele, LIST_ELEMENT)) {
        return parseListElement(ele, bd);
    }
    //如果子元素是<set>，使用解析set集合子元素的方法解析
    else if (nodeNameEquals(ele, SET_ELEMENT)) {
        return parseSetElement(ele, bd);
    }
    //如果子元素是<map>，使用解析map集合子元素的方法解析
    else if (nodeNameEquals(ele, MAP_ELEMENT)) {
        return parseMapElement(ele, bd);
    }
    //如果子元素是<props>，使用解析props集合子元素的方法解析
    else if (nodeNameEquals(ele, PROPS_ELEMENT)) {
        return parsePropsElement(ele);
    }
    //既不是ref，又不是value，也不是集合，则子元素配置错误，返回null
    else {
        error("Unknown property sub-element: [" + ele.getNodeName() + "]", ele);
        return null;
    }
}
```

​		通过上述源码分析，我们明白了在Spring配置文件中，对`<property>`元素中配置的 array、list、set、map、prop等各种集合子元素的都通过上述方法解析，生成对应的数据对象，比如ManagedList、ManagedArray、ManagedSet等，这些Managed类是Spring对象BeanDefiniton的数据封装，对集合数据类型的具体解析有各自的解析方法实现，解析方法的命名非常规范，一目了然，我们对`<list>`集合元素的解析方法进行源码分析，了解其实现过程。

**15、载入`<list>`的子元素**

​		在BeanDefinitionParserDelegate类中的parseListElement()方法就是具体实现解析`<property>`元素中的`<list>`集合子元素，源码如下∶

```java
//解析<list>集合子元素
public List<Object> parseListElement(Element collectionEle, @Nullable BeanDefinition bd) {
    //获取<list>元素中的value-type属性，即获取集合元素的数据类型
    String defaultElementType = collectionEle.getAttribute(VALUE_TYPE_ATTRIBUTE);
    //获取<list>集合元素中的所有子节点
    NodeList nl = collectionEle.getChildNodes();
    //Spring中将List封装为ManagedList
    ManagedList<Object> target = new ManagedList<>(nl.getLength());
    target.setSource(extractSource(collectionEle));
    //设置集合目标数据类型
    target.setElementTypeName(defaultElementType);
    target.setMergeEnabled(parseMergeAttribute(collectionEle));
    //具体的<list>元素解析
    parseCollectionElements(nl, target, bd, defaultElementType);
    return target;
}

//具体解析<list>集合元素，<array>、<list>和<set>都使用该方法解析
protected void parseCollectionElements(
    NodeList elementNodes, Collection<Object> target, @Nullable BeanDefinition bd, String defaultElementType) {
    //遍历集合所有节点
    for (int i = 0; i < elementNodes.getLength(); i++) {
        Node node = elementNodes.item(i);
        //节点不是description节点
        if (node instanceof Element && !nodeNameEquals(node, DESCRIPTION_ELEMENT)) {
            //将解析的元素加入集合中，递归调用下一个子元素
            target.add(parsePropertySubElement((Element) node, bd, defaultElementType));
        }
    }
}
```

​		经过对Spring Bean配置信息转换的Document对象中的元素层层解析 Spring IOC现在已经将XML 形式定义的 Bean配置信息转换为 Spring IOC所识别的数据结构——BeanDefinition，它是Bean配置信息中配置的POJO对象在Spring IOC容器中的映射，我们可以通过AbstractBeanDefinition为入口，看到了IOC容器进行索引、查询和操作。

​		通过Spring IOC容器对 Bean配置资源的解析后，IOC容器大致完成了管理 Bean对象的准备工作即初始化过程，但是最为重要的依赖注入还没有发生，现在在IOC容器中BeanDefinition存储的只是一些静态信息，接下来需要向容器注册 Bean定义信息才能全部完成IOC容器的初始化过程

**16、分配注册策略**

​		让我们继续跟踪程序的执行顺序，接下来我们来分析 DefaultBeanDefinitionDocumentReader 对Bean 定义转换的 Document 对象解析的流程中，在其 parseDefaultElement()方法中完成对Document 对象的解析后得到封装 BeanDefinition的 BeanDefinitionHold对象，然后调用BeanDefinitionReaderUtils的 registerBeanDefinition()方法向IOC容器注册解析的 Bean，BeanDefinitionReaderUtils的注册的源码如下∶

BeanDefinitionReaderUtils

```java
//将解析的BeanDefinitionHold注册到容器中
public static void registerBeanDefinition(
    BeanDefinitionHolder definitionHolder, BeanDefinitionRegistry registry)
    throws BeanDefinitionStoreException {

    // Register bean definition under primary name.
    //获取解析的BeanDefinition的名称
    String beanName = definitionHolder.getBeanName();
    //向IOC容器注册BeanDefinition
    registry.registerBeanDefinition(beanName, definitionHolder.getBeanDefinition());

    // Register aliases for bean name, if any.
    //如果解析的BeanDefinition有别名，向容器为其注册别名
    String[] aliases = definitionHolder.getAliases();
    if (aliases != null) {
        for (String alias : aliases) {
            registry.registerAlias(beanName, alias);
        }
    }
}
```

​		当调用BeanDefinitionReaderUtils 向IOC容器注册解析的 BeanDefinition时，真正完成注册功能的是 DefaultListableBeanFactory。

**17、向容器注册**

​		DefaultListableBeanFactory中使用一个 HashMap的集合对象存放IOC容器中注册解析的BeanDefinition，

​		DefaultListableBeanFactory类图如下

<img src="images/Spring Framework容器-6.jpg" alt="Spring Framework容器-6" style="zoom:30%;" />

​		向IOC容器注册的主要源码如下∶

```java
//存储注册信息的BeanDefinition
private final Map<String, BeanDefinition> beanDefinitionMap = new ConcurrentHashMap<>(256);

//向IOC容器注册解析的BeanDefiniton
@Override
public void registerBeanDefinition(String beanName, BeanDefinition beanDefinition)
    throws BeanDefinitionStoreException {

    Assert.hasText(beanName, "Bean name must not be empty");
    Assert.notNull(beanDefinition, "BeanDefinition must not be null");

    //校验解析的BeanDefiniton
    if (beanDefinition instanceof AbstractBeanDefinition) {
        try {
            ((AbstractBeanDefinition) beanDefinition).validate();
        }
        catch (BeanDefinitionValidationException ex) {
            throw new BeanDefinitionStoreException(beanDefinition.getResourceDescription(), beanName,
                                                   "Validation of bean definition failed", ex);
        }
    }

    BeanDefinition oldBeanDefinition;

    oldBeanDefinition = this.beanDefinitionMap.get(beanName);

    if (oldBeanDefinition != null) {
        if (!isAllowBeanDefinitionOverriding()) {
            throw new BeanDefinitionStoreException(beanDefinition.getResourceDescription(), beanName,
                                                   "Cannot register bean definition [" + beanDefinition + "] for bean '" + beanName +
                                                   "': There is already [" + oldBeanDefinition + "] bound.");
        }
        else if (oldBeanDefinition.getRole() < beanDefinition.getRole()) {
            // e.g. was ROLE_APPLICATION, now overriding with ROLE_SUPPORT or ROLE_INFRASTRUCTURE
            if (this.logger.isWarnEnabled()) {
                this.logger.warn("Overriding user-defined bean definition for bean '" + beanName +
                                 "' with a framework-generated bean definition: replacing [" +
                                 oldBeanDefinition + "] with [" + beanDefinition + "]");
            }
        }
        else if (!beanDefinition.equals(oldBeanDefinition)) {
            if (this.logger.isInfoEnabled()) {
                this.logger.info("Overriding bean definition for bean '" + beanName +
                                 "' with a different definition: replacing [" + oldBeanDefinition +
                                 "] with [" + beanDefinition + "]");
            }
        }
        else {
            if (this.logger.isDebugEnabled()) {
                this.logger.debug("Overriding bean definition for bean '" + beanName +
                                  "' with an equivalent definition: replacing [" + oldBeanDefinition +
                                  "] with [" + beanDefinition + "]");
            }
        }
        this.beanDefinitionMap.put(beanName, beanDefinition);
    }
    else {
        if (hasBeanCreationStarted()) {
            // Cannot modify startup-time collection elements anymore (for stable iteration)
            //注册的过程中需要线程同步，以保证数据的一致性
            synchronized (this.beanDefinitionMap) {
                this.beanDefinitionMap.put(beanName, beanDefinition);
                List<String> updatedDefinitions = new ArrayList<>(this.beanDefinitionNames.size() + 1);
                updatedDefinitions.addAll(this.beanDefinitionNames);
                updatedDefinitions.add(beanName);
                this.beanDefinitionNames = updatedDefinitions;
                if (this.manualSingletonNames.contains(beanName)) {
                    Set<String> updatedSingletons = new LinkedHashSet<>(this.manualSingletonNames);
                    updatedSingletons.remove(beanName);
                    this.manualSingletonNames = updatedSingletons;
                }
            }
        }
        else {
            // Still in startup registration phase
            this.beanDefinitionMap.put(beanName, beanDefinition);
            this.beanDefinitionNames.add(beanName);
            this.manualSingletonNames.remove(beanName);
        }
        this.frozenBeanDefinitionNames = null;
    }

    //检查是否有同名的BeanDefinition已经在IOC容器中注册
    if (oldBeanDefinition != null || containsSingleton(beanName)) {
        //重置所有已经注册过的BeanDefinition的缓存
        resetBeanDefinition(beanName);
    }
}
```

​		至此，Bean配置信息中配置的 Bean被解析过后，已经注册到IOC容器中，被容器管理起来，真正完了IOC容器初始化所做的全部工作。现在IOC容器中已经建立了整个 Bean的配置信息，这些BeanDefinition信息已经可以使用，并且可以被检索，IOC容器的作用就是对这些注册的Bean定义信息进行处理和维护。这些的注册的Bean定义信息是IOC容器控制反转的基础，正是有了这些注册的数据，容器才可以进行依赖注入。

### 2.4.4Annotation的 IOC 初始化

#### 2.4.4.1Annotation的前世今生

从Spring2.0以后的版本中，Spring也引入了基于注解（Annotation）方式的配置，注解（Annotation）JDK1.5中引入的一个新特性，用于简化 Bean的配置，可以取代 XML 配置文件。开发人员对注解（Annotation）的态度也是萝卜青菜各有所爱，个人认为注解可以大大简化配置，提高开发速度，但也给后期维护增加了难度。目前来说XML方式发展的相对成熟，方便于统一管理。随着 Spring Boot的兴
起，基于注解的开发甚至实现了零配置。但作为个人的习惯而言，还是倾向于XML配置文件和注解（Annotation）相互配合使用。Spring IOC容器对于类级别的注解和类内部的注解分以下两种处理策略∶

1. 类级别的注解∶如@Component、@Repository、@Controller、@Service 以及JavaEE6的@ManagedBean和@Named注解，都是添加在类上面的类级别注解，Spring容器根据注解的过滤规则扫描读取注解 Bean定义类，并将其注册到Spring IOC容器中。
2. 类内部的注解∶如@Autowire、@Value、@Resource 以及EJB和WebService相关的注解等，都是添加在类内部的字段或者方法上的类内部注解，SpringlOC容器通过 Bean后置注解处理器解析Bean内部的注解。下面将根据这两种处理策略，分别分析 Spring 处理注解相关的源码。

#### 2.4.4.2定位 Bean 扫描路径

​		在 Spring中管理注解 Bean定义的容器有两个∶AnnotationConfigApplicationContext和AnnotationConfigWebApplicationContex。这两个类是专门处理 Spring 注解方式配置的容器，直接依赖于注解作为容器配置信息来源的 IOC 容器。AnnotationConfigWebApplicationContext是AnnotationConfigApplicationContext的Web版本，两者的用法以及对注解的处理方式几乎没有差别。现在我们以AnnotationConfigApplicationContext 为例看看它的源码∶

```java
public class AnnotationConfigApplicationContext extends GenericApplicationContext implements AnnotationConfigRegistry {

	//保存一个读取注解的Bean定义读取器，并将其设置到容器中
	private final AnnotatedBeanDefinitionReader reader;

	//保存一个扫描指定类路径中注解Bean定义的扫描器，并将其设置到容器中
	private final ClassPathBeanDefinitionScanner scanner;

	//默认构造函数，初始化一个空容器，容器不包含任何 Bean 信息，需要在稍后通过调用其register()
	//方法注册配置类，并调用refresh()方法刷新容器，触发容器对注解Bean的载入、解析和注册过程
	public AnnotationConfigApplicationContext() {
		this.reader = new AnnotatedBeanDefinitionReader(this);
		this.scanner = new ClassPathBeanDefinitionScanner(this);
	}

	public AnnotationConfigApplicationContext(DefaultListableBeanFactory beanFactory) {
		super(beanFactory);
		this.reader = new AnnotatedBeanDefinitionReader(this);
		this.scanner = new ClassPathBeanDefinitionScanner(this);
	}

	//最常用的构造函数，通过将涉及到的配置类传递给该构造函数，以实现将相应配置类中的Bean自动注册到容器中
	public AnnotationConfigApplicationContext(Class<?>... annotatedClasses) {
		this();
		register(annotatedClasses);
		refresh();
	}

	//该构造函数会自动扫描以给定的包及其子包下的所有类，并自动识别所有的Spring Bean，将其注册到容器中
	public AnnotationConfigApplicationContext(String... basePackages) {
		this();
		//我们自己模拟写过
		scan(basePackages);
		refresh();
	}

	@Override
	public void setEnvironment(ConfigurableEnvironment environment) {
		super.setEnvironment(environment);
		this.reader.setEnvironment(environment);
		this.scanner.setEnvironment(environment);
	}

	//为容器的注解Bean读取器和注解Bean扫描器设置Bean名称产生器
	public void setBeanNameGenerator(BeanNameGenerator beanNameGenerator) {
		this.reader.setBeanNameGenerator(beanNameGenerator);
		this.scanner.setBeanNameGenerator(beanNameGenerator);
		getBeanFactory().registerSingleton(
				AnnotationConfigUtils.CONFIGURATION_BEAN_NAME_GENERATOR, beanNameGenerator);
	}

	//为容器的注解Bean读取器和注解Bean扫描器设置作用范围元信息解析器
	public void setScopeMetadataResolver(ScopeMetadataResolver scopeMetadataResolver) {
		this.reader.setScopeMetadataResolver(scopeMetadataResolver);
		this.scanner.setScopeMetadataResolver(scopeMetadataResolver);
	}

	//为容器注册一个要被处理的注解Bean，新注册的Bean，必须手动调用容器的
	//refresh()方法刷新容器，触发容器对新注册的Bean的处理
	public void register(Class<?>... annotatedClasses) {
		Assert.notEmpty(annotatedClasses, "At least one annotated class must be specified");
		this.reader.register(annotatedClasses);
	}

	//扫描指定包路径及其子包下的注解类，为了使新添加的类被处理，必须手动调用
	//refresh()方法刷新容器
	public void scan(String... basePackages) {
		Assert.notEmpty(basePackages, "At least one base package must be specified");
		this.scanner.scan(basePackages);
	}

	public <T> void registerBean(Class<T> annotatedClass, Object... constructorArguments) {
		registerBean(null, annotatedClass, constructorArguments);
	}

	public <T> void registerBean(@Nullable String beanName, Class<T> annotatedClass, Object... constructorArguments) {
		this.reader.doRegisterBean(annotatedClass, null, beanName, null,
				bd -> {
					for (Object arg : constructorArguments) {
						bd.getConstructorArgumentValues().addGenericArgumentValue(arg);
					}
				});
	}

	@Override
	public <T> void registerBean(@Nullable String beanName, Class<T> beanClass, @Nullable Supplier<T> supplier,
			BeanDefinitionCustomizer... customizers) {

		this.reader.doRegisterBean(beanClass, supplier, beanName, null, customizers);
	}

}

```

​		通过上面的源码分析，我们可以看啊到Spring 对注解的处理分为两种方式∶

1. 直接将注解 Bean注册到容器中可以在初始化容器时注册;也可以在容器创建之后手动调用注册方法向容器注册，然后通过手动刷新容器，使得容器对注册的注解 Bean进行处理。
2. 通过扫描指定的包及其子包下的所有类

​		在初始化注解容器时指定要自动扫描的路径，如果容器创建以后向给定路径动态添加了注解 Bean，则需要手动调用容器扫描的方法，然后手动刷新容器，使得容器对所注册的Bean进行处理。接下来，将会对两种处理方式详细分析其实现过程。

#### 2.4.4.3读取 Annotation元数据

​		当创建注解处理容器时，如果传入的初始参数是具体的注解 Bean定义类时，注解容器读取并注册。

​		**1）、AnnotationConfigApplicationContext通过调用注解 Bean 定义读取器**

​		AnnotatedBeanDefinitionReader的 register（）方法向容器注册指定的注解 Bean注解 Bean定义读取器向容器注册注解 Bean 的源码如下∶

```java
//注册多个注解Bean定义类
public void register(Class<?>... annotatedClasses) {
    for (Class<?> annotatedClass : annotatedClasses) {
        registerBean(annotatedClass);
    }
}

//注册一个注解Bean定义类
public void registerBean(Class<?> annotatedClass) {
    doRegisterBean(annotatedClass, null, null, null);
}

public <T> void registerBean(Class<T> annotatedClass, @Nullable Supplier<T> instanceSupplier) {
    doRegisterBean(annotatedClass, instanceSupplier, null, null);
}

public <T> void registerBean(Class<T> annotatedClass, String name, @Nullable Supplier<T> instanceSupplier) {
    doRegisterBean(annotatedClass, instanceSupplier, name, null);
}

//Bean定义读取器注册注解Bean定义的入口方法
@SuppressWarnings("unchecked")
public void registerBean(Class<?> annotatedClass, Class<? extends Annotation>... qualifiers) {
    doRegisterBean(annotatedClass, null, null, qualifiers);
}

//Bean定义读取器向容器注册注解Bean定义类
@SuppressWarnings("unchecked")
public void registerBean(Class<?> annotatedClass, String name, Class<? extends Annotation>... qualifiers) {
    doRegisterBean(annotatedClass, null, name, qualifiers);
}

//Bean定义读取器向容器注册注解Bean定义类
<T> void doRegisterBean(Class<T> annotatedClass, @Nullable Supplier<T> instanceSupplier, @Nullable String name,
                        @Nullable Class<? extends Annotation>[] qualifiers, BeanDefinitionCustomizer... definitionCustomizers) {

    //根据指定的注解Bean定义类，创建Spring容器中对注解Bean的封装的数据结构
    AnnotatedGenericBeanDefinition abd = new AnnotatedGenericBeanDefinition(annotatedClass);
    if (this.conditionEvaluator.shouldSkip(abd.getMetadata())) {
        return;
    }

    abd.setInstanceSupplier(instanceSupplier);
    //========第一步===========================

    //解析注解Bean定义的作用域，若@Scope("prototype")，则Bean为原型类型；
    //若@Scope("singleton")，则Bean为单态类型
    ScopeMetadata scopeMetadata = this.scopeMetadataResolver.resolveScopeMetadata(abd);
    //为注解Bean定义设置作用域
    abd.setScope(scopeMetadata.getScopeName());
    //为注解Bean定义生成Bean名称
    String beanName = (name != null ? name : this.beanNameGenerator.generateBeanName(abd, this.registry));


    //========第二步===========================
    //处理注解Bean定义中的通用注解
    AnnotationConfigUtils.processCommonDefinitionAnnotations(abd);
    //如果在向容器注册注解Bean定义时，使用了额外的限定符注解，则解析限定符注解。
    //主要是配置的关于autowiring自动依赖注入装配的限定条件，即@Qualifier注解
    //Spring自动依赖注入装配默认是按类型装配，如果使用@Qualifier则按名称
    if (qualifiers != null) {
        for (Class<? extends Annotation> qualifier : qualifiers) {
            //如果配置了@Primary注解，设置该Bean为autowiring自动依赖注入装//配时的首选
            if (Primary.class == qualifier) {
                abd.setPrimary(true);
            }
            //如果配置了@Lazy注解，则设置该Bean为非延迟初始化，如果没有配置，
            //则该Bean为预实例化
            else if (Lazy.class == qualifier) {
                abd.setLazyInit(true);
            }
            //如果使用了除@Primary和@Lazy以外的其他注解，则为该Bean添加一
            //个autowiring自动依赖注入装配限定符，该Bean在进autowiring
            //自动依赖注入装配时，根据名称装配限定符指定的Bean
            else {
                abd.addQualifier(new AutowireCandidateQualifier(qualifier));
            }
        }
    }
    for (BeanDefinitionCustomizer customizer : definitionCustomizers) {
        customizer.customize(abd);
    }

    //创建一个指定Bean名称的Bean定义对象，封装注解Bean定义类数据
    BeanDefinitionHolder definitionHolder = new BeanDefinitionHolder(abd, beanName);
    //========第三步===========================
    //根据注解Bean定义类中配置的作用域，创建相应的代理对象
    definitionHolder = AnnotationConfigUtils.applyScopedProxyMode(scopeMetadata, definitionHolder, this.registry);
    //========第四步===========================
    //向IOC容器注册注解Bean类定义对象
    BeanDefinitionReaderUtils.registerBeanDefinition(definitionHolder, this.registry);
}

```

​		从上面的源码我们可以看出，注册注解 Bean定义类的基本步骤∶

1. 需要使用注解元数据解析器解析注解 Bean中关于作用域的配置。
2. 使用AnnotationConfigUtils的processCommonDefinitionAnnotations（）方法处理注解 Bean定义类中通用的注解。
3. 使用AnnotationConfigUtils的 applyScopedProxyMode（）方法创建对于作用域的代理对象。
4. 通过 BeanDefinitionReaderUtils 向容器注册 Bean。下面我们继续分析这 4步的具体实现过程

​		**2）、AnnotationScopeMetadataResolver解析作用域元数据**

​		AnnotationScopeMetadataResolver通过 resolveScopeMetadata()方法解析注解 Bean定义类的作用域元信息，即判断注册的 Bean是原生类型（prototype）还是单态（singleton）类型，其源码如下∶

```java
//解析注解Bean定义类中的作用域元信息
@Override
public ScopeMetadata resolveScopeMetadata(BeanDefinition definition) {
    ScopeMetadata metadata = new ScopeMetadata();
    if (definition instanceof AnnotatedBeanDefinition) {
        AnnotatedBeanDefinition annDef = (AnnotatedBeanDefinition) definition;
        //从注解Bean定义类的属性中查找属性为”Scope”的值，即@Scope注解的值
        //AnnotationConfigUtils.attributesFor()方法将Bean
        //中所有的注解和注解的值存放在一个map集合中
        AnnotationAttributes attributes = AnnotationConfigUtils.attributesFor(
            annDef.getMetadata(), this.scopeAnnotationType);
        //将获取到的@Scope注解的值设置到要返回的对象中
        if (attributes != null) {
            metadata.setScopeName(attributes.getString("value"));
            //获取@Scope注解中的proxyMode属性值，在创建代理对象时会用到
            ScopedProxyMode proxyMode = attributes.getEnum("proxyMode");
            //如果@Scope的proxyMode属性为DEFAULT或者NO
            if (proxyMode == ScopedProxyMode.DEFAULT) {
                //设置proxyMode为NO
                proxyMode = this.defaultProxyMode;
            }
            //为返回的元数据设置proxyMode
            metadata.setScopedProxyMode(proxyMode);
        }
    }
    //返回解析的作用域元信息对象
    return metadata;
}
```

​		上述代码中的 annDef.getMetadata().getAnnotationAttibutes()方法就是获取对象中指定类型的注解的值。

​		**3）、AnnotationConfigUtils 处理注解 Bean 定义类中的通用注解**

​		AnnotationConfigUtils类的processCommonDefinitionAnnotations()在向容器注册 Bean之前，首先对注解 Bean定义类中的通用 Spring 注解进行处理，源码如下∶

```java
public static void processCommonDefinitionAnnotations(AnnotatedBeanDefinition abd) {
    processCommonDefinitionAnnotations(abd, abd.getMetadata());
}

//处理Bean定义中通用注解
static void processCommonDefinitionAnnotations(AnnotatedBeanDefinition abd, AnnotatedTypeMetadata metadata) {
    AnnotationAttributes lazy = attributesFor(metadata, Lazy.class);
    //如果Bean定义中有@Lazy注解，则将该Bean预实例化属性设置为@lazy注解的值
    if (lazy != null) {
        abd.setLazyInit(lazy.getBoolean("value"));
    }

    else if (abd.getMetadata() != metadata) {
        lazy = attributesFor(abd.getMetadata(), Lazy.class);
        if (lazy != null) {
            abd.setLazyInit(lazy.getBoolean("value"));
        }
    }
    //如果Bean定义中有@Primary注解，则为该Bean设置为autowiring自动依赖注入装配的首选对象
    if (metadata.isAnnotated(Primary.class.getName())) {
        abd.setPrimary(true);
    }
    //如果Bean定义中有@ DependsOn注解，则为该Bean设置所依赖的Bean名称，
    //容器将确保在实例化该Bean之前首先实例化所依赖的Bean
    AnnotationAttributes dependsOn = attributesFor(metadata, DependsOn.class);
    if (dependsOn != null) {
        abd.setDependsOn(dependsOn.getStringArray("value"));
    }

    if (abd instanceof AbstractBeanDefinition) {
        AbstractBeanDefinition absBd = (AbstractBeanDefinition) abd;
        AnnotationAttributes role = attributesFor(metadata, Role.class);
        if (role != null) {
            absBd.setRole(role.getNumber("value").intValue());
        }
        AnnotationAttributes description = attributesFor(metadata, Description.class);
        if (description != null) {
            absBd.setDescription(description.getString("value"));
        }
    }
}
```

​		**4）、AnnotationConfigUtils根据注解 Bean 定义类中配置的作用域为其应用相应的代理策略**

​		AnnotationConfigUtils类的 applyScopedProxyMode()方法根据注解 Bean 定义类中配置的作用域@Scope 注解的值，为 Bean 定义应用相应的代理模式，主要是在 Spring面向切面编程（AOP）中使用。源码如下∶

```java
//根据作用域为Bean应用引用的代码模式
static BeanDefinitionHolder applyScopedProxyMode(
    ScopeMetadata metadata, BeanDefinitionHolder definition, BeanDefinitionRegistry registry) {

    //获取注解Bean定义类中@Scope注解的proxyMode属性值
    ScopedProxyMode scopedProxyMode = metadata.getScopedProxyMode();
    //如果配置的@Scope注解的proxyMode属性值为NO，则不应用代理模式
    if (scopedProxyMode.equals(ScopedProxyMode.NO)) {
        return definition;
    }
    //获取配置的@Scope注解的proxyMode属性值，如果为TARGET_CLASS
    //则返回true，如果为INTERFACES，则返回false
    boolean proxyTargetClass = scopedProxyMode.equals(ScopedProxyMode.TARGET_CLASS);
    //为注册的Bean创建相应模式的代理对象
    return ScopedProxyCreator.createScopedProxy(definition, registry, proxyTargetClass);
}
```

​		这段为 Bean 引用创建相应模式的代理，这里不做深入的分析。

​		5）、BeanDefinitionReaderUtils 向容器注册 Bean

​		BeanDefinitionReaderUtils主要是校验 BeanDefinition信息，然后将 Bean添加到容器中一个管理BeanDefinition的HashMap中。

#### 2.4.4.4扫描指定包并解析为 BeanDefinition

​		当创建注解处理容器时，如果传入的初始参数是注解 Bean定义类所在的包时，注解容器将扫描给定的包及其子包，将扫描到的注解 Bean 定义载入并注册。

​		**1）、ClassPathBeanDefinitionScanner 扫描给定的包及其子包**

​		AnnotationConfigApplicationContext 通 过 调 用 类 路 径 Bean 定 义 扫 描 器ClassPathBeanDefinitionScanner扫描给定包及其子包下的所有类，主要源码如下∶

```java
public class ClassPathBeanDefinitionScanner extends ClassPathScanningCandidateComponentProvider {

·····
    
	//创建一个类路径Bean定义扫描器
	public ClassPathBeanDefinitionScanner(BeanDefinitionRegistry registry) {
		this(registry, true);
	}

	//为容器创建一个类路径Bean定义扫描器，并指定是否使用默认的扫描过滤规则。
	//即Spring默认扫描配置：@Component、@Repository、@Service、@Controller
	//注解的Bean，同时也支持JavaEE6的@ManagedBean和JSR-330的@Named注解
	public ClassPathBeanDefinitionScanner(BeanDefinitionRegistry registry, boolean useDefaultFilters) {
		this(registry, useDefaultFilters, getOrCreateEnvironment(registry));
	}

	public ClassPathBeanDefinitionScanner(BeanDefinitionRegistry registry, boolean useDefaultFilters,
			Environment environment) {

		this(registry, useDefaultFilters, environment,
				(registry instanceof ResourceLoader ? (ResourceLoader) registry : null));
	}

	public ClassPathBeanDefinitionScanner(BeanDefinitionRegistry registry, boolean useDefaultFilters,
			Environment environment, @Nullable ResourceLoader resourceLoader) {

		Assert.notNull(registry, "BeanDefinitionRegistry must not be null");
		//为容器设置加载Bean定义的注册器
		this.registry = registry;

		if (useDefaultFilters) {
			registerDefaultFilters();
		}
		setEnvironment(environment);
		//为容器设置资源加载器
		setResourceLoader(resourceLoader);
	}
····
	//调用类路径Bean定义扫描器入口方法
	public int scan(String... basePackages) {
		//获取容器中已经注册的Bean个数
		int beanCountAtScanStart = this.registry.getBeanDefinitionCount();

		//启动扫描器扫描给定包
		doScan(basePackages);

		// Register annotation config processors, if necessary.
		//注册注解配置(Annotation config)处理器
		if (this.includeAnnotationConfig) {
			AnnotationConfigUtils.registerAnnotationConfigProcessors(this.registry);
		}

		//返回注册的Bean个数
		return (this.registry.getBeanDefinitionCount() - beanCountAtScanStart);
	}

	//类路径Bean定义扫描器扫描给定包及其子包
	protected Set<BeanDefinitionHolder> doScan(String... basePackages) {
		Assert.notEmpty(basePackages, "At least one base package must be specified");
		//创建一个集合，存放扫描到Bean定义的封装类
		Set<BeanDefinitionHolder> beanDefinitions = new LinkedHashSet<>();
		//遍历扫描所有给定的包
		for (String basePackage : basePackages) {
			//调用父类ClassPathScanningCandidateComponentProvider的方法
			//扫描给定类路径，获取符合条件的Bean定义
			Set<BeanDefinition> candidates = findCandidateComponents(basePackage);
			//遍历扫描到的Bean
			for (BeanDefinition candidate : candidates) {
				//获取Bean定义类中@Scope注解的值，即获取Bean的作用域
				ScopeMetadata scopeMetadata = this.scopeMetadataResolver.resolveScopeMetadata(candidate);
				//为Bean设置注解配置的作用域
				candidate.setScope(scopeMetadata.getScopeName());
				//为Bean生成名称
				String beanName = this.beanNameGenerator.generateBeanName(candidate, this.registry);
				//如果扫描到的Bean不是Spring的注解Bean，则为Bean设置默认值，
				//设置Bean的自动依赖注入装配属性等
				if (candidate instanceof AbstractBeanDefinition) {
					postProcessBeanDefinition((AbstractBeanDefinition) candidate, beanName);
				}
				//如果扫描到的Bean是Spring的注解Bean，则处理其通用的Spring注解
				if (candidate instanceof AnnotatedBeanDefinition) {
					//处理注解Bean中通用的注解，在分析注解Bean定义类读取器时已经分析过
					AnnotationConfigUtils.processCommonDefinitionAnnotations((AnnotatedBeanDefinition) candidate);
				}
				//根据Bean名称检查指定的Bean是否需要在容器中注册，或者在容器中冲突
				if (checkCandidate(beanName, candidate)) {
					BeanDefinitionHolder definitionHolder = new BeanDefinitionHolder(candidate, beanName);
					//根据注解中配置的作用域，为Bean应用相应的代理模式
					definitionHolder =
							AnnotationConfigUtils.applyScopedProxyMode(scopeMetadata, definitionHolder, this.registry);
					beanDefinitions.add(definitionHolder);
					//向容器注册扫描到的Bean
					registerBeanDefinition(definitionHolder, this.registry);
				}
			}
		}
		return beanDefinitions;
	}
····
}

```

​		类路径Bean定义扫描器 ClassPathBeanDefinitionScanner主要通过 findCandidateComponents（）方法调用其父类ClassPathScanningCandidateComponentProvider类来扫描获取给定包及其子包下的类。

​		**2）、ClassPathScanningCandidateComponentProvider 扫描给定包及其子包的类**

​		ClassPathScanningCandidateComponentProvider类的 findCandidateComponents()方法具体实现扫描给定类路径包的功能，主要源码如下∶

```java
public class ClassPathScanningCandidateComponentProvider implements EnvironmentCapable, ResourceLoaderAware {
····
	//保存过滤规则要包含的注解，即Spring默认的@Component、@Repository、@Service、
	//@Controller注解的Bean，以及JavaEE6的@ManagedBean和JSR-330的@Named注解
	private final List<TypeFilter> includeFilters = new LinkedList<>();

	//保存过滤规则要排除的注解
	private final List<TypeFilter> excludeFilters = new LinkedList<>();
····
	//构造方法，该方法在子类ClassPathBeanDefinitionScanner的构造方法中被调用
	public ClassPathScanningCandidateComponentProvider(boolean useDefaultFilters) {
		this(useDefaultFilters, new StandardEnvironment());
	}

	public ClassPathScanningCandidateComponentProvider(boolean useDefaultFilters, Environment environment) {
		//如果使用Spring默认的过滤规则，则向容器注册过滤规则
		if (useDefaultFilters) {
			registerDefaultFilters();
		}
		setEnvironment(environment);
		setResourceLoader(null);
	}
····
	//向容器注册过滤规则
	@SuppressWarnings("unchecked")
	protected void registerDefaultFilters() {
		//向要包含的过滤规则中添加@Component注解类，注意Spring中@Repository
		//@Service和@Controller都是Component，因为这些注解都添加了@Component注解
		this.includeFilters.add(new AnnotationTypeFilter(Component.class));
		//获取当前类的类加载器
		ClassLoader cl = ClassPathScanningCandidateComponentProvider.class.getClassLoader();
		try {
			//向要包含的过滤规则添加JavaEE6的@ManagedBean注解
			this.includeFilters.add(new AnnotationTypeFilter(
					((Class<? extends Annotation>) ClassUtils.forName("javax.annotation.ManagedBean", cl)), false));
			logger.debug("JSR-250 'javax.annotation.ManagedBean' found and supported for component scanning");
		}
		catch (ClassNotFoundException ex) {
			// JSR-250 1.1 API (as included in Java EE 6) not available - simply skip.
		}
		try {
			//向要包含的过滤规则添加@Named注解
			this.includeFilters.add(new AnnotationTypeFilter(
					((Class<? extends Annotation>) ClassUtils.forName("javax.inject.Named", cl)), false));
			logger.debug("JSR-330 'javax.inject.Named' annotation found and supported for component scanning");
		}
		catch (ClassNotFoundException ex) {
			// JSR-330 API not available - simply skip.
		}
	}

····
	//扫描给定类路径的包
	public Set<BeanDefinition> findCandidateComponents(String basePackage) {
		if (this.componentsIndex != null && indexSupportsIncludeFilters()) {
			return addCandidateComponentsFromIndex(this.componentsIndex, basePackage);
		}
		else {
			return scanCandidateComponents(basePackage);
		}
	}
····
	private Set<BeanDefinition> addCandidateComponentsFromIndex(CandidateComponentsIndex index, String basePackage) {
		//创建存储扫描到的类的集合
		Set<BeanDefinition> candidates = new LinkedHashSet<>();
		try {
			Set<String> types = new HashSet<>();
			for (TypeFilter filter : this.includeFilters) {
				String stereotype = extractStereotype(filter);
				if (stereotype == null) {
					throw new IllegalArgumentException("Failed to extract stereotype from "+ filter);
				}
				types.addAll(index.getCandidateTypes(basePackage, stereotype));
			}
			boolean traceEnabled = logger.isTraceEnabled();
			boolean debugEnabled = logger.isDebugEnabled();
			for (String type : types) {
				//为指定资源获取元数据读取器，元信息读取器通过汇编(ASM)读//取资源元信息
				MetadataReader metadataReader = getMetadataReaderFactory().getMetadataReader(type);
				//如果扫描到的类符合容器配置的过滤规则
				if (isCandidateComponent(metadataReader)) {
					//通过汇编(ASM)读取资源字节码中的Bean定义元信息
					AnnotatedGenericBeanDefinition sbd = new AnnotatedGenericBeanDefinition(
							metadataReader.getAnnotationMetadata());
					if (isCandidateComponent(sbd)) {
						if (debugEnabled) {
							logger.debug("Using candidate component class from index: " + type);
						}
						candidates.add(sbd);
					}
					else {
						if (debugEnabled) {
							logger.debug("Ignored because not a concrete top-level class: " + type);
						}
					}
				}
				else {
					if (traceEnabled) {
						logger.trace("Ignored because matching an exclude filter: " + type);
					}
				}
			}
		}
		catch (IOException ex) {
			throw new BeanDefinitionStoreException("I/O failure during classpath scanning", ex);
		}
		return candidates;
	}
····
	//判断元信息读取器读取的类是否符合容器定义的注解过滤规则
	protected boolean isCandidateComponent(MetadataReader metadataReader) throws IOException {
		//如果读取的类的注解在排除注解过滤规则中，返回false
		for (TypeFilter tf : this.excludeFilters) {
			if (tf.match(metadataReader, getMetadataReaderFactory())) {
				return false;
			}
		}
		//如果读取的类的注解在包含的注解的过滤规则中，则返回ture
		for (TypeFilter tf : this.includeFilters) {
			if (tf.match(metadataReader, getMetadataReaderFactory())) {
				return isConditionMatch(metadataReader);
			}
		}
		//如果读取的类的注解既不在排除规则，也不在包含规则中，则返回false
		return false;
	}
····
}

```

#### 2.4.4.5注册注解 BeanDefinition

​		AnnotationConfigWebApplicationContext是 AnnotationConfigApplicationContext 的Web版，它们对于注解 Bean的注册和扫描是基本相同的，但是AnnotationConfigWebApplicationContext 对注解 Bean定义的载入稍有不同，AnnotationConfigWebApplicationContext注入注解Bean定义源码如下∶

```java

//载入注解Bean定义资源
@Override
protected void loadBeanDefinitions(DefaultListableBeanFactory beanFactory) {
    //为容器设置注解Bean定义读取器
    AnnotatedBeanDefinitionReader reader = getAnnotatedBeanDefinitionReader(beanFactory);
    //为容器设置类路径Bean定义扫描器
    ClassPathBeanDefinitionScanner scanner = getClassPathBeanDefinitionScanner(beanFactory);

    //获取容器的Bean名称生成器
    BeanNameGenerator beanNameGenerator = getBeanNameGenerator();
    //为注解Bean定义读取器和类路径扫描器设置Bean名称生成器
    if (beanNameGenerator != null) {
        reader.setBeanNameGenerator(beanNameGenerator);
        scanner.setBeanNameGenerator(beanNameGenerator);
        beanFactory.registerSingleton(AnnotationConfigUtils.CONFIGURATION_BEAN_NAME_GENERATOR, beanNameGenerator);
    }

    //获取容器的作用域元信息解析器
    ScopeMetadataResolver scopeMetadataResolver = getScopeMetadataResolver();
    //为注解Bean定义读取器和类路径扫描器设置作用域元信息解析器
    if (scopeMetadataResolver != null) {
        reader.setScopeMetadataResolver(scopeMetadataResolver);
        scanner.setScopeMetadataResolver(scopeMetadataResolver);
    }

    if (!this.annotatedClasses.isEmpty()) {
        if (logger.isInfoEnabled()) {
            logger.info("Registering annotated classes: [" +
                        StringUtils.collectionToCommaDelimitedString(this.annotatedClasses) + "]");
        }
        reader.register(this.annotatedClasses.toArray(new Class<?>[this.annotatedClasses.size()]));
    }

    if (!this.basePackages.isEmpty()) {
        if (logger.isInfoEnabled()) {
            logger.info("Scanning base packages: [" +
                        StringUtils.collectionToCommaDelimitedString(this.basePackages) + "]");
        }
        scanner.scan(this.basePackages.toArray(new String[this.basePackages.size()]));
    }

    //获取容器定义的Bean定义资源路径
    String[] configLocations = getConfigLocations();
    //如果定位的Bean定义资源路径不为空
    if (configLocations != null) {
        for (String configLocation : configLocations) {
            try {
                //使用当前容器的类加载器加载定位路径的字节码类文件
                Class<?> clazz = ClassUtils.forName(configLocation, getClassLoader());
                if (logger.isInfoEnabled()) {
                    logger.info("Successfully resolved class for [" + configLocation + "]");
                }
                reader.register(clazz);
            }
            catch (ClassNotFoundException ex) {
                if (logger.isDebugEnabled()) {
                    logger.debug("Could not load class for config location [" + configLocation +
                                 "] - trying package scan. " + ex);
                }
                //如果容器类加载器加载定义路径的Bean定义资源失败
                //则启用容器类路径扫描器扫描给定路径包及其子包中的类
                int count = scanner.scan(configLocation);
                if (logger.isInfoEnabled()) {
                    if (count == 0) {
                        logger.info("No annotated classes found for specified class/package [" + configLocation + "]");
                    }
                    else {
                        logger.info("Found " + count + " annotated classes in package [" + configLocation + "]");
                    }
                }
            }
        }
    }
}
```

​		以上就是解析和注入注解配置资源的全过程分析。

### 2.4.5IOC容器初始化小结

​		现在通过上面的代码，总结一下 IOC 容器初始化的基本步骤∶

1. 初始化的入口在容器实现中的 refresh）调用来完成。
2. 对 Bean定义载入 IOC容器使用的方法是loadBeanDefinition()

​		其中的大致过程如下∶通过ResourceLoader来完成资源文件位置的定位，DefaultResourceLoader 是默认的实现，同时上下文本身就给出了ResourcelLoader的实现，可以从类路径，文件系统URL等方式来定为资源位置。如果是XmlBeanfFactory 作为 IOC容器，那么需要为它指定 Bean定义的资源，也就是说 Bean 定 义文 件时通过抽象成 Resource 来被 IOC 容器处理的，容器通过
BeanDefinitionReader 来 完 成 定 义信 息的 解析 和 Bean 信 息的注册，往往使用的是XmlBeanDefinitionReader来解析 Bean的 XML 定义文件-实际的处理过程是委托给BeanDefinitionParserDelegate来完成的，从而得到bean的定义信息，这些信息在Spring中使用BeanDefinition对象来表示-这个名字可以让我们想到loadBeanDefinitionO，registerBeanDefinition 这些相关方法。它们都是为处理 BeanDefinitin服务的，容器解析得到 BeanDefinition 以后，需要把它在IOC容器中注册，这由IOC 实现 BeanDefinitionRegistry接口来实现。注册过程就是在IOC容器内部维护的一个HashMap来保存得到的BeanDefinition的过程。这个HashMap是IOC容器持有Bean信息的场所，以后对 Bean的操作都是围绕这个 HashMap 来实现的。

​		然后我们就可以通过BeanFactory和ApplicationContext来享受到Spring IOC的服务了，在使用IOC 容器的时候，我们注意到除了少量粘合代码，绝大多数以正确IOC风格编写的应用程序代码完全不用关心如何到达工厂，因为容器将把这些对象与容器管理的其他对象钩在一起。基本的策略是把工厂放到已知的地方最好是放在对预期使用的上下文有意义的地方以及代码将实际需要访问工厂的地方。Spring 本身提供了对声明式载入web应用程序用法的应用程序上下文并将其存储在ServletContext中的框架实现。

以下是容器初始化全过程的时序图∶

![Spring Framework容器-7](images/Spring Framework容器-7.jpg)

## 2.5依赖注入

依赖注入发生的时间

​		当Spring loC容器完成了Bean定义资源的定位、载入和解析注册以后loC容器中已经管理类Bean 定义的相关数据，但是此时 loC容器还没有对所管理的 Bean进行依赖注入，依赖注入在以下两种情况发生∶

1. 用户第一次调用 getBean()方法时，loC容器触发依赖注入。
2. 当用户在配置文件中将`<bean>`元素配置了lazy-init=false 属性，即让容器在解析注册 Bean定义时进行预实例化，触发依赖注入。

​		BeanFactory接口定义了Spring loC容器的基本功能规范，是Spring loC容器所应遵守的最底层和最基本的编程规范。BeanFactory接口中定义了几个getBean()方法，就是用户向loC容器索取管理的Bean的方法，我们通过分析其子类的具体实现，理解Spring loC容器在用户索取 Bean时如何完成依赖注入。

BeanFactory类关系图如下：

<img src="images/Spring Framework注入-1.jpg" alt="Spring Framework注入-1" style="zoom:35%;" />

​		在BeanFactory中我们可以看到getBean（String…)方法，但它具体实现在AbstractBeanFactory中。

### 2.5.1寻找获取 Bean的入口

AbstractBeanFactory的 getBean（）相关方法的源码如下∶

```java
//获取IOC容器中指定名称的Bean
@Override
public Object getBean(String name) throws BeansException {
    //doGetBean才是真正向IoC容器获取被管理Bean的过程
    return doGetBean(name, null, null, false);
}

//获取IOC容器中指定名称和类型的Bean
@Override
public <T> T getBean(String name, @Nullable Class<T> requiredType) throws BeansException {
    //doGetBean才是真正向IoC容器获取被管理Bean的过程
    return doGetBean(name, requiredType, null, false);
}

//获取IOC容器中指定名称和参数的Bean
@Override
public Object getBean(String name, Object... args) throws BeansException {
    //doGetBean才是真正向IoC容器获取被管理Bean的过程
    return doGetBean(name, null, args, false);
}

//获取IOC容器中指定名称、类型和参数的Bean
public <T> T getBean(String name, @Nullable Class<T> requiredType, @Nullable Object... args)
    throws BeansException {
    //doGetBean才是真正向IoC容器获取被管理Bean的过程
    return doGetBean(name, requiredType, args, false);
}

@SuppressWarnings("unchecked")
//真正实现向IOC容器获取Bean的功能，也是触发依赖注入功能的地方
protected <T> T doGetBean(final String name, @Nullable final Class<T> requiredType,
                          @Nullable final Object[] args, boolean typeCheckOnly) throws BeansException {

    //根据指定的名称获取被管理Bean的名称，剥离指定名称中对容器的相关依赖
    //如果指定的是别名，将别名转换为规范的Bean名称
    final String beanName = transformedBeanName(name);
    Object bean;

    // Eagerly check singleton cache for manually registered singletons.
    //先从缓存中取是否已经有被创建过的单态类型的Bean
    //对于单例模式的Bean整个IOC容器中只创建一次，不需要重复创建
    Object sharedInstance = getSingleton(beanName);
    //IOC容器创建单例模式Bean实例对象
    if (sharedInstance != null && args == null) {
        if (logger.isDebugEnabled()) {
            //如果指定名称的Bean在容器中已有单例模式的Bean被创建
            //直接返回已经创建的Bean
            if (isSingletonCurrentlyInCreation(beanName)) {
                logger.debug("Returning eagerly cached instance of singleton bean '" + beanName +
                             "' that is not fully initialized yet - a consequence of a circular reference");
            }
            else {
                logger.debug("Returning cached instance of singleton bean '" + beanName + "'");
            }
        }
        //获取给定Bean的实例对象，主要是完成FactoryBean的相关处理
        //注意：BeanFactory是管理容器中Bean的工厂，而FactoryBean是
        //创建创建对象的工厂Bean，两者之间有区别
        bean = getObjectForBeanInstance(sharedInstance, name, beanName, null);
    }

    else {
        // Fail if we're already creating this bean instance:
        // We're assumably within a circular reference.
        //缓存没有正在创建的单例模式Bean
        //缓存中已经有已经创建的原型模式Bean
        //但是由于循环引用的问题导致实例化对象失败
        if (isPrototypeCurrentlyInCreation(beanName)) {
            throw new BeanCurrentlyInCreationException(beanName);
        }

        // Check if bean definition exists in this factory.
        //对IOC容器中是否存在指定名称的BeanDefinition进行检查，首先检查是否
        //能在当前的BeanFactory中获取的所需要的Bean，如果不能则委托当前容器
        //的父级容器去查找，如果还是找不到则沿着容器的继承体系向父级容器查找
        BeanFactory parentBeanFactory = getParentBeanFactory();
        //当前容器的父级容器存在，且当前容器中不存在指定名称的Bean
        if (parentBeanFactory != null && !containsBeanDefinition(beanName)) {
            // Not found -> check parent.
            //解析指定Bean名称的原始名称
            String nameToLookup = originalBeanName(name);
            if (parentBeanFactory instanceof AbstractBeanFactory) {
                return ((AbstractBeanFactory) parentBeanFactory).doGetBean(
                    nameToLookup, requiredType, args, typeCheckOnly);
            }
            else if (args != null) {
                // Delegation to parent with explicit args.
                //委派父级容器根据指定名称和显式的参数查找
                return (T) parentBeanFactory.getBean(nameToLookup, args);
            }
            else {
                // No args -> delegate to standard getBean method.
                //委派父级容器根据指定名称和类型查找
                return parentBeanFactory.getBean(nameToLookup, requiredType);
            }
        }

        //创建的Bean是否需要进行类型验证，一般不需要
        if (!typeCheckOnly) {
            //向容器标记指定的Bean已经被创建
            markBeanAsCreated(beanName);
        }

        try {
            //根据指定Bean名称获取其父级的Bean定义
            //主要解决Bean继承时子类合并父类公共属性问题
            final RootBeanDefinition mbd = getMergedLocalBeanDefinition(beanName);
            checkMergedBeanDefinition(mbd, beanName, args);

            // Guarantee initialization of beans that the current bean depends on.
            //获取当前Bean所有依赖Bean的名称
            String[] dependsOn = mbd.getDependsOn();
            //如果当前Bean有依赖Bean
            if (dependsOn != null) {
                for (String dep : dependsOn) {
                    if (isDependent(beanName, dep)) {
                        throw new BeanCreationException(mbd.getResourceDescription(), beanName,
                                                        "Circular depends-on relationship between '" + beanName + "' and '" + dep + "'");
                    }
                    //递归调用getBean方法，获取当前Bean的依赖Bean
                    registerDependentBean(dep, beanName);
                    //把被依赖Bean注册给当前依赖的Bean
                    getBean(dep);
                }
            }

            // Create bean instance.
            //创建单例模式Bean的实例对象
            if (mbd.isSingleton()) {
                //这里使用了一个匿名内部类，创建Bean实例对象，并且注册给所依赖的对象
                sharedInstance = getSingleton(beanName, () -> {
                    try {
                        //创建一个指定Bean实例对象，如果有父级继承，则合并子类和父类的定义
                        return createBean(beanName, mbd, args);
                    }
                    catch (BeansException ex) {
                        // Explicitly remove instance from singleton cache: It might have been put there
                        // eagerly by the creation process, to allow for circular reference resolution.
                        // Also remove any beans that received a temporary reference to the bean.
                        //显式地从容器单例模式Bean缓存中清除实例对象
                        destroySingleton(beanName);
                        throw ex;
                    }
                });
                //获取给定Bean的实例对象
                bean = getObjectForBeanInstance(sharedInstance, name, beanName, mbd);
            }

            //IOC容器创建原型模式Bean实例对象
            else if (mbd.isPrototype()) {
                // It's a prototype -> create a new instance.
                //原型模式(Prototype)是每次都会创建一个新的对象
                Object prototypeInstance = null;
                try {
                    //回调beforePrototypeCreation方法，默认的功能是注册当前创建的原型对象
                    beforePrototypeCreation(beanName);
                    //创建指定Bean对象实例
                    prototypeInstance = createBean(beanName, mbd, args);
                }
                finally {
                    //回调afterPrototypeCreation方法，默认的功能告诉IOC容器指定Bean的原型对象不再创建
                    afterPrototypeCreation(beanName);
                }
                //获取给定Bean的实例对象
                bean = getObjectForBeanInstance(prototypeInstance, name, beanName, mbd);
            }

            //要创建的Bean既不是单例模式，也不是原型模式，则根据Bean定义资源中
            //配置的生命周期范围，选择实例化Bean的合适方法，这种在Web应用程序中
            //比较常用，如：request、session、application等生命周期
            else {
                String scopeName = mbd.getScope();
                final Scope scope = this.scopes.get(scopeName);
                //Bean定义资源中没有配置生命周期范围，则Bean定义不合法
                if (scope == null) {
                    throw new IllegalStateException("No Scope registered for scope name '" + scopeName + "'");
                }
                try {
                    //这里又使用了一个匿名内部类，获取一个指定生命周期范围的实例
                    Object scopedInstance = scope.get(beanName, () -> {
                        beforePrototypeCreation(beanName);
                        try {
                            return createBean(beanName, mbd, args);
                        }
                        finally {
                            afterPrototypeCreation(beanName);
                        }
                    });
                    //获取给定Bean的实例对象
                    bean = getObjectForBeanInstance(scopedInstance, name, beanName, mbd);
                }
                catch (IllegalStateException ex) {
                    throw new BeanCreationException(beanName,
                                                    "Scope '" + scopeName + "' is not active for the current thread; consider " +
                                                    "defining a scoped proxy for this bean if you intend to refer to it from a singleton",
                                                    ex);
                }
            }
        }
        catch (BeansException ex) {
            cleanupAfterBeanCreationFailure(beanName);
            throw ex;
        }
    }

    // Check if required type matches the type of the actual bean instance.
    //对创建的Bean实例对象进行类型检查
    if (requiredType != null && !requiredType.isInstance(bean)) {
        try {
            T convertedBean = getTypeConverter().convertIfNecessary(bean, requiredType);
            if (convertedBean == null) {
                throw new BeanNotOfRequiredTypeException(name, requiredType, bean.getClass());
            }
            return convertedBean;
        }
        catch (TypeMismatchException ex) {
            if (logger.isDebugEnabled()) {
                logger.debug("Failed to convert bean '" + name + "' to required type '" +
                             ClassUtils.getQualifiedName(requiredType) + "'", ex);
            }
            throw new BeanNotOfRequiredTypeException(name, requiredType, bean.getClass());
        }
    }
    return (T) bean;
}
```

> 这里有个比较熟悉的身影双重检查锁，单例模式创建必备
>
> ```java
> @Nullable
> protected Object getSingleton(String beanName, boolean allowEarlyReference) {
>     Object singletonObject = this.singletonObjects.get(beanName);
>     if (singletonObject == null && isSingletonCurrentlyInCreation(beanName)) {
>         synchronized (this.singletonObjects) {
>             singletonObject = this.earlySingletonObjects.get(beanName);
>             if (singletonObject == null && allowEarlyReference) {
>                 ObjectFactory<?> singletonFactory = this.singletonFactories.get(beanName);
>                 if (singletonFactory != null) {
>                     singletonObject = singletonFactory.getObject();
>                     this.earlySingletonObjects.put(beanName, singletonObject);
>                     this.singletonFactories.remove(beanName);
>                 }
>             }
>         }
>     }
>     return singletonObject;
> }
> ```

​		通过上面对向 loC容器获取 Bean方法的分析，我们可以看到在 Spring中，如果 Bean定义的单例模式(Singleton），则容器在创建之前先从缓存中查找，以确保整个容器中只存在一个实例对象。如果Bean定义的是原型模式（Prototype），则容器每次都会创建一个新的实例对象。除此之外，Bean定义还可以扩展为指定其生命周期范围。

​		上面的源码只是定义了根据Bean定义的模式 采取的不同创建 Bean实例对象的策略 具体的Bean 实例对象的创建过程由实现了ObjectFactory接口的匿名内部类的 createBean()方法完成，ObjectFactory 使 用 委 派 模 式，具 体 的 Bean 实 例 创建 过 程 交 由 其 实 现 类AbstractAutowireCapableBeanFactory完成 我们继续分析 AbstractAutowireCapableBeanFactory 的createBean（）方法的源码，理解其创建 Bean 实例的具体实现过程。

### 2.5.3 开始实例化

​		AbstractAutowireCapableBeanFactory类实现了ObjectFactory接口，创建容器指定的 Bean实例对象，同时还对创建的 Bean实例对象进行初始化处理。其创建 Bean实例对象的方法源码如下∶

```java
//创建Bean实例对象
@Override
protected Object createBean(String beanName, RootBeanDefinition mbd, @Nullable Object[] args)
    throws BeanCreationException {

    if (logger.isDebugEnabled()) {
        logger.debug("Creating instance of bean '" + beanName + "'");
    }
    RootBeanDefinition mbdToUse = mbd;

    // Make sure bean class is actually resolved at this point, and
    // clone the bean definition in case of a dynamically resolved Class
    // which cannot be stored in the shared merged bean definition.
    //判断需要创建的Bean是否可以实例化，即是否可以通过当前的类加载器加载
    Class<?> resolvedClass = resolveBeanClass(mbd, beanName);
    if (resolvedClass != null && !mbd.hasBeanClass() && mbd.getBeanClassName() != null) {
        mbdToUse = new RootBeanDefinition(mbd);
        mbdToUse.setBeanClass(resolvedClass);
    }

    // Prepare method overrides.
    //校验和准备Bean中的方法覆盖
    try {
        mbdToUse.prepareMethodOverrides();
    }
    catch (BeanDefinitionValidationException ex) {
        throw new BeanDefinitionStoreException(mbdToUse.getResourceDescription(),
                                               beanName, "Validation of method overrides failed", ex);
    }

    try {
        // Give BeanPostProcessors a chance to return a proxy instead of the target bean instance.
        //如果Bean配置了初始化前和初始化后的处理器，则试图返回一个需要创建Bean的代理对象
        Object bean = resolveBeforeInstantiation(beanName, mbdToUse);
        if (bean != null) {
            return bean;
        }
    }
    catch (Throwable ex) {
        throw new BeanCreationException(mbdToUse.getResourceDescription(), beanName,
                                        "BeanPostProcessor before instantiation of bean failed", ex);
    }

    try {
        //创建Bean的入口
        Object beanInstance = doCreateBean(beanName, mbdToUse, args);
        if (logger.isDebugEnabled()) {
            logger.debug("Finished creating instance of bean '" + beanName + "'");
        }
        return beanInstance;
    }
    catch (BeanCreationException ex) {
        // A previously detected exception with proper bean creation context already...
        throw ex;
    }
    catch (ImplicitlyAppearedSingletonException ex) {
        // An IllegalStateException to be communicated up to DefaultSingletonBeanRegistry...
        throw ex;
    }
    catch (Throwable ex) {
        throw new BeanCreationException(
            mbdToUse.getResourceDescription(), beanName, "Unexpected exception during bean creation", ex);
    }
}
//真正创建Bean的方法
protected Object doCreateBean(final String beanName, final RootBeanDefinition mbd, final @Nullable Object[] args)
    throws BeanCreationException {

    // Instantiate the bean.
    //封装被创建的Bean对象
    BeanWrapper instanceWrapper = null;
    if (mbd.isSingleton()) {
        instanceWrapper = this.factoryBeanInstanceCache.remove(beanName);
    }
    if (instanceWrapper == null) {
        instanceWrapper = createBeanInstance(beanName, mbd, args);
    }
    final Object bean = instanceWrapper.getWrappedInstance();
    //获取实例化对象的类型
    Class<?> beanType = instanceWrapper.getWrappedClass();
    if (beanType != NullBean.class) {
        mbd.resolvedTargetType = beanType;
    }

    // Allow post-processors to modify the merged bean definition.
    //调用PostProcessor后置处理器
    synchronized (mbd.postProcessingLock) {
        if (!mbd.postProcessed) {
            try {
                applyMergedBeanDefinitionPostProcessors(mbd, beanType, beanName);
            }
            catch (Throwable ex) {
                throw new BeanCreationException(mbd.getResourceDescription(), beanName,
                                                "Post-processing of merged bean definition failed", ex);
            }
            mbd.postProcessed = true;
        }
    }

    // Eagerly cache singletons to be able to resolve circular references
    // even when triggered by lifecycle interfaces like BeanFactoryAware.
    //向容器中缓存单例模式的Bean对象，以防循环引用
    boolean earlySingletonExposure = (mbd.isSingleton() && this.allowCircularReferences &&
                                      isSingletonCurrentlyInCreation(beanName));
    if (earlySingletonExposure) {
        if (logger.isDebugEnabled()) {
            logger.debug("Eagerly caching bean '" + beanName +
                         "' to allow for resolving potential circular references");
        }
        //这里是一个匿名内部类，为了防止循环引用，尽早持有对象的引用
        addSingletonFactory(beanName, () -> getEarlyBeanReference(beanName, mbd, bean));
    }

    // Initialize the bean instance.
    //Bean对象的初始化，依赖注入在此触发
    //这个exposedObject在初始化完成之后返回作为依赖注入完成后的Bean
    Object exposedObject = bean;
    try {
        //将Bean实例对象封装，并且Bean定义中配置的属性值赋值给实例对象
        populateBean(beanName, mbd, instanceWrapper);
        //初始化Bean对象
        exposedObject = initializeBean(beanName, exposedObject, mbd);
    }
    catch (Throwable ex) {
        if (ex instanceof BeanCreationException && beanName.equals(((BeanCreationException) ex).getBeanName())) {
            throw (BeanCreationException) ex;
        }
        else {
            throw new BeanCreationException(
                mbd.getResourceDescription(), beanName, "Initialization of bean failed", ex);
        }
    }

    if (earlySingletonExposure) {
        //获取指定名称的已注册的单例模式Bean对象
        Object earlySingletonReference = getSingleton(beanName, false);
        if (earlySingletonReference != null) {
            //根据名称获取的已注册的Bean和正在实例化的Bean是同一个
            if (exposedObject == bean) {
                //当前实例化的Bean初始化完成
                exposedObject = earlySingletonReference;
            }
            //当前Bean依赖其他Bean，并且当发生循环引用时不允许新创建实例对象
            else if (!this.allowRawInjectionDespiteWrapping && hasDependentBean(beanName)) {
                String[] dependentBeans = getDependentBeans(beanName);
                Set<String> actualDependentBeans = new LinkedHashSet<>(dependentBeans.length);
                //获取当前Bean所依赖的其他Bean
                for (String dependentBean : dependentBeans) {
                    //对依赖Bean进行类型检查
                    if (!removeSingletonIfCreatedForTypeCheckOnly(dependentBean)) {
                        actualDependentBeans.add(dependentBean);
                    }
                }
                if (!actualDependentBeans.isEmpty()) {
                    throw new BeanCurrentlyInCreationException(beanName,
                                                               "Bean with name '" + beanName + "' has been injected into other beans [" +
                                                               StringUtils.collectionToCommaDelimitedString(actualDependentBeans) +
                                                               "] in its raw version as part of a circular reference, but has eventually been " +
                                                               "wrapped. This means that said other beans do not use the final version of the " +
                                                               "bean. This is often the result of over-eager type matching - consider using " +
                                                               "'getBeanNamesOfType' with the 'allowEagerInit' flag turned off, for example.");
                }
            }
        }
    }

    // Register bean as disposable.
    //注册完成依赖注入的Bean
    try {
        registerDisposableBeanIfNecessary(beanName, bean, mbd);
    }
    catch (BeanDefinitionValidationException ex) {
        throw new BeanCreationException(
            mbd.getResourceDescription(), beanName, "Invalid destruction signature", ex);
    }

    return exposedObject;
}
```

​		通过上面的源码注释，我们看到具体的依赖注入实现其实就在以下两个方法中∶

1. createBeanlnstance（）方法，生成 Bean所包含的java 对象实例。
2. populateBean（）方法，对 Bean属性的依赖注入进行处理。

​		下面继续分析这两个方法的代码实现。

### 2.5.4 选择 Bean实例化策略

​		在createBeanlnstance()方法中，根据指定的初始化策略，使用简单工厂、工厂方法或者容器的自动装配特性生成Java 实例对象，创建对象的源码如下∶

```java
//创建Bean的实例对象
protected BeanWrapper createBeanInstance(String beanName, RootBeanDefinition mbd, @Nullable Object[] args) {
    // Make sure bean class is actually resolved at this point.
    //检查确认Bean是可实例化的
    Class<?> beanClass = resolveBeanClass(mbd, beanName);

    //使用工厂方法对Bean进行实例化
    if (beanClass != null && !Modifier.isPublic(beanClass.getModifiers()) && !mbd.isNonPublicAccessAllowed()) {
        throw new BeanCreationException(mbd.getResourceDescription(), beanName,
                                        "Bean class isn't public, and non-public access not allowed: " + beanClass.getName());
    }

    Supplier<?> instanceSupplier = mbd.getInstanceSupplier();
    if (instanceSupplier != null) {
        return obtainFromSupplier(instanceSupplier, beanName);
    }

    if (mbd.getFactoryMethodName() != null)  {
        //调用工厂方法实例化
        return instantiateUsingFactoryMethod(beanName, mbd, args);
    }

    // Shortcut when re-creating the same bean...
    //使用容器的自动装配方法进行实例化
    boolean resolved = false;
    boolean autowireNecessary = false;
    if (args == null) {
        synchronized (mbd.constructorArgumentLock) {
            if (mbd.resolvedConstructorOrFactoryMethod != null) {
                resolved = true;
                autowireNecessary = mbd.constructorArgumentsResolved;
            }
        }
    }
    if (resolved) {
        if (autowireNecessary) {
            //配置了自动装配属性，使用容器的自动装配实例化
            //容器的自动装配是根据参数类型匹配Bean的构造方法
            return autowireConstructor(beanName, mbd, null, null);
        }
        else {
            //使用默认的无参构造方法实例化
            return instantiateBean(beanName, mbd);
        }
    }

    // Need to determine the constructor...
    //使用Bean的构造方法进行实例化
    Constructor<?>[] ctors = determineConstructorsFromBeanPostProcessors(beanClass, beanName);
    if (ctors != null ||
        mbd.getResolvedAutowireMode() == RootBeanDefinition.AUTOWIRE_CONSTRUCTOR ||
        mbd.hasConstructorArgumentValues() || !ObjectUtils.isEmpty(args))  {
        //使用容器的自动装配特性，调用匹配的构造方法实例化
        return autowireConstructor(beanName, mbd, ctors, args);
    }

    // No special handling: simply use no-arg constructor.
    //使用默认的无参构造方法实例化
    return instantiateBean(beanName, mbd);
}
//使用默认的无参构造方法实例化Bean对象
protected BeanWrapper instantiateBean(final String beanName, final RootBeanDefinition mbd) {
    try {
        Object beanInstance;
        final BeanFactory parent = this;
        //获取系统的安全管理接口，JDK标准的安全管理API
        if (System.getSecurityManager() != null) {
            //这里是一个匿名内置类，根据实例化策略创建实例对象
            beanInstance = AccessController.doPrivileged((PrivilegedAction<Object>) () ->
                                                         getInstantiationStrategy().instantiate(mbd, beanName, parent),
                                                         getAccessControlContext());
        }
        else {
            //将实例化的对象封装起来
            beanInstance = getInstantiationStrategy().instantiate(mbd, beanName, parent);
        }
        BeanWrapper bw = new BeanWrapperImpl(beanInstance);
        initBeanWrapper(bw);
        return bw;
    }
    catch (Throwable ex) {
        throw new BeanCreationException(
            mbd.getResourceDescription(), beanName, "Instantiation of bean failed", ex);
    }
}

```

​		经过对上面的代码分析，我们可以看出，对使用工厂方法和自动装配特性的 Bean的实例化相对比较清楚，调用相应的工厂方法或者参数匹配的构造方法即可完成实例化对象的工作，但是对于我们最常使用的默认无参构造方法就需要使用相应的初始化策略（JDK的反射机制或者CGLib）来进行初始化了，在方法getInstantiationStrategy（0.instantiate（）中就具体实现类使用初始策略实例化对象。

### 2.5.5执行 Bean 实例化

​		在 使 用 默 认 的 无 参 构 造 方 法 创 建 Bean 的 实 例化 对 象 时，方 法`getInstantiationStrategy().instantiate(mbd, beanName, parent)`调用了 SimplelnstantiationStrategy类中的实例化 Bean的方法，其源码如下∶

```java
//使用初始化策略实例化Bean对象
@Override
public Object instantiate(RootBeanDefinition bd, @Nullable String beanName, BeanFactory owner) {
    // Don't override the class with CGLIB if no overrides.
    //如果Bean定义中没有方法覆盖，则就不需要CGLIB父类类的方法
    if (!bd.hasMethodOverrides()) {
        Constructor<?> constructorToUse;
        synchronized (bd.constructorArgumentLock) {
            //获取对象的构造方法或工厂方法
            constructorToUse = (Constructor<?>) bd.resolvedConstructorOrFactoryMethod;
            //如果没有构造方法且没有工厂方法
            if (constructorToUse == null) {
                //使用JDK的反射机制，判断要实例化的Bean是否是接口
                final Class<?> clazz = bd.getBeanClass();
                if (clazz.isInterface()) {
                    throw new BeanInstantiationException(clazz, "Specified class is an interface");
                }
                try {
                    if (System.getSecurityManager() != null) {
                        //这里是一个匿名内置类，使用反射机制获取Bean的构造方法
                        constructorToUse = AccessController.doPrivileged(
                            (PrivilegedExceptionAction<Constructor<?>>) () -> clazz.getDeclaredConstructor());
                    }
                    else {
                        constructorToUse =	clazz.getDeclaredConstructor();
                    }
                    bd.resolvedConstructorOrFactoryMethod = constructorToUse;
                }
                catch (Throwable ex) {
                    throw new BeanInstantiationException(clazz, "No default constructor found", ex);
                }
            }
        }
        //使用BeanUtils实例化，通过反射机制调用”构造方法.newInstance(arg)”来进行实例化
        return BeanUtils.instantiateClass(constructorToUse);
    }
    else {
        // Must generate CGLIB subclass.
        //使用CGLIB来实例化对象
        return instantiateWithMethodInjection(bd, beanName, owner);
    }
}

```

​		通过上面的代码分析，我们看到了如果 Bean有方法被覆盖了，则使用 JDK的反射机制进行实例化，否则，使用CGLib 进行实例化。

​		instantiateWithMethodInjection()方 法 调 用 SimpleInstantiationStrategy的 子 类CglibSubclassingInstantiationStrategy使用CGLib来进行初始化，其源码如下∶

```java
@Override
protected Object instantiateWithMethodInjection(RootBeanDefinition bd, @Nullable String beanName, BeanFactory owner) {
    return instantiateWithMethodInjection(bd, beanName, owner, null);
}

@Override
protected Object instantiateWithMethodInjection(RootBeanDefinition bd, @Nullable String beanName, BeanFactory owner,
                                                @Nullable Constructor<?> ctor, @Nullable Object... args) {

    // Must generate CGLIB subclass...
    return new CglibSubclassCreator(bd, owner).instantiate(ctor, args);
}

private static class CglibSubclassCreator {

    private static final Class<?>[] CALLBACK_TYPES = new Class<?>[]
    {NoOp.class, LookupOverrideMethodInterceptor.class, ReplaceOverrideMethodInterceptor.class};

    private final RootBeanDefinition beanDefinition;

    private final BeanFactory owner;

    CglibSubclassCreator(RootBeanDefinition beanDefinition, BeanFactory owner) {
        this.beanDefinition = beanDefinition;
        this.owner = owner;
    }

    //使用CGLIB进行Bean对象实例化
    public Object instantiate(@Nullable Constructor<?> ctor, @Nullable Object... args) {
        //创建代理子类
        Class<?> subclass = createEnhancedSubclass(this.beanDefinition);
        Object instance;
        if (ctor == null) {
            instance = BeanUtils.instantiateClass(subclass);
        }
        else {
            try {
                Constructor<?> enhancedSubclassConstructor = subclass.getConstructor(ctor.getParameterTypes());
                instance = enhancedSubclassConstructor.newInstance(args);
            }
            catch (Exception ex) {
                throw new BeanInstantiationException(this.beanDefinition.getBeanClass(),
                                                     "Failed to invoke constructor for CGLIB enhanced subclass [" + subclass.getName() + "]", ex);
            }
        }
        // SPR-10785: set callbacks directly on the instance instead of in the
        // enhanced class (via the Enhancer) in order to avoid memory leaks.
        Factory factory = (Factory) instance;
        factory.setCallbacks(new Callback[] {NoOp.INSTANCE,
                                             new LookupOverrideMethodInterceptor(this.beanDefinition, this.owner),
                                             new ReplaceOverrideMethodInterceptor(this.beanDefinition, this.owner)});
        return instance;
    }

    /**
		 * Create an enhanced subclass of the bean class for the provided bean
		 * definition, using CGLIB.
		 */
    private Class<?> createEnhancedSubclass(RootBeanDefinition beanDefinition) {
        //CGLIB中的类
        Enhancer enhancer = new Enhancer();
        //将Bean本身作为其基类
        enhancer.setSuperclass(beanDefinition.getBeanClass());
        enhancer.setNamingPolicy(SpringNamingPolicy.INSTANCE);
        if (this.owner instanceof ConfigurableBeanFactory) {
            ClassLoader cl = ((ConfigurableBeanFactory) this.owner).getBeanClassLoader();
            enhancer.setStrategy(new ClassLoaderAwareGeneratorStrategy(cl));
        }
        enhancer.setCallbackFilter(new MethodOverrideCallbackFilter(beanDefinition));
        enhancer.setCallbackTypes(CALLBACK_TYPES);
        //使用CGLIB的createClass方法生成实例对象
        return enhancer.createClass();
    }
}
```

> 这里为什么要使用静态内部类`CglibSubclassCreator`

​		CGLib 是一个常用的字节码生成器的类库，它提供了一系列API实现 Java 字节码的生成和转换功能。我们在学习 JDK的动态代理时都知道，JDK的动态代理只能针对接口，如果一个类没有实现任何接口，要对其进行动态代理只能使用CGLib。

### 2.5.6准备依赖注入

​		在前面的分析中我们已经了解 到 Bean的依赖注入主要分为两个步骤，首先调用createBeanInstance()方法生成Bean所包含的Java 对象实例。然后，调用populateBean()方法，对Bean属性的依赖注入进行处理。

​		上面我们已经分析了容器初始化生成 Bean所包含的Java实例对象的过程，现在我们继续分析生成对象后，Spring loC容器是如何将 Bean的属性依赖关系注入 Bean实例对象中并设置好的，回到AbstractAutowireCapableBeanFactory的populateBean（）方法，对属性依赖注入的代码如下∶

```java
//将Bean属性设置到生成的实例对象上
protected void populateBean(String beanName, RootBeanDefinition mbd, @Nullable BeanWrapper bw) {
    if (bw == null) {
        if (mbd.hasPropertyValues()) {
            throw new BeanCreationException(
                mbd.getResourceDescription(), beanName, "Cannot apply property values to null instance");
        }
        else {
            // Skip property population phase for null instance.
            return;
        }
    }

    // Give any InstantiationAwareBeanPostProcessors the opportunity to modify the
    // state of the bean before properties are set. This can be used, for example,
    // to support styles of field injection.
    boolean continueWithPropertyPopulation = true;

    if (!mbd.isSynthetic() && hasInstantiationAwareBeanPostProcessors()) {
        for (BeanPostProcessor bp : getBeanPostProcessors()) {
            if (bp instanceof InstantiationAwareBeanPostProcessor) {
                InstantiationAwareBeanPostProcessor ibp = (InstantiationAwareBeanPostProcessor) bp;
                if (!ibp.postProcessAfterInstantiation(bw.getWrappedInstance(), beanName)) {
                    continueWithPropertyPopulation = false;
                    break;
                }
            }
        }
    }

    if (!continueWithPropertyPopulation) {
        return;
    }
    //获取容器在解析Bean定义资源时为BeanDefiniton中设置的属性值
    PropertyValues pvs = (mbd.hasPropertyValues() ? mbd.getPropertyValues() : null);

    //对依赖注入处理，首先处理autowiring自动装配的依赖注入
    if (mbd.getResolvedAutowireMode() == RootBeanDefinition.AUTOWIRE_BY_NAME ||
        mbd.getResolvedAutowireMode() == RootBeanDefinition.AUTOWIRE_BY_TYPE) {
        MutablePropertyValues newPvs = new MutablePropertyValues(pvs);

        // Add property values based on autowire by name if applicable.
        //根据Bean名称进行autowiring自动装配处理
        if (mbd.getResolvedAutowireMode() == RootBeanDefinition.AUTOWIRE_BY_NAME) {
            autowireByName(beanName, mbd, bw, newPvs);
        }

        // Add property values based on autowire by type if applicable.
        //根据Bean类型进行autowiring自动装配处理
        if (mbd.getResolvedAutowireMode() == RootBeanDefinition.AUTOWIRE_BY_TYPE) {
            autowireByType(beanName, mbd, bw, newPvs);
        }

        pvs = newPvs;
    }

    //对非autowiring的属性进行依赖注入处理
    boolean hasInstAwareBpps = hasInstantiationAwareBeanPostProcessors();
    boolean needsDepCheck = (mbd.getDependencyCheck() != RootBeanDefinition.DEPENDENCY_CHECK_NONE);

    if (hasInstAwareBpps || needsDepCheck) {
        if (pvs == null) {
            pvs = mbd.getPropertyValues();
        }
        PropertyDescriptor[] filteredPds = filterPropertyDescriptorsForDependencyCheck(bw, mbd.allowCaching);
        if (hasInstAwareBpps) {
            for (BeanPostProcessor bp : getBeanPostProcessors()) {
                if (bp instanceof InstantiationAwareBeanPostProcessor) {
                    InstantiationAwareBeanPostProcessor ibp = (InstantiationAwareBeanPostProcessor) bp;
                    pvs = ibp.postProcessPropertyValues(pvs, filteredPds, bw.getWrappedInstance(), beanName);
                    if (pvs == null) {
                        return;
                    }
                }
            }
        }
        if (needsDepCheck) {
            checkDependencies(beanName, mbd, filteredPds, pvs);
        }
    }

    if (pvs != null) {
        //对属性进行注入
        applyPropertyValues(beanName, mbd, bw, pvs);
    }
}
//解析并注入依赖属性的过程
protected void applyPropertyValues(String beanName, BeanDefinition mbd, BeanWrapper bw, PropertyValues pvs) {
    if (pvs.isEmpty()) {
        return;
    }

    //封装属性值
    MutablePropertyValues mpvs = null;
    List<PropertyValue> original;

    if (System.getSecurityManager() != null) {
        if (bw instanceof BeanWrapperImpl) {
            //设置安全上下文，JDK安全机制
            ((BeanWrapperImpl) bw).setSecurityContext(getAccessControlContext());
        }
    }

    if (pvs instanceof MutablePropertyValues) {
        mpvs = (MutablePropertyValues) pvs;
        //属性值已经转换
        if (mpvs.isConverted()) {
            // Shortcut: use the pre-converted values as-is.
            try {
                //为实例化对象设置属性值
                bw.setPropertyValues(mpvs);
                return;
            }
            catch (BeansException ex) {
                throw new BeanCreationException(
                    mbd.getResourceDescription(), beanName, "Error setting property values", ex);
            }
        }
        //获取属性值对象的原始类型值
        original = mpvs.getPropertyValueList();
    }
    else {
        original = Arrays.asList(pvs.getPropertyValues());
    }

    //获取用户自定义的类型转换
    TypeConverter converter = getCustomTypeConverter();
    if (converter == null) {
        converter = bw;
    }
    //创建一个Bean定义属性值解析器，将Bean定义中的属性值解析为Bean实例对象的实际值
    BeanDefinitionValueResolver valueResolver = new BeanDefinitionValueResolver(this, beanName, mbd, converter);

    // Create a deep copy, resolving any references for values.

    //为属性的解析值创建一个拷贝，将拷贝的数据注入到实例对象中
    List<PropertyValue> deepCopy = new ArrayList<>(original.size());
    boolean resolveNecessary = false;
    for (PropertyValue pv : original) {
        //属性值不需要转换
        if (pv.isConverted()) {
            deepCopy.add(pv);
        }
        //属性值需要转换
        else {
            String propertyName = pv.getName();
            //原始的属性值，即转换之前的属性值
            Object originalValue = pv.getValue();
            //转换属性值，例如将引用转换为IOC容器中实例化对象引用
            Object resolvedValue = valueResolver.resolveValueIfNecessary(pv, originalValue);
            //转换之后的属性值
            Object convertedValue = resolvedValue;
            //属性值是否可以转换
            boolean convertible = bw.isWritableProperty(propertyName) &&
                !PropertyAccessorUtils.isNestedOrIndexedProperty(propertyName);
            if (convertible) {
                //使用用户自定义的类型转换器转换属性值
                convertedValue = convertForProperty(resolvedValue, propertyName, bw, converter);
            }
            // Possibly store converted value in merged bean definition,
            // in order to avoid re-conversion for every created bean instance.
            //存储转换后的属性值，避免每次属性注入时的转换工作
            if (resolvedValue == originalValue) {
                if (convertible) {
                    //设置属性转换之后的值
                    pv.setConvertedValue(convertedValue);
                }
                deepCopy.add(pv);
            }
            //属性是可转换的，且属性原始值是字符串类型，且属性的原始类型值不是
            //动态生成的字符串，且属性的原始值不是集合或者数组类型
            else if (convertible && originalValue instanceof TypedStringValue &&
                     !((TypedStringValue) originalValue).isDynamic() &&
                     !(convertedValue instanceof Collection || ObjectUtils.isArray(convertedValue))) {
                pv.setConvertedValue(convertedValue);
                //重新封装属性的值
                deepCopy.add(pv);
            }
            else {
                resolveNecessary = true;
                deepCopy.add(new PropertyValue(pv, convertedValue));
            }
        }
    }
    if (mpvs != null && !resolveNecessary) {
        //标记属性值已经转换过
        mpvs.setConverted();
    }

    // Set our (possibly massaged) deep copy.
    //进行属性依赖注入
    try {
        bw.setPropertyValues(new MutablePropertyValues(deepCopy));
    }
    catch (BeansException ex) {
        throw new BeanCreationException(
            mbd.getResourceDescription(), beanName, "Error setting property values", ex);
    }
}
```

​		分析上述代码，我们可以看出，对属性的注入过程分以下两种情况∶

1. 属性值类型不需要强制转换时，不需要解析属性值，直接准备进行依赖注入。
2. 属性值需要进行类型强制转换时，如对其他对象的引用等，首先需要解析属性值，然后对解析后的属性值进行依赖注入。

​		对属性值的解析是在 BeanDefinitionvalueResolver类中的 resolveValuelfNecessary（）方法中进行的，对属性值的依赖注入是通过 bw.setPropertyValues0方法实现的，在分析属性值的依赖注入之前，我们先分析一下对属性值的解析过程。

### 2.5.7解析属性注入规则

​		当容器在对属性进行依赖注入时，如果发现属性值需要进行类型转换，如属性值是容器中另一个Bean 实例对象的引用，则容器首先需要根据属性值解析出所引用的对象，然后才能将该引用对象注入到目标实例对象的属性上去，对属性进行解析的由 resolveValuelfNecessary0方法实现，其源码如下∶

BeanDefinitionValueResolver

```java
//解析属性值，对注入类型进行转换
@Nullable
public Object resolveValueIfNecessary(Object argName, @Nullable Object value) {
    // We must check each value to see whether it requires a runtime reference
    // to another bean to be resolved.
    //对引用类型的属性进行解析
    if (value instanceof RuntimeBeanReference) {
        RuntimeBeanReference ref = (RuntimeBeanReference) value;
        //调用引用类型属性的解析方法
        return resolveReference(argName, ref);
    }
    //对属性值是引用容器中另一个Bean名称的解析
    else if (value instanceof RuntimeBeanNameReference) {
        String refName = ((RuntimeBeanNameReference) value).getBeanName();
        refName = String.valueOf(doEvaluate(refName));
        //从容器中获取指定名称的Bean
        if (!this.beanFactory.containsBean(refName)) {
            throw new BeanDefinitionStoreException(
                "Invalid bean name '" + refName + "' in bean reference for " + argName);
        }
        return refName;
    }
    //对Bean类型属性的解析，主要是Bean中的内部类
    else if (value instanceof BeanDefinitionHolder) {
        // Resolve BeanDefinitionHolder: contains BeanDefinition with name and aliases.
        BeanDefinitionHolder bdHolder = (BeanDefinitionHolder) value;
        return resolveInnerBean(argName, bdHolder.getBeanName(), bdHolder.getBeanDefinition());
    }
    else if (value instanceof BeanDefinition) {
        // Resolve plain BeanDefinition, without contained name: use dummy name.
        BeanDefinition bd = (BeanDefinition) value;
        String innerBeanName = "(inner bean)" + BeanFactoryUtils.GENERATED_BEAN_NAME_SEPARATOR +
            ObjectUtils.getIdentityHexString(bd);
        return resolveInnerBean(argName, innerBeanName, bd);
    }
    //对集合数组类型的属性解析
    else if (value instanceof ManagedArray) {
        // May need to resolve contained runtime references.
        ManagedArray array = (ManagedArray) value;
        //获取数组的类型
        Class<?> elementType = array.resolvedElementType;
        if (elementType == null) {
            //获取数组元素的类型
            String elementTypeName = array.getElementTypeName();
            if (StringUtils.hasText(elementTypeName)) {
                try {
                    //使用反射机制创建指定类型的对象
                    elementType = ClassUtils.forName(elementTypeName, this.beanFactory.getBeanClassLoader());
                    array.resolvedElementType = elementType;
                }
                catch (Throwable ex) {
                    // Improve the message by showing the context.
                    throw new BeanCreationException(
                        this.beanDefinition.getResourceDescription(), this.beanName,
                        "Error resolving array type for " + argName, ex);
                }
            }
            //没有获取到数组的类型，也没有获取到数组元素的类型
            //则直接设置数组的类型为Object
            else {
                elementType = Object.class;
            }
        }
        //创建指定类型的数组
        return resolveManagedArray(argName, (List<?>) value, elementType);
    }
    //解析list类型的属性值
    else if (value instanceof ManagedList) {
        // May need to resolve contained runtime references.
        return resolveManagedList(argName, (List<?>) value);
    }
    //解析set类型的属性值
    else if (value instanceof ManagedSet) {
        // May need to resolve contained runtime references.
        return resolveManagedSet(argName, (Set<?>) value);
    }
    //解析map类型的属性值
    else if (value instanceof ManagedMap) {
        // May need to resolve contained runtime references.
        return resolveManagedMap(argName, (Map<?, ?>) value);
    }
    //解析props类型的属性值，props其实就是key和value均为字符串的map
    else if (value instanceof ManagedProperties) {
        Properties original = (Properties) value;
        //创建一个拷贝，用于作为解析后的返回值
        Properties copy = new Properties();
        original.forEach((propKey, propValue) -> {
            if (propKey instanceof TypedStringValue) {
                propKey = evaluate((TypedStringValue) propKey);
            }
            if (propValue instanceof TypedStringValue) {
                propValue = evaluate((TypedStringValue) propValue);
            }
            if (propKey == null || propValue == null) {
                throw new BeanCreationException(
                    this.beanDefinition.getResourceDescription(), this.beanName,
                    "Error converting Properties key/value pair for " + argName + ": resolved to null");
            }
            copy.put(propKey, propValue);
        });
        return copy;
    }
    //解析字符串类型的属性值
    else if (value instanceof TypedStringValue) {
        // Convert value to target type here.
        TypedStringValue typedStringValue = (TypedStringValue) value;
        Object valueObject = evaluate(typedStringValue);
        try {
            //获取属性的目标类型
            Class<?> resolvedTargetType = resolveTargetType(typedStringValue);
            if (resolvedTargetType != null) {
                //对目标类型的属性进行解析，递归调用
                return this.typeConverter.convertIfNecessary(valueObject, resolvedTargetType);
            }
            //没有获取到属性的目标对象，则按Object类型返回
            else {
                return valueObject;
            }
        }
        catch (Throwable ex) {
            // Improve the message by showing the context.
            throw new BeanCreationException(
                this.beanDefinition.getResourceDescription(), this.beanName,
                "Error converting typed String value for " + argName, ex);
        }
    }
    else if (value instanceof NullBean) {
        return null;
    }
    else {
        return evaluate(value);
    }
}
//解析引用类型的属性值
@Nullable
private Object resolveReference(Object argName, RuntimeBeanReference ref) {
    try {
        Object bean;
        //获取引用的Bean名称
        String refName = ref.getBeanName();
        refName = String.valueOf(doEvaluate(refName));
        //如果引用的对象在父类容器中，则从父类容器中获取指定的引用对象
        if (ref.isToParent()) {
            if (this.beanFactory.getParentBeanFactory() == null) {
                throw new BeanCreationException(
                    this.beanDefinition.getResourceDescription(), this.beanName,
                    "Can't resolve reference to bean '" + refName +
                    "' in parent factory: no parent factory available");
            }
            bean = this.beanFactory.getParentBeanFactory().getBean(refName);
        }
        //从当前的容器中获取指定的引用Bean对象，如果指定的Bean没有被实例化
        //则会递归触发引用Bean的初始化和依赖注入
        else {
            bean = this.beanFactory.getBean(refName);
            //将当前实例化对象的依赖引用对象
            this.beanFactory.registerDependentBean(refName, this.beanName);
        }
        if (bean instanceof NullBean) {
            bean = null;
        }
        return bean;
    }
    catch (BeansException ex) {
        throw new BeanCreationException(
            this.beanDefinition.getResourceDescription(), this.beanName,
            "Cannot resolve reference to bean '" + ref.getBeanName() + "' while setting " + argName, ex);
    }
}

//解析array类型的属性
private Object resolveManagedArray(Object argName, List<?> ml, Class<?> elementType) {
    //创建一个指定类型的数组，用于存放和返回解析后的数组
    Object resolved = Array.newInstance(elementType, ml.size());
    for (int i = 0; i < ml.size(); i++) {
        //递归解析array的每一个元素，并将解析后的值设置到resolved数组中，索引为i
        Array.set(resolved, i,
                  resolveValueIfNecessary(new KeyedArgName(argName, i), ml.get(i)));
    }
    return resolved;
}

//解析list类型的属性
private List<?> resolveManagedList(Object argName, List<?> ml) {
    List<Object> resolved = new ArrayList<>(ml.size());
    for (int i = 0; i < ml.size(); i++) {
        //递归解析list的每一个元素
        resolved.add(
            resolveValueIfNecessary(new KeyedArgName(argName, i), ml.get(i)));
    }
    return resolved;
}

//解析set类型的属性
private Set<?> resolveManagedSet(Object argName, Set<?> ms) {
    Set<Object> resolved = new LinkedHashSet<>(ms.size());
    int i = 0;
    //递归解析set的每一个元素
    for (Object m : ms) {
        resolved.add(resolveValueIfNecessary(new KeyedArgName(argName, i), m));
        i++;
    }
    return resolved;
}

//解析map类型的属性
private Map<?, ?> resolveManagedMap(Object argName, Map<?, ?> mm) {
    Map<Object, Object> resolved = new LinkedHashMap<>(mm.size());
    //递归解析map中每一个元素的key和value
    for (Map.Entry<?, ?> entry : mm.entrySet()) {
        Object resolvedKey = resolveValueIfNecessary(argName, entry.getKey());
        Object resolvedValue = resolveValueIfNecessary(
            new KeyedArgName(argName, entry.getKey()), entry.getValue());
        resolved.put(resolvedKey, resolvedValue);
    }
    return resolved;
}
```

​		通过上面的代码分析，我们明白了Spring是如何将引用类型，内部类以及集合类型等属性进行解析的，属性值解析完成后就可以进行依赖注入了，依赖注入的过程就是Bean对象实例设置到它所依赖的Bean对象属性上去。而真正的依赖注入是通过 `bw.setPropertyValues();`方法实现的，该方法也使用了委托模式，在 BeanWrapper 接口中至少定义了方法声明，依赖注入的具体实现交由其实现类BeanWrapperlmpl来完成，下面我们就分析依 BeanWrapperlmpl中赖注入相关的源码。

### 2.5.8注入赋值

​		BeanWrapperlmpl类主要是对容器中完成初始化的 Bean 实例对象进行属性的依赖注入，即把Bean对象设置到它所依赖的另一个Bean的属性中去。然而，BeanWrapperlmpl中的注入方法实际上由 AbstractNestablePropertyAccessor来实现的，其相关源码如下∶

> 因为实际调用的是setPropertyValues但是这里没有找到setPropertyValues()方法在哪里

```java
//实现属性依赖注入功能
protected void setPropertyValue(PropertyTokenHolder tokens, PropertyValue pv) throws BeansException {
    if (tokens.keys != null) {
        processKeyedProperty(tokens, pv);
    }
    else {
        processLocalProperty(tokens, pv);
    }
}

//实现属性依赖注入功能
@SuppressWarnings("unchecked")
private void processKeyedProperty(PropertyTokenHolder tokens, PropertyValue pv) {
    //调用属性的getter方法，获取属性的值
    Object propValue = getPropertyHoldingValue(tokens);
    PropertyHandler ph = getLocalPropertyHandler(tokens.actualName);
    if (ph == null) {
        throw new InvalidPropertyException(
            getRootClass(), this.nestedPath + tokens.actualName, "No property handler found");
    }
    Assert.state(tokens.keys != null, "No token keys");
    String lastKey = tokens.keys[tokens.keys.length - 1];

    //注入array类型的属性值
    if (propValue.getClass().isArray()) {
        Class<?> requiredType = propValue.getClass().getComponentType();
        int arrayIndex = Integer.parseInt(lastKey);
        Object oldValue = null;
        try {
            if (isExtractOldValueForEditor() && arrayIndex < Array.getLength(propValue)) {
                oldValue = Array.get(propValue, arrayIndex);
            }
            Object convertedValue = convertIfNecessary(tokens.canonicalName, oldValue, pv.getValue(),
                                                       requiredType, ph.nested(tokens.keys.length));
            //获取集合类型属性的长度
            int length = Array.getLength(propValue);
            if (arrayIndex >= length && arrayIndex < this.autoGrowCollectionLimit) {
                Class<?> componentType = propValue.getClass().getComponentType();
                Object newArray = Array.newInstance(componentType, arrayIndex + 1);
                System.arraycopy(propValue, 0, newArray, 0, length);
                setPropertyValue(tokens.actualName, newArray);
                //调用属性的getter方法，获取属性的值
                propValue = getPropertyValue(tokens.actualName);
            }
            //将属性的值赋值给数组中的元素
            Array.set(propValue, arrayIndex, convertedValue);
        }
        catch (IndexOutOfBoundsException ex) {
            throw new InvalidPropertyException(getRootClass(), this.nestedPath + tokens.canonicalName,
                                               "Invalid array index in property path '" + tokens.canonicalName + "'", ex);
        }
    }

    //注入list类型的属性值
    else if (propValue instanceof List) {
        //获取list集合的类型
        Class<?> requiredType = ph.getCollectionType(tokens.keys.length);
        List<Object> list = (List<Object>) propValue;
        //获取list集合的size
        int index = Integer.parseInt(lastKey);
        Object oldValue = null;
        if (isExtractOldValueForEditor() && index < list.size()) {
            oldValue = list.get(index);
        }
        //获取list解析后的属性值
        Object convertedValue = convertIfNecessary(tokens.canonicalName, oldValue, pv.getValue(),
                                                   requiredType, ph.nested(tokens.keys.length));
        int size = list.size();
        //如果list的长度大于属性值的长度，则多余的元素赋值为null
        if (index >= size && index < this.autoGrowCollectionLimit) {
            for (int i = size; i < index; i++) {
                try {
                    list.add(null);
                }
                catch (NullPointerException ex) {
                    throw new InvalidPropertyException(getRootClass(), this.nestedPath + tokens.canonicalName,
                                                       "Cannot set element with index " + index + " in List of size " +
                                                       size + ", accessed using property path '" + tokens.canonicalName +
                                                       "': List does not support filling up gaps with null elements");
                }
            }
            list.add(convertedValue);
        }
        else {
            try {
                //将值添加到list中
                list.set(index, convertedValue);
            }
            catch (IndexOutOfBoundsException ex) {
                throw new InvalidPropertyException(getRootClass(), this.nestedPath + tokens.canonicalName,
                                                   "Invalid list index in property path '" + tokens.canonicalName + "'", ex);
            }
        }
    }

    //注入map类型的属性值
    else if (propValue instanceof Map) {
        //获取map集合key的类型
        Class<?> mapKeyType = ph.getMapKeyType(tokens.keys.length);
        //获取map集合value的类型
        Class<?> mapValueType = ph.getMapValueType(tokens.keys.length);
        Map<Object, Object> map = (Map<Object, Object>) propValue;
        // IMPORTANT: Do not pass full property name in here - property editors
        // must not kick in for map keys but rather only for map values.
        TypeDescriptor typeDescriptor = TypeDescriptor.valueOf(mapKeyType);
        //解析map类型属性key值
        Object convertedMapKey = convertIfNecessary(null, null, lastKey, mapKeyType, typeDescriptor);
        Object oldValue = null;
        if (isExtractOldValueForEditor()) {
            oldValue = map.get(convertedMapKey);
        }
        // Pass full property name and old value in here, since we want full
        // conversion ability for map values.
        //解析map类型属性value值
        Object convertedMapValue = convertIfNecessary(tokens.canonicalName, oldValue, pv.getValue(),
                                                      mapValueType, ph.nested(tokens.keys.length));
        //将解析后的key和value值赋值给map集合属性
        map.put(convertedMapKey, convertedMapValue);
    }

    else {
        throw new InvalidPropertyException(getRootClass(), this.nestedPath + tokens.canonicalName,
                                           "Property referenced in indexed property path '" + tokens.canonicalName +
                                           "' is neither an array nor a List nor a Map; returned value was [" + propValue + "]");
    }
}
private Object getPropertyHoldingValue(PropertyTokenHolder tokens) {
    // Apply indexes and map keys: fetch value for all keys but the last one.
    Assert.state(tokens.keys != null, "No token keys");
    PropertyTokenHolder getterTokens = new PropertyTokenHolder(tokens.actualName);
    getterTokens.canonicalName = tokens.canonicalName;
    getterTokens.keys = new String[tokens.keys.length - 1];
    System.arraycopy(tokens.keys, 0, getterTokens.keys, 0, tokens.keys.length - 1);

    Object propValue;
    try {
        //获取属性值
        propValue = getPropertyValue(getterTokens);
    }
    catch (NotReadablePropertyException ex) {
        throw new NotWritablePropertyException(getRootClass(), this.nestedPath + tokens.canonicalName,
                                               "Cannot access indexed value in property referenced " +
                                               "in indexed property path '" + tokens.canonicalName + "'", ex);
    }

    if (propValue == null) {
        // null map value case
        if (isAutoGrowNestedPaths()) {
            int lastKeyIndex = tokens.canonicalName.lastIndexOf('[');
            getterTokens.canonicalName = tokens.canonicalName.substring(0, lastKeyIndex);
            propValue = setDefaultValue(getterTokens);
        }
        else {
            throw new NullValueInNestedPathException(getRootClass(), this.nestedPath + tokens.canonicalName,
                                                     "Cannot access indexed value in property referenced " +
                                                     "in indexed property path '" + tokens.canonicalName + "': returned null");
        }
    }
    return propValue;
}

```

​		通过对上面注入依赖代码的分析，我们已经明白了Spring loC容器是如何将属性的值注入到Bean 实例对象中去的∶

1. 1）、对于集合类型的属性，将其属性值解析为目标类型的集合后直接赋值给属性。
2. 2）、对于非集合类型的属性，大量使用了JDK的反射机制，通过属性的getter()方法获取指定属性注入前的值，同时调用属性的 setter（）方法为属性设置注入后的值。看到这里相信很多人都明白了Spring的 setter()注入原理。

![Spring Framework注入-2](images/Spring Framework注入-2.png)

​		至此 Spring loC容器对Bean定义资源文件的定位，载入、解析和依赖注入已经全部分析完毕，现在Spring loC容器中管理了一系列靠依赖关系联系起来的 Bean，程序不需要应用自己手动创建所需的对象，Spring loC容器会在我们使用的时候自动为我们创建，并且为我们注入好相关的依赖，这就是Spring 核心功能的控制反转和依赖注入的相关功能。

### 2.5.9loC容器中那些鲜为人知的细节

​		通过前面章节中对Spring loC容器的源码分析，我们已经基本上了解了Spring loC容器对 Bean定义资源的定位、载入和注册过程，同时也清楚了当用户通过getBean()方法向loC容器获取被管理的Bean时，loC容器对Bean进行的初始化和依赖注入过程，这些是Spring loC容器的基本功能特性。Spring loC容器还有一些高级特性，如使用lazy-init属性对 Bean预初始化、FactoryBean产生或者修饰 Bean对象的生成、loC容器初始化 Bean过程中使用BeanPostProcessor后置处理器对Bean声明周期事件管理等。

#### 2.5.9.1关于延时加载

​		通过前面我们对 loC容器的实现和工作原理分析 我们已经知道loC容器的初始化过程就是对 Bean 定义资源的定位、载入和注册，此时容器对Bean的依赖注入并没有发生，依赖注入主要是在应用程序第一次向容器索取 Bean时，通过 getBean（）方法的调用完成。当Bean定义资源的`<Bean>`元素中配置了lazy-init=false属性时，容器将会在初始化的时候对所配置的Bean进行预实例化，Bean的依赖注入在容器初始化的时候就已经完成。这样，当应用程序第一次向容器索取被管理的Bean时，就不用再初始化和对 Bean进行依赖注入了，直接从容器中获取已经完成依赖注入的现成 Bean，可以提高应用第一次向容器获取 Bean的性能。

**1、 refresh（）方法**

​		从loC容器的初始化过程开始，我们知道loC容器读入已经定位的Bean定义资源是从 refresh（）方法开始的，我们首先从AbstractApplicationContext类的 refresh（）方法入手分析，源码如下∶

```java
@Override
public void refresh() throws BeansException, IllegalStateException {
    synchronized (this.startupShutdownMonitor) {
        // Prepare this context for refreshing.
        //1、调用容器准备刷新的方法，获取容器的当时时间，同时给容器设置同步标识
        prepareRefresh();

        // Tell the subclass to refresh the internal bean factory.
        //2、告诉子类启动refreshBeanFactory()方法，Bean定义资源文件的载入从
        //子类的refreshBeanFactory()方法启动
        ConfigurableListableBeanFactory beanFactory = obtainFreshBeanFactory();

        // Prepare the bean factory for use in this context.
        //3、为BeanFactory配置容器特性，例如类加载器、事件处理器等
        prepareBeanFactory(beanFactory);

        try {
            // Allows post-processing of the bean factory in context subclasses.
            //4、为容器的某些子类指定特殊的BeanPost事件处理器
            postProcessBeanFactory(beanFactory);

            // Invoke factory processors registered as beans in the context.
            //5、调用所有注册的BeanFactoryPostProcessor的Bean
            invokeBeanFactoryPostProcessors(beanFactory);

            // Register bean processors that intercept bean creation.
            //6、为BeanFactory注册BeanPost事件处理器.
            //BeanPostProcessor是Bean后置处理器，用于监听容器触发的事件
            registerBeanPostProcessors(beanFactory);

            // Initialize message source for this context.
            //7、初始化信息源，和国际化相关.
            initMessageSource();

            // Initialize event multicaster for this context.
            //8、初始化容器事件传播器.
            initApplicationEventMulticaster();

            // Initialize other special beans in specific context subclasses.
            //9、调用子类的某些特殊Bean初始化方法
            onRefresh();

            // Check for listener beans and register them.
            //10、为事件传播器注册事件监听器.
            registerListeners();

            // Instantiate all remaining (non-lazy-init) singletons.
            //11、初始化所有剩余的单例Bean
            finishBeanFactoryInitialization(beanFactory);

            // Last step: publish corresponding event.
            //12、初始化容器的生命周期事件处理器，并发布容器的生命周期事件
            finishRefresh();
        }

        catch (BeansException ex) {
            if (logger.isWarnEnabled()) {
                logger.warn("Exception encountered during context initialization - " +
                            "cancelling refresh attempt: " + ex);
            }

            // Destroy already created singletons to avoid dangling resources.
            //13、销毁已创建的Bean
            destroyBeans();

            // Reset 'active' flag.
            //14、取消refresh操作，重置容器的同步标识。
            cancelRefresh(ex);

            // Propagate exception to caller.
            throw ex;
        }

        finally {
            // Reset common introspection caches in Spring's core, since we
            // might not ever need metadata for singleton beans anymore...
            //15、重设公共缓存
            resetCommonCaches();
        }
    }
}
```

​		在 refresh()方法中`ConfigurableListableBeanFactory beanFactory = obtainFreshBeanFactory();`启动了Bean定义资源的载入、注册过程，而`finishBeanFactoryInitialization(beanFactory);`方法是对注册后的Bean定义中的预实例化（lazy-init=false，Spring默认就是预实例化，即为 true）的 Bean进行处理的地方。

**2、finishBeanFactoryInitialization 处理预实例化 Bean**

​		当Bean定义资源被载入loC容器之后，容器将 Bean 定义资源解析为容器内部的数据结构BeanDefinition注册到容器中 AbstractApplicationContext类中的finishBeanFactorylInitialization（）方法对配置了预实例化属性的 Bean 进行预初始化过程，源码如下∶

```java
//对配置了lazy-init属性的Bean进行预实例化处理
protected void finishBeanFactoryInitialization(ConfigurableListableBeanFactory beanFactory) {
    // Initialize conversion service for this context.
    //这是Spring3以后新加的代码，为容器指定一个转换服务(ConversionService)
    //在对某些Bean属性进行转换时使用
    if (beanFactory.containsBean(CONVERSION_SERVICE_BEAN_NAME) &&
        beanFactory.isTypeMatch(CONVERSION_SERVICE_BEAN_NAME, ConversionService.class)) {
        beanFactory.setConversionService(
            beanFactory.getBean(CONVERSION_SERVICE_BEAN_NAME, ConversionService.class));
    }

    // Register a default embedded value resolver if no bean post-processor
    // (such as a PropertyPlaceholderConfigurer bean) registered any before:
    // at this point, primarily for resolution in annotation attribute values.
    if (!beanFactory.hasEmbeddedValueResolver()) {
        beanFactory.addEmbeddedValueResolver(strVal -> getEnvironment().resolvePlaceholders(strVal));
    }

    // Initialize LoadTimeWeaverAware beans early to allow for registering their transformers early.
    String[] weaverAwareNames = beanFactory.getBeanNamesForType(LoadTimeWeaverAware.class, false, false);
    for (String weaverAwareName : weaverAwareNames) {
        getBean(weaverAwareName);
    }

    // Stop using the temporary ClassLoader for type matching.
    //为了类型匹配，停止使用临时的类加载器
    beanFactory.setTempClassLoader(null);

    // Allow for caching all bean definition metadata, not expecting further changes.
    //缓存容器中所有注册的BeanDefinition元数据，以防被修改
    beanFactory.freezeConfiguration();

    // Instantiate all remaining (non-lazy-init) singletons.
    //对配置了lazy-init属性的单态模式Bean进行预实例化处理
    beanFactory.preInstantiateSingletons();
}
```

ConfigurableListableBeanFactory是一个接口，其preInstantiateSingletons()方法由其子类DefaultListableBeanFactory提供。

**3、DefaultListableBeanFactory 对配置 lazy-init属性单态 Bean的预实例化**

```java
//对配置lazy-init属性单态Bean的预实例化
@Override
public void preInstantiateSingletons() throws BeansException {
    if (this.logger.isDebugEnabled()) {
        this.logger.debug("Pre-instantiating singletons in " + this);
    }

    // Iterate over a copy to allow for init methods which in turn register new bean definitions.
    // While this may not be part of the regular factory bootstrap, it does otherwise work fine.
    List<String> beanNames = new ArrayList<>(this.beanDefinitionNames);

    // Trigger initialization of all non-lazy singleton beans...
    for (String beanName : beanNames) {
        //获取指定名称的Bean定义
        RootBeanDefinition bd = getMergedLocalBeanDefinition(beanName);
        //Bean不是抽象的，是单态模式的，且lazy-init属性配置为false
        if (!bd.isAbstract() && bd.isSingleton() && !bd.isLazyInit()) {
            //如果指定名称的bean是创建容器的Bean
            if (isFactoryBean(beanName)) {
                //FACTORY_BEAN_PREFIX=”&”，当Bean名称前面加”&”符号
                //时，获取的是产生容器对象本身，而不是容器产生的Bean.
                //调用getBean方法，触发容器对Bean实例化和依赖注入过程
                final FactoryBean<?> factory = (FactoryBean<?>) getBean(FACTORY_BEAN_PREFIX + beanName);
                //标识是否需要预实例化
                boolean isEagerInit;
                if (System.getSecurityManager() != null && factory instanceof SmartFactoryBean) {
                    //一个匿名内部类
                    isEagerInit = AccessController.doPrivileged((PrivilegedAction<Boolean>) () ->
                                                                ((SmartFactoryBean<?>) factory).isEagerInit(),
                                                                getAccessControlContext());
                }
                else {
                    isEagerInit = (factory instanceof SmartFactoryBean &&
                                   ((SmartFactoryBean<?>) factory).isEagerInit());
                }
                if (isEagerInit) {
                    //调用getBean方法，触发容器对Bean实例化和依赖注入过程
                    getBean(beanName);
                }
            }
            else {
                getBean(beanName);
            }
        }
    }

    // Trigger post-initialization callback for all applicable beans...
    for (String beanName : beanNames) {
        Object singletonInstance = getSingleton(beanName);
        if (singletonInstance instanceof SmartInitializingSingleton) {
            final SmartInitializingSingleton smartSingleton = (SmartInitializingSingleton) singletonInstance;
            if (System.getSecurityManager() != null) {
                AccessController.doPrivileged((PrivilegedAction<Object>) () -> {
                    smartSingleton.afterSingletonsInstantiated();
                    return null;
                }, getAccessControlContext());
            }
            else {
                smartSingleton.afterSingletonsInstantiated();
            }
        }
    }
}

```

​		通过对 lazy-init 处理源码的分析，我们可以看出，如果设置了lazy-init属性，则容器在完成 Bean 呈义的注册之后，会通过getBean方法，触发对指定Bean的初始化和依赖注入过程，这样当应用第一次向容器索取所需的Bean时，容器不再需要对Bean进行初始化和依赖注入，直接从已经完成实例化和依赖注入的Bean中取一个现成的 Bean，这样就提高了第一次获取 Bean的性能。

#### 2.5.9.2关于FactoryBean和 BeanFactory

​		在Spring中，有两个很容易混淆的类∶BeanFactory和 FactoryBean。

​		BeanFactory∶Bean工厂，是一个工厂（Factory），我们Spring loC容器的最顶层接口就是这个BeanFactory，它的作用是管理 Bean，即实例化、定位、配置应用程序中的对象及建立这些对象间的依赖。

​		FactoryBean∶工厂 Bean，是一个Bean，作用是产生其他 bean 实例。通常情况下，这种Bean 没有什么特别的要求，仅需要提供一个工厂方法，该方法用来返回其他 Bean实例。通常情况下，Bean 无须自己实现工厂模式，Spring容器担任工厂角色;但少数情况下，容器中的Bean 本身就是工厂，其作用是产生其它 Bean实例。

​		当用户使用容器本身时可以使用转义字符"&"来得到 FactoryBean本身以区别通过FactoryBean 产生的实例对象和 FactoryBean对象本身。在 BeanFactory 中通过如下代码定义了该转义字符∶String FACTORYBEANPREFIX ="&";

如果myJndiObject是一个FactoryBean，则使用&myJndiObject得到的是myIndiobject对象而不是myJndiObject产生出来的对象。

**1、Factory Bean源码∶**

```java
//工厂Bean，用于产生其他对象
public interface FactoryBean<T> {

	//获取容器管理的对象实例
	@Nullable
	T getObject() throws Exception;

	//获取Bean工厂创建的对象的类型
	@Nullable
	Class<?> getObjectType();

	//Bean工厂创建的对象是否是单态模式，如果是单态模式，则整个容器中只有一个实例
	//对象，每次请求都返回同一个实例对象
	default boolean isSingleton() {
		return true;
	}

}

```

**2、AbstractBeanFactory的 getBean（）方法调用 FactoryBean∶**

​		在前面我们分析 Spring loC容器实例化 Bean并进行依赖注入过程的源码时，提到在getBean（）方法触发容器实例化 Bean 的时候会调用AbstractBeanFactory的doGetBean（）方法来进行实例化的过程，源码如下∶

```java
@SuppressWarnings("unchecked")
//真正实现向IOC容器获取Bean的功能，也是触发依赖注入功能的地方
protected <T> T doGetBean(final String name, @Nullable final Class<T> requiredType,
                          @Nullable final Object[] args, boolean typeCheckOnly) throws BeansException {

    //根据指定的名称获取被管理Bean的名称，剥离指定名称中对容器的相关依赖
    //如果指定的是别名，将别名转换为规范的Bean名称
    final String beanName = transformedBeanName(name);
    Object bean;

    // Eagerly check singleton cache for manually registered singletons.
    //先从缓存中取是否已经有被创建过的单态类型的Bean
    //对于单例模式的Bean整个IOC容器中只创建一次，不需要重复创建
    Object sharedInstance = getSingleton(beanName);
    //IOC容器创建单例模式Bean实例对象
    if (sharedInstance != null && args == null) {
        if (logger.isDebugEnabled()) {
            //如果指定名称的Bean在容器中已有单例模式的Bean被创建
            //直接返回已经创建的Bean
            if (isSingletonCurrentlyInCreation(beanName)) {
                logger.debug("Returning eagerly cached instance of singleton bean '" + beanName +
                             "' that is not fully initialized yet - a consequence of a circular reference");
            }
            else {
                logger.debug("Returning cached instance of singleton bean '" + beanName + "'");
            }
        }
        //获取给定Bean的实例对象，主要是完成FactoryBean的相关处理
        //注意：BeanFactory是管理容器中Bean的工厂，而FactoryBean是
        //创建创建对象的工厂Bean，两者之间有区别
        bean = getObjectForBeanInstance(sharedInstance, name, beanName, null);
    }

    else {
        // Fail if we're already creating this bean instance:
        // We're assumably within a circular reference.
        //缓存没有正在创建的单例模式Bean
        //缓存中已经有已经创建的原型模式Bean
        //但是由于循环引用的问题导致实例化对象失败
        if (isPrototypeCurrentlyInCreation(beanName)) {
            throw new BeanCurrentlyInCreationException(beanName);
        }

        // Check if bean definition exists in this factory.
        //对IOC容器中是否存在指定名称的BeanDefinition进行检查，首先检查是否
        //能在当前的BeanFactory中获取的所需要的Bean，如果不能则委托当前容器
        //的父级容器去查找，如果还是找不到则沿着容器的继承体系向父级容器查找
        BeanFactory parentBeanFactory = getParentBeanFactory();
        //当前容器的父级容器存在，且当前容器中不存在指定名称的Bean
        if (parentBeanFactory != null && !containsBeanDefinition(beanName)) {
            // Not found -> check parent.
            //解析指定Bean名称的原始名称
            String nameToLookup = originalBeanName(name);
            if (parentBeanFactory instanceof AbstractBeanFactory) {
                return ((AbstractBeanFactory) parentBeanFactory).doGetBean(
                    nameToLookup, requiredType, args, typeCheckOnly);
            }
            else if (args != null) {
                // Delegation to parent with explicit args.
                //委派父级容器根据指定名称和显式的参数查找
                return (T) parentBeanFactory.getBean(nameToLookup, args);
            }
            else {
                // No args -> delegate to standard getBean method.
                //委派父级容器根据指定名称和类型查找
                return parentBeanFactory.getBean(nameToLookup, requiredType);
            }
        }

        //创建的Bean是否需要进行类型验证，一般不需要
        if (!typeCheckOnly) {
            //向容器标记指定的Bean已经被创建
            markBeanAsCreated(beanName);
        }

        try {
            //根据指定Bean名称获取其父级的Bean定义
            //主要解决Bean继承时子类合并父类公共属性问题
            final RootBeanDefinition mbd = getMergedLocalBeanDefinition(beanName);
            checkMergedBeanDefinition(mbd, beanName, args);

            // Guarantee initialization of beans that the current bean depends on.
            //获取当前Bean所有依赖Bean的名称
            String[] dependsOn = mbd.getDependsOn();
            //如果当前Bean有依赖Bean
            if (dependsOn != null) {
                for (String dep : dependsOn) {
                    if (isDependent(beanName, dep)) {
                        throw new BeanCreationException(mbd.getResourceDescription(), beanName,
                                                        "Circular depends-on relationship between '" + beanName + "' and '" + dep + "'");
                    }
                    //递归调用getBean方法，获取当前Bean的依赖Bean
                    registerDependentBean(dep, beanName);
                    //把被依赖Bean注册给当前依赖的Bean
                    getBean(dep);
                }
            }

            // Create bean instance.
            //创建单例模式Bean的实例对象
            if (mbd.isSingleton()) {
                //这里使用了一个匿名内部类，创建Bean实例对象，并且注册给所依赖的对象
                sharedInstance = getSingleton(beanName, () -> {
                    try {
                        //创建一个指定Bean实例对象，如果有父级继承，则合并子类和父类的定义
                        return createBean(beanName, mbd, args);
                    }
                    catch (BeansException ex) {
                        // Explicitly remove instance from singleton cache: It might have been put there
                        // eagerly by the creation process, to allow for circular reference resolution.
                        // Also remove any beans that received a temporary reference to the bean.
                        //显式地从容器单例模式Bean缓存中清除实例对象
                        destroySingleton(beanName);
                        throw ex;
                    }
                });
                //获取给定Bean的实例对象
                bean = getObjectForBeanInstance(sharedInstance, name, beanName, mbd);
            }

            //IOC容器创建原型模式Bean实例对象
            else if (mbd.isPrototype()) {
                // It's a prototype -> create a new instance.
                //原型模式(Prototype)是每次都会创建一个新的对象
                Object prototypeInstance = null;
                try {
                    //回调beforePrototypeCreation方法，默认的功能是注册当前创建的原型对象
                    beforePrototypeCreation(beanName);
                    //创建指定Bean对象实例
                    prototypeInstance = createBean(beanName, mbd, args);
                }
                finally {
                    //回调afterPrototypeCreation方法，默认的功能告诉IOC容器指定Bean的原型对象不再创建
                    afterPrototypeCreation(beanName);
                }
                //获取给定Bean的实例对象
                bean = getObjectForBeanInstance(prototypeInstance, name, beanName, mbd);
            }

            //要创建的Bean既不是单例模式，也不是原型模式，则根据Bean定义资源中
            //配置的生命周期范围，选择实例化Bean的合适方法，这种在Web应用程序中
            //比较常用，如：request、session、application等生命周期
            else {
                String scopeName = mbd.getScope();
                final Scope scope = this.scopes.get(scopeName);
                //Bean定义资源中没有配置生命周期范围，则Bean定义不合法
                if (scope == null) {
                    throw new IllegalStateException("No Scope registered for scope name '" + scopeName + "'");
                }
                try {
                    //这里又使用了一个匿名内部类，获取一个指定生命周期范围的实例
                    Object scopedInstance = scope.get(beanName, () -> {
                        beforePrototypeCreation(beanName);
                        try {
                            return createBean(beanName, mbd, args);
                        }
                        finally {
                            afterPrototypeCreation(beanName);
                        }
                    });
                    //获取给定Bean的实例对象
                    bean = getObjectForBeanInstance(scopedInstance, name, beanName, mbd);
                }
                catch (IllegalStateException ex) {
                    throw new BeanCreationException(beanName,
                                                    "Scope '" + scopeName + "' is not active for the current thread; consider " +
                                                    "defining a scoped proxy for this bean if you intend to refer to it from a singleton",
                                                    ex);
                }
            }
        }
        catch (BeansException ex) {
            cleanupAfterBeanCreationFailure(beanName);
            throw ex;
        }
    }

    // Check if required type matches the type of the actual bean instance.
    //对创建的Bean实例对象进行类型检查
    if (requiredType != null && !requiredType.isInstance(bean)) {
        try {
            T convertedBean = getTypeConverter().convertIfNecessary(bean, requiredType);
            if (convertedBean == null) {
                throw new BeanNotOfRequiredTypeException(name, requiredType, bean.getClass());
            }
            return convertedBean;
        }
        catch (TypeMismatchException ex) {
            if (logger.isDebugEnabled()) {
                logger.debug("Failed to convert bean '" + name + "' to required type '" +
                             ClassUtils.getQualifiedName(requiredType) + "'", ex);
            }
            throw new BeanNotOfRequiredTypeException(name, requiredType, bean.getClass());
        }
    }
    return (T) bean;
}
//获取给定Bean的实例对象，主要是完成FactoryBean的相关处理
protected Object getObjectForBeanInstance(
    Object beanInstance, String name, String beanName, @Nullable RootBeanDefinition mbd) {

    // Don't let calling code try to dereference the factory if the bean isn't a factory.
    //容器已经得到了Bean实例对象，这个实例对象可能是一个普通的Bean，
    //也可能是一个工厂Bean，如果是一个工厂Bean，则使用它创建一个Bean实例对象，
    //如果调用本身就想获得一个容器的引用，则指定返回这个工厂Bean实例对象
    //如果指定的名称是容器的解引用(dereference，即是对象本身而非内存地址)，
    //且Bean实例也不是创建Bean实例对象的工厂Bean
    if (BeanFactoryUtils.isFactoryDereference(name) && !(beanInstance instanceof FactoryBean)) {
        throw new BeanIsNotAFactoryException(transformedBeanName(name), beanInstance.getClass());
    }

    // Now we have the bean instance, which may be a normal bean or a FactoryBean.
    // If it's a FactoryBean, we use it to create a bean instance, unless the
    // caller actually wants a reference to the factory.
    //如果Bean实例不是工厂Bean，或者指定名称是容器的解引用，
    //调用者向获取对容器的引用，则直接返回当前的Bean实例
    if (!(beanInstance instanceof FactoryBean) || BeanFactoryUtils.isFactoryDereference(name)) {
        return beanInstance;
    }

    //处理指定名称不是容器的解引用，或者根据名称获取的Bean实例对象是一个工厂Bean
    //使用工厂Bean创建一个Bean的实例对象
    Object object = null;
    if (mbd == null) {
        //从Bean工厂缓存中获取给定名称的Bean实例对象
        object = getCachedObjectForFactoryBean(beanName);
    }
    //让Bean工厂生产给定名称的Bean对象实例
    if (object == null) {
        // Return bean instance from factory.
        FactoryBean<?> factory = (FactoryBean<?>) beanInstance;
        // Caches object obtained from FactoryBean if it is a singleton.
        //如果从Bean工厂生产的Bean是单态模式的，则缓存
        if (mbd == null && containsBeanDefinition(beanName)) {
            //从容器中获取指定名称的Bean定义，如果继承基类，则合并基类相关属性
            mbd = getMergedLocalBeanDefinition(beanName);
        }
        //如果从容器得到Bean定义信息，并且Bean定义信息不是虚构的，
        //则让工厂Bean生产Bean实例对象
        boolean synthetic = (mbd != null && mbd.isSynthetic());
        //调用FactoryBeanRegistrySupport类的getObjectFromFactoryBean方法，
        //实现工厂Bean生产Bean对象实例的过程
        object = getObjectFromFactoryBean(factory, beanName, !synthetic);
    }
    return object;
}
```

​		在上面获取给定 Bean 的实例对象的 getObjectForBeanlnstance（）方法中，会 调用FactoryBeanRegistrySupport类的getObjectFromFactoryBean()方法，该方法实现了Bean 工厂生产 Bean 实例对象。

​		Dereference（解引用）∶一个在C/C++中应用比较多的术语，在C++中，"'"是解引用符号，而"&"是引用符号，解引用是指变量指向的是所引用对象的本身数据，而不是引用对象的内存地址。

**3、AbstractBeanFactory 生产 Bean实例对象**

​		AbstractBeanFactory类中生产Bean实例对象的主要源码如下∶

FactoryBeanRegistrySupport

```java
//Bean工厂生产Bean实例对象
protected Object getObjectFromFactoryBean(FactoryBean<?> factory, String beanName, boolean shouldPostProcess) {
    //Bean工厂是单态模式，并且Bean工厂缓存中存在指定名称的Bean实例对象
    if (factory.isSingleton() && containsSingleton(beanName)) {
        //多线程同步，以防止数据不一致
        synchronized (getSingletonMutex()) {
            //直接从Bean工厂缓存中获取指定名称的Bean实例对象
            Object object = this.factoryBeanObjectCache.get(beanName);
            //Bean工厂缓存中没有指定名称的实例对象，则生产该实例对象
            if (object == null) {
                //调用Bean工厂的getObject方法生产指定Bean的实例对象
                object = doGetObjectFromFactoryBean(factory, beanName);
                // Only post-process and store if not put there already during getObject() call above
                // (e.g. because of circular reference processing triggered by custom getBean calls)
                Object alreadyThere = this.factoryBeanObjectCache.get(beanName);
                if (alreadyThere != null) {
                    object = alreadyThere;
                }
                else {
                    if (shouldPostProcess) {
                        try {
                            object = postProcessObjectFromFactoryBean(object, beanName);
                        }
                        catch (Throwable ex) {
                            throw new BeanCreationException(beanName,
                                                            "Post-processing of FactoryBean's singleton object failed", ex);
                        }
                    }
                    //将生产的实例对象添加到Bean工厂缓存中
                    this.factoryBeanObjectCache.put(beanName, object);
                }
            }
            return object;
        }
    }
    //调用Bean工厂的getObject方法生产指定Bean的实例对象
    else {
        Object object = doGetObjectFromFactoryBean(factory, beanName);
        if (shouldPostProcess) {
            try {
                object = postProcessObjectFromFactoryBean(object, beanName);
            }
            catch (Throwable ex) {
                throw new BeanCreationException(beanName, "Post-processing of FactoryBean's object failed", ex);
            }
        }
        return object;
    }
}

/**
	 * Obtain an object to expose from the given FactoryBean.
	 * @param factory the FactoryBean instance
	 * @param beanName the name of the bean
	 * @return the object obtained from the FactoryBean
	 * @throws BeanCreationException if FactoryBean object creation failed
	 * @see org.springframework.beans.factory.FactoryBean#getObject()
	 */
//调用Bean工厂的getObject方法生产指定Bean的实例对象
private Object doGetObjectFromFactoryBean(final FactoryBean<?> factory, final String beanName)
    throws BeanCreationException {

    Object object;
    try {
        if (System.getSecurityManager() != null) {
            AccessControlContext acc = getAccessControlContext();
            try {
                //实现PrivilegedExceptionAction接口的匿名内置类
                //根据JVM检查权限，然后决定BeanFactory创建实例对象
                object = AccessController.doPrivileged((PrivilegedExceptionAction<Object>) () ->
                                                       factory.getObject(), acc);
            }
            catch (PrivilegedActionException pae) {
                throw pae.getException();
            }
        }
        else {
            //调用BeanFactory接口实现类的创建对象方法
            object = factory.getObject();
        }
    }
    catch (FactoryBeanNotInitializedException ex) {
        throw new BeanCurrentlyInCreationException(beanName, ex.toString());
    }
    catch (Throwable ex) {
        throw new BeanCreationException(beanName, "FactoryBean threw exception on object creation", ex);
    }

    // Do not accept a null value for a FactoryBean that's not fully
    // initialized yet: Many FactoryBeans just return null then.
    //创建出来的实例对象为null，或者因为单态对象正在创建而返回null
    if (object == null) {
        if (isSingletonCurrentlyInCreation(beanName)) {
            throw new BeanCurrentlyInCreationException(
                beanName, "FactoryBean which is currently in creation returned null from getObject");
        }
        object = new NullBean();
    }
    return object;
}
```

​		面的源码分析中，我们可以看出，BeanFactory接口调用其实现类的getObject方法来实现创建 Bean 实例对象的功能。

**4、工厂 Bean的实现类 getObject 方法创建 Bean 实例对象**

​		FactoryBean的实现类有非常多，比如∶Proxy、RMI、JNDI、ServletContextFactoryBean等等，FactoryBean接口为Spring 容器提供了一个很好的封装机制，具体的getObject（）有不同的实现类根据不同的实现策略来具体提供，我们分析一个最简单的AnnotationTestBeanFactory的实现源码∶

```java
public class AnnotationTestBeanFactory implements FactoryBean<FactoryCreatedAnnotationTestBean> {

	private final FactoryCreatedAnnotationTestBean instance = new FactoryCreatedAnnotationTestBean();

	public AnnotationTestBeanFactory() {
		this.instance.setName("FACTORY");
	}

	@Override
	public FactoryCreatedAnnotationTestBean getObject() throws Exception {
		return this.instance;
	}

	//AnnotationTestBeanFactory产生Bean实例对象的实现
	@Override
	public Class<? extends IJmxTestBean> getObjectType() {
		return FactoryCreatedAnnotationTestBean.class;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}

}
```

​		其他的Proxy，RMI，JNDl等等，都是根据相应的策略提供getObject（的实现。这里不做——分析，这已经不是Spring的核心功能，感兴趣的小伙可以再去深入研究。

### 2.5.10再述 autowiring

​		Spring loC容器提供了两种管理 Bean 依赖关系的方式∶

1. 显式管理∶通过 BeanDefinition的属性值和构造方法实现 Bean依赖关系管理。
2. autowiring∶Spring loC容器的依赖自动装配功能，不需要对Bean属性的依赖关系做显式的声明只需要在配置好autowiring属性，loC容器会自动使用反射查找属性的类型和名称，然后基于属性的类型或者名称来自动匹配容器中管理的 Bean，从而自动地完成依赖注入。

​		通过对 autowiring自动装配特性的理解，我们知道容器对 Bean的自动装配发生在容器对 Bean依赖注入的过程中。在前面对Spring loC容器的依赖注入过程源码分析中，我们已经知道了容器对Bean 实例对象的属性注入的处理发生在AbstractAutoWireCapableBeanFactory类中的populateBean()方法中，我们通过程序流程分析 autowiring的实现原理∶

**1、AbstractAutoWireCapableBeanFactory 对 Bean 实例进行属性依赖注入**

​		应用第一次通过getBean（）方法（配置了lazy-init预实例化属性的除外）向 loC容器索取 Bean时，容 器 创建 Bean 实 例对 象，并 且 对 Bean 实 例对 象 进行 属 性 依 赖 注 入AbstractAutoWireCapableBeanFactory的populateBean（）方法就是实现 Bean属性依赖注入的功能，其主要源码如下∶

```java
//将Bean属性设置到生成的实例对象上
protected void populateBean(String beanName, RootBeanDefinition mbd, @Nullable BeanWrapper bw) {
    if (bw == null) {
        if (mbd.hasPropertyValues()) {
            throw new BeanCreationException(
                mbd.getResourceDescription(), beanName, "Cannot apply property values to null instance");
        }
        else {
            // Skip property population phase for null instance.
            return;
        }
    }

    // Give any InstantiationAwareBeanPostProcessors the opportunity to modify the
    // state of the bean before properties are set. This can be used, for example,
    // to support styles of field injection.
    boolean continueWithPropertyPopulation = true;

    if (!mbd.isSynthetic() && hasInstantiationAwareBeanPostProcessors()) {
        for (BeanPostProcessor bp : getBeanPostProcessors()) {
            if (bp instanceof InstantiationAwareBeanPostProcessor) {
                InstantiationAwareBeanPostProcessor ibp = (InstantiationAwareBeanPostProcessor) bp;
                if (!ibp.postProcessAfterInstantiation(bw.getWrappedInstance(), beanName)) {
                    continueWithPropertyPopulation = false;
                    break;
                }
            }
        }
    }

    if (!continueWithPropertyPopulation) {
        return;
    }
    //获取容器在解析Bean定义资源时为BeanDefiniton中设置的属性值
    PropertyValues pvs = (mbd.hasPropertyValues() ? mbd.getPropertyValues() : null);

    //对依赖注入处理，首先处理autowiring自动装配的依赖注入
    if (mbd.getResolvedAutowireMode() == RootBeanDefinition.AUTOWIRE_BY_NAME ||
        mbd.getResolvedAutowireMode() == RootBeanDefinition.AUTOWIRE_BY_TYPE) {
        MutablePropertyValues newPvs = new MutablePropertyValues(pvs);

        // Add property values based on autowire by name if applicable.
        //根据Bean名称进行autowiring自动装配处理
        if (mbd.getResolvedAutowireMode() == RootBeanDefinition.AUTOWIRE_BY_NAME) {
            autowireByName(beanName, mbd, bw, newPvs);
        }

        // Add property values based on autowire by type if applicable.
        //根据Bean类型进行autowiring自动装配处理
        if (mbd.getResolvedAutowireMode() == RootBeanDefinition.AUTOWIRE_BY_TYPE) {
            autowireByType(beanName, mbd, bw, newPvs);
        }

        pvs = newPvs;
    }

    //对非autowiring的属性进行依赖注入处理
    boolean hasInstAwareBpps = hasInstantiationAwareBeanPostProcessors();
    boolean needsDepCheck = (mbd.getDependencyCheck() != RootBeanDefinition.DEPENDENCY_CHECK_NONE);

    if (hasInstAwareBpps || needsDepCheck) {
        if (pvs == null) {
            pvs = mbd.getPropertyValues();
        }
        PropertyDescriptor[] filteredPds = filterPropertyDescriptorsForDependencyCheck(bw, mbd.allowCaching);
        if (hasInstAwareBpps) {
            for (BeanPostProcessor bp : getBeanPostProcessors()) {
                if (bp instanceof InstantiationAwareBeanPostProcessor) {
                    InstantiationAwareBeanPostProcessor ibp = (InstantiationAwareBeanPostProcessor) bp;
                    pvs = ibp.postProcessPropertyValues(pvs, filteredPds, bw.getWrappedInstance(), beanName);
                    if (pvs == null) {
                        return;
                    }
                }
            }
        }
        if (needsDepCheck) {
            checkDependencies(beanName, mbd, filteredPds, pvs);
        }
    }

    if (pvs != null) {
        //对属性进行注入
        applyPropertyValues(beanName, mbd, bw, pvs);
    }
}
```

**2、Spring loC容器根据Bean名称或者类型进行 autowiring 自动依赖注入**

```java
//根据名称对属性进行自动依赖注入
protected void autowireByName(
    String beanName, AbstractBeanDefinition mbd, BeanWrapper bw, MutablePropertyValues pvs) {

    //对Bean对象中非简单属性(不是简单继承的对象，如8中原始类型，字符串，URL等都是简单属性)进行处理
    String[] propertyNames = unsatisfiedNonSimpleProperties(mbd, bw);
    for (String propertyName : propertyNames) {
        //如果Spring IOC容器中包含指定名称的Bean
        if (containsBean(propertyName)) {
            //调用getBean方法向IOC容器索取指定名称的Bean实例，迭代触发属性的初始化和依赖注入
            Object bean = getBean(propertyName);
            //为指定名称的属性赋予属性值
            pvs.add(propertyName, bean);
            //指定名称属性注册依赖Bean名称，进行属性依赖注入
            registerDependentBean(propertyName, beanName);
            if (logger.isDebugEnabled()) {
                logger.debug("Added autowiring by name from bean name '" + beanName +
                             "' via property '" + propertyName + "' to bean named '" + propertyName + "'");
            }
        }
        else {
            if (logger.isTraceEnabled()) {
                logger.trace("Not autowiring property '" + propertyName + "' of bean '" + beanName +
                             "' by name: no matching bean found");
            }
        }
    }
}

/**
	 * Abstract method defining "autowire by type" (bean properties by type) behavior.
	 * <p>This is like PicoContainer default, in which there must be exactly one bean
	 * of the property type in the bean factory. This makes bean factories simple to
	 * configure for small namespaces, but doesn't work as well as standard Spring
	 * behavior for bigger applications.
	 * @param beanName the name of the bean to autowire by type
	 * @param mbd the merged bean definition to update through autowiring
	 * @param bw BeanWrapper from which we can obtain information about the bean
	 * @param pvs the PropertyValues to register wired objects with
	 */
//根据类型对属性进行自动依赖注入
protected void autowireByType(
    String beanName, AbstractBeanDefinition mbd, BeanWrapper bw, MutablePropertyValues pvs) {

    //获取用户定义的类型转换器
    TypeConverter converter = getCustomTypeConverter();
    if (converter == null) {
        converter = bw;
    }

    //存放解析的要注入的属性
    Set<String> autowiredBeanNames = new LinkedHashSet<>(4);
    //对Bean对象中非简单属性(不是简单继承的对象，如8中原始类型，字符
    //URL等都是简单属性)进行处理
    String[] propertyNames = unsatisfiedNonSimpleProperties(mbd, bw);
    for (String propertyName : propertyNames) {
        try {
            //获取指定属性名称的属性描述器
            PropertyDescriptor pd = bw.getPropertyDescriptor(propertyName);
            // Don't try autowiring by type for type Object: never makes sense,
            // even if it technically is a unsatisfied, non-simple property.
            //不对Object类型的属性进行autowiring自动依赖注入
            if (Object.class != pd.getPropertyType()) {
                //获取属性的setter方法
                MethodParameter methodParam = BeanUtils.getWriteMethodParameter(pd);
                // Do not allow eager init for type matching in case of a prioritized post-processor.
                //检查指定类型是否可以被转换为目标对象的类型
                boolean eager = !PriorityOrdered.class.isInstance(bw.getWrappedInstance());
                //创建一个要被注入的依赖描述
                DependencyDescriptor desc = new AutowireByTypeDependencyDescriptor(methodParam, eager);
                //根据容器的Bean定义解析依赖关系，返回所有要被注入的Bean对象
                Object autowiredArgument = resolveDependency(desc, beanName, autowiredBeanNames, converter);
                if (autowiredArgument != null) {
                    //为属性赋值所引用的对象
                    pvs.add(propertyName, autowiredArgument);
                }
                for (String autowiredBeanName : autowiredBeanNames) {
                    //指定名称属性注册依赖Bean名称，进行属性依赖注入
                    registerDependentBean(autowiredBeanName, beanName);
                    if (logger.isDebugEnabled()) {
                        logger.debug("Autowiring by type from bean name '" + beanName + "' via property '" +
                                     propertyName + "' to bean named '" + autowiredBeanName + "'");
                    }
                }
                //释放已自动注入的属性
                autowiredBeanNames.clear();
            }
        }
        catch (BeansException ex) {
            throw new UnsatisfiedDependencyException(mbd.getResourceDescription(), beanName, propertyName, ex);
        }
    }
}
```

​		通过上面的源码分析，我们可以看出来通过属性名进行自动依赖注入的相对比通过属性类型进行自动依赖注入要稍微简单一些，但是真正实现属性注入的是 DefaultSingletonBeanRegistry类的registerDependentBean（）方法。

**3、DefaultSingletonBeanRegistry的 registerDependentBean（）方法对属性注入**

```java
//为指定的Bean注入依赖的Bean
public void registerDependentBean(String beanName, String dependentBeanName) {
    // A quick check for an existing entry upfront, avoiding synchronization...
    //处理Bean名称，将别名转换为规范的Bean名称
    String canonicalName = canonicalName(beanName);
    Set<String> dependentBeans = this.dependentBeanMap.get(canonicalName);
    if (dependentBeans != null && dependentBeans.contains(dependentBeanName)) {
        return;
    }

    // No entry yet -> fully synchronized manipulation of the dependentBeans Set
    //多线程同步，保证容器内数据的一致性
    //先从容器中：bean名称-->全部依赖Bean名称集合找查找给定名称Bean的依赖Bean
    synchronized (this.dependentBeanMap) {
        //获取给定名称Bean的所有依赖Bean名称
        dependentBeans = this.dependentBeanMap.get(canonicalName);
        if (dependentBeans == null) {
            //为Bean设置依赖Bean信息
            dependentBeans = new LinkedHashSet<>(8);
            this.dependentBeanMap.put(canonicalName, dependentBeans);
        }
        //向容器中：bean名称-->全部依赖Bean名称集合添加Bean的依赖信息
        //即，将Bean所依赖的Bean添加到容器的集合中
        dependentBeans.add(dependentBeanName);
    }
    //从容器中：bean名称-->指定名称Bean的依赖Bean集合找查找给定名称Bean的依赖Bean
    synchronized (this.dependenciesForBeanMap) {
        Set<String> dependenciesForBean = this.dependenciesForBeanMap.get(dependentBeanName);
        if (dependenciesForBean == null) {
            dependenciesForBean = new LinkedHashSet<>(8);
            this.dependenciesForBeanMap.put(dependentBeanName, dependenciesForBean);
        }
        //向容器中：bean名称-->指定Bean的依赖Bean名称集合添加Bean的依赖信息
        //即，将Bean所依赖的Bean添加到容器的集合中
        dependenciesForBean.add(canonicalName);
    }
}
```

​		通过对 autowiring的源码分析，我们可以看出，autowiring的实现过程∶

a、对 Bean的属性代调用 getBean（）方法，完成依赖 Bean 的初始化和依赖注入。
b、将依赖 Bean的属性引用设置到被依赖的Bean属性上。
c、将依赖 Bean 的名称和被依赖 Bean 的名称存储在 loC容器的集合中。

​		Spring loC容器的autowiring属性自动依赖注入是一个很方便的特性，可以简化开发时的配置，旦是凡是都有两面性，自动属性依赖注入也有不足，首先，Bean的依赖关系在 配置文件中无法很清楚地看出来，对于维护造成一定困难。其次，由于自动依赖注入是Spring容器自动执行的，容器是不会智能判断的，如果配置不当，将会带来无法预料的后果，所以自动依赖注入特性在使用时还是综合考虑。

# 3胜多负少