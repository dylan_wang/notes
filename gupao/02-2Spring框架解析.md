## 2.6 Spring AOP

### 2.6.1 AOP应用场景

​		AOP是OOP的延续，是Aspect Oriented Programming的缩写，意思是面向切面编程。可以通过预编译方式和运行期动态代理实现在不修改源代码的情况下给程序动态统一添加功能的一种技术。AOP 设计模式孜孜不倦追求的是调用者和被调用者之间的解耦，AOP可以说也是这种目标的一种实现。我们现在做的一些非业务，如∶日志、事务、安全等都会写在业务代码中（也即是说，这些非业务类横切于业务类），但这些代码往往是重复，复制——粘贴式的代码会给程序的维护带来不便，AOP就实现了把这些业务需求与系统需求分开来做。这种解决的方式也称代理机制。

### 2.6.2 AOP基础概念

**1、切面（Aspect）**

​		抽象定义为"一个关注点的模块化，这个关注点可能会横切多个对象"。"切面"在ApplicationContext 中`<aop:aspect>`来配置。

​		连接点（Joinpoint）∶程序执行过程中的某一行为，例如，MemberService.get的调用或者MemberService.delete抛出异常等行为。

**2、通知（Advice）**

​		"切面"对于某个"连接点"所产生的动作。其中，一个"切面"可以包含多个"Advice"。

**3、切入点（Pointcut）**

​		匹配连接点的断言，在AOP中通知和一个切入点表达式关联。切面中的所有通知所关注的连接点，由切入点表达式来决定。

**4、目标对象（Target Object）**

​		被一个或者多个切面所通知的对象。例如，AServcielmpl和BServicelmpl，当然在实际运行时，Spring AOP采用代理实现，实际AOP操作的是 TargetObject的代理对象。

**5、AOP代理（AOP Proxy）**

​		Spring AOP中有两种代理方式，JDK 动态代理和CGLib代理。默认情况下，TargetObject 实现了接口时，则采用 JDK动态代理，例如，AServicelmpl;反之，采用CGLib代理，例如，BServicelmpl。强制使用CGLib代理需要将`<aop:config>`的proxy-target-class属性设为 true。

**6、前置通知（Before Advice）**

​		在某连接点（JoinPoint）之前执行的通知，但这个通知不能阻止连接点前的执行。ApplicationContext中在`<aop:aspect>`里面使用`<aop:before>`元素进行声明。例如，TestAspect中的 doBefore 方法。

**7、后置通知（After Advice）**

​		当某连接点退出的时候执行的通知（不论是正常返回还是异常退出）。ApplicationContext中在`<aop:aspect>`里面使用`<aop:after>`元素进行声明。例如，ServiceAspect中的 returnAfter方法，所以Teser 中调用 UserService.delete抛出异常时，returnAfter 方法仍然执行。

**8、返回后通知（After Return Advice）**

​		某连接点正常完成后执行的通知，不包括抛出异常的情况。ApplicationContext 中在`<aop:aspect>`里面使用`<after-returning>`元素进行声明。

**9、环绕通知（Around Advice）**

​		包围一个连接点的通知，类似Web中 Servlet规范中的Filter的doFilter方法。可以在方法的调用前后完成自定义的行为，也可以选择不执行。ApplicationContext中在`<aop:aspect>`里面使用`<aop:around>`元素进行声明。例如，ServiceAspect 中的 around方法。

**10、异常通知（After Throwing Advice）**

​		在方法抛出异常退出时执行的通知。ApplicationContext中在`<aop:aspect>`里面使用`<aop:after-throwing>`元素进行声明。例如，ServiceAspect中的 returnThrow方法。

​		注∶可以将多个通知应用到一个目标对象上，即可以将多个切面织入到同一目标对象。

### 2.6.3 AOP配置

​		使用SpringAOP可以基于两种方式，一种是比较方便和强大的注解方式，另一种则是中规中矩的xml 配置方式。

​		先说注解，使用注解配置Spring AOP总体分为两步，第一步是在xml文件中声明激活自动扫描组件功能，同时激活自动代理功能（来测试 AOP的注解功能）∶

```xml

```

第二步是为 Aspect切面类添加注解∶

```java

```

测试

```java

```

控制台输出

```log
=这是一条华丽的分割线=
com.gupaoedu.aop.aspect. AnnotaionAspect@6ef714ao
[INFO ][13:04:46] com.gupaoedu.aop.aspect.AnnotaionAspect- before execution(void com.gupaoedu.aop.service.MemberService.save(Member))
[INFO][13:04:46] com.gupaoedu.aop.aspect.ArgsAspect- beforeArgUser execution(void com.gupaoedu.aop.service.MemberService.save(Member))
[INFO][13:04:46] com.gupaoedu.aop.aspect.AnnotaionAspect - save member Method.. 
[INFO][13:04:46]com.gupaoedu.aop.aspect.AnnotaionAspect - around execution(void com.gupaoedu.aop.service.MemberService.save(Member)) Use time :38 ms!
[INFO][13:04:46]com.gupaoedu.aop.aspect.AnnotaionAspect-after execution(void com.gupaoedu.aop.service.MemberService.save(Member))
[INFO][13:04:46] com.gupaoedu.aop.aspect.AnnotaionAspect-afterReturn execution(void com.gupaoedu.aop.service. MemberService.save(Member)
 ====这是一条华丽的分割线==
[INFO][13:04:46] com.gupaoedu.aop.aspect.AnnotaionAspect-before execution(boolean com.gupaoedu.aop.service.MemberService.delete(long))
[INFO][13:04:46] com.gupaoedu.aop.aspect.ArgsAspect- beforeArgId execution(boolean com.gupaoedu.aop.service.MemberService.delete(long))ID:1
[INFO][13:04:46] com.gupaoedu.aop.aspect.AnnotaionAspect-delete Method .
[INFO][13:04:46] com.gupaoedu.aop.aspect.AnnotaionAspect- around execution(boolean
com.gupaoedu.aop.service.MemberService.delete（long）） Use time ∶3 ms with exception∶spring aop ThrowAdvice演示
[INFO][13:04:46]com.gupaoedu.aop.aspect.AnnotaionAspect - after execution(boolean com.gupaoedu.aop.service.MemberService.delete(long))
[INFO][13:04:46] com.gupaoedu.aop.aspect.AnnotaionAspect-afterReturn execution(boolean com.gupaoedu.aop.service.MemberService.delete(long))
```

​		可以看到，正如我们预期的那样，虽然我们并没有对MemberService类包括其调用方式做任何改变，但是 Spring 仍然拦截到了其中方法的调用，或许这正是AOP的魔力所在。再简单说一下 xml 配置方式，其实也一样简单∶

```xml

```

​		个人觉得不如注解灵活和强大，你可以不同意这个观点，但是不知道如下的代码会不会让你的想法有所改善∶

```java

```

​		以下是MemberService的代码∶

```java

```

​		应该说学习 Spring AOP有两个难点，第一点在于理解 AOP的理念和相关概念，第二点在于灵活掌握和使用切入点表达式。概念的理解通常不在一朝一夕，慢慢浸泡的时间长了，自然就明白了，下面我们简单地介绍一下切入点表达式的配置规则吧。

​		通常情况下，表达式中使用"execution"就可以满足大部分的要求。表达式格式如下∶

```xml
execution(modifiers-pattern? ret-type-pattern declaring-type-pattern? name-pattern(param-pattern) throws-pattern?)
```

modifiers-pattern∶方法的操作权限

ret-type-pattern∶返回值

declaring-type-pattern∶方法所在的包

name-pattern∶方法名

parm-pattern∶参数名

throws-pattern∶异常

​		其中，除 ret-type-pattern和 name-pattern 之外，其他都是可选的。上例中，`execution(*com.spring.service.*.*（..）`表示com.spring.service包下，返回值为任意类型;方法名任意;参数不作限制的所有方法。

​		最后说一下通知参数，可以通过 args来绑定参数，这样就可以在通知（Advice）中访问具体参数了。例如，`<aop:aspect>`配置如下∶

```xml
<aop:config>
	<aop:aspect ref="xmlAspect">
		<aop:pointcut id="simplePointcut"
			expression="execution(* com.gupaoedu.aop.service..*(..))and args(msg,.)"/>
        <aop:after pointcut-ref="simplePointcut"Method="after"/>
    </aop:aspect>
</aop:config>
```

​		上面的代码 args（msg…）是指将切入点方法上的第一个String类型参数添加到参数名为 msg的通知的入参上，这样就可以直接使用该参数啦。

​		在上面的Aspect 切面 Bean中已经看到了，每个通知方法第一个参数都是JoinPoint。其实，在Spring 中，任何通知（Advice）方法都可以将第一个参数定义为 org.aspectj.lang.JoinPoint类型用以接受当前连接点对象。JoinPoint接口提供了一系列有用的方法，比如 getArgs()（返回方法参数）、getThis()（返回代理对象）、getTarget()（返回目标）、getSignature()（返回正在被通知的方法相关信息）和 toString()（打印出正在被通知的方法的有用信息）。

### 2.6.4 源码分析

#### 2.6.4.1 寻找入口

​		Spring的AOP是通过接入 BeanPostProcessor后置处理器开始的，它是 Spring IOC容器经常使用到的一个特性，这个Bean后置处理器是一个监听器，可以监听容器触发的Bean声明周期事件。后置处理器向容器注册以后，容器中管理的Bean就具备了接收IOC容器事件回调的能力。

​		BeanPostProcessor的使用非常简单，只需要提供一个实现接口 BeanPostProcessor的实现类，然后在 Bean的配置文件中设置即可。

**1、BeanPostProcessor 源码**

```java
public interface BeanPostProcessor {

	//为在Bean的初始化前提供回调入口
	@Nullable
	default Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
		return bean;
	}

	//为在Bean的初始化之后提供回调入口
	@Nullable
	default Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		return bean;
	}

}
```

​		这两个回调的入口都是和容器管理的Bean的生命周期事件紧密相关，可以为用户提供在Spring IOC容器初始化 Bean 过程中自定义的处理操作。

**2、AbstractAutowireCapableBeanFactory类对容器生成的 Bean添加后置处理器**

​		BeanPostProcessor后置处理器的调用发生在Spring IOC容器完成对Bean实例对象的创建和属性的依赖注入完成之后在对Spring依赖注入的源码分析过程中我们知道当应用程序第一次调用getBean()方法（lazy-init预实例化除外)向Spring IOC容器索取指定 Bean时触发Spring IOC容器创建 Bean实例对象并进行依赖注入的过程，其中真正实现创建 Bean 对象并进行依赖注入的方法是AbstractAutowireCapableBeanFactory类的 doCreateBean（）方法，主要源码如下∶

```java
//真正创建Bean的方法
protected Object doCreateBean(final String beanName, final RootBeanDefinition mbd, final @Nullable Object[] args)
    throws BeanCreationException {

    // Instantiate the bean.
    //封装被创建的Bean对象
    BeanWrapper instanceWrapper = null;
    if (mbd.isSingleton()) {
        instanceWrapper = this.factoryBeanInstanceCache.remove(beanName);
    }
    if (instanceWrapper == null) {
        instanceWrapper = createBeanInstance(beanName, mbd, args);
    }
    final Object bean = instanceWrapper.getWrappedInstance();
    //获取实例化对象的类型
    Class<?> beanType = instanceWrapper.getWrappedClass();
    if (beanType != NullBean.class) {
        mbd.resolvedTargetType = beanType;
    }

    // Allow post-processors to modify the merged bean definition.
    //调用PostProcessor后置处理器
    synchronized (mbd.postProcessingLock) {
        if (!mbd.postProcessed) {
            try {
                applyMergedBeanDefinitionPostProcessors(mbd, beanType, beanName);
            }
            catch (Throwable ex) {
                throw new BeanCreationException(mbd.getResourceDescription(), beanName,
                                                "Post-processing of merged bean definition failed", ex);
            }
            mbd.postProcessed = true;
        }
    }

    // Eagerly cache singletons to be able to resolve circular references
    // even when triggered by lifecycle interfaces like BeanFactoryAware.
    //向容器中缓存单例模式的Bean对象，以防循环引用
    boolean earlySingletonExposure = (mbd.isSingleton() && this.allowCircularReferences &&
                                      isSingletonCurrentlyInCreation(beanName));
    if (earlySingletonExposure) {
        if (logger.isDebugEnabled()) {
            logger.debug("Eagerly caching bean '" + beanName +
                         "' to allow for resolving potential circular references");
        }
        //这里是一个匿名内部类，为了防止循环引用，尽早持有对象的引用
        addSingletonFactory(beanName, () -> getEarlyBeanReference(beanName, mbd, bean));
    }

    // Initialize the bean instance.
    //Bean对象的初始化，依赖注入在此触发
    //这个exposedObject在初始化完成之后返回作为依赖注入完成后的Bean
    Object exposedObject = bean;
    try {
        //将Bean实例对象封装，并且Bean定义中配置的属性值赋值给实例对象
        populateBean(beanName, mbd, instanceWrapper);
        //初始化Bean对象
        exposedObject = initializeBean(beanName, exposedObject, mbd);
    }
    catch (Throwable ex) {
        if (ex instanceof BeanCreationException && beanName.equals(((BeanCreationException) ex).getBeanName())) {
            throw (BeanCreationException) ex;
        }
        else {
            throw new BeanCreationException(
                mbd.getResourceDescription(), beanName, "Initialization of bean failed", ex);
        }
    }

    if (earlySingletonExposure) {
        //获取指定名称的已注册的单例模式Bean对象
        Object earlySingletonReference = getSingleton(beanName, false);
        if (earlySingletonReference != null) {
            //根据名称获取的已注册的Bean和正在实例化的Bean是同一个
            if (exposedObject == bean) {
                //当前实例化的Bean初始化完成
                exposedObject = earlySingletonReference;
            }
            //当前Bean依赖其他Bean，并且当发生循环引用时不允许新创建实例对象
            else if (!this.allowRawInjectionDespiteWrapping && hasDependentBean(beanName)) {
                String[] dependentBeans = getDependentBeans(beanName);
                Set<String> actualDependentBeans = new LinkedHashSet<>(dependentBeans.length);
                //获取当前Bean所依赖的其他Bean
                for (String dependentBean : dependentBeans) {
                    //对依赖Bean进行类型检查
                    if (!removeSingletonIfCreatedForTypeCheckOnly(dependentBean)) {
                        actualDependentBeans.add(dependentBean);
                    }
                }
                if (!actualDependentBeans.isEmpty()) {
                    throw new BeanCurrentlyInCreationException(beanName,
                                                               "Bean with name '" + beanName + "' has been injected into other beans [" +
                                                               StringUtils.collectionToCommaDelimitedString(actualDependentBeans) +
                                                               "] in its raw version as part of a circular reference, but has eventually been " +
                                                               "wrapped. This means that said other beans do not use the final version of the " +
                                                               "bean. This is often the result of over-eager type matching - consider using " +
                                                               "'getBeanNamesOfType' with the 'allowEagerInit' flag turned off, for example.");
                }
            }
        }
    }

    // Register bean as disposable.
    //注册完成依赖注入的Bean
    try {
        registerDisposableBeanIfNecessary(beanName, bean, mbd);
    }
    catch (BeanDefinitionValidationException ex) {
        throw new BeanCreationException(
            mbd.getResourceDescription(), beanName, "Invalid destruction signature", ex);
    }

    return exposedObject;
}
```

​		从上面的代码中我们知道，为 Bean 实例对象添加 BeanPostProcessor后置处理器的入口的是initializeBean（）方法。

**3、initializeBean（）方法为容器产生的Bean 实例对象添加 BeanPostProcessor后置处理器**

​		同样在AbstractAutowireCapableBeanFactory类中，initializeBean（）方法实现为容器创建的Bean 实例对象添加 BeanPostProcessor后置处理器，源码如下∶

```java
//初始容器创建的Bean实例对象，为其添加BeanPostProcessor后置处理器
protected Object initializeBean(final String beanName, final Object bean, @Nullable RootBeanDefinition mbd) {
    //JDK的安全机制验证权限
    if (System.getSecurityManager() != null) {
        //实现PrivilegedAction接口的匿名内部类
        AccessController.doPrivileged((PrivilegedAction<Object>) () -> {
            invokeAwareMethods(beanName, bean);
            return null;
        }, getAccessControlContext());
    }
    else {
        //为Bean实例对象包装相关属性，如名称，类加载器，所属容器等信息
        invokeAwareMethods(beanName, bean);
    }

    Object wrappedBean = bean;
    //对BeanPostProcessor后置处理器的postProcessBeforeInitialization
    //回调方法的调用，为Bean实例初始化前做一些处理
    if (mbd == null || !mbd.isSynthetic()) {
        wrappedBean = applyBeanPostProcessorsBeforeInitialization(wrappedBean, beanName);
    }

    //调用Bean实例对象初始化的方法，这个初始化方法是在Spring Bean定义配置
    //文件中通过init-method属性指定的
    try {
        invokeInitMethods(beanName, wrappedBean, mbd);
    }
    catch (Throwable ex) {
        throw new BeanCreationException(
            (mbd != null ? mbd.getResourceDescription() : null),
            beanName, "Invocation of init method failed", ex);
    }
    //对BeanPostProcessor后置处理器的postProcessAfterInitialization
    //回调方法的调用，为Bean实例初始化之后做一些处理
    if (mbd == null || !mbd.isSynthetic()) {
        wrappedBean = applyBeanPostProcessorsAfterInitialization(wrappedBean, beanName);
    }

    return wrappedBean;
}

@Override
//调用BeanPostProcessor后置处理器实例对象初始化之前的处理方法
public Object applyBeanPostProcessorsBeforeInitialization(Object existingBean, String beanName)
    throws BeansException {
    Object result = existingBean;
    //遍历容器为所创建的Bean添加的所有BeanPostProcessor后置处理器
    for (BeanPostProcessor beanProcessor : getBeanPostProcessors()) {
        //调用Bean实例所有的后置处理中的初始化前处理方法，为Bean实例对象在
        //初始化之前做一些自定义的处理操作
        Object current = beanProcessor.postProcessBeforeInitialization(result, beanName);
        if (current == null) {
            return result;
        }
        result = current;
    }
    return result;
}

@Override
//调用BeanPostProcessor后置处理器实例对象初始化之后的处理方法
public Object applyBeanPostProcessorsAfterInitialization(Object existingBean, String beanName)
    throws BeansException {

    Object result = existingBean;
    //遍历容器为所创建的Bean添加的所有BeanPostProcessor后置处理器
    for (BeanPostProcessor beanProcessor : getBeanPostProcessors()) {
        //调用Bean实例所有的后置处理中的初始化后处理方法，为Bean实例对象在
        //初始化之后做一些自定义的处理操作
        Object current = beanProcessor.postProcessAfterInitialization(result, beanName);
        if (current == null) {
            return result;
        }
        result = current;
    }
    return result;
}
```

​		BeanPostProcessor是一个接口，其初始化前的操作方法和初始化后的操作方法均委托其实现子类来实现，在Spring 中，BeanPostProcessor的实现子类非常的多，分别完成不同的操作，如∶AOP面向切面编程的注册通知适配器、Bean 对象的数据校验、Bean继承属性、方法的合并等等，我们以最简单的AOP切面织入来简单了解其主要的功能。下面我们来分析其中一个创建 AOP代理对象的子类AbstractAutoProxyCreator类。该类重写了postProcessAfterlnitialization（）方法。选择代理策略进入 postProcessAfterlnitialization（）方法，我们发现调到了一个非常核心的方法wraplfNecessary()，其源码如下∶

```java
@Override
public Object postProcessAfterInitialization(@Nullable Object bean, String beanName) throws BeansException {
    if (bean != null) {
        Object cacheKey = getCacheKey(bean.getClass(), beanName);
        if (!this.earlyProxyReferences.contains(cacheKey)) {
            return wrapIfNecessary(bean, beanName, cacheKey);
        }
    }
    return bean;
}

protected Object wrapIfNecessary(Object bean, String beanName, Object cacheKey) {
    if (StringUtils.hasLength(beanName) && this.targetSourcedBeans.contains(beanName)) {
        return bean;
    }

    // 判断是否不应该代理这个bean
    if (Boolean.FALSE.equals(this.advisedBeans.get(cacheKey))) {
        return bean;
    }
    /*
		 * 判断是否是一些InfrastructureClass或者是否应该跳过这个bean。
		 * 所谓InfrastructureClass就是指Advice/PointCut/Advisor等接口的实现类。
		 * shouldSkip默认实现为返回false,由于是protected方法，子类可以覆盖。
		 */
    if (isInfrastructureClass(bean.getClass()) || shouldSkip(bean.getClass(), beanName)) {
        this.advisedBeans.put(cacheKey, Boolean.FALSE);
        return bean;
    }

    // 获取这个bean的advice
    // Create proxy if we have advice.
    //一会再讲，扫描所有的相关的方法（PointCut原始方法，哪些方法需要被代理）
    Object[] specificInterceptors = getAdvicesAndAdvisorsForBean(bean.getClass(), beanName, null);
    if (specificInterceptors != DO_NOT_PROXY) {
        this.advisedBeans.put(cacheKey, Boolean.TRUE);
        // 创建代理
        Object proxy = createProxy(
            bean.getClass(), beanName, specificInterceptors, new SingletonTargetSource(bean));
        this.proxyTypes.put(cacheKey, proxy.getClass());
        return proxy;
    }

    this.advisedBeans.put(cacheKey, Boolean.FALSE);
    return bean;
}

protected Object createProxy(Class<?> beanClass, @Nullable String beanName,
                             @Nullable Object[] specificInterceptors, TargetSource targetSource) {

    if (this.beanFactory instanceof ConfigurableListableBeanFactory) {
        AutoProxyUtils.exposeTargetClass((ConfigurableListableBeanFactory) this.beanFactory, beanName, beanClass);
    }

    ProxyFactory proxyFactory = new ProxyFactory();
    proxyFactory.copyFrom(this);

    if (!proxyFactory.isProxyTargetClass()) {
        if (shouldProxyTargetClass(beanClass, beanName)) {
            proxyFactory.setProxyTargetClass(true);
        }
        else {
            evaluateProxyInterfaces(beanClass, proxyFactory);
        }
    }

    Advisor[] advisors = buildAdvisors(beanName, specificInterceptors);
    proxyFactory.addAdvisors(advisors);
    proxyFactory.setTargetSource(targetSource);
    customizeProxyFactory(proxyFactory);

    proxyFactory.setFrozen(this.freezeProxy);
    if (advisorsPreFiltered()) {
        proxyFactory.setPreFiltered(true);
    }

    return proxyFactory.getProxy(getProxyClassLoader());
}
```

​		整个过程跟下来，我发现最终调用的是 proxyFactory.getProxy（）方法。到这里我们大概能够猜到proxyFactory有 JDK和CGLib的，那么我们该如何选择呢?最终调用的是DefaultAopProxyFactory的 createAopProxy（）方法∶

```java
@SuppressWarnings("serial")
public class DefaultAopProxyFactory implements AopProxyFactory, Serializable {

	@Override
	public AopProxy createAopProxy(AdvisedSupport config) throws AopConfigException {
		if (config.isOptimize() || config.isProxyTargetClass() || hasNoUserSuppliedProxyInterfaces(config)) {
			Class<?> targetClass = config.getTargetClass();
			if (targetClass == null) {
				throw new AopConfigException("TargetSource cannot determine target class: " +
						"Either an interface or a target is required for proxy creation.");
			}
			if (targetClass.isInterface() || Proxy.isProxyClass(targetClass)) {
				return new JdkDynamicAopProxy(config);
			}
			return new ObjenesisCglibAopProxy(config);
		}
		else {
			return new JdkDynamicAopProxy(config);
		}
	}
    
	private boolean hasNoUserSuppliedProxyInterfaces(AdvisedSupport config) {
		Class<?>[] ifcs = config.getProxiedInterfaces();
		return (ifcs.length == 0 || (ifcs.length == 1 && SpringProxy.class.isAssignableFrom(ifcs[0])));
	}

}
```

#### 2.6.4.2 调用代理方法

​		分析调用逻辑之前先上类图，看看Spring中主要的AOP组件∶

<img src="images/Spring Framework切面-1.png" alt="Spring Framework切面-1" style="zoom:30%;" />

​		上面我们已经了解到Spring提供了两种方式来生成代理方式有 JDKProxy和CGLib。下面我们来研究一下 Spring 如何使用JDK来生成代理对象，具体的生成代码放在JdkDynamicAopProxy这个类中，直接上相关代码∶

```java
/**
 * 获取代理类要实现的接口,除了Advised对象中配置的,还会加上SpringProxy, Advised(opaque=false)
 * 检查上面得到的接口中有没有定义 equals或者hashcode的接口
 * 调用Proxy.newProxyInstance创建代理对象
 */
@Override
public Object getProxy(@Nullable ClassLoader classLoader) {
    if (logger.isDebugEnabled()) {
        logger.debug("Creating JDK dynamic proxy: target source is " + this.advised.getTargetSource());
    }
    Class<?>[] proxiedInterfaces = AopProxyUtils.completeProxiedInterfaces(this.advised, true);
    findDefinedEqualsAndHashCodeMethods(proxiedInterfaces);
    return Proxy.newProxyInstance(classLoader, proxiedInterfaces, this);
}
```

​		通过注释我们应该已经看得非常明白代理对象的生成过程，此处不再赘述。下面的问题是，代理对象生成了，那切面是如何织入的?

​		我们知道 InvocationHandler是 JDK动态代理的核心，生成的代理对象的方法调用都会委托到InvocationHandlerinvoke（方法。而从JdkDynamicAopProxy的源码我们可以看到这个类其实也实现了InvocationHandler，下面我们分析 Spring AOP是如何织入切面的，直接上源码看invoke（）方法∶

```java
@Override
@Nullable
public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
    MethodInvocation invocation;
    Object oldProxy = null;
    boolean setProxyContext = false;

    TargetSource targetSource = this.advised.targetSource;
    Object target = null;

    try {
        //eqauls()方法，具目标对象未实现此方法
        if (!this.equalsDefined && AopUtils.isEqualsMethod(method)) {
            // The target does not implement the equals(Object) method itself.
            return equals(args[0]);
        }
        //hashCode()方法，具目标对象未实现此方法
        else if (!this.hashCodeDefined && AopUtils.isHashCodeMethod(method)) {
            // The target does not implement the hashCode() method itself.
            return hashCode();
        }
        else if (method.getDeclaringClass() == DecoratingProxy.class) {
            // There is only getDecoratedClass() declared -> dispatch to proxy config.
            return AopProxyUtils.ultimateTargetClass(this.advised);
        }
        //Advised接口或者其父接口中定义的方法,直接反射调用,不应用通知
        else if (!this.advised.opaque && method.getDeclaringClass().isInterface() &&
                 method.getDeclaringClass().isAssignableFrom(Advised.class)) {
            // Service invocations on ProxyConfig with the proxy config...
            return AopUtils.invokeJoinpointUsingReflection(this.advised, method, args);
        }

        Object retVal;

        if (this.advised.exposeProxy) {
            // Make invocation available if necessary.
            oldProxy = AopContext.setCurrentProxy(proxy);
            setProxyContext = true;
        }

        // Get as late as possible to minimize the time we "own" the target,
        // in case it comes from a pool.
        //获得目标对象的类
        target = targetSource.getTarget();
        Class<?> targetClass = (target != null ? target.getClass() : null);

        // Get the interception chain for this method.
        //获取可以应用到此方法上的Interceptor列表
        List<Object> chain = this.advised.getInterceptorsAndDynamicInterceptionAdvice(method, targetClass);

        // Check whether we have any advice. If we don't, we can fallback on direct
        // reflective invocation of the target, and avoid creating a MethodInvocation.
        //如果没有可以应用到此方法的通知(Interceptor)，此直接反射调用 method.invoke(target, args)
        if (chain.isEmpty()) {
            // We can skip creating a MethodInvocation: just invoke the target directly
            // Note that the final invoker must be an InvokerInterceptor so we know it does
            // nothing but a reflective operation on the target, and no hot swapping or fancy proxying.
            Object[] argsToUse = AopProxyUtils.adaptArgumentsIfNecessary(method, args);
            retVal = AopUtils.invokeJoinpointUsingReflection(target, method, argsToUse);
        }
        else {
            // We need to create a method invocation...
            //创建MethodInvocation
            invocation = new ReflectiveMethodInvocation(proxy, target, method, args, targetClass, chain);
            // Proceed to the joinpoint through the interceptor chain.
            retVal = invocation.proceed();
        }

        // Massage return value if necessary.
        Class<?> returnType = method.getReturnType();
        if (retVal != null && retVal == target &&
            returnType != Object.class && returnType.isInstance(proxy) &&
            !RawTargetAccess.class.isAssignableFrom(method.getDeclaringClass())) {
            // Special case: it returned "this" and the return type of the method
            // is type-compatible. Note that we can't help if the target sets
            // a reference to itself in another returned object.
            retVal = proxy;
        }
        else if (retVal == null && returnType != Void.TYPE && returnType.isPrimitive()) {
            throw new AopInvocationException(
                "Null return value from advice does not match primitive return type for: " + method);
        }
        return retVal;
    }
    finally {
        if (target != null && !targetSource.isStatic()) {
            // Must have come from TargetSource.
            targetSource.releaseTarget(target);
        }
        if (setProxyContext) {
            // Restore old proxy.
            AopContext.setCurrentProxy(oldProxy);
        }
    }
}
```

​		主要实现思路可以简述为∶首先获取应用到此方法上的通知链（Interceptor Chain）。如果有通知，则应用通知，并执行JoinPoint;如果没有通知，则直接反射执行JoinPoint。而这里的关键是通知链是如何获取的以及它又是如何执行的呢?现在来逐一分析。首先，从上面的代码可以看到，通知链是通过Advised.getInterceptorsAndDynamicInterceptionAdvice（这个方法来获取的，我们来看下这个方法的实现逻辑∶

AdvisedSupport

```java
public List<Object> getInterceptorsAndDynamicInterceptionAdvice(Method method, @Nullable Class<?> targetClass) {
    MethodCacheKey cacheKey = new MethodCacheKey(method);
    // 从缓存中获取
    List<Object> cached = this.methodCache.get(cacheKey);
    // 缓存未命中，则进行下一步处理
    if (cached == null) {
        // 获取所有的拦截器
        cached = this.advisorChainFactory.getInterceptorsAndDynamicInterceptionAdvice(
            this, method, targetClass);
        // 存入缓存
        this.methodCache.put(cacheKey, cached);
    }
    return cached;
}
```

​		通过上面的源码我们可以看到，实际获取通知的实现逻辑其实是由 AdvisorChainFactory的getInterceptorsAndDynamiclnterceptionAdvice（）方法来完成的，且获取到的结果会被缓存。

下面分析 getlnterceptorsAndDynamiclnterceptionAdvice（）方法的实现∶

DefaultAdvisorChainFactory

```java
/**
	 * 从提供的配置实例config中获取advisor列表,遍历处理这些advisor.如果是IntroductionAdvisor,
	 * 则判断此Advisor能否应用到目标类targetClass上.如果是PointcutAdvisor,则判断
	 * 此Advisor能否应用到目标方法method上.将满足条件的Advisor通过AdvisorAdaptor转化成Interceptor列表返回.
	 */
@Override
public List<Object> getInterceptorsAndDynamicInterceptionAdvice(
    Advised config, Method method, @Nullable Class<?> targetClass) {

    // This is somewhat tricky... We have to process introductions first,
    // but we need to preserve order in the ultimate list.
    List<Object> interceptorList = new ArrayList<>(config.getAdvisors().length);
    Class<?> actualClass = (targetClass != null ? targetClass : method.getDeclaringClass());
    //查看是否包含IntroductionAdvisor
    boolean hasIntroductions = hasMatchingIntroductions(config, actualClass);
    //这里实际上注册一系列AdvisorAdapter,用于将Advisor转化成MethodInterceptor
    AdvisorAdapterRegistry registry = GlobalAdvisorAdapterRegistry.getInstance();

    for (Advisor advisor : config.getAdvisors()) {
        if (advisor instanceof PointcutAdvisor) {
            // Add it conditionally.
            PointcutAdvisor pointcutAdvisor = (PointcutAdvisor) advisor;
            if (config.isPreFiltered() || pointcutAdvisor.getPointcut().getClassFilter().matches(actualClass)) {
                //这个地方这两个方法的位置可以互换下
                //将Advisor转化成Interceptor
                MethodInterceptor[] interceptors = registry.getInterceptors(advisor);
                //检查当前advisor的pointcut是否可以匹配当前方法
                MethodMatcher mm = pointcutAdvisor.getPointcut().getMethodMatcher();
                if (MethodMatchers.matches(mm, method, actualClass, hasIntroductions)) {
                    if (mm.isRuntime()) {
                        // Creating a new object instance in the getInterceptors() method
                        // isn't a problem as we normally cache created chains.
                        for (MethodInterceptor interceptor : interceptors) {
                            interceptorList.add(new InterceptorAndDynamicMethodMatcher(interceptor, mm));
                        }
                    }
                    else {
                        interceptorList.addAll(Arrays.asList(interceptors));
                    }
                }
            }
        }
        else if (advisor instanceof IntroductionAdvisor) {
            IntroductionAdvisor ia = (IntroductionAdvisor) advisor;
            if (config.isPreFiltered() || ia.getClassFilter().matches(actualClass)) {
                Interceptor[] interceptors = registry.getInterceptors(advisor);
                interceptorList.addAll(Arrays.asList(interceptors));
            }
        }
        else {
            Interceptor[] interceptors = registry.getInterceptors(advisor);
            interceptorList.addAll(Arrays.asList(interceptors));
        }
    }

    return interceptorList;
}

```

​	这个方法执行完成后，Advised中配置能够应用到连接点（JoinPoint）或者目标类(Target Object)的 Advisor 全部被转化成了Methodlnterceptor，接下来我们再看下得到的拦截器链是怎么起作用的。

```java
if (chain.isEmpty()) {
    Object[] argsToUse = AopProxyUtils.adaptArgumentsIfNecessary(method, args);
    retVal = AopUtils.invokeJoinpointUsingReflection(target, method, argsToUse);
}
else {
    // We need to create a method invocation...
    //创建MethodInvocation
    invocation = new ReflectiveMethodInvocation(proxy, target, method, args, targetClass, chain);
    // Proceed to the joinpoint through the interceptor chain.
    retVal = invocation.proceed();
}
```

​		从这段代码可以看出，如果得到的拦截器链为空，则直接反射调用目标方法，否则创建MethodlInvocation，调用其 proceed（）方法，触发拦截器链的执行，来看下具体代码∶

ReflectiveMethodInvocation

```java
@Override
@Nullable
public Object proceed() throws Throwable {
    //	We start with an index of -1 and increment early.
    //如果Interceptor执行完了，则执行joinPoint
    if (this.currentInterceptorIndex == this.interceptorsAndDynamicMethodMatchers.size() - 1) {
        return invokeJoinpoint();
    }

    Object interceptorOrInterceptionAdvice =
        this.interceptorsAndDynamicMethodMatchers.get(++this.currentInterceptorIndex);
    //如果要动态匹配joinPoint
    if (interceptorOrInterceptionAdvice instanceof InterceptorAndDynamicMethodMatcher) {
        // Evaluate dynamic method matcher here: static part will already have
        // been evaluated and found to match.
        InterceptorAndDynamicMethodMatcher dm =
            (InterceptorAndDynamicMethodMatcher) interceptorOrInterceptionAdvice;
        //动态匹配：运行时参数是否满足匹配条件
        if (dm.methodMatcher.matches(this.method, this.targetClass, this.arguments)) {
            return dm.interceptor.invoke(this);
        }
        else {
            // Dynamic matching failed.
            // Skip this interceptor and invoke the next in the chain.
            //动态匹配失败时,略过当前Intercetpor,调用下一个Interceptor
            return proceed();
        }
    }
    else {
        // It's an interceptor, so we just invoke it: The pointcut will have
        // been evaluated statically before this object was constructed.
        //执行当前Intercetpor
        return ((MethodInterceptor) interceptorOrInterceptionAdvice).invoke(this);
    }
}
```

​		至此，通知链就完美地形成了。我们再往下来看invokeJoinpointUsingReflection（）方法，其实就是反射调用∶

```java
@Nullable
public static Object invokeJoinpointUsingReflection(@Nullable Object target, Method method, Object[] args)
    throws Throwable {

    // Use reflection to invoke the method.
    try {
        ReflectionUtils.makeAccessible(method);
        return method.invoke(target, args);
    }
    catch (InvocationTargetException ex) {
        // Invoked method threw a checked exception.
        // We must rethrow it. The client won't see the interceptor.
        throw ex.getTargetException();
    }
    catch (IllegalArgumentException ex) {
        throw new AopInvocationException("AOP configuration seems to be invalid: tried calling method [" +
                                         method + "] on target [" + target + "]", ex);
    }
    catch (IllegalAccessException ex) {
        throw new AopInvocationException("Could not access method [" + method + "]", ex);
    }
}
```

​		Spring AOP源码就分析到这儿，相信小伙伴们应该有了基本思路，下面时序图来一波。

![Spring Framework切面-2](images/Spring Framework切面-2.jpg)

### 2.6.5 触发通知

​		在为AopProxy代理对象配置拦截器的实现中，有一个取得拦截器的配置过程，这个过程是由 DefaultAdvisorChainFactory实现的，这个工厂类负责生成拦截器链，在它的getInterceptorsAndDynamicInterceptionAdvice方法中，有一个适配器和注册过程，通过配置 Spring 预先设计好的拦截器，Spring 加入了它对 AOP实现的处理。

```java
/**
 * 从提供的配置实例config中获取advisor列表,遍历处理这些advisor.如果是IntroductionAdvisor,
 * 则判断此Advisor能否应用到目标类targetClass上.如果是PointcutAdvisor,则判断
 * 此Advisor能否应用到目标方法method上.将满足条件的Advisor通过AdvisorAdaptor转化成Interceptor列表返回.
 */
@Override
public List<Object> getInterceptorsAndDynamicInterceptionAdvice(
    Advised config, Method method, @Nullable Class<?> targetClass) {

    // This is somewhat tricky... We have to process introductions first,
    // but we need to preserve order in the ultimate list.
    List<Object> interceptorList = new ArrayList<>(config.getAdvisors().length);
    Class<?> actualClass = (targetClass != null ? targetClass : method.getDeclaringClass());
    //查看是否包含IntroductionAdvisor
    boolean hasIntroductions = hasMatchingIntroductions(config, actualClass);
    //这里实际上注册一系列AdvisorAdapter,用于将Advisor转化成MethodInterceptor
    AdvisorAdapterRegistry registry = GlobalAdvisorAdapterRegistry.getInstance();

    for (Advisor advisor : config.getAdvisors()) {
        if (advisor instanceof PointcutAdvisor) {
            // Add it conditionally.
            PointcutAdvisor pointcutAdvisor = (PointcutAdvisor) advisor;
            if (config.isPreFiltered() || pointcutAdvisor.getPointcut().getClassFilter().matches(actualClass)) {
                //这个地方这两个方法的位置可以互换下
                //将Advisor转化成Interceptor
                MethodInterceptor[] interceptors = registry.getInterceptors(advisor);
                //检查当前advisor的pointcut是否可以匹配当前方法
                MethodMatcher mm = pointcutAdvisor.getPointcut().getMethodMatcher();
                if (MethodMatchers.matches(mm, method, actualClass, hasIntroductions)) {
                    if (mm.isRuntime()) {
                        // Creating a new object instance in the getInterceptors() method
                        // isn't a problem as we normally cache created chains.
                        for (MethodInterceptor interceptor : interceptors) {
                            interceptorList.add(new InterceptorAndDynamicMethodMatcher(interceptor, mm));
                        }
                    }
                    else {
                        interceptorList.addAll(Arrays.asList(interceptors));
                    }
                }
            }
        }
        else if (advisor instanceof IntroductionAdvisor) {
            IntroductionAdvisor ia = (IntroductionAdvisor) advisor;
            if (config.isPreFiltered() || ia.getClassFilter().matches(actualClass)) {
                Interceptor[] interceptors = registry.getInterceptors(advisor);
                interceptorList.addAll(Arrays.asList(interceptors));
            }
        }
        else {
            Interceptor[] interceptors = registry.getInterceptors(advisor);
            interceptorList.addAll(Arrays.asList(interceptors));
        }
    }

    return interceptorList;
}
```

​		GlobalAdvisorAdapterRegistry负责拦截器的适配和注册过程。

```java
public abstract class GlobalAdvisorAdapterRegistry {

	/**
	 * Keep track of a single instance so we can return it to classes that request it.
	 */
	private static AdvisorAdapterRegistry instance = new DefaultAdvisorAdapterRegistry();

	/**
	 * Return the singleton {@link DefaultAdvisorAdapterRegistry} instance.
	 */
	public static AdvisorAdapterRegistry getInstance() {
		return instance;
	}

	/**
	 * Reset the singleton {@link DefaultAdvisorAdapterRegistry}, removing any
	 * {@link AdvisorAdapterRegistry#registerAdvisorAdapter(AdvisorAdapter) registered}
	 * adapters.
	 */
	static void reset() {
		instance = new DefaultAdvisorAdapterRegistry();
	}

}
```

​		与GlobalAdvisorAdapterRegistry起到了适配器和单例模式的作用，提供了一个DefaultAdvisorAdapterRegistry，它用来完成各种通知的适配和注册过程。

```java
@SuppressWarnings("serial")
public class DefaultAdvisorAdapterRegistry implements AdvisorAdapterRegistry, Serializable {

	private final List<AdvisorAdapter> adapters = new ArrayList<>(3);


	/**
	 * Create a new DefaultAdvisorAdapterRegistry, registering well-known adapters.
	 */
	public DefaultAdvisorAdapterRegistry() {
		registerAdvisorAdapter(new MethodBeforeAdviceAdapter());
		registerAdvisorAdapter(new AfterReturningAdviceAdapter());
		registerAdvisorAdapter(new ThrowsAdviceAdapter());
	}


	@Override
	public Advisor wrap(Object adviceObject) throws UnknownAdviceTypeException {
		if (adviceObject instanceof Advisor) {
			return (Advisor) adviceObject;
		}
		if (!(adviceObject instanceof Advice)) {
			throw new UnknownAdviceTypeException(adviceObject);
		}
		Advice advice = (Advice) adviceObject;
		if (advice instanceof MethodInterceptor) {
			// So well-known it doesn't even need an adapter.
			return new DefaultPointcutAdvisor(advice);
		}
		for (AdvisorAdapter adapter : this.adapters) {
			// Check that it is supported.
			if (adapter.supportsAdvice(advice)) {
				return new DefaultPointcutAdvisor(advice);
			}
		}
		throw new UnknownAdviceTypeException(advice);
	}

	@Override
	public MethodInterceptor[] getInterceptors(Advisor advisor) throws UnknownAdviceTypeException {
		List<MethodInterceptor> interceptors = new ArrayList<>(3);
		Advice advice = advisor.getAdvice();
		if (advice instanceof MethodInterceptor) {
			interceptors.add((MethodInterceptor) advice);
		}
		for (AdvisorAdapter adapter : this.adapters) {
			if (adapter.supportsAdvice(advice)) {
				interceptors.add(adapter.getInterceptor(advisor));
			}
		}
		if (interceptors.isEmpty()) {
			throw new UnknownAdviceTypeException(advisor.getAdvice());
		}
		return interceptors.toArray(new MethodInterceptor[interceptors.size()]);
	}

	@Override
	public void registerAdvisorAdapter(AdvisorAdapter adapter) {
		this.adapters.add(adapter);
	}

}
```

​		DefaultAdvisorAdapterRegistry设置了一系列的是配置，正是这些适配器的实现，为Spring AOP提供了编织能力。下面以MethodBeforeAdviceAdapter为例，看具体实现∶

```java
@SuppressWarnings("serial")
class MethodBeforeAdviceAdapter implements AdvisorAdapter, Serializable {

	@Override
	public boolean supportsAdvice(Advice advice) {
		return (advice instanceof MethodBeforeAdvice);
	}

	@Override
	public MethodInterceptor getInterceptor(Advisor advisor) {
		MethodBeforeAdvice advice = (MethodBeforeAdvice) advisor.getAdvice();
		return new MethodBeforeAdviceInterceptor(advice);
	}

}
```

​		Spring AOP为了实现advice的织入，设计了特定的拦截器对这些功能进行了封装。我们接着看 MethodBeforeAdvicelnterceptor如何完成封装的?

```java
@SuppressWarnings("serial")
public class MethodBeforeAdviceInterceptor implements MethodInterceptor, Serializable {

	private MethodBeforeAdvice advice;


	/**
	 * Create a new MethodBeforeAdviceInterceptor for the given advice.
	 * @param advice the MethodBeforeAdvice to wrap
	 */
	public MethodBeforeAdviceInterceptor(MethodBeforeAdvice advice) {
		Assert.notNull(advice, "Advice must not be null");
		this.advice = advice;
	}

	@Override
	public Object invoke(MethodInvocation mi) throws Throwable {
		this.advice.before(mi.getMethod(), mi.getArguments(), mi.getThis() );
		return mi.proceed();
	}

}
```

​		可以看到，invoke方法中，首先触发了advice的before 回调，然后才是proceed。

​		AfterReturningAdviceInterceptor的源码∶

```java
@SuppressWarnings("serial")
public class AfterReturningAdviceInterceptor implements MethodInterceptor, AfterAdvice, Serializable {

	private final AfterReturningAdvice advice;


	/**
	 * Create a new AfterReturningAdviceInterceptor for the given advice.
	 * @param advice the AfterReturningAdvice to wrap
	 */
	public AfterReturningAdviceInterceptor(AfterReturningAdvice advice) {
		Assert.notNull(advice, "Advice must not be null");
		this.advice = advice;
	}

	@Override
	public Object invoke(MethodInvocation mi) throws Throwable {
		Object retVal = mi.proceed();
		this.advice.afterReturning(retVal, mi.getMethod(), mi.getArguments(), mi.getThis());
		return retVal;
	}

}
```

ThrowsAdviceInterceptor的源码∶

```java
@Override
public Object invoke(MethodInvocation mi) throws Throwable {
    try {
        return mi.proceed();
    }
    catch (Throwable ex) {
        Method handlerMethod = getExceptionHandler(ex);
        if (handlerMethod != null) {
            invokeHandlerMethod(mi, ex, handlerMethod);
        }
        throw ex;
    }
}

private void invokeHandlerMethod(MethodInvocation mi, Throwable ex, Method method) throws Throwable {
    Object[] handlerArgs;
    if (method.getParameterCount() == 1) {
        handlerArgs = new Object[] { ex };
    }
    else {
        handlerArgs = new Object[] {mi.getMethod(), mi.getArguments(), mi.getThis(), ex};
    }
    try {
        method.invoke(this.throwsAdvice, handlerArgs);
    }
    catch (InvocationTargetException targetEx) {
        throw targetEx.getTargetException();
    }
}
```

​		至此，我们知道了对目标对象的增强是通过拦截器实现的，最后还是上时序图∶

### 2.6.6手写AOP

## 2.7 Spring MVC

### 2.7.1 请求处理流程

​		Spring MVC相对于前面的章节算是比较简单的，我们首先引用《Spring in Action》上的一张图来了解 Spring MVC的核心组件和大致处理流程∶

<img src="images/Spring FrameworkMVC-1.png" alt="Spring FrameworkMVC-1" style="zoom:50%;" />

​		从上图中看到

​		①、DispatcherServlet是SpringMVC中的前端控制器（Front Controller），负责接收Request并将Request转发给对应的处理组件。

​		②、HanlerMapping 是 SpringMVC 中完成 url 到Controller 映射的组件。DispatcherServlet 接收 Request，然后从 HandlerMapping 查找处理 Request 的Controller。

​		③、Controller处理 Request，并返回 ModelAndView 对象，Controller是SpringMVC 中负责处理 Request的组件（类似于 Struts2中的 Action），ModelAndView 是封装结果视图的组件。

​		④、⑤、⑥视图解析器解析ModelAndView对象并返回对应的视图给客户端。在前面的章节中我们已经大致了解到，容器初始化时会建立所有 url和Controller 中的Method的对应关系，保存到HandlerMapping中，用户请求是根据 Request请求的url快速定位到Controller中的某个方法。在Spring中先将 url和Controller的对应关系，保存到Map<url，Controller>中。Web容器启动时会通知 Spring初始化容器（加载Bean的定义信息和初始化所有单例 Bean），然后SpringMVC会遍历容器中的 Bean，取每一个Controller中的所有方法访问的 url然后将url和Controller保存到一个Map 中;这样就可以根据 Request 快速定位到Controller，因为最终处理 Request的是Controller 中的方法，Map 中只保留了url和Controller 中的对应关系，所以要根据Request的 url 进一步确认Controller 中的 Method，这一步工作的原理就是拼接Controller的 url（Controller 上@RequestMapping 的值）和方法的 url（Method上@RequestMapping 的值），与 request的url进行匹配，找到匹配的那个方法;确定处理请求的Method后，接下来的任务就是参数绑定，把 Request中参数绑定到方法的形式参数上，这一步是整个请求处理过程中最复杂的一个步骤。

### 2.7.2 九大组件

**HandlerMappings**

​		HandlerMapping是用来查找Handler的，也就是处理器，具体的表现形式可以是类也可以是方法。比如，标注了@RequestMapping的每个 method 都可以看成是一个Handler，由 Handler来负责实际的请求处理。HandlerMapping 在请求到达之后，它的作用便是找到请求相应的处理器 Handler和 Interceptors。

**HandlerAdapters**

​		从名字上看，这是一个适配器。因为 Spring MVC中Handler可以是任意形式的，只要能够处理请求便行，但是把请求交给 Servlet的时候，由于Servlet 的方法结构都是如doService（HttpServletRequest req，HttpServletResponse resp）这样的形式，让固定为Servlet 处理方法调用Handler来进行处理，这一步工作便是 HandlerAdapter要做的事。

**HandlerExceptionResolvers**

​		这个组件的名字上看，这个就是用来处理 Handler过程中产生的异常情况的组件。具体来说，此组件的作用是根据异常设置ModelAndView，之后再交给 render方法进行渲染，而 render 便将 ModelAndView 渲 染成 页面。不过有一点HandlerExceptionResolver 只是用于解析对请求做处理阶段产生的异常而渲染阶段的异常则不归他管了，这也是Spring MVC 组件设计的一大原则分工明确互不干涉。

**ViewResolvers**

​		解析器，相信大家对这个应该都很熟悉了。因为通常在SpringMVC的配置文件中，都会配上一个该接口的实现类来进行视图的解析。这个组件的主要作用，便是将String的视图名和Locale解析为View类型的视图。这个接口只有一个resolveViewName()方法。从方法的定义就可以看出，Controller层返回的String类型的视图名 viewName，最终会在这里被解析成为View.View是用来渲染页面的，也就是说，它会将程序返回的数和数据填入模板中，最终生成 html 文件。ViewResolver在这个过程中，主要做两件大事，即，ViewResolver会找到渲染所用的模板（使用什么模板来渲染?）和所用的技术（其实也就是视图的类型，如JSP啊还是其他什么 Blabla的）填入参数。默认情况
Spring MVC会为我们自动配置一个InternalResourceVviewResolver，这个是针对JSP类型视图的。

**RequestToViewNameTranslator**

​		这个组件的作用，在于从 Request中获取 viewName.因为ViewResolver是根据ViewName 查找View，但有的Handler处理完成之后，没有设置View也没有设置ViewName，便要通过这个组件来从Request 中查找 viewName。

**LocaleResolver**

​		在上面我们有看到ViewResolver的 resolveviewName（）方法，需要两个参数。那么第二个参数Locale是从哪来的呢，这就是LocaleResolver要做的事了。LocaleResolver用于从 request 中解析出 Locale，在中国大陆地区，Locale当然就会是zh-CN 之类用来表示一个区域。这个类也是i18n的基础。

**ThemeResolver**

​		从名字便可看出，这个类是用来解析主题的。主题，就是样式，，图片以及它们所形成的的集合。Spring MVC中一套主题对应一个 properties 文件，里面存放着跟前主题相关的所有资源，如图片，css样式等。创建主题非常简单，只需准备好资源，然后新建一个"主题名.properties"并将资源设置进去，放在classpath下，便可以在页面中使用了。Spring MVC中跟主题有关的类有 ThemeResolver， ThemeSource 和Theme。ThemeResolver 负责从 request中解析出主题名，ThemeSource 则根据主题名找到具体的主题，其抽象也就是 Theme，通过 Theme来获取主题和具体的资源。

**MultipartResolver**

​		其实这是一个大家很熟悉的组件，MultipartResolver 用于处理上传请求，通过将普通的Request包装成MultipartHttpServletRequest来实现。MultipartHttpServletRequest可以通过getFile（）直接获得文件，如果是多个文件上传，还可以通过调用 getFileMap 得到 Map<FileName， File>这样的结构。MultipartResolver的作用就是用来封装普通的 request，使其拥有处理文件上传的功能。

**FlashMapManager**

​		说到 FlashMapManager，就得先提一下 FlashMap。

​		FlashMap 用于重定向 Redirect 时的参数数据传递，比如，在处理用户订单提交时，为了避免重复提交，可以处理完 post请求后 redirect到一个get请求，这个get请求可以用来显示订单详情之类的信息。这样做虽然可以规避用户刷新重新提交表单的问题，是在这个页面上要显示订单的信息，那这些数据从哪里去获取呢，因为 redirect重定向是没有传递参数这一功能的，如果不想把参数写进 url（其实也不推荐这么做，url有长度限制不说，把参数都直接暴露，感觉也不安全），那么就可以通过 flashMap 来传递。只需 要 在 redirect 之 前，将 要 传 递的 数 据 写 入 request（可 以 通 过ServletRequestAttributes.getRequest()获 得）的 属 性OUTPUT_FLASH_MAP_ATTRIBUTE中，这样在 redirect之后的 handler 中 spring就会自动将其设置到Model中，在显示订单信息的页面上，就可以直接从Model中取得数据了。而 FlashMapManager就是用来管理 FlashMap的。

### 2.7.3 源码分析

​		根据上面分析的Spring MVC工作机制，从三个部分来分析Spring MVC的源代码。

其一，ApplicationContext初始化时用Map保存所有url和Controller类的对应关系

其二，根据请求url找到对应的Controller，并从Controller中找到处理请求的方法;

其三，Request参数绑定到方法的形参，执行方法处理请求，并返回结果视图。

**初始化阶段**

​		我们首先找到DispatcherServlet这个类，必然是寻找 init（）方法。然后，我们发现其init 方法其实在父类HttpServletBean 中，其源码如下∶

```java
@Override
public final void init() throws ServletException {
    if (logger.isDebugEnabled()) {
        logger.debug("Initializing servlet '" + getServletName() + "'");
    }

    // Set bean properties from init parameters.
    PropertyValues pvs = new ServletConfigPropertyValues(getServletConfig(), this.requiredProperties);
    if (!pvs.isEmpty()) {
        try {
            //定位资源
            BeanWrapper bw = PropertyAccessorFactory.forBeanPropertyAccess(this);
            //加载配置信息
            ResourceLoader resourceLoader = new ServletContextResourceLoader(getServletContext());
            bw.registerCustomEditor(Resource.class, new ResourceEditor(resourceLoader, getEnvironment()));
            initBeanWrapper(bw);
            bw.setPropertyValues(pvs, true);
        }
        catch (BeansException ex) {
            if (logger.isErrorEnabled()) {
                logger.error("Failed to set bean properties on servlet '" + getServletName() + "'", ex);
            }
            throw ex;
        }
    }

    // Let subclasses do whatever initialization they like.
    initServletBean();

    if (logger.isDebugEnabled()) {
        logger.debug("Servlet '" + getServletName() + "' configured successfully");
    }
}
```

​		我们看到在这段代码中，又调用了一个重要的 initServletBean（）方法。进入initServletBean（）方法看到以下源码∶

FrameworkServlet

```java
@Override
protected final void initServletBean() throws ServletException {
    getServletContext().log("Initializing Spring FrameworkServlet '" + getServletName() + "'");
    if (this.logger.isInfoEnabled()) {
        this.logger.info("FrameworkServlet '" + getServletName() + "': initialization started");
    }
    long startTime = System.currentTimeMillis();

    try {

        this.webApplicationContext = initWebApplicationContext();
        initFrameworkServlet();
    }
    catch (ServletException ex) {
        this.logger.error("Context initialization failed", ex);
        throw ex;
    }
    catch (RuntimeException ex) {
        this.logger.error("Context initialization failed", ex);
        throw ex;
    }

    if (this.logger.isInfoEnabled()) {
        long elapsedTime = System.currentTimeMillis() - startTime;
        this.logger.info("FrameworkServlet '" + getServletName() + "': initialization completed in " +
                         elapsedTime + " ms");
    }
}
```

​		这段代码中最主要的逻辑就是初始化IOC容器，最终会调用refresh（）方法，前面的章节中对IOC容器的初始化细节我们已经详细掌握，在此我们不再赘述。我们看到上面的代码中，IOC容器初始化之后，最后有调用了onRefresh（）方法。这个方法最终是在DisptcherServlet 中实现，来看源码∶

```java
@Override
protected void onRefresh(ApplicationContext context) {
    initStrategies(context);
}

/**
	 * Initialize the strategy objects that this servlet uses.
	 * <p>May be overridden in subclasses in order to initialize further strategy objects.
	 */
//初始化策略
protected void initStrategies(ApplicationContext context) {
    //多文件上传的组件
    initMultipartResolver(context);
    //初始化本地语言环境
    initLocaleResolver(context);
    //初始化模板处理器
    initThemeResolver(context);
    //handlerMapping
    initHandlerMappings(context);
    //初始化参数适配器
    initHandlerAdapters(context);
    //初始化异常拦截器
    initHandlerExceptionResolvers(context);
    //初始化视图预处理器
    initRequestToViewNameTranslator(context);
    //初始化视图转换器
    initViewResolvers(context);
    //FlashMap管理器
    initFlashMapManager(context);
}
```

​		到这一步就完成了Spring MVC的九大组件的初始化。接下来 我们来看url和Controlle的关系是 如 何 建立 的 呢?HandlerMapping的 子 类AbstractDetectingUrlHandlerMapping实现了 initApplicationContext（）方法，所以我们直接看子类中的初始化容器方法。

```java
@Override
public void initApplicationContext() throws ApplicationContextException {
    super.initApplicationContext();
    detectHandlers();
}
/**
 * 建立当前ApplicationContext中的所有controller和url的对应关系
 */
protected void detectHandlers() throws BeansException {
    ApplicationContext applicationContext = obtainApplicationContext();
    if (logger.isDebugEnabled()) {
        logger.debug("Looking for URL mappings in application context: " + applicationContext);
    }
    // 获取ApplicationContext容器中所有bean的Name
    String[] beanNames = (this.detectHandlersInAncestorContexts ?
                          BeanFactoryUtils.beanNamesForTypeIncludingAncestors(applicationContext, Object.class) :
                          applicationContext.getBeanNamesForType(Object.class));

    // Take any bean name that we can determine URLs for.
    // 遍历beanNames,并找到这些bean对应的url
    for (String beanName : beanNames) {
        // 找bean上的所有url(controller上的url+方法上的url),该方法由对应的子类实现
        String[] urls = determineUrlsForHandler(beanName);
        if (!ObjectUtils.isEmpty(urls)) {
            // URL paths found: Let's consider it a handler.
            // 保存urls和beanName的对应关系,put it to Map<urls,beanName>,该方法在父类AbstractUrlHandlerMapping中实现
            registerHandler(urls, beanName);
        }
        else {
            if (logger.isDebugEnabled()) {
                logger.debug("Rejected bean name '" + beanName + "': no URL paths identified");
            }
        }
    }
}
/** 获取controller中所有方法的url,由子类实现,典型的模板模式 **/
protected abstract String[] determineUrlsForHandler(String beanName);
```

​		determineUrlsForHandler（String beanName）方法的作用是获取每个Controller 中的url，，不同的子类有不同的实现，这是一个典型的模板设计模式。因为开发中我们用的最多的就是用注解来配置Controller 中的 url，BeanNameUrlHandlerMapping是AbstractDetectingUrlHandlerMapping的子类，处理注解形式的url映射.所以我们这里以BeanNameUrlHandlerMapping 来 进 行 分析。我 们 看BeanNameUrlHandlerMapping是如何查 beanName 上所有映射的 url。

```java
@Override
protected String[] determineUrlsForHandler(String beanName) {
    List<String> urls = new ArrayList<>();
    if (beanName.startsWith("/")) {
        urls.add(beanName);
    }
    String[] aliases = obtainApplicationContext().getAliases(beanName);
    for (String alias : aliases) {
        if (alias.startsWith("/")) {
            urls.add(alias);
        }
    }
    return StringUtils.toStringArray(urls);
}
```

​		到这里HandlerMapping 组件就已经建立所有url和Controller的对应关系。

**运行调用阶段**

​		这一步是由请求触发的，所以入口为 DispatcherServlet的核心方法为doService（），doService（）中的核心逻辑由 doDispatch（）实现，源代码如下∶

```java
/** 中央控制器,控制请求的转发 **/
protected void doDispatch(HttpServletRequest request, HttpServletResponse response) throws Exception {
    HttpServletRequest processedRequest = request;
    HandlerExecutionChain mappedHandler = null;
    boolean multipartRequestParsed = false;

    WebAsyncManager asyncManager = WebAsyncUtils.getAsyncManager(request);

    try {
        ModelAndView mv = null;
        Exception dispatchException = null;

        try {
            // 1.检查是否是文件上传的请求
            processedRequest = checkMultipart(request);
            multipartRequestParsed = (processedRequest != request);

            // Determine handler for the current request.
            // 2.取得处理当前请求的controller,这里也称为hanlder,处理器,
            // 	 第一个步骤的意义就在这里体现了.这里并不是直接返回controller,
            //	 而是返回的HandlerExecutionChain请求处理器链对象,
            //	 该对象封装了handler和interceptors.
            mappedHandler = getHandler(processedRequest);
            // 如果handler为空,则返回404
            if (mappedHandler == null) {
                noHandlerFound(processedRequest, response);
                return;
            }

            // Determine handler adapter for the current request.
            //3. 获取处理request的处理器适配器handler adapter
            HandlerAdapter ha = getHandlerAdapter(mappedHandler.getHandler());

            // Process last-modified header, if supported by the handler.
            // 处理 last-modified 请求头
            String method = request.getMethod();
            boolean isGet = "GET".equals(method);
            if (isGet || "HEAD".equals(method)) {
                long lastModified = ha.getLastModified(request, mappedHandler.getHandler());
                if (logger.isDebugEnabled()) {
                    logger.debug("Last-Modified value for [" + getRequestUri(request) + "] is: " + lastModified);
                }
                if (new ServletWebRequest(request, response).checkNotModified(lastModified) && isGet) {
                    return;
                }
            }

            if (!mappedHandler.applyPreHandle(processedRequest, response)) {
                return;
            }

            // Actually invoke the handler.
            // 4.实际的处理器处理请求,返回结果视图对象
            mv = ha.handle(processedRequest, response, mappedHandler.getHandler());

            if (asyncManager.isConcurrentHandlingStarted()) {
                return;
            }

            // 结果视图对象的处理
            applyDefaultViewName(processedRequest, mv);
            mappedHandler.applyPostHandle(processedRequest, response, mv);
        }
        catch (Exception ex) {
            dispatchException = ex;
        }
        catch (Throwable err) {
            // As of 4.3, we're processing Errors thrown from handler methods as well,
            // making them available for @ExceptionHandler methods and other scenarios.
            dispatchException = new NestedServletException("Handler dispatch failed", err);
        }
        processDispatchResult(processedRequest, response, mappedHandler, mv, dispatchException);
    }
    catch (Exception ex) {
        triggerAfterCompletion(processedRequest, response, mappedHandler, ex);
    }
    catch (Throwable err) {
        triggerAfterCompletion(processedRequest, response, mappedHandler,
                               new NestedServletException("Handler processing failed", err));
    }
    finally {
        if (asyncManager.isConcurrentHandlingStarted()) {
            // Instead of postHandle and afterCompletion
            if (mappedHandler != null) {
                // 请求成功响应之后的方法
                mappedHandler.applyAfterConcurrentHandlingStarted(processedRequest, response);
            }
        }
        else {
            // Clean up any resources used by a multipart request.
            if (multipartRequestParsed) {
                cleanupMultipart(processedRequest);
            }
        }
    }
}
```

​		getHandler（processedRequest）方法实际上就是从 HandlerMapping 中找到 url和Controller的对应关系。也就是Map<url，Controller>。我们知道，最终处理Request 的是Controller 中的方法，我们现在只是知道了Controller，我们如何确认Controllel 中处理Request的方法呢?继续往下看。
​		从Map<urls，beanName>中取得Controller后，经过拦截器的预处理方法，再通过反射获取该方法上的注解和参数，解析方法和参数上的注解，然后反射调用方法获取ModelAndView结果视图。最后，调用的就是RequestMappingHandlerAdapter的handle（）中的核心逻辑由handleInternal（request， response， handler）实现。

```java
@Override
protected ModelAndView handleInternal(HttpServletRequest request,
                                      HttpServletResponse response, HandlerMethod handlerMethod) throws Exception {

    ModelAndView mav;
    checkRequest(request);

    // Execute invokeHandlerMethod in synchronized block if required.
    if (this.synchronizeOnSession) {
        HttpSession session = request.getSession(false);
        if (session != null) {
            Object mutex = WebUtils.getSessionMutex(session);
            synchronized (mutex) {
                mav = invokeHandlerMethod(request, response, handlerMethod);
            }
        }
        else {
            // No HttpSession available -> no mutex necessary
            mav = invokeHandlerMethod(request, response, handlerMethod);
        }
    }
    else {
        // No synchronization on session demanded at all...
        mav = invokeHandlerMethod(request, response, handlerMethod);
    }

    if (!response.containsHeader(HEADER_CACHE_CONTROL)) {
        if (getSessionAttributesHandler(handlerMethod).hasSessionAttributes()) {
            applyCacheSeconds(response, this.cacheSecondsForSessionAttributeHandlers);
        }
        else {
            prepareResponse(response);
        }
    }

    return mav;
}
```

​		整个处理过程中最核心的逻辑其实就是拼接Controller的url和方法的url，与Request 的 url 进行匹配，找到匹配的方法。

AbstractHandlerMethodMapping

```java
@Override
protected HandlerMethod getHandlerInternal(HttpServletRequest request) throws Exception {
    String lookupPath = getUrlPathHelper().getLookupPathForRequest(request);
    if (logger.isDebugEnabled()) {
        logger.debug("Looking up handler method for path " + lookupPath);
    }
    this.mappingRegistry.acquireReadLock();
    try {
        HandlerMethod handlerMethod = lookupHandlerMethod(lookupPath, request);
        if (logger.isDebugEnabled()) {
            if (handlerMethod != null) {
                logger.debug("Returning handler method [" + handlerMethod + "]");
            }
            else {
                logger.debug("Did not find handler method for [" + lookupPath + "]");
            }
        }
        return (handlerMethod != null ? handlerMethod.createWithResolvedBean() : null);
    }
    finally {
        this.mappingRegistry.releaseReadLock();
    }
}
```

​		通过上面的代码分析，已经可以找到处理 Request的Controller中的方法了，现在看如何解析该方法上的参数，并反射调用该方法。

RequestMappingHandlerAdapter

```java
/** 获取处理请求的方法,执行并返回结果视图 **/
@Nullable
protected ModelAndView invokeHandlerMethod(HttpServletRequest request,
                                           HttpServletResponse response, HandlerMethod handlerMethod) throws Exception {

    ServletWebRequest webRequest = new ServletWebRequest(request, response);
    try {
        WebDataBinderFactory binderFactory = getDataBinderFactory(handlerMethod);
        ModelFactory modelFactory = getModelFactory(handlerMethod, binderFactory);

        ServletInvocableHandlerMethod invocableMethod = createInvocableHandlerMethod(handlerMethod);
        if (this.argumentResolvers != null) {
            invocableMethod.setHandlerMethodArgumentResolvers(this.argumentResolvers);
        }
        if (this.returnValueHandlers != null) {
            invocableMethod.setHandlerMethodReturnValueHandlers(this.returnValueHandlers);
        }
        invocableMethod.setDataBinderFactory(binderFactory);
        invocableMethod.setParameterNameDiscoverer(this.parameterNameDiscoverer);

        ModelAndViewContainer mavContainer = new ModelAndViewContainer();
        mavContainer.addAllAttributes(RequestContextUtils.getInputFlashMap(request));
        modelFactory.initModel(webRequest, mavContainer, invocableMethod);
        mavContainer.setIgnoreDefaultModelOnRedirect(this.ignoreDefaultModelOnRedirect);

        AsyncWebRequest asyncWebRequest = WebAsyncUtils.createAsyncWebRequest(request, response);
        asyncWebRequest.setTimeout(this.asyncRequestTimeout);

        WebAsyncManager asyncManager = WebAsyncUtils.getAsyncManager(request);
        asyncManager.setTaskExecutor(this.taskExecutor);
        asyncManager.setAsyncWebRequest(asyncWebRequest);
        asyncManager.registerCallableInterceptors(this.callableInterceptors);
        asyncManager.registerDeferredResultInterceptors(this.deferredResultInterceptors);

        if (asyncManager.hasConcurrentResult()) {
            Object result = asyncManager.getConcurrentResult();
            mavContainer = (ModelAndViewContainer) asyncManager.getConcurrentResultContext()[0];
            asyncManager.clearConcurrentResult();
            if (logger.isDebugEnabled()) {
                logger.debug("Found concurrent result value [" + result + "]");
            }
            invocableMethod = invocableMethod.wrapConcurrentResult(result);
        }

        invocableMethod.invokeAndHandle(webRequest, mavContainer);
        if (asyncManager.isConcurrentHandlingStarted()) {
            return null;
        }

        return getModelAndView(mavContainer, modelFactory, webRequest);
    }
    finally {
        webRequest.requestCompleted();
    }
}
```

​		invocableMethod.invokeAndHandle（）最终要实现的目的就是∶完成 Request 中的参数和方法参数上数据的绑定。Spring MVC中提供两种 Request参数到方法中参数的绑定方式∶

1、通过注解进行绑定，@RequestParam。

2、通过参数名称进行绑定。

​		使用注解进行绑定，我们只要在方法参数前面声明@RequestParam（"name"），就可以将 request中参数name的值绑定到方法的该参数上。使用参数名称进行绑定的前提是必须要获取方法中参数的名称，Java 反射只提供了获取方法的参数的类型，并没有提供获取参数名称的方法。SpringMVC解决这个问题的方法是用 asm框架读取字节码文件，获取方法的参数名称。asm框架是一个字节码操作框架，关于asm更多介绍可以参考其官网。个人建议，使用注解来完成参数绑定，这样就可以省去asm框架的读取字节码的操作。

InvocableHandlerMethod

```java
@Nullable
public Object invokeForRequest(NativeWebRequest request, @Nullable ModelAndViewContainer mavContainer,
                               Object... providedArgs) throws Exception {

    Object[] args = getMethodArgumentValues(request, mavContainer, providedArgs);
    if (logger.isTraceEnabled()) {
        logger.trace("Invoking '" + ClassUtils.getQualifiedMethodName(getMethod(), getBeanType()) +
                     "' with arguments " + Arrays.toString(args));
    }
    Object returnValue = doInvoke(args);
    if (logger.isTraceEnabled()) {
        logger.trace("Method [" + ClassUtils.getQualifiedMethodName(getMethod(), getBeanType()) +
                     "] returned [" + returnValue + "]");
    }
    return returnValue;
}
private Object[] getMethodArgumentValues(NativeWebRequest request, @Nullable ModelAndViewContainer mavContainer,
                                         Object... providedArgs) throws Exception {

    MethodParameter[] parameters = getMethodParameters();
    Object[] args = new Object[parameters.length];
    for (int i = 0; i < parameters.length; i++) {
        MethodParameter parameter = parameters[i];
        parameter.initParameterNameDiscovery(this.parameterNameDiscoverer);
        args[i] = resolveProvidedArgument(parameter, providedArgs);
        if (args[i] != null) {
            continue;
        }
        if (this.argumentResolvers.supportsParameter(parameter)) {
            try {
                args[i] = this.argumentResolvers.resolveArgument(
                    parameter, mavContainer, request, this.dataBinderFactory);
                continue;
            }
            catch (Exception ex) {
                if (logger.isDebugEnabled()) {
                    logger.debug(getArgumentResolutionErrorMessage("Failed to resolve", i), ex);
                }
                throw ex;
            }
        }
        if (args[i] == null) {
            throw new IllegalStateException("Could not resolve method parameter at index " +
                                            parameter.getParameterIndex() + " in " + parameter.getExecutable().toGenericString() +
                                            ": " + getArgumentResolutionErrorMessage("No suitable resolver for", i));
        }
    }
    return args;
}

```

​		关于asm框架获取方法参数的部分这里就不再进行分析了。感兴趣的小伙伴可以继续深入了解这个处理过程。
​		这里方法的参数值列表也获取到了，就可以直接进行方法的调用了。整个请求过程中最复杂的一步就是在这里了。到这里整个请求处理过程的关键步骤都已了解。理解了Spring MVC中的请求处理流程，整个代码还是比较清晰的。最后我们再来梳理一下Spring MVC核心组件的关联关系（如下图）∶

![Spring FrameworkMVC-2](images/Spring FrameworkMVC-2.png)

最后来一张时序图

![Spring FrameworkMVC-3](images/Spring FrameworkMVC-3.png)

## 2.8 Spring 数据访问

### 2.8.1 事务管理

#### 2.8.1.1 事务的基础配置

```xml
<aop:aspectj-autoproxy proxy-target-class="true"/>

<bean id="transactionManager" class="org.springframework.jdbc.datasource.DataSourceTransactionManager">
    <property name="dataSource" ref="dataSource"/>
</bean>

<!-- 配置事务通知属性 -->
<tx:advice id="transactionAdvice" transaction-manager="transactionManager">
    <tx:attributes>
        <tx:method name="add*" propagation="REQUIRED" rollback-for="Exception,RuntimeException,SQLException"/>
        <tx:method name="remove*" propagation="REQUIRED" rollback-for="Exception,RuntimeException,SQLException"/>
        <tx:method name="edit*" propagation="REQUIRED" rollback-for="Exception,RuntimeException,SQLException"/>
        <tx:method name="login" propagation="NOT_SUPPORTED"/>
        <tx:method name="query*" read-only="true"/>
    </tx:attributes>
</tx:advice>

<aop:config>
    <aop:advisor advice-ref="transactionAdvice" pointcut-ref="transactionPointcut"/>
    <aop:aspect ref="dataSource">
        <aop:pointcut id="transactionPointcut" expression="execution(public * com.gupaoedu..*.service..*Service.*(..))" />
    </aop:aspect>
</aop:config>
```

​		Spring事务管理基于 AOP来实现，主要是统一封装非功能性需求。

#### 2.8.1.2 事务原理详解

**1、事务基本概念**

​		事务（Transaction）是访问并可能更新数据库中各种数据项的一个程序执行单元（unit）。特点∶事务是恢复和并发控制的基本单位。事务应该具有4个属性∶原子性、一致性、隔离性、持久性。这四个属性通常称为 ACID 特性。

​		原子性（Automicity）。一个事务是一个不可分割的工作单位，事务中包括的诸操作要么都做，要么都不做。

​		一致性（Consistency）。事务必须是使数据库从一个一致性状态变到另一个一致性状态。一致性与原子性是密切相关的。

​		隔离性（Lsolation）。一个事务的执行不能被其他事务干扰。即一个事务内部的操作及与用的数据对并发的其他事务是隔离的，并发执行的各个事务之间不能互相干扰。

​		持久性（Durability）。持久性也称永久性（Permanence），指一个事务一旦提交，它
对数据库中数据的改变就应该是永久性的。接下来的其他操作或故障不应该对其有任何影响。

**2、事务的基本原理**

​		Spring事务的本质其实就是数据库对事务的支持，没有数据库的事务支持，spring是无法提供事务功能的。对于纯JDBC操作数据库，想要用到事务，可以按照以下步骤进行：

	1. 获取连接Connection con=DriverManager.getConnection（）
 	2. 开启事务 con.setAutoCommit（true/false）;
 	3. 执行 CRUD
 	4. 提交事务/回滚事务 con.commit（）/con.rollback（;
 	5. 关闭连接 conn.lose（）;

​		使用Spring的事务管理功能后我们可以不再写步骤 2和 4的代码 而是由Spirng 自动完成。那么Spring 是如何在我们书写的CRUD 之前和之后开启事务和关闭事务的尼?解决这个问题，也就可以从整体上理解 Spring的事务管理实现原理了。

​		下面简单地介绍下，注解方式为例子：

​		配置文件开启注解驱动，在相关的类和方法上通过注解`@Transactional`标识。Spring在启动的时候会去解析生成相关的 bean，这时候会查看拥有相关注解的类和方法，并且为这些类和方法生成代理，并根据`@Transaction`的相关参数进行相关配置注入，这样就在代理中为我们把相关的事务处理掉了（开启正常提交事务，异常回滚事务）。真正的数据库层的事务提交和回滚是通过 binlog或者 redo log实现的。

**3、Spring 事务的传播属性**

​		所谓 spring事务的传播属性，就是定义在存在多个事务同时存在的时候，spring应该如可处理这些事务的行为。这些属性在TransactionDefinition中定义，具体常量的解释见。

TransactionDefinition中的定义

```java
int PROPAGATION_REQUIRED = 0;

int PROPAGATION_SUPPORTS = 1;

int PROPAGATION_MANDATORY = 2;

int PROPAGATION_REQUIRES_NEW = 3;

int PROPAGATION_NOT_SUPPORTED = 4;

int PROPAGATION_NEVER = 5;

int PROPAGATION_NESTED = 6;
```

定义描述

| 常量名称                  | 常量解释                                                     |
| ------------------------- | ------------------------------------------------------------ |
| PROPAGATION_REQUIRED      | 支持当前事务，如果当前没有事务，就新建一个事务。这是最常见的选择，也是 Spring 默认的事务的传播。 |
| PROPAGATION_SUPPORTS      | 支持当前事务，如果当前没有事务，就以非事务方式执行。         |
| PROPAGATION_MANDATORY     | 支持当前事务，如果当前没有事务，就抛出异常。                 |
| PROPAGATION_REQUIRES_NEW  | 新建事务，如果当前存在事务，把当前事务挂起。新建的事务将和被挂起的事务没有任何关系，是两个独立的事务，外层事务失败回滚之后，不能回滚内层事务执行的结果，内层事务失败抛出异常，外层事务捕获，也可以不处理回滚操作 |
| PROPAGATION_NOT_SUPPORTED | 以非事务方式执行操作，如果当前存在事务，就把当前事务挂起。   |
| PROPAGATION_NEVER         | 以非事务方式执行，如果当前存在事务，则抛出异常。             |
| PROPAGATION_NESTED        | 如果一个活动的事务存在，则运行在一个嵌套的事务中。如果没有活动事务，则按REQUIRED属性执行。它使用了一个单独的事务，这个事务拥有多个可以回滚的保存点。内部事务的回滚不会对外部事务造成影响。它只对DataSourceTransactionManager事务管理器起效。 |

**4、数据库隔离级别**

| 隔离级别         | 隔离级别的值 | 导致的问题导                                                 |
| ---------------- | ------------ | ------------------------------------------------------------ |
| Read-Uncommitted | 0            | 致脏读                                                       |
| Read-Committed   | 1            | 避免脏读，允许不可重复读和幻读                               |
| Repeatable-Read  | 2            | 避免脏读，不可重复读，允许幻读                               |
| Serializable     | 3            | 串行化读，事务只能一个一个执行，避免了脏读、不可重复读、幻读。<br/>执行效率慢，使用时慎重 |

​		脏读∶一个事务对数据进行了增删改，但未提交，另一个事务可以读取到未提交的数据。如果第一个事务这时候回滚了，那么第二个事务就读到了脏数据。

​		不可重复读∶一个事务中发生了两次读操作，第一次读操作和第二次操作之间，另外一个事务对数据进行了修改，这时候两次读取的数据是不一致的。

​		幻读∶第一个事务对一定范围的数据进行批量修改，第二个事务在这个范围增加一条数据，这时候第一个事务就会丢失对新增数据的修改。

​		总结∶

​		隔离级别越高，越能保证数据的完整性和一致性，但是对并发性能的影响也越大。大多数的数据库默认隔离级别为 Read Commited，比如 SqlServer、Oracle 少数数据库默认隔离级别为∶Repeatable Read 比如∶MySQL InnoDB

------

**5、Spring中的隔离级别**

| 常量                       | 解释                                                         |
| -------------------------- | ------------------------------------------------------------ |
| ISOLATION_DEFAULT          | 这是个 PlatfromTransactionManager 默认的隔离级别，使用数据库默认的事务隔离级别。另外四个与 JDBC 的隔离级别相对应。 |
| ISOLATION_READ_UNCOMMITTED | 这是事务最低的隔离级别，它允许另外一个事务可以看到这个事务未提交的数据。这种隔离级别会产生脏读，不可重复读和幻像读。 |
| ISOLATION_READ_COMMITTED   | 保证一个事务修改的数据提交后才能被另外一个事务读取。另外一个事务不能读取该事务未提交的数据。 |
| ISOLATION_REPEATABLE_READ  | 这种事务隔离级别可以防止脏读，不可重复读。但是可能出现幻像读。 |
| ISOLATION_SERIALIZABLE     | 这是花费最高代价但是最可靠的事务隔离级别。事务被处理为顺序执行。 |

**6、事务的嵌套**

​		通过上面的理论知识的铺垫，我们大致知道了数据库事务和 Spring 事务的一些属性和特点，接下来我们通过分析一些嵌套事务的场景，来深入理解 Spring 事务传播的机制。假设外层事务 ServiceA的 MethodA()调用 内层Service B的 MethodB() 

**PROPAGATION_REQUIRED（spring 默认）**

​		如果ServiceB.MethodB（）的事务级别定义为 PROPAGATION_REQUIRED，那么执行ServiceA.MethodA（）的时候Spring 已经起了事务，这时调用 ServiceB.MethodB（），ServiceB.MethodB（）看到自己已经运行在 ServiceA.MethodA（）的事务内部，就不再起新的事务。

​		假如ServiceB.MethodB（）运行的时候发现自己没有在事务中，他就会为自己分配一个事务。

​		这样，在 ServiceA.MethodA（）或者在 ServiceB.MethodB（）内的任何地方出现异常，事务都会被回滚。

**PROPAGATION_REQUIRES_NEW**

​		比如我们设计 ServiceA.MethodA（）的事务级别为 PROPAGATION_REQUIRED，ServiceB.MethodB（）的事务级别为 PROPAGATION_REQUIRES_NEW。那么当执行到 ServiceB.MethodB（）的时候，ServiceA.MethodA（）所在的事务就会挂起，ServiceB.MethodB（）会起一个新的事务，等待 ServiceB.MethodB（）的事务完成以后，它才继续执行。

​		它与 PROPAGATION_REQUIRED 的事务区别在于事务的回滚程度了。因为ServiceB.MethodB（） 是新起一个事务，那么就是存在两个不同的事务。如果ServiceB.MethodB（） 已 经提 交，那 么 ServiceA.MethodA（） 失 败 回 滚，ServiceB.MethodB（）是不会回滚的。如果 ServiceB.MethodB（）失败回滚，如果他出的异常被 ServiceA.MethodA（）捕获ServiceA.MethodA（）事务仍然可能提交（主要看B抛出的异常是不是A 会回滚的异常）。

**PROPAGATION_SUPPORTS**

​		假设ServiceB.MethodB（）的事务级别为 PROPAGATION_SUPPORTS，那么当执行到ServiceB.MethodB（）时，如果发现 ServiceA.MethodA（）已经开启了一个事务，则加入当前的事务，如果发现 ServiceA.MethodA（）没有开启事务，则自己也不开启事务。这种时候，内部方法的事务性完全依赖于最外层的事务。

**PROPAGATION_NESTED**

​		现在的 情况就变得比较复杂了，ServiceB.MethodB（）的事务属性被配置为PROPAGATION_NESTED，此时两者之间又将如何协作呢? ServiceB.MethodB（）如果 rollback，那么内部事务（即 ServiceB.MethodB（））将回滚到它执行前的 SavePoint 而外部事务（即ServiceA.MethodA（））可以有以下两种处理方式∶

捕获异常，执行异常分支逻辑

```java
void MethodA(){
	try{
		ServiceB.MethodB();
    }catch(SomeException){
		//执行其他业务，如 ServiceC.MethodC();
    }
}
```

​		这种方式也是嵌套事务最有价值的地方，它起到了分支执行的效果，如果ServiceB.MethodB （）失败，那么执行 ServiceC.MethodC（），而 ServiceB.MethodB（） 已经回滚到它执行之前的 SavePoint， 所以不会产生脏数据（相当于此方法从未执行过），这种特性可以用在某些特殊的业务中，而 PROPAGATION_REQUIRED 和PROPAGATION_REQUIRES_NEW都没有办法做到这一点。

​		外部事务回滚/提交 代码不做任何修改，那么如果内部事务（ServiceB.MethodB（））rollback，那么首先 ServiceB.MethodB（）回滚到它执行之前的 SavePoint（在任何情况下都会如此），外部事务（即 ServiceA.MethodA（））将根据具体的配置决定自己是commit 还是 rollback。

​		另外三种事务传播属性基本用不到，在此不做分析。

**7、Spring事务API 架构图**

![Spring Framework事务-1](images/Spring Framework事务-1.png)



​		使用Spring进行基本的JDBC访问数据库有多种选择。Spring 至少提供了三种不同的工作模式∶JdbcTemplate，一个在 Spring2.5中新提供的 SimplelJdbc类能够更好的处理数据库元数据;还有一种称之为RDBMS Object的风格的面向对象封装方式，有点类似于JDO的查询设计。我们在这里简要列举你采取某一种工作方式的主要理由.不过请注意，即使你选择了其中的一种工作模式，你依然可以在你的代码中混用其他任何一种模式以获取其带来的好处和优势。所有的工作模式都必须要求 JDBC 2.0以上的数据库驱动的支持，其中一些高级的功能可能需要JDBC 3.0以上的数据库驱动支持。

​		JdbcTemplate-这是经典的也是最常用的Spring对于JDBC 访问的方案。这也是最低级别的封装，其他的工作模式事实上在底层使用了JdbcTemplate作为其底层的实现基础。JdbcTemplate在 JDK 1.4以上的环境上工作得很好。

​		NamedParameterJdbcTemplate-对JdbcTemplate做了封装，提供了更加便捷的基于命名参数的使用方式而不是传统的 JDBC所使用的"?"作为参数的占位符。这种方式你需要为某个SQL指定许多个参数时，显得更加直观而易用。该特性必须工作在JDK 14以上。

​		SimpleldbcTemplate-这个类结合了JdbcTemplate 和NamedParameterJdbcTemplate的最常用的功能，同时它也利用了一些Java 5的特性所带来的优势，例如泛型、varargs和 autoboxing等，从而提供了更加简便的 API访问方式。需要工作在 Java5以上的环境中。

​		SimpleJdbclnsert 和 SimpleJdbcCall-这两个类可以充分利用数据库元数据的特性来简化配置。通过使用这两个类进行编程，你可以仅仅提供数据库表名或者存储过程的名称以及一个 Map作为参数。其中Map的 key需要与数据库表中的字段保持一致。这两个类通常和 SimpleJdbcTemplate配合使用。这两个类需要工作在JDK5以上，同时数据库需要提供足够的元数据信息。

​		RDBMS 对象包括MappingSqlQuery，SqlUpdate and StoredProcedure-这种方式允许你在初始化你的数据访问层时创建可重用并且线程安全的对象。该对象在你定义了你的查询语句，声明查询参数并编译相应的 Query 之后被模型化。一旦模型化完成，任何执行函数就可以传入不同的参数对之进行多次调用。这种方式需要工作在 JDK 1.4以上。

#### 2.8.1.3 数据管理模块

**1、异常处理接口**

​		SQLExceptionTranslator 是一 个接 口，如果你需要在 SQLException 和org.springframework.dao.DataAccessException之间作转换，那么必须实现该接口。转换器类的实现可以采用一般通用的做法（比如使用 JDBC的SQLState code），如果为了使转换更准确，也可以进行定制（比如使用 Oracle的error code）。

​		SQLErrorCodeSQLExceptionTranslator是SQLExceptionTranslator的默认实现。该实现使用指定数据库厂商的 error code，比采用SQLState更精确。转换过程基于一个JavaBean（类型为 SQLErrorCodes）中的 error code。 这个 JavaBean 由SQLErrorCodesFactory工厂类创建，其中的内容来自于"sql-error-codes.xml"配置文件。该文件中的数据库厂商代码基于 Database MetaData 信 息中的DatabaseProductName，从而配合当前数据库的使用。

​		SQLErrorCodeSQLExceptionTranslator 使用以下的匹配规则∶

首先检查是否存在完成定制转换的子类实现。通常SQLErrorCodeSQLExceptionTranslator 这个类可以作为一个具体类使用，不需要进行定制，那么这个规则将不适用。

​		接着将SQLException的error code与错误代码集中的error code进行匹配。默认情记下错误代码集将从SQLErrorCodesFactory取得。错误代码集来自classpath下的sql-error-codes.xml文件，它们将与数据库metadata信息中的 database name进行映射。
使用 fallback翻译器。SQLStateSQLExceptionTranslator类是缺省的 fallback翻译器。

**2、config模块**
		NamespaceHandler接口，DefaultBeanDefinitionDocumentReader 使用该接口来处理在 spring xml 配置文件中自定义的命名空间。

​		在Jdbc模块，我们使用JdbcNamespaceHandler来处理jdbc配置的命名空间，其代码如下∶

```java
public class JdbcNamespaceHandler extends NamespaceHandlerSupport{
	@override public void init(){
        registerBeanDefinitionParser("embeded-database", new EmbeddedDatabaseBeanDefinitionParser();
        registerBeanDefinitionParser("initialize-database", new InitializeDatabaseBeanDefinitionParser();
    }
}
```

​		其中，EmbeddedDatabaseBeanDefinitionParser继承了AbstractBeanDefinitionParser，解析`<embedded-database>`元素，并使用EmbeddedDatabaseFactoryBean 创建一个 BeanDefinition。顺便介绍一下用到的软件包 org.w3c.dom。

​		软件包 org.w3c.dom∶为文档对象模型（DOM）提供接口，该模型是 Java API for XM Processing 的组件 API。该 Document Object Model Level 2Core API允许程序动态访问和更新文档的内容和结构。

- Attr∶Attr 接口表示 Element 对象中的属性。
- CDATASection∶CDATA 节用于转义文本块，该文本块包含的字符如果不转义则会被视为标记。
- CharacterData∶CharacterData 接口使用属性集合和用于访问 DOM 中字符数据的方法扩展节点。
- Comment∶此接口继承自CharacterData 表示注释的内容，即起始`'<!--'`和结`'->'`之间的所有字符。
- Document∶Document接口表示整个 HTML 或 XML 文档。
- DocumentFragment∶DocumentFragment 是"轻量级"或"最小"Document 对象。
- DocumentType∶每个 Document 都有 doctype 属性，该属性的值可以为 null，也可以为 DocumentType 对象。
- DOMConfiguration∶该 DOMConfiguration接口表示文档的配置，并维护一个可i 别的参数表。
- DOMError∶DOMError是一个描述错误的接口。
- DOMErrorHandler∶DOMErrorHandler 是在报告处理 XML 数据时发生的错误或在进行某些其他处理（如验证文档）时 DOM 实现可以调用的回调接口。
- DOMImplementation∶DOMImplementation 接口为执行独立于文档对象模型的任何特定实例的操作提供了许多方法。
- DOMImplementationList∶DOMImplementationList 接口提供对 DOM 实现的有序集合的抽象，没有定义或约束如何实现此集合。
- DOMImplementationSource∶此接口允许 DOM 实现程序根据请求的功能和版本提供一个或多个实现，如下所述。
- DOMLocator∶DOMLocator 是一个描述位置（如发生错误的位置）的接口。
- MStringList∶DOMStringLlist接口提供对 DOMString 值的有序集合的抽象，没有定义或约束此集合是如何实现的。
- Element∶Element 接口表示 HTML 或 XML 文档中的一个元素。
- Entity∶此接口表示在 XML 文档中解析和未解析的已知实体。
- EntityReference∶EntityReference 节点可以用来在树中表示实体引用。
- NamedNodeMap∶实现 NamedNodeMap 接口的对象用于表示可以通过名称访问的节点的集合。
  NameList：NameList 接口提供对并行的名称和名称空间值对（可以为 null 值）的有序集合的抽象，无需定义或约束如何实现此集合。
- Node∶该 Node 接口是整个文档对象模型的主要数据类型。
- NodeList∶NodeList 接口提供对节点的有序集合的抽象，没有定义或约束如何实现此集合。
- Notation∶此接口表示在 DTD 中声明的表示法。
- ProcessingInstruction∶ProcessingInstruction接口表示"处理指令"，该指令作为一种在文档的文本中保持特定于处理器的信息的方法在 XML 中使用。
- Text∶该 Text 接口继承自CharacterData，并且表示 Element 或 Attr的文本内容（在 XML中称为 字符数据）。
- Typelnfo∶Typelnfo 接口表示从 Element 或 Attr 节点引用的类型，用与文档相关的模式指定。
- UserDataHandler∶当使用Node.setUserData（）将一个对象与节点上的键相关联时，当克隆、导入或重命名该对象关联的节点时应用程序可以提供调用的处理程序。

**3、Core模块**

​		**3.1JdbcTemplate**

<img src="images/Spring Framework事务-2.png" alt="Spring Framework事务-2" style="zoom:25%;" />

​		**3.2 RowMapper**

<img src="images/Spring Framework事务-3.png" alt="Spring Framework事务-3" style="zoom:30%;" />

​		**3.3元数据 metaData 模块**

​		本节中spring应用到工厂模式，结合代码可以更具体了解。

<img src="images/Spring Framework事务-4.png" alt="Spring Framework事务-4" style="zoom:50%;" />

​		CallMetaDataProviderFactory创建 CallMetaDataProvider的工厂类，其代码如下∶

```java
public class CallMetaDataProviderFactory {

	public static final List<String> supportedDatabaseProductsForProcedures = Arrays.asList(
			"Apache Derby",
			"DB2",
			"MySQL",
			"Microsoft SQL Server",
			"Oracle",
			"PostgreSQL",
			"Sybase"
		);

	public static final List<String> supportedDatabaseProductsForFunctions = Arrays.asList(
			"MySQL",
			"Microsoft SQL Server",
			"Oracle",
			"PostgreSQL"
		);

	private static final Log logger = LogFactory.getLog(CallMetaDataProviderFactory.class);

	static public CallMetaDataProvider createMetaDataProvider(DataSource dataSource, final CallMetaDataContext context) {
		try {
			CallMetaDataProvider result = (CallMetaDataProvider) JdbcUtils.extractDatabaseMetaData(dataSource, databaseMetaData -> {
				String databaseProductName = JdbcUtils.commonDatabaseName(databaseMetaData.getDatabaseProductName());
				boolean accessProcedureColumnMetaData = context.isAccessCallParameterMetaData();
				if (context.isFunction()) {
					if (!supportedDatabaseProductsForFunctions.contains(databaseProductName)) {
						if (logger.isWarnEnabled()) {
							logger.warn(databaseProductName + " is not one of the databases fully supported for function calls " +
									"-- supported are: " + supportedDatabaseProductsForFunctions);
						}
						if (accessProcedureColumnMetaData) {
							logger.warn("Metadata processing disabled - you must specify all parameters explicitly");
							accessProcedureColumnMetaData = false;
						}
					}
				}
				else {
					if (!supportedDatabaseProductsForProcedures.contains(databaseProductName)) {
						if (logger.isWarnEnabled()) {
							logger.warn(databaseProductName + " is not one of the databases fully supported for procedure calls " +
									"-- supported are: " + supportedDatabaseProductsForProcedures);
						}
						if (accessProcedureColumnMetaData) {
							logger.warn("Metadata processing disabled - you must specify all parameters explicitly");
							accessProcedureColumnMetaData = false;
						}
					}
				}
				CallMetaDataProvider provider;
				if ("Oracle".equals(databaseProductName)) {
					provider = new OracleCallMetaDataProvider(databaseMetaData);
				}
				else if ("DB2".equals(databaseProductName)) {
					provider = new Db2CallMetaDataProvider((databaseMetaData));
				}
				else if ("Apache Derby".equals(databaseProductName)) {
					provider = new DerbyCallMetaDataProvider((databaseMetaData));
				}
				else if ("PostgreSQL".equals(databaseProductName)) {
					provider = new PostgresCallMetaDataProvider((databaseMetaData));
				}
				else if ("Sybase".equals(databaseProductName)) {
					provider = new SybaseCallMetaDataProvider((databaseMetaData));
				}
				else if ("Microsoft SQL Server".equals(databaseProductName)) {
					provider = new SqlServerCallMetaDataProvider((databaseMetaData));
				}
				else if ("HDB".equals(databaseProductName)) {
					provider = new HanaCallMetaDataProvider((databaseMetaData));
				}
				else {
					provider = new GenericCallMetaDataProvider(databaseMetaData);
				}
				if (logger.isDebugEnabled()) {
					logger.debug("Using " + provider.getClass().getName());
				}
				provider.initializeWithMetaData(databaseMetaData);
				if (accessProcedureColumnMetaData) {
					provider.initializeWithProcedureColumnMetaData(databaseMetaData,
							context.getCatalogName(), context.getSchemaName(), context.getProcedureName());
				}
				return provider;
			});
			return result;
		}
		catch (MetaDataAccessException ex) {
			throw new DataAccessResourceFailureException("Error retrieving database metadata", ex);
		}
	}

}
```

​		TableMetaDataProviderFactory创建 TableMetaDataProvider工厂类，其创建过程如下∶

```java
public class TableMetaDataProviderFactory {

	private static final Log logger = LogFactory.getLog(TableMetaDataProviderFactory.class);

	public static TableMetaDataProvider createMetaDataProvider(DataSource dataSource, TableMetaDataContext context) {
		try {
			TableMetaDataProvider result = (TableMetaDataProvider) JdbcUtils.extractDatabaseMetaData(dataSource, databaseMetaData -> {
				String databaseProductName =
						JdbcUtils.commonDatabaseName(databaseMetaData.getDatabaseProductName());
				boolean accessTableColumnMetaData = context.isAccessTableColumnMetaData();
				TableMetaDataProvider provider;
				if ("Oracle".equals(databaseProductName)) {
					provider = new OracleTableMetaDataProvider(databaseMetaData,
							context.isOverrideIncludeSynonymsDefault());
				}
				else if ("HSQL Database Engine".equals(databaseProductName)) {
					provider = new HsqlTableMetaDataProvider(databaseMetaData);
				}
				else if ("PostgreSQL".equals(databaseProductName)) {
					provider = new PostgresTableMetaDataProvider(databaseMetaData);
				}
				else if ("Apache Derby".equals(databaseProductName)) {
					provider = new DerbyTableMetaDataProvider(databaseMetaData);
				}
				else {
					provider = new GenericTableMetaDataProvider(databaseMetaData);
				}
				if (logger.isDebugEnabled()) {
					logger.debug("Using " + provider.getClass().getSimpleName());
				}
				provider.initializeWithMetaData(databaseMetaData);
				if (accessTableColumnMetaData) {
					provider.initializeWithTableColumnMetaData(databaseMetaData, context.getCatalogName(),
							context.getSchemaName(), context.getTableName());
				}
				return provider;
			});
			return result;
		}
		catch (MetaDataAccessException ex) {
			throw new DataAccessResourceFailureException("Error retrieving database metadata", ex);
		}
	}

}
```

​		**3.4 使用 SqlParameterSource提供参数值**

​		使用Map来指定参数值有时候工作得非常好，但是这并不是最简单的使用方式。Spring 是供了一些其他的 SqlParameterSource 实现类来指定参数值。我们首先可以看看BeanPropertySqlParameterSource类，这是一个非常简便的指定参数的实现类，只要你有一个符合JavaBean规范的类就行了。它将使用其中的getter方法来获取参数值，SqIParameter 封装了定义 sql 参数的对象。CallableStateMentCallback ，PrePareStateMentCallback，StateMentCallback，ConnectionCallback 回调类分别对应JdbcTemplate 中的不同处理方法。

<img src="images/Spring Framework事务-5.png" alt="Spring Framework事务-5" style="zoom:30%;" />

​		**3.5 simple实现**

<img src="images/Spring Framework事务-6.png" alt="Spring Framework事务-6" style="zoom:33%;" />

**4、DataSource**
		spring通过 DataSource获取数据库的连接。Datasource是jdbc规范的一部分，它通过ConnectionFactory获取。一个容器和框架可以在应用代码层中隐藏连接池和事务管理。

​		当使用spring的jdbc层，你可以通过JNDI来获取 DataSource，也可以通过你自己配置的第三方连接池实现来获取。流行的第三方实现由apache Jakarta Commons dbcp和c3p0。

<img src="images/Spring Framework事务-7.png" alt="Spring Framework事务-7" style="zoom:33%;" />

​		TransactionAwareDataSourceProxy 作为目标 DataSource的一个代理，在对目标DataSource包装的同时，还增加了Spring的事务管理能力，在这一点上，这个类的功能非常像J2EE服务器所提供的事务化的JNDI DataSource。

**Note**

​		该类几乎很少被用到，除非现有代码在被调用的时候需要一个标准的 JDBC DataSource 接口实现作为参数。这种情况下，这个类可以使现有代码参与Spring的事务管理。通常最好的做法是使用更高层的抽象 来对数据源进行管理，比如 JdbcTemplate和DataSourceUtils 等等。

​		注意∶DriverManagerDataSource 仅限于测试使用，因为它没有提供池的功能，这会导致在多个请求获取连接时性能很差。

**5、object模块**

![Spring Framework事务-8](images/Spring Framework事务-8.png)

**6、JdbcTemplate**

​		JdbcTemplate是core包的核心类。它替我们完成了资源的创建以及释放工作，从而简化了我们对 JDBC的使用。它还可以帮助我们避免一些常见的错误，比如忘记关闭数据库连接。JdbcTemplate将完成JDBC核心处理流程，比如 SQL语句的创建、执行，把SQL 语句的生成以及查询结果的提取工作留给我们的应用代码。它可以完成SQL查询、更新以及调用存储过程可以对ResultSet进行遍历并加以提取。它还可以捕获JDBC 异常并将其转换成 org.springframework.dao 包中定义的 通用的信息更丰富的异常。使用 JdbcTemplate进行编码只需要根据明确定义的一组契约来实现回调接口。PreparedStatementCreator 回 调接 口 通 过 给 定的 Connection 创建一个PreparedStatement，包含 SQL和任何相关的参数。CallbleStatementCreateor实现同样的处理，只不过它创建的是CallableStatement。RowCallbackHandler接口则从数据集的每一行中提取值。

​		我们可以在DAO实现类中通过传递一个DataSource 引用来完成JdbcTemplate的实例化也可以在Spring的IOC容器中配置一个 JdbcTemplate的bean并赋予DAO实现类作为一个实例。需要注意的是 DataSource在 Spring的 IOC容器中总是配制成一个Bean，第一种情况下，DataSource bean将传递给 service，第二种情况下 DataSource bean传递给JdbcTemplate bean。

**7、NamedParameterJdbcTemplate**

​		NamedParameterJdbcTemplate类为 JDBC操作增加了命名参数的特性支持，而不是传统的使用（'?'）作为参数的占位符。NamedParameterJdbcTemplate 类对dbcTemplate类进行了封装，在底层，JdbcTemplate 完成了多数的工作。

#### 2.8.1.4 浅谈分布式事务

​		现今互联网界，分布式系统和微服务架构盛行。一个简单操作，在服务端非常可能是由多个服务和数据库实例协同完成的。在一致性要求较高的场景下，多个独立操作之间的—致性问题显得格外棘手。
​		基于水平扩容能力和成本考虑，传统的强一致的解决方案（e.g.单机事务）纷纷被抛弃。其理论依据就是响当当的CAP原理。往往为了可用性和分区容错性，忍痛放弃强一致支持，转而追求最终一致性。

**分布式系统的特性**

​		分布式系统中，同时满足CAP定律中的一致性Consistency、可用性 Availability和分区容错性 Partition Tolerance三者是不可能的。在绝大多数的场景，都需要牺牲强一致性来换取系统的高可用性，系统往往只需要保证最终一致性。

​		分布式事务服务（Distributed Transaction Service，DTS）是一个分布式事务框架，用来保障在大规模分布式环境下事务的最终一致性。
​		CAP理论告诉我们在分布式存储系统中，最多只能实现上面的两点。而由于当前的网络硬件肯定会出现延迟丢包等问题，所以分区容忍性是我们必须需要实现的，所以我们只能在一致性和可用性之间进行权衡。

​		为了保障系统的可用性，互联网系统大多将强一致性需求转换成最终一致性的需求，并通过系统执行幂等性的保证，保证数据的最终一致性。

**数据一致性理解∶**

​		强一致性∶当更新操作完成之后，任何多个后续进程或者线程的访问都会返回最新的更新过的值。这种是对用户最友好的，就是用户上一次写什么，下一次就保证能读到什么。根据CAP理论，这种实现需要牺牲可用性。

​		弱一致性∶系统并不保证后续进程或者线程的访问都会返回最新的更新过的值。系统在数据写入成功之后，不承诺立即可以读到最新写入的值，也不会具体的承诺多久之后可以读到。

​		最终一致性∶弱一致性的特定形式。系统保证在没有后续更新的前提下，系统最终返回上一次更新操作的值。在没有故障发生的前提下，不一致窗口的时间主要受通信延迟，系统负载和复制副本的个数影响。DNS 是一个典型的最终一致性系统。

# dlkf