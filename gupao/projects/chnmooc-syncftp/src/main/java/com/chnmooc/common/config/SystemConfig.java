package com.chnmooc.common.config;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.chnmooc.common.utils.PropertieUtils;


/**
 * 同步单例配置
 * 
 * @author tanyongde
 * @createDate 2012-8-26
 */
public final class SystemConfig {
	private static Log log = LogFactory.getLog(SystemConfig.class);
	private static Set<String> allKeys = new HashSet<String>();//配置文件中所有的key
	private static final Map<String,String> keyValue = new HashMap<String,String>();//配置文件中所有的键值对
	private static SystemConfig config;
	
	public static String charSet = "utf-8";
	public static String rootDir = "/home/ftphome/";
	public static String usersRootDir="/home/ftphome/users/";
	public static String classRootDir="/home/ftphome/class/";
	public static String resourceRootDir="/home/ftphome/resource/";
	public static String filesDepot = "/home/FilesDepot/";
	public static String ftpPort = "1231";
	
	public static SystemConfig getInstance(){ return config; }
	
	private SystemConfig() {}
	
	public static String chartset = "utf-8";
	
	static{
		//单例
		config = null == config ? new SystemConfig() : config;
		//加载基础配置
		keyValue.put("system.charSet", "charSet");
		
		allKeys.addAll(keyValue.keySet());
		
		loadFromProp();
//		System.out.println(ffmpegPath);
	}
	
	/**
	 * 初始化系统，加载相关的配置信息及系统常量
	 */
	public static void loadFromProp(){
		PropertieUtils config = new PropertieUtils("system.properties");
		try{
			Set<String> keys = config.stringPropertyNames();
			for(String key : keys){
				if(!keyValue.values().contains(key)){
					keyValue.put(key, key);
					allKeys.add(key);
				}
			}
			for (String key : allKeys) {
				setValue(key,config.getString(key));
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(null != config){
				config.close();
			}
		}
	}
	
	/**
	 * 往静态配置中设值
	 * @param key allKeys 中的任意值
	 * @param value 设置值
	 */
	public static void setValue(String key,String value){
		try{
			String fieldName = keyValue.get(key);
			Field field = SystemConfig.class.getDeclaredField(fieldName);
			field.setAccessible(true);
			field.set(config, value);
		}catch(Exception e){
			log.error(e.getStackTrace());
		}
	}
	
	public static Set<String> getAllKeys(){
		return allKeys;
	}

	public static String getCharSet() {
		return charSet;
	}
	
}
