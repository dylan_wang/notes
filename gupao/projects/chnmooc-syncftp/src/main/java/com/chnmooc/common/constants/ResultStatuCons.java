package com.chnmooc.common.constants;

/**
 * 返回结果状态值
 * @author tanyongde
 * @date 2012-11-26
 */
public class ResultStatuCons {
	
	//======= 页面访问返回的状态码  =======
	/** 成功 */
	public static final int SUCCESS = 0;
	/** 内部错误 */
	public static final int ERROR = 1;
	/** 超时 */
	public static final int TIME_OUT = 2;
	/** 参数无效 */
	public static final int INVALID = 3;
	/** 没有权限访问 */
	public static final int UN_ALLOW = 4;
	
	
	//============ 用户登录返回的状态码 ============
	/**登录成功  */
	public static final int LOGIN_SUCCESS = 0;
	/** 登录失败 */
	public static final int LOGIN_ERROR = 1;
	/** 用户名错误  */
	public static final int LOGIN_NAME_ERR = 2;
	/** 密码错误 */
	public static final int LOGIN_PASS_ERR = 3;
	/** 该用户已过期 */
	public static final int LOGIN_OVER_TIME = 4;
	/** 用户超出登录次数限制 */
	public static final int LOGIN_OUT_COUNT = 5;
	/** 用户被禁用 */
	public static final int LOGIN_UNVALIABLE = 6;
	/** 用户被删除*/
	public static final int LOGIN_REMOVED = 7;
	/** 用户信息为空 */
	public static final int LOGIN_EMPTY = 8;
	
}
