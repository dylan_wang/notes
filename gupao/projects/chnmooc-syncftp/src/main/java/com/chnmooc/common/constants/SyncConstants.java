package com.chnmooc.common.constants;

public class SyncConstants {
	/** 文件类型文件夹 */
	public static final String FILE_TYPE_DIR = "DIR";
	/** 文件类型文件 */
	public static final String FILE_TYPE_FILE = "FILE";
	
	/** 文件分组 用户组 */
	public static final String FILE_GROUP_USERS = "USERS";
	/** 文件分组 资源组 */
	public static final String FILE_GROUP_RESOURCE = "RESOURCE";
	/** 文件分组 班级组 */
	public static final String FILE_GROUP_CLASS = "CLASS";
	
	
	
	/** 已生效 */
	public static final String ENABLE = "1";

	/** 未生效 */
	public static final String DISABLE = "0";
	
	
	/** Redis服务配置地址名称 */
	public static String REDIS_REF_HOSTS = "redis_ref_hosts";
}
