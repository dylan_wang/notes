package com.chnmooc.common.servlets;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;

import javax.activation.MimetypesFileTypeMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.chnmooc.core.file.OFile;
import com.chnmooc.core.ftp.FtpServer;
import com.chnmooc.common.config.SystemConfig;
import com.chnmooc.common.constants.SyncConstants;
import com.chnmooc.common.utils.DESStaticKey;
import com.chnmooc.common.utils.StringUtils;
import com.chnmooc.common.utils.ToolKit;
import com.chnmooc.common.utils.Tools;
import com.chnmooc.syncftp.model.UFile;
import com.chnmooc.syncftp.service.facade.IAccessService;
import com.oreilly.servlet.MultipartRequest;

public class ApplicationServlet extends DispatcherServlet{
	private static boolean isDestroy = false;
	
	private final static String KEY = "TFmrluhJJC/=AiisvHkxutuyKGwzn3JhZfxX";
	private final static int PERMISSION_READ = 1;
	private final static int PERMISSION_WRITE = 2;
	private final static int PERMISSION_DELETE = 3;
	
	private Log logger = LogFactory.getLog(ApplicationServlet.class);
	
	private IAccessService accessService;
	
	@Override
	protected WebApplicationContext initWebApplicationContext() {
		WebApplicationContext app = super.initWebApplicationContext();
		accessService = app.getBean(IAccessService.class);
		initFtpServer(app);
		return app;
	}
	
	
	/**
	 * 获取文件列表
	 * 
	 * @param httpExchange
	 * @param f
	 * @throws IOException
	 */
	public void list(String uname, String path, HttpServletRequest req, HttpServletResponse res) throws IOException {
		JSONArray result = new JSONArray();
		if(!path.endsWith("/")){
			path += "/";
		}
		List<UFile> list = accessService.getFiles(path).getData();
		for (UFile uf : list) {
			if(uf.getFtype().equals(SyncConstants.FILE_TYPE_DIR)){
				JSONObject fo = new JSONObject();
				fo.put("name", uf.getFname());
				fo.put("type", "dir");
				result.add(fo);
			}else{
				String lastTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(uf.getLastModified());
				String downurl = DESStaticKey.encrypt(uname + "|" + path + "/" + uf.getFname(), KEY);
				
				JSONObject fo = new JSONObject();
				fo.put("name", uf.getFname());
				fo.put("ext", uf.getExt());
				fo.put("size", Tools.changeUnit(uf.getFsize()));
				fo.put("last", lastTime);
				fo.put("downurl", downurl);
				fo.put("type", "file");
				result.add(fo);
			}
		}
		sendJson(req, res, result.toJSONString());
	}
	

	/**
	 * 获取文件进入下载
	 * 
	 * @param f
	 * @param result
	 */
	public void download(HttpServletRequest req, HttpServletResponse res, String path) {
		try {

			UFile f = accessService.getFileByPath(path).getData();
			if(null == f){ return; }
			String filename = f.getFname();
			MimetypesFileTypeMap mimeTypesMap = new MimetypesFileTypeMap();
			SimpleDateFormat dateFormatter = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.US);
			String realPath = SystemConfig.filesDepot + f.getObjectId().replaceAll("(.{3})", "$1/") + "/" + f.getObjectId();

			res.setHeader("last-modified", dateFormatter.format(new Date(f.getLastModified())));
			res.setHeader("Content-Disposition", "attachment;filename=\"" + new String(filename.getBytes("gb2312"), "ISO8859-1") + "\"");
			res.setHeader("content-ength", f.getFsize() + "");
			res.setHeader("Content-Type", mimeTypesMap.getContentType(path));
		
			//Image srcImage = javax.imageio.ImageIO.read(file);
			//imageWidth = srcImage.getWidth(null);
           // imageHeight = srcImage.getHeight(null);

			OutputStream toClient = new BufferedOutputStream(res.getOutputStream());
			RandomAccessFile raf = new RandomAccessFile(realPath , "r");
			byte[] bytes = new byte[65535];
			int size = 0;
			try {
				while ((size = raf.read(bytes, 0, bytes.length)) != -1) {
					toClient.write(bytes, 0, size);
					toClient.flush();
				}
			} finally {
				toClient.close();
				raf.close();
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * 创建文件夹
	 * 
	 * @param httpExchange
	 * @param f
	 * @throws IOException
	 */
	public void createFolder(HttpServletRequest req, HttpServletResponse res,String path) throws IOException {
		boolean flag = accessService.createDir(path).getData();
		if(flag){
			sendJson(req, res,flag ? "{result:true, code:0}" : "{result:false, code:-1}");
		}else{
			sendJson(req, res, "{result:false, code:-2}");
		}
	}

	/**
	 * 删除文件
	 * 
	 * @param httpExchange
	 * @param f
	 * @throws IOException
	 */
	public void delete(HttpServletRequest req, HttpServletResponse res, String path) throws IOException {
		boolean flag = accessService.removeByPath(path).getData();
		sendJson(req, res, flag ? "{result:true}" : "{result:false}");
	}

	/**
	 * 重命名
	 * 
	 * @param httpExchange
	 * @param f
	 * @throws IOException
	 */
	public void rename(HttpServletRequest req, HttpServletResponse res, String fromPath, String toPath) throws IOException {
		boolean flag = accessService.renameByPath(fromPath, toPath).getData();
		if(flag){
			sendJson(req, res, "{result:true, code:0}");
		}else{
			sendJson(req, res, "{result:false, code:-3}");
		}
//		if (!f.exists()) {
//			sendJson(req, res, "{result:false, code:-1}");
//			return;
//		}
//		if (dest.exists()) {
//			sendJson(req, res, "{result:false, code:-2}");
//			return;
//		}
//		if (f.isDirectory()) {
//			if(Tools.mv(f.getPath(), dest.getPath())){
//				sendJson(req, res, "{result:true, code:0}");
//			} else {
//				sendJson(req, res, "{result:false, code:-3}");
//			}
//		} else {
//			sendJson(req, res, f.renameTo(dest) ? "{result:true, code:0}" : "{result:false, code:-3}");
//		}
	}

	/**
	 * 发送json消息给前台
	 * 
	 * @param httpExchange
	 * @param msg
	 * @throws IOException
	 */
	private void sendJson(HttpServletRequest req, HttpServletResponse res, String msg) throws IOException {
		String callback = req.getParameter("callback");
		if(callback == null) callback = "";
		
		StringBuffer responseMsg = new StringBuffer(callback).append("(").append(msg);
		byte[] resmsg = responseMsg.append(")").toString().getBytes("UTF-8");
		res.setCharacterEncoding("UTF-8");
		res.setContentType("text/html; charset=UTF-8");
		res.setHeader("Access-Control-Allow-Origin","*");
		res.getWriter().print(new String(resmsg, "UTF-8"));
	}
	
	private void sendHtml(HttpServletRequest req, HttpServletResponse res, String html) throws IOException {
		String callback = req.getParameter("callback");
		if(callback == null) callback = "";
		
		//StringBuffer responseMsg = new StringBuffer(callback).append("(").append(msg);
		//byte[] resmsg = responseMsg.append(")").toString().getBytes("UTF-8");
		res.setCharacterEncoding("UTF-8");
		res.setContentType("text/html; charset=UTF-8");
		res.setHeader("Access-Control-Allow-Origin","*");
		res.getWriter().print(html);
	}

	/**
	 * 发送错误
	 * 
	 * @param ctx
	 * @param status
	 * @throws IOException
	 */
	private void sendError(HttpServletResponse res, int status) throws IOException {
		res.sendError(status);
	}

	
	
	
	
	/**
	 * 上传
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	public void upload(HttpServletRequest request, HttpServletResponse response, String uname,String path) throws IOException  {
		int inmaxPostSize = Integer.MAX_VALUE;
		try {
			JSONArray result = new JSONArray();
			String encode = ToolKit.getString(request, "encode");
			if(StringUtils.isEmpty(encode)){
				MultipartRequest mr = new MultipartRequest(request, accessService.getAbsolutePath(path).getData(), inmaxPostSize, "UTF-8");
				Enumeration<String> files = mr.getFileNames();
				while (files.hasMoreElements()) {
					String fileName = (String) files.nextElement();
					File src = mr.getFile(fileName);
					OFile f = new OFile(src.getPath());
					f.save(SystemConfig.filesDepot);
					accessService.createFile(path + f.getName(), f.getInfo().getFileCode(),src);
					
					
					String ext = f.getName().substring(f.getName().lastIndexOf("."));
					JSONObject o = new JSONObject();
					o.put("originalName", path + f.getName());
					o.put("name", f.getName());
					o.put("url", DESStaticKey.encrypt(uname + "|" + path + "/" + f.getName(), KEY));
					o.put("size", f.length());
					o.put("type", ext);
					o.put("state", "SUCCESS");
					result.add(o);
				}
			}else{
				
			}
//			JSON result = new JSONObject();
//			if(arr.size() == 1){
//				result = (JSONObject)arr.get(0);
//			}else{
//				result = arr;
//			}
			String script = ToolKit.getString(request, "script");
			if("1".equals(script)){
				StringBuffer resultScript = new StringBuffer();
				String domain = ToolKit.getString(request, "domain");
				if(!StringUtils.isEmpty(domain)){
					resultScript.append("<script>document.domain=\"" + domain + "\";</script>");
				}
				resultScript.append("<script>(" + result.toJSONString() + ")</script>");
				sendHtml(request, response, resultScript.toString());
			}else{
				sendJson(request, response, result.toJSONString());
			}
		} catch (Exception e) {
			e.printStackTrace();
			sendError(response, 500);
		}
	}
	
	@Override
	protected void doService(HttpServletRequest request,HttpServletResponse response) throws Exception {
		int permission = 0;
		try {
			String cookies = request.getHeader("Cookie");
			if(null == cookies){return;}
			String userkey = cookies.replaceAll(".*userkey=([^\"|;]+).*", "$1");
			String uname = Tools.getUserName(userkey);
			if(uname == null){return;}
			String[] path = URLDecoder.decode(request.getRequestURI(), "UTF-8").split("[?]")[0].split("@");
			
			OFile currFile = null;
			String currPath = "";
			UFile currUFile = null;
					
			// 识别取文件分类类型
			String tag = path[0].replaceFirst("^[/]?([^/]+).*", "$1");
			
			if ("resource".equals(tag) || "class".equals(tag)) {
				String id = request.getParameter("id");
				currPath = path[0].replaceFirst("^[/]*(resource|class){1}", "");
				String rootDir = "resource".equals(tag) ? SystemConfig.resourceRootDir : SystemConfig.classRootDir;
				accessService.changeRoot(rootDir,id);
				
				currFile = new OFile(String.format("%s/%s/%s/%s",rootDir, tag, id, currPath));
				
				
				// 检查加密参数
				String key = request.getParameter("k");
				if (key == null || key.length() < 1) {
					//
					sendError(response, 403);
					return;
				}
				
				// 检查权限
				String controlPer = request.getParameter("c");
				if (key.equalsIgnoreCase(Tools.md5(id + KEY + controlPer + userkey))) {
					if ("r".endsWith(controlPer)) {
						permission = PERMISSION_READ;
					} else if ("rw".endsWith(controlPer)) {
						permission = PERMISSION_READ | PERMISSION_WRITE;
					} else if ("rd".endsWith(controlPer)) {
						permission = PERMISSION_READ | PERMISSION_DELETE;
					} else if ("wd".endsWith(controlPer)) {
						permission = PERMISSION_WRITE | PERMISSION_DELETE;
					} else if ("rwd".endsWith(controlPer)) {
						permission = PERMISSION_READ | PERMISSION_WRITE | PERMISSION_DELETE;
					}
				}

				// 检查跟路径
				if (!currFile.exists()) {
					new OFile(SystemConfig.rootDir + "/" + tag + "/" + id).mkdirs();
				}
				
			} else if ("users".equals(tag)){
				currPath = path[0].replaceFirst("^[/]?users", "");
				
				accessService.changeRoot(SystemConfig.usersRootDir,uname);
				
				currFile = Tools.getFile(uname, SystemConfig.usersRootDir, currPath);
				permission = PERMISSION_READ | PERMISSION_WRITE | PERMISSION_DELETE;
				
			} else {
				// 开放下载支持
				String upath = DESStaticKey.decrypt(path[0].replaceFirst("/", ""), KEY);
				if (upath != null && upath.length() > 0) {
					String[] ups = upath.split("\\|");
					if(ups.length == 2){
						currPath = ups[1].replaceAll("//", "/");
					}
				}
				accessService.changeRoot(SystemConfig.usersRootDir,uname);
				permission = PERMISSION_READ;
			}
			
			if ("".equals(currPath) || permission == 0) {
				sendError(response, 403);
				return;
			}

			if(!currPath.endsWith("/") && currPath.indexOf(".") == -1){
				currPath = (currPath + "/").replaceAll("//", "/");
			}
			currUFile = new UFile(currPath);
			if (path.length > 1) {
				String[] cmd = path[path.length - 1].split(":");
				if ("download".equals(cmd[0])) {
					
					if((permission & PERMISSION_READ) != PERMISSION_READ) {
						sendError(response, 403);
						return;
					}
					
					download(request, response, currPath);
				} else if ("createFolder".equals(cmd[0]) && cmd.length == 2) {
					
					if((permission & PERMISSION_WRITE) != PERMISSION_WRITE) {
						sendError(response, 403);
						return;
					}
					
					createFolder(request, response, currPath + cmd[1]);
				} else if ("delete".equals(cmd[0])) {
					
					if((permission & PERMISSION_DELETE) != PERMISSION_DELETE) {
						sendError(response, 403);
						return;
					}
					
					delete(request, response, currPath + cmd[1]);
				} else if ("upload".equals(cmd[0])) {
					
					if((permission & PERMISSION_WRITE) != PERMISSION_WRITE) {
						sendError(response, 403);
						return;
					}
					
					 upload(request, response, uname,currPath);
				} else if ("rename".equals(cmd[0]) && cmd.length == 3) {
					
					if((permission & PERMISSION_WRITE) != PERMISSION_WRITE) {
						sendError(response, 403);
						return;
					}
					
					rename(request, response, currPath  + cmd[1], currPath  + cmd[2]);
				} else {
					list(uname, currPath, request, response);
				}
			} else if (SyncConstants.FILE_TYPE_DIR.equals(currUFile.getFtype())) {
				
				if((permission & PERMISSION_READ) != PERMISSION_READ) {
					sendError(response, 403);
					return;
				}
				
				list(uname, currPath, request, response);
			} else if (SyncConstants.FILE_TYPE_FILE.equals(currUFile.getFtype())) {
				
				if((permission & PERMISSION_READ) != PERMISSION_READ) {
					sendError(response, 403);
					return;
				}
				
				download(request, response, currPath);
			} else {
				sendError(response, 404);
			}
		} catch (Exception e) {
			e.printStackTrace();
			sendError(response, 404);
		}
		
//		super.doService(request, response);
	}


	public void initFtpServer(final WebApplicationContext app) {
		
		logger.info("准备启动FTP服务...");
		
		logger.info("FTP根目录" + SystemConfig.rootDir);
		logger.info("FTP下载源目录"+ SystemConfig.filesDepot);
		
//		for (String name : app.getBeanDefinitionNames()) {
//			System.out.println(name);
//		}
		
		new Thread(new Runnable() {
			public void run() {
				while (!isDestroy) {
					try {
						logger.info("启动FTP服务，port:" + SystemConfig.ftpPort + ".");
						FtpServer.start(new String[] { SystemConfig.rootDir, SystemConfig.ftpPort },app);
					} catch (Exception e) {
						logger.info("FTP服务启动失败.");
						e.printStackTrace();
					}
					try { Thread.sleep(3000); } catch (InterruptedException e) {}
				}
			}
		}).start();
	}

	@Override
	public void destroy() {
		isDestroy = true;
		super.destroy();
	}
	
}
