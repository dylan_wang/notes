package com.chnmooc.common.utils;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

/**
 * DESStatic加密
 */
public class DESStaticKey {

	public static String encrypt(String str, String key) {
		try {
			if (str == null || str.length() < 1) return "";
			DESKeySpec keySpec = new DESKeySpec(key.getBytes());
			SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
			SecretKey secretKey = keyFactory.generateSecret(keySpec);
			Cipher c1 = Cipher.getInstance("DES");
			c1.init(Cipher.ENCRYPT_MODE, secretKey);
			byte[] cipherByte = c1.doFinal(str.getBytes());
			return bytes2Hex(cipherByte);
		} catch ( Exception e ) {
			return "";
		}
	}

	public static String decrypt(String str, String key) {
		try {
			if (str == null || str.length() < 1) return "";
			DESKeySpec keySpec = new DESKeySpec(key.getBytes());
			SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
			SecretKey secretKey = keyFactory.generateSecret(keySpec);
			Cipher c1 = Cipher.getInstance("DES");
			c1.init(Cipher.DECRYPT_MODE, secretKey);
			return new String(c1.doFinal(hex2byte(str)));
		} catch ( Exception e ) {
			e.printStackTrace();
			return "";
		}
	}

	private static String bytes2Hex(byte[] bts) {
		String des = "";
		String tmp = null;
		for ( int i = 0; i < bts.length; i++ ) {
			tmp = (Integer.toHexString(bts[i] & 0xFF));
			if (tmp.length() == 1) {
				des += "0";
			}
			des += tmp;
		}
		return des;
	}

	private static byte[] hex2byte(String hexStr) {
		try {
			byte[] bts = new byte[hexStr.length() / 2];
			for ( int i = 0, j = 0; j < bts.length; j++ ) {
				bts[j] = (byte) Integer.parseInt(hexStr.substring(i, i + 2), 16);
				i += 2;
			}
			return bts;
		} catch ( Exception e ) {
			return "".getBytes();
		}
	}
}
