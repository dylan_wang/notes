package com.chnmooc.common.utils;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.ConcurrentHashMap;

import redis.clients.jedis.exceptions.JedisConnectionException;

import com.alibaba.fastjson.JSON;
import com.chnmooc.common.constants.SyncConstants;
import com.chnmooc.common.redis.JsonCacheClient;
import com.chnmooc.core.file.OFile;
import com.twmacinta.util.MD5;

public class Tools {
	private static String osname = System.getProperties().getProperty("os.name").toLowerCase();
	private static boolean isLinux = osname.indexOf("window") < 0;
	private static ConcurrentHashMap<String, String> filecache = new ConcurrentHashMap<String, String>();
//	private static Jedis jedis = null;
	private static JsonCacheClient jedis = new JsonCacheClient(SyncConstants.REDIS_REF_HOSTS);

	/**
	 * 从redis取用户信息
	 * 
	 * @param userkey
	 * @return
	 */
	public static String getUnameByRedis(String userkey) {
		try {
//			if (jedis == null || !jedis.isConnected()) {
//				jedis = new Jedis("test.chnmooc.com", 6379, 30000);
//			}
			Object user = jedis.get(userkey);
			if(null != user){
				return JSON.toJSONString(user);
			}else{
				return null;
			}
		} catch (JedisConnectionException e) {
			e.printStackTrace();
//			jedis = null;
			return null;
		}
	}

	/**
	 * 从redis获取uname
	 * 
	 * @param userkey
	 * @return
	 */
	public static String getUserName(String userkey) {
		if (userkey != null && userkey.length() > 0) {
			String result = getUnameByRedis(userkey);
			if(result == null){ return null; }
			return result.replaceAll(".*\"uname\":\"([^\"]+)\".*", "$1");
		}
		return null;
	}

	/**
	 * 获取文件路径位置
	 * 
	 * @param f
	 * @param result
	 */
	public static OFile getFile(String uname, String rootDir, String path) {
		OFile currFile = null;
		if (uname != null && uname.length() > 0) {
			String filepath = filecache.get(uname);
			if (filepath == null) {
				if (uname != null && uname.length() > 0) {
					filepath = rootDir + "/" + uname.replaceAll("([\\@|\\.])", "/") + "/" + uname + "/";
					filecache.put(uname, filepath);
				}
			}
			if (filepath != null) {
				// 初始化用户根目录
				currFile = new OFile(filepath);
				if (!currFile.exists()) {
					currFile.mkdirs();
				}
				currFile = new OFile(filepath + path);
			}
		}
		return currFile;
	}

	public static String md5(String str) {
		try {
			MD5 md5 = new MD5();
			md5.Update(str, null);
			return md5.asHex();
		} catch (UnsupportedEncodingException e) {
			return null;
		}
	}

	/**
	 * 移动重命名
	 * 
	 * @param src
	 * @param dest
	 * @return
	 */
	public static boolean mv(String src, String dest) {
		try {
			if (isLinux) {
				Runtime.getRuntime().exec(String.format("mv %s %s", src, dest)).waitFor();
				return new File(dest).exists();
			} else {
				System.out.println(src);
				System.out.println(dest);
				return new File(src).renameTo(new File(dest));
			}
		} catch (Exception e) {
			return false;
		}
	}

	public static String changeUnit(double size) {
		String[] unit = new String[] { "B", "KB", "MB", "GB", "TB" };
		int c = 0;
		while (size > 1024 && c++ < 5) {
			size /= 1024.0;
		}
		return String.format("%.2f%s", size, unit[c]);
	}

}
