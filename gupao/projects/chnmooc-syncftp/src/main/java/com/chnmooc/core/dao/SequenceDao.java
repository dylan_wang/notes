package com.chnmooc.core.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.stereotype.Repository;

import com.chnmooc.common.dao.jdbc.BaseDaoSupport;
import com.chnmooc.common.utils.DataUtils;
import com.chnmooc.common.utils.StringUtils;
import com.chnmooc.core.model.SysSequence;

@Repository(value="sequenceDao")
public class SequenceDao extends BaseDaoSupport<SysSequence, String> {

	private static final String PK_COLUMN = "businessCode";
	
	@Override
	@Resource(name="dataSource")
	public void setDataSource(DataSource dataSource) {
		super.setDataSourceReadOnly(dataSource);
		super.setDataSourceWrite(dataSource);
	}

	@Override
	protected String getPKColumn() {
		return PK_COLUMN;
	}

	private final String prefix = "";//前缀
	private final String splitFlag = "";//分隔符
	private final Integer numLen = 8;//长度
	

	public String getNextVal(String businessCode,String prefixValue,String splitFlagValue,Integer numLength,boolean autoLength){
		List<Object> paramList = new ArrayList<Object>();
		StringBuffer whereSql = new StringBuffer();
		whereSql.append(" and businessCode = ?");	paramList.add(businessCode);
		if(!StringUtils.isEmpty(prefixValue)){whereSql.append(" and prefixValue = ?");paramList.add(prefixValue);}
		if(!StringUtils.isEmpty(splitFlagValue)){whereSql.append(" and splitFlagValue = ?");paramList.add(splitFlagValue);}
		Object [] params = new Object[paramList.size()];
		for (int i = 0 ; i < paramList.size() ; i ++) {params[i] = paramList.get(i);}
		List<Map<String,Object>> list = super.findBySql("select max(currentNum) max ,min(currentNum) min ,count(currentNum) currentNum from sys_sequence where 1 = 1 " + whereSql,params);
		
		Map<String,Object> mapArr = (Map<String,Object>) list.get(0);
		int count = Integer.parseInt(String.valueOf(mapArr.get("currentNum")));
		if (count == 0) {
			try {
				throw new Exception(String.format("没有业务代码[%s]提供初始化！", businessCode));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		String maxNo = DataUtils.nullToZero(mapArr.get("max").toString());
		String minNo = DataUtils.nullToZero(mapArr.get("min").toString());
		if (maxNo.equals(minNo)) {
			int max = Integer.parseInt(minNo, 10) + 1;
			maxNo = pull(max, numLength);
			super.update("UPDATE sys_sequence SET currentNum = ? WHERE businessCode = ? ", max, businessCode);
		}
		return new StringBuffer().append(prefixValue).append(splitFlagValue).append(maxNo).toString();
	}
	
	
	public String getNextVal(String businessCode,String prefixValue,Integer numLength){
		return getNextVal(businessCode, prefixValue, splitFlag, numLength,false);
	}
	
	
	public String getNextVal(String businessCode,Integer numLength){
		return getNextVal(businessCode, prefix, splitFlag, numLength,false);
	}
	
	public String getNextVal(String businessCode){
		return getNextVal(businessCode, prefix, splitFlag, numLen,false);
	}
	
	public String getNextVal(String businessCode,boolean autoLength){
		return getNextVal(businessCode, prefix, splitFlag, numLen ,autoLength);
	}
	
	/**
	 * 拉长顺序号
	 * 
	 * @param value 顺序号
	 * @param len  拉长到的位数
	 * @return 拉长的顺序号字符串
	 */
	public String pull(int value, int len) {
		String serialText = "00000000000000000000";
		String s = Integer.toString(value);
		serialText = serialText.substring(0, len - s.length());
		return (serialText + s);
	}
}
