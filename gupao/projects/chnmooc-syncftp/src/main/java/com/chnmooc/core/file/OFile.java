package com.chnmooc.core.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;

import com.chnmooc.common.utils.Tools;

import com.twmacinta.util.MD5;

public class OFile extends File {
	private static final long serialVersionUID = 1L;
	private OFileInfo fileInfo = new OFileInfo();
	private boolean isOfile = false;
	
	public OFile(String pathname) {
		super(pathname);
		isOfile = isOFile();
	}
	
	@Override
	public OFile[] listFiles() {
		String[] files = list();
		OFile[] fs = null;
		if (files != null) {
			int n = files.length;
			fs = new OFile[n];
			for (int i = 0; i < n; i++) {
				fs[i] = new OFile(getPath() + "/" + files[i]);
			}
		}
		return fs;
	}
	
	@Override
	public OFile[] listFiles(FilenameFilter filter) {
		String ss[] = list();
		if (ss == null) return null;
		ArrayList<OFile> v = new ArrayList<OFile>();
		for (int i = 0; i < ss.length; i++) {
			if ((filter == null) || filter.accept(this, ss[i])) {
				v.add(new OFile(getPath() + "/" + ss[i]));
			}
		}
		return (OFile[]) (v.toArray(new File[v.size()]));
	}

	/**
	 * 取文件大小
	 */
	@Override
	public long length() {
		if (fileInfo.length == null) {
			if (fileInfo.realFile() != null) {
				return fileInfo.realFile().length();
			}
		} else {
			return fileInfo.length;
		}
		return super.length();
	}
	
	public String getRealFilePath(){
		if(fileInfo == null || fileInfo.canonicalPath == null){
			return getPath();
		}
		return fileInfo.canonicalPath;
	}
	
	/**
	 * 保存文件(分离)
	 * @param root
	 * @return
	 */
	public boolean save(String root){
		try {
			if (isOfile) throw new IOException("已经是优化体, 不能更新到仓库.");
			String filemd5 = MD5.asHex(MD5.getHash(this));
			fileInfo.realFile = new File(String.format("%s/%s/%s", root, filemd5.replaceAll("(.{3})", "$1/"), filemd5));
			fileInfo.fileCode = filemd5;
			boolean mvok = false;
			if (!fileInfo.realFile().exists()) {
				fileInfo.realFile().getParentFile().mkdirs();
				mvok = Tools.mv(super.getCanonicalPath(), fileInfo.realFile().getCanonicalPath());
			} else mvok = true;
			
			if (mvok) {
//				ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(fileInfo.realFile));
//				out.writeObject(fileInfo);
//				out.close();
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean read(){
		ObjectInputStream in = null;
		try {
			in = new ObjectInputStream(new FileInputStream(this));
			fileInfo = (OFileInfo) (in.readObject());
			in.close(); 
			return true;
		} catch (Exception e) {
			if(in != null) try { in.close(); } catch (IOException e1) {}
		}
		return false;
	}
	
	private boolean isOFile(){
		return (this.length() < 1024 && read());
	}

	
	public OFileInfo getInfo(){
		return this.fileInfo;
	}
	public static void main(String[] args) {
		OFile test = new OFile("D:\\ftptest\\test\\qq\\com\\test@qq.com\\nginx-0.7.9.tar.gz");
		System.out.println(test.save("D:/ftptest/"));
		//System.out.println(test.read());
	}
}
