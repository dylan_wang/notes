package com.chnmooc.core.file;

import java.io.Externalizable;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import com.twmacinta.util.MD5;

public class OFileInfo implements Externalizable {
	private static final long serialVersionUID = 1L;

	File realFile = null;
	String fileCode = null;
	String canonicalPath = null;
	Long length = null;

	public OFileInfo() {
	}

	/**
	 * 获取真实文件
	 * 
	 * @return
	 */
	File realFile() {
		if (realFile == null) {
			if (canonicalPath == null) {
				return null;
			}
			realFile = new File(canonicalPath);
		}
		return realFile;
	}

	String filecode() {
		try {
			if (fileCode == null) {
				fileCode = MD5.asHex(MD5.getHash(realFile));
			}
		} catch (IOException e) {
			fileCode = null;
		}
		return fileCode;
	}

	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
		this.canonicalPath = (String) in.readObject();
		this.length = (Long) in.readObject();
		this.fileCode = (String) in.readObject();
	}

	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		out.writeObject(realFile.getCanonicalPath());
		out.writeObject(realFile.length());
		out.writeObject(fileCode);
	}
	
	public String getFileCode(){
		return this.fileCode;
	}
}
