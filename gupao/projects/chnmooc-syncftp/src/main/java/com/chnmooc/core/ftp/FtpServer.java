package com.chnmooc.core.ftp;

import java.io.*;
import java.net.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;

import com.chnmooc.common.config.SystemConfig;
import com.chnmooc.syncftp.service.facade.ILimiterService;

public class FtpServer {
	ServerSocket listenSocket = null; // 监听Socket
	private int SERVER_PORT = 21; // 监听端口
	boolean keepRunning = true;
	String rootDir = ".";
	ThreadGroup group = null;
	Log logger = LogFactory.getLog(FtpServer.class);
	ApplicationContext app;

	public FtpServer(ThreadGroup group,ApplicationContext app) {
		this.group = group;
		this.app = app;
	}

	public void RunFtpServer() throws Exception {
		// 监听
		try {
			listenSocket = new ServerSocket(SERVER_PORT);
		} catch (IOException excpt) {
			logger.info("Sorry to open port " + SERVER_PORT + ": " + excpt);
			System.exit(1);
		}

		logger.info("Usage: ftpsvr [rootdir [port]]");
		logger.info("Root = " + rootDir);
		logger.info("Port = " + SERVER_PORT);
		Socket clientSocket = null;
		logger.info("FTP Server started successfully");

		try {
			while (keepRunning) {
				// 接受连接
				clientSocket = listenSocket.accept();
				FtpThread oneFtpThread = createFtpThread(clientSocket, app.getBean(ILimiterService.class));
				// 启动
				oneFtpThread.start();
			}
		} catch (IOException excpt) {
			logger.info("Sorry ,Failed I/O:" + excpt);
		} finally {
			listenSocket.close();
		}
	}

	private FtpThread createFtpThread(Socket clientSocket, ILimiterService limit) {
		// 创建线程
		FtpThread oneFtpThread = new FtpThread(clientSocket, group);
		limit.setServer(this);
		limit.setThread(oneFtpThread);
		oneFtpThread.theLimiter = limit;
		return oneFtpThread;
	}

	public void close() {
		this.keepRunning = false;
		try {
			this.listenSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public int getServerPort() {
		return SERVER_PORT;
	}

	public void setServerPort(int server_port) {
		SERVER_PORT = server_port;
	}

	public String getRootDir() {
		return rootDir;
	}

	public void setRootDir(String rootDir) {
		this.rootDir = rootDir;
	}

	public static void start(String argv[],ApplicationContext app) throws Exception {		
		if(argv.length < 1) argv = new String[]{SystemConfig.usersRootDir, SystemConfig.ftpPort};
		FtpServer svr = new FtpServer(Thread.currentThread().getThreadGroup(),app);
		if (svr != null) {
			if (argv.length > 0) {
				svr.rootDir = argv[0].trim();
				if (svr.rootDir.charAt(svr.rootDir.length() - 1) == '\\')
					svr.rootDir += ".";

				if (argv.length > 1) {
					svr.SERVER_PORT = Integer.parseInt(argv[1]);
				}
			} else {
				String cfgRoot = System.getProperty("ftp.root");
				if (cfgRoot != null && cfgRoot.length() != 0) {
					svr.rootDir = cfgRoot;
				}
			}

			svr.RunFtpServer();
		}
	}
}
