/**
 * FTP工作线程
 */

package com.chnmooc.core.ftp;

import java.io.*;
import java.net.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.chnmooc.common.constants.SyncConstants;
import com.chnmooc.syncftp.model.UFile;
import com.chnmooc.syncftp.service.facade.IAccessService;
import com.chnmooc.syncftp.service.facade.ILimiterService;

public class FtpThread extends Thread {
	
	private Log logger = LogFactory.getLog(FtpThread.class);
	
	/** 本Ftp实例的Socket连接 */
	Socket theSocket = null;
	/** 登录限制器 */
	ILimiterService theLimiter = null;
	/** 权限控制器 */
	IAccessService theAccess = null;

	boolean Running = true;

	/** 下一次文件传输方式（'A'或'I'） */
	char dataType = 'I';
	/** 数据线程 */
	DataThread theDataThread = null;
	/** 重起点 */
	long restOff = 0;

	/** 命令行 */
	String nextline = null;
	/** 命令线程输入流 */
	BufferedReader in = null;
	/** 命令线程输出流 */
	PrintWriter out = null;

	/** 登录用户名 */
	String userName = "Anonymous";
	/** 登录密码 */
	String userPwd = "";
	/** 当前目录 */
	String currDir = "/";

	ThreadGroup group = null;

	static int serial = 1;
	
	/**
	 * 构造函数
	 * 
	 * @param sckt 网络连接
	 * @param lmt 登录限制器
	 */
	public FtpThread(Socket sckt, ThreadGroup group) {
		super(group, "FtpThread-" + serial++);
		this.group = group;
		theSocket = sckt;
		this.setDaemon(true);
	}

	public void run() {
		String cmd = null;
		String param = null;
		String strPath = null;
		String renfrom = null;
		boolean loginSucc = false;

		try {
			in = new BufferedReader(new InputStreamReader(theSocket.getInputStream(), "GB2312"));
			out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(theSocket.getOutputStream(), "GB2312")), true);

			// 连接成功信息
			out.println("220 Welcome to chnmooc.com");
			logger.info("Connected from " + theSocket.getInetAddress().getHostAddress());

			while (Running) {
				// 获取命令行
				try {
					nextline = in.readLine();
					if (nextline == null) break;
				} catch (Exception excpt) {
					logger.info("Forced to close: " + excpt);
					break;
				}

				// 分解命令和参数
				logger.info(nextline);
				cmd = FtpTools.SubStr(nextline, 1);
				param = nextline.substring(FtpTools.SubStrPos(nextline, 2));

				// 用户名 ----------------------------------------
				if (cmd.equalsIgnoreCase("USER")) {
					userName = param;
					loginSucc = theLimiter.login(userName, userPwd, theSocket.getInetAddress().getHostAddress());

					if (loginSucc) { // 登录成功
						theAccess = theLimiter.getAccess();
						out.println("230 Login Success!");
					} else {
						out.println("331 " + theLimiter.getReason());
					}
				}

				// 密码 ----------------------------------------
				else if (cmd.equalsIgnoreCase("PASS")) {
					userPwd = param;
					loginSucc = theLimiter.login(userName, userPwd, theSocket.getInetAddress().getHostAddress());

					if (loginSucc) {
						theAccess = theLimiter.getAccess();
						out.println("230 Login Success!");
					} else {
						out.println("530 " + theLimiter.getReason());
					}
				}

				// 退出 ----------------------------------------
				else if (cmd.equalsIgnoreCase("QUIT")) {
					out.println("221 Good bye!"); // 准备关闭
					Running = false;
				}

				// 系统信息 ----------------------------------------
				else if (cmd.equalsIgnoreCase("SYST")) {
					out.println("215 Unix Type: L8");
				}

				//请求服务器等待数据连接
				else if (cmd.equalsIgnoreCase("PASV")) {
					if (theDataThread != null)
						theDataThread.close();
					theDataThread = new DataThread(theLimiter, out, group);
					theDataThread.PASV(theSocket.getLocalAddress().getHostAddress());
				}

				//
				else if (cmd.equalsIgnoreCase("EPSV")) {
					if (theDataThread != null)
						theDataThread.close();
					theDataThread = new DataThread(theLimiter, out, group);
					theDataThread.EPSV(theSocket.getLocalAddress().getHostAddress());
				}

				//
				else if (cmd.equalsIgnoreCase("PORT")) {
					if (theDataThread != null)
						theDataThread.close();
					theDataThread = new DataThread(theLimiter, out, group);
					theDataThread.PORT(param);
				}

				else if (cmd.equalsIgnoreCase("EPRT")) {
					if (theDataThread != null)
						theDataThread.close();
					theDataThread = new DataThread(theLimiter, out, group);
					theDataThread.EPRT(param);
				}

				// //////////////////////////////////////////////////

				else if (!loginSucc) {
					out.println("530 Login first");
				}

				//
				else if (cmd.equalsIgnoreCase("CWD")) {
					String tmpDir = FtpTools.fmtPath(currDir, param + "/");
					UFile d = theAccess.getFileByPath(tmpDir).getData();
					if(d == null){
						currDir = "/";
						out.println("250 \"" + currDir + "\" is current directory.");
					}else if(SyncConstants.FILE_TYPE_DIR.equals(d.getFtype())){
						currDir = tmpDir;
						out.println("250 \"" + currDir + "\" is current directory.");
					}else{
						out.println("500 No such directory.");
					}
//					File d = new File(theAccess.getAbsolutePath(tmpDir));
//					if (d.exists() && d.isDirectory()) {
//						currDir = tmpDir;
//						out.println("250 \"" + currDir + "\" is current directory.");
//					} else
//						out.println("500 No such directory.");
				}

				//
				else if (cmd.equalsIgnoreCase("LIST")) {
					if (param.equalsIgnoreCase("-aL") || param.equalsIgnoreCase("-a") || param.equalsIgnoreCase("-L"))
						param = "";
					strPath = FtpTools.fmtPath(currDir, param);
					// dataType = 'A';
					int pd = strPath.lastIndexOf('/');
					if (theDataThread == null)
						out.println("450 PASV or PORT first.");
					else if (theDataThread.isClosed)
						out.println("450 PASV or PORT first.");
					else
						theDataThread.LIST(strPath.substring(0, pd + 1), strPath.substring(pd + 1));
				}

				//
				else if (cmd.equalsIgnoreCase("TYPE")) {
					char Type = Character.toUpperCase((param + ' ').charAt(0));
					if (Type == 'A' || Type == 'I') {
						dataType = Type;
						out.println("200 Set type to " + param);
					} else
						out.println("504 Type " + Type + " NOT supported");
				}

				//
				else if (cmd.equalsIgnoreCase("RETR")) {
					strPath = FtpTools.fmtPath(currDir, param);
					if (theDataThread == null)
						out.println("450 PASV or PORT first.");
					else if (theDataThread.isClosed)
						out.println("450 PASV or PORT first.");
					else
						theDataThread.RETR(strPath, restOff, dataType);
					restOff = 0;
				}

				//保存上传的文件
				else if (cmd.equalsIgnoreCase("STOR")) {
					strPath = FtpTools.fmtPath(currDir, param);
					if (theDataThread == null)
						out.println("450 PASV or PORT first.");
					else if (theDataThread.isClosed)
						out.println("450 PASV or PORT first.");
					else
						theDataThread.STOR(strPath, restOff, dataType);
					restOff = 0;
				}
				
				else if (cmd.equalsIgnoreCase("APPE")) {
					strPath = FtpTools.fmtPath(currDir, param);
					if (theDataThread == null)
						out.println("450 PASV or PORT first.");
					else if (theDataThread.isClosed)
						out.println("450 PASV or PORT first.");
					else {
						File d = new File(theAccess.getAbsolutePath(strPath + ".dl").getData());
						if (d.exists() && !d.isDirectory()) {
							theDataThread.STOR(strPath, d.length(), dataType);
						} else {
							out.println("500 No such file.");
						}
					}
				}

				//
				else if (cmd.equalsIgnoreCase("REST")) {
					restOff = Long.parseLong("0" + param);
					out.println("350 Restart from " + param + " OK");
				}

				//
				else if (cmd.equalsIgnoreCase("SIZE")) {
					strPath = FtpTools.fmtPath(currDir, param);
					UFile file = new UFile(strPath);
					if(theAccess.exists(file.getXpath()).getData()){
						out.println("213 " + String.valueOf(file.getFsize()));
					}else{
						out.println("500 No such file.");
					}
//					File f = new File(theAccess.getAbsolutePath(strPath + ".dl").getData());
//					if (f.exists() && f.isFile())
//						out.println("213 " + String.valueOf(f.length()));
//					else
//						out.println("500 No such file.");
				}

				//
				else if (cmd.equalsIgnoreCase("NLST")) {
					strPath = FtpTools.fmtPath(currDir, param);
					// dataType = 'A';
					int pd = strPath.lastIndexOf('/');
					if (theDataThread == null)
						out.println("450 PASV or PORT first.");
					else if (theDataThread.isClosed)
						out.println("450 PASV or PORT first.");
					else
						theDataThread.NLST(strPath.substring(0, pd + 1), strPath.substring(pd + 1));
				}

				// 获当前目录 ----------------------------------------
				else if (cmd.equalsIgnoreCase("PWD") || cmd.equalsIgnoreCase("XPWD")) {
					out.println("257 \"" + currDir + "\" is current directory.");
				}

				//
				else if (cmd.equalsIgnoreCase("ABOR") || FtpTools.right(cmd, 3).equalsIgnoreCase("BOR")) {
					if (theDataThread != null) {
						theDataThread.close();
						theDataThread = null;
						sleep(10);
					}
					out.println("226 Aborted.");
				}

				//
				else if (cmd.equalsIgnoreCase("DELE")) {
					strPath = FtpTools.fmtPath(currDir, param);
					if (theAccess.getAccessPermission(strPath, IAccessService.OPER_DELETE).getData() && !theAccess.isVirtualNode(strPath).getData()) {
						
						UFile f = theAccess.getFileByPath(strPath).getData();
						if(null == f){
							out.println("550 No such file \"" + strPath + "\"");
						}
						long fsize = f.getFsize();
						
						if(theAccess.removeByPath(currDir).getData()){
							theAccess.setUseSpace(strPath, -fsize);
							//currDir = f.getXpath();//切换当前目录到当前目录的父级
							out.println("250 Delete succeeded.");
						}else{
							out.println("550 Delete \"" + strPath + "\" failed.");
						}
						
//						File f = new File(theAccess.getAbsolutePath(strPath).getData());
//						long fsize = f.length();
//						if (!f.exists() || f.isDirectory())
//							out.println("550 No such file \"" + strPath + "\"");
//						else if (!f.delete())
//							out.println("550 Delete \"" + strPath + "\" failed.");
//						else {
//							theAccess.setUseSpace(strPath, -fsize);
//							out.println("250 Delete succeeded.");
//						}
					} else
						out.println("450 Delete \"" + strPath + "\" not permitted.");
				}

				//
				else if (cmd.equalsIgnoreCase("RNFR")) {
					strPath = FtpTools.fmtPath(currDir, param);
					renfrom = null;
					if (theAccess.getAccessPermission(strPath, IAccessService.OPER_RENAME).getData() && !theAccess.isVirtualNode(strPath).getData()) {
						
						UFile uf = theAccess.getFileByPath(strPath).getData();
						if(null != uf){
							out.println("350 OK");
							renfrom = strPath;
						}else{
							out.println("500 No such File or Directory of \"" + strPath + "\"");
						}
//						String strAbsPath = theAccess.getAbsolutePath(strPath).getData();
//						File f = new File(strAbsPath);
//						if (f.exists()) {
//							out.println("350 OK");
//							renfrom = strAbsPath;
//						} else
//							out.println("500 No such File or Directory of \"" + strPath + "\"");
					} else
						out.println("450 Rename \"" + strPath + "\" not permitted.");
				}

				//
				else if (cmd.equalsIgnoreCase("RNTO")) {
					strPath = FtpTools.fmtPath(currDir, param);
					if (renfrom == null)
						out.println("500 RNFR First.");
					else {
						UFile old = theAccess.getFileByPath(strPath).getData();
						if(null != old){
							out.println("553 \"" + strPath + "\" already exists.");
						}else{
							if(theAccess.renameByPath(renfrom, strPath).getData()){
								out.println("250 Rename succeeded.");
							}else{
								out.println("553 Rename to \"" + strPath + "\" failed.");
							}
						}
//						String strAbsPath = theAccess.getAbsolutePath(strPath).getData();
//						File f = new File(strAbsPath);
//						if (f.exists())
//							out.println("553 \"" + strPath + "\" already exists.");
//						else {
//							File ffrom = new File(renfrom);
//							if (ffrom.renameTo(f))
//								out.println("250 Rename succeeded.");
//							else
//								out.println("553 Rename to \"" + strPath + "\" failed.");
//						}
					}
				}

				//
				else if (cmd.equalsIgnoreCase("MKD") || cmd.equalsIgnoreCase("XMKD")) {
					strPath = FtpTools.fmtPath(currDir, param);
					if (theAccess.getAccessPermission(strPath, IAccessService.OPER_MKDIR).getData()) {
						if(theAccess.exists(strPath).getData()){
							out.println("550 \"" + strPath + "\" already exists.");
						}else{
							if(theAccess.createDir(strPath).getData()){
								out.println("257 MkDir succeeded.");
							}else{
								out.println("550 MkDir failed.");
							}
						}
					} else
						out.println("450 MkDir \"" + strPath + "\" not permitted.");
				}

				//
				else if (cmd.equalsIgnoreCase("RMD") || cmd.equalsIgnoreCase("XRMD")) {
					strPath = FtpTools.fmtPath(currDir, param);
					if (theAccess.getAccessPermission(strPath, IAccessService.OPER_RMDIR).getData()) {
						
//						File d = new File(theAccess.getAbsolutePath(strPath).getData());
//						if (!d.exists() || !d.isDirectory())
//							out.println("500 No such directory.");
//						else {
						try{
							if(theAccess.removeByPath(strPath).getData()){
								out.println("250 Delete succeeded.");
							}else{
								out.println("550 Delete failed.");
							}
//							if (FtpTools.deleteDir(d))
//								out.println("250 Delete succeeded.");
//							else
//								out.println("550 Delete failed.");
						}catch(Exception e){
							out.println("500 Delete failed.");
						}
//						}
					} else
						out.println("450 Rmdir \"" + strPath + "\" not permitted.");
				}

				// --- 传输模式 -----------------------------------------
				else if (cmd.equalsIgnoreCase("MODE")) {
					if (Character.toUpperCase((param + ' ').charAt(0)) == 'S')
						out.println("200 Mode 'S' OK");
					else
						out.println("504 Mode '" + param + "'Not supported");
				}

				//
				else if (cmd.equalsIgnoreCase("STRU")) {
					if (Character.toUpperCase((param + ' ').charAt(0)) == 'F')
						out.println("200 Stru 'F' OK");
					else
						out.println("504 Stru '" + param + "' Not supported");
				}
				
				//
				else if (cmd.equalsIgnoreCase("SITE")) {
					String[] params = param.split("\\s+");
					
					// 修改文件时间
					if ("UTIME".equalsIgnoreCase(params[0])) {
//						File fileToChange = new File(theAccess.getAbsolutePath(params[1]));
						if (!theAccess.exists(params[1]).getData()){
							out.println("500");
						} else {
							//String formatm = params[2].length() == 14 ? "yyyyMMddHHmmss" : "yyyyMMddHHmmssSSS";
							//if (fileToChange.setLastModified(FtpTools.StringToDate(params[2], formatm).getTime()))
								out.println("200 UTIME command succesful");
							//else
								//out.println("201");
						}
					} else if ("COMPARE".equalsIgnoreCase(params[0])) {
						try {
							File f = new File(theAccess.getAbsolutePath(params[1]).getData());
							Date fileDate = new Date(f.lastModified());
							Long lf = Long.valueOf(new SimpleDateFormat("yyyyMMddHHmmssSSS", Locale.US).format(fileDate));
							Long rf = Long.valueOf(params[2]);
							if (lf > rf) {
								out.println("201");
							} else if (lf < rf) {
								out.println("202");
							} else out.println("200");
						} catch (Exception e) {
							out.println("200");
						}
					} else {
						out.println("502 Command " + cmd + " not understood");
					}
				}

				//
				else if (cmd.equalsIgnoreCase("NOOP")) {
					out.println("200 NOOP");
				}

				//切换到当前目录的父目录
				else if (cmd.equalsIgnoreCase("CDUP")) {
					String tmpDir = currDir;
					tmpDir = (tmpDir.endsWith("/") ? tmpDir.substring(0,tmpDir.lastIndexOf("/")) : tmpDir);
					currDir = (tmpDir.substring(0,tmpDir.lastIndexOf("/")) + "/");
					out.println("250 CWD command succesful");
				}
				
				// //
				// else if(cmd.equalsIgnoreCase("")){
				// }

				
				             
		                                           
		                                         
		                                          
		                                          
				// 已实现：
				// USER, QUIT, PORT, TYPE, MODE, STRU, RETR, STOR, NOOP,SITE,XMKD,CWD
				// PASV, CMD , PWD ,XPWD, LIST, SYST, PASS, NLST,APPE,ABOR
				// RNFR, RNTO, MKD , RMD,XRMD , DELE, REST

				// 命令未实现
		         //ALLO,CDUP,FEAT,CDUP,XCWD,STAT,XCUP,OPTS,ACCT,HELP,STOU,AUTH
		         //REIN,SMNT,PBSZ,MDTM,PROT
				else{
					out.println("502 Command " + cmd + " not understood");
				}
			}

			// 正常关闭
			in.close();
			out.close();

		} catch (NullPointerException nulle) {
			try {
				if (in != null)
					in.close();
				if (out != null)
					out.close();
			} catch (IOException ioe) {
			}
		} catch (Exception e) {
			logger.info(e);
		} finally {
			close();
			logger.info("Disconnected.");
		}
	}

	/** 关闭线程 */
	public void close() {
		// 关闭数据线程
		if (theDataThread != null) {
			theDataThread.close();
		}

		// 关闭连接并断开引用
		try {
			in.close();
			theSocket.close();
		} catch (Exception e) {
		}

		// 停止循环
		Running = false;

		// 断开对权限控制的引用
		theAccess = null;

		// 关闭限制并断开引用
		if (theLimiter != null) {
			theLimiter.close();
		}
	}


}
