package com.chnmooc.core.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "sys_sequence")
public class SysSequence implements Serializable {

	private static final long serialVersionUID = -8000495080554633345L;

	private String businessCode;// 业务代码
	private boolean hasPrefix;// 是否有前缀
	private String prefixValue;// 前缀
	private boolean hasSplitFlag;// 是否分隔符
	private String splitFlagValue;// 分隔符
	private int numLength;// 长度
	private int currentNum;// 当前序号

	@Id
	public String getBusinessCode() {
		return businessCode;
	}

	public void setBusinessCode(String businessCode) {
		this.businessCode = businessCode;
	}

	public boolean isHasPrefix() {
		return hasPrefix;
	}

	public void setHasPrefix(boolean hasPrefix) {
		this.hasPrefix = hasPrefix;
	}

	public String getPrefixValue() {
		return prefixValue;
	}

	public void setPrefixValue(String prefixValue) {
		this.prefixValue = prefixValue;
	}

	public boolean isHasSplitFlag() {
		return hasSplitFlag;
	}

	public void setHasSplitFlag(boolean hasSplitFlag) {
		this.hasSplitFlag = hasSplitFlag;
	}

	public String getSplitFlagValue() {
		return splitFlagValue;
	}

	public void setSplitFlagValue(String splitFlagValue) {
		this.splitFlagValue = splitFlagValue;
	}

	public int getNumLength() {
		return numLength;
	}

	public void setNumLength(int numLength) {
		this.numLength = numLength;
	}

	public int getCurrentNum() {
		return currentNum;
	}

	public void setCurrentNum(int currentNum) {
		this.currentNum = currentNum;
	}

}
