package com.chnmooc.core.mvc.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.chnmooc.common.ResultMsg;
import com.chnmooc.common.config.SystemConfig;
import com.chnmooc.common.redis.JsonCacheClient;
import com.chnmooc.common.utils.StringUtils;


/**
 * 后台基础Action,一般都继承此类
 * @author tanyongde
 *
 */
public class BaseAction {
	
	protected static final Logger logger = LoggerFactory.getLogger(BaseAction.class);
	/**
	 * 公共跳转方法
	 * 
	 * @param viewName 视图名
	 * @param modelMap 参数列表
	 * @param request 
	 * @return
	 */
	protected ModelAndView jumpToView(String viewName, ModelMap modelMap,HttpServletRequest request) {
		return jumpToView(viewName,modelMap);
	}
	
	/**
	 * 公共跳转方法
	 * @param viewName
	 * @param modelMap
	 * @return
	 */
	protected ModelAndView jumpToView(String viewName, ModelMap modelMap) {
		if(modelMap == null) {
			return new ModelAndView(viewName);
		}
		modelMap.put("systemConfig", SystemConfig.getInstance());
		modelMap.put("date", new Date());
		return new ModelAndView(viewName, modelMap);
	}
	
	/**
	 * 公共跳转方法
	 * @param viewName
	 * @param modelMap
	 * @return
	 */
	protected ModelAndView jumpToView(String viewName) {
		return jumpToView(viewName,null);
	}

	/**
	 * 调用客户端js方法
	 * @param msg
	 * @return
	 */
	protected  ModelAndView callbackMethod(HttpServletRequest request,String jsonStr){
		String callbackName = request.getParameter("callback");
		String domain = request.getParameter("domain");
		return callbackMethod(domain,callbackName, new String[]{jsonStr});
	}
	
	/**
	 * 调用客户端js方法
	 * @param msg
	 * @return
	 */
	protected  ModelAndView callbackMethod(String domain,String methodName,String[] params){
		if(StringUtils.isEmpty(methodName)){
			return outToView("<script></script>");
		}
		if(!StringUtils.isEmpty(domain)){
			domain = "window.document.domain=\"" + domain + "\";";
		}else{
			domain = "";
		}
		StringBuffer paramStr = new StringBuffer();
		if(!(null == params || 0 == params.length)){
			for (int i = 0; i < params.length; i++) {
				if(i > 0){
					paramStr.append(",");
				}
				paramStr.append(params[i]);
			}
		}
		String msg = "<script>" + domain + "window.parent." + methodName + "(" + 
				paramStr.toString() + ");</script>";
		return outToView(msg);
	}
	
	/**
	 * 返回 jsonp 处理
	 * @param request
	 * @param jsonStr
	 * @return
	 */
	protected ModelAndView callBackForJsonp(HttpServletRequest request,String jsonStr){
		StringBuffer retrunStr = new StringBuffer();
		StringBuffer callback = new StringBuffer();
		callback.append(jsonStr);
		String callbackName = request.getParameter("callback");
		String script = request.getParameter("script");
		if(!StringUtils.isEmpty(callbackName)){
			callback.insert(0, "(");
			callback.insert(0, callbackName);
			callback.insert(callback.length(),")");
			retrunStr.append(callback.toString());
		}else{
			retrunStr.append(callback.toString());
		}
		if("1".equals(script)){
			retrunStr = new StringBuffer();
			String domain = request.getParameter("domain");
			if(!StringUtils.isEmpty(domain)){
				domain = "window.document.domain=\"" + domain + "\";";
			}else{
				domain = "";
			}
			retrunStr.append("<script type=\"text/javascript\">");
			retrunStr.append(domain);
			retrunStr.append(callback.toString().replaceFirst(callbackName, "window.parent." + callbackName));
			retrunStr.append("</script>");
		}
		return outToView(retrunStr.toString());
	}
	
	/**
	 * 调用客户端js方法(未测试)
	 * 2013-12-23 14:30
	 */
	protected  ModelAndView callback(String methodName,ResultMsg<?> msg){
		String json = "";
		if(null != msg){
			json = JSONObject.toJSONString(msg);
		}
		if(StringUtils.isEmpty(methodName)){
			return outToView(String.format("%s",json));
		}else{
			return outToView(String.format("%s(%s);",methodName,json));
		}
		
	}
	
	
	/**
	 * 调用客户端js方法
	 * @param msg
	 * @return
	 */
	protected  ModelAndView callbackMethod(String methodName,ResultMsg<?> msg){
		String json = "";
		if(null != msg){
			json = JSONObject.toJSONString(msg);
		}
		return outToView("<script>window.parent." + methodName + "(" + json + ");</script>");
	}
	
	/**
	 * 输出信息（用于解决输出JSON中文为乱码的问题）
	 * 
	 * @param msg（输出对象）
	 * @return
	 */
	protected ModelAndView outToView(String msg) {
		return new ModelAndView("/common/out", "msg",msg);
	}
	
	/**
	 * 输出信息（用于解决输出JSON中文为乱码的问题）
	 * 
	 * @param outObj
	 *            （输出对象）
	 * @return
	 */
	protected ModelAndView outToView(Object outObj) {
		return new ModelAndView("/common/out", "msg",JSONObject.toJSONString(outObj));
	}
	
	/**
	 * 输出信息（用于解决输出JSON中文为乱码的问题）
	 * 
	 * @param list
	 *            （输出对象）
	 * @return
	 */
	protected ModelAndView outToView(List<Object> list) {
		return new ModelAndView("/common/out", "msg",JSONArray.toJSONString(list));
	}
	
	/**
	 * 输出json内容
	 *  
	 * @param response
	 * @param html
	 */
	protected void doOutputJson(HttpServletResponse response, String json) {
		PrintWriter writer = null;
		try {
			response.setCharacterEncoding("UTF-8");
			response.setContentType("application/json; charset=UTF-8");
			writer = response.getWriter();
			writer.print(json);
			writer.flush();
		} catch (IOException e) {
			logger.error("out put tab json error: " + e);
		} finally {
			try {
				writer.close();
			} catch (Exception e2) {
				logger.error(e2 + "");
			}
		}
	}
	
	/**
	 * 错误页面
	 * 
	 * @param title
	 * @param txt
	 * @return
	 */
	protected ModelAndView error(String title, String txt) {
		ModelAndView modeAndView = new ModelAndView();
		try {
			modeAndView.setViewName("redirect:/common/errorPage/err404.jsp?t="
					+ java.net.URLEncoder.encode(title, "UTF-8") + "&c="
					+ java.net.URLEncoder.encode(txt, "UTF-8"));
		} catch (Exception e) {
		}
		return modeAndView;
	}

	protected ModelAndView errorForbidden() {
		ModelAndView modeAndView = new ModelAndView();
		modeAndView.setViewName("/common/errorPage/err500");
		return modeAndView;
	}

	protected ModelAndView error404() {
		ModelAndView modeAndView = new ModelAndView();
		modeAndView.setViewName("/common/errorPage/err404");
		return modeAndView;
	}

	protected String getIp(HttpServletRequest request) {
		String ip = request.getHeader("x-forwarded-for");
		if (ip != null && ip.indexOf(',') > 7)
			ip = ip.split(",")[0];
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("X-Real-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		return ip;
	}
	
	
	
	/**
	 * 输出html内容
	 * 
	 * @param response
	 * @param html
	 */
	protected void doOutputHtml(HttpServletResponse response, String html) {
		PrintWriter writer = null;
		try {
			response.setCharacterEncoding("UTF-8");
			response.setContentType("text/html; charset=UTF-8");
			writer = response.getWriter();
			writer.print(html);
			writer.flush();
		} catch (IOException e) {
			logger.error("out put tab json error: " + e);
		} finally {
			try {
				writer.close();
			} catch (Exception e2) {
				logger.error(e2 + "");
			}
		}
	}
	
	protected String getCookieValue(HttpServletRequest request, String name) {
		Cookie[] cookies = request.getCookies();
		if (cookies == null) {
			return null;
		}

		for (int i = 0; i < cookies.length; i++) {
			Cookie cookie = cookies[i];
			if (cookie.getName().equals(name)) {
				return cookie.getValue();
			}
		}

		return null;
	}
	
	protected String getDomainHost(HttpServletRequest request){
		String serverName = request.getServerName();
		return getDomainHost(serverName);
	}
	
	protected String getDomainHost(String serverName){
		if(serverName == null){
			return null;
		}
		
		String[] names = serverName.split("\\.");
		int len = names.length;
		if(len < 3){
			return serverName;
		}
		return serverName.substring(serverName.indexOf(names[len-2]));
	}
	
	protected String getHttpUrl(String url){
		if(url.indexOf("http") == -1){
			url = "http://"+url;
		}
		return url;
	}
	
}
