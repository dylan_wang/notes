package com.chnmooc.syncftp.dao;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.stereotype.Repository;

import com.chnmooc.common.dao.jdbc.BaseDaoSupport;
import com.chnmooc.common.dao.jdbc.QueryRule;
import com.chnmooc.syncftp.model.Member;

@Repository("memberDao")
public class MemberDao extends BaseDaoSupport<Member, Long> {
	
	@Override
	@Resource(name = "dataSource")
	public void setDataSource(DataSource dataSource) {
		super.setDataSourceReadOnly(dataSource);
		super.setDataSourceWrite(dataSource);
	}

	@Override
	protected String getPKColumn() {
		return "ID";
	}
	
	
	/**
	 * 用户登录时查询
	 * @param name
	 * @param password
	 * @param type
	 * @return
	 */
	public Member queryForLogin(String name,String password){
//		String sql = "select * from member where name = ? and password = ? limit 1";
		QueryRule queryRule = QueryRule.getInstance();
		queryRule.andEqual("name", name);
		queryRule.andEqual("password", password);
		return super.findUnique(queryRule);
	}
	
}
