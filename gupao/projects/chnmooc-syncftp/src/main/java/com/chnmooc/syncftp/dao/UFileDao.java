package com.chnmooc.syncftp.dao;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.stereotype.Repository;

import com.chnmooc.common.constants.SyncConstants;
import com.chnmooc.common.dao.jdbc.BaseDaoSupport;
import com.chnmooc.common.dao.jdbc.QueryRule;
import com.chnmooc.syncftp.model.UFile;

@Repository("uFileDao")
public class UFileDao extends BaseDaoSupport<UFile, Long>{
	@Override
	@Resource(name = "dataSource")
	public void setDataSource(DataSource dataSource) {
		super.setDataSourceReadOnly(dataSource);
		super.setDataSourceWrite(dataSource);
	}

	@Override
	protected String getPKColumn() {
		return "id";
	}
	
	/**
	 * 插入一个文件
	 * @param file
	 * @return
	 */
	public boolean insertFile(UFile file){
		return super.insertOne(file);
	}
	
	/**
	 * 检查某个文件是否存在
	 * @param group
	 * @param uname
	 * @param xpath
	 * @return
	 */
	public boolean queryExists(String group,String uname,String xpath){
		UFile f = new UFile(xpath);
		QueryRule queryRule = QueryRule.getInstance();
		queryRule.andEqual("fgroup", group);
		queryRule.andEqual("uname", uname);
		queryRule.andEqual("xpath", f.getXpath());
		queryRule.andEqual("fname", f.getFname());
		queryRule.andEqual("deleted", SyncConstants.DISABLE);
		List<UFile> result = super.find(queryRule);
		return !(result == null || result.size() == 0);
	}
	
	
	/**
	 * 获取某一个文件
	 * @param grounp
	 * @param uname
	 * @param xpath
	 * @param fname
	 * @return
	 */
	public UFile queryByPath(String group,String uname,String xpath){
		UFile f = new UFile(xpath);
		QueryRule queryRule = QueryRule.getInstance();
		queryRule.andEqual("fgroup", group);
		queryRule.andEqual("uname", uname);
		queryRule.andEqual("xpath", f.getXpath());
		queryRule.andEqual("fname", f.getFname());
		queryRule.andEqual("deleted", SyncConstants.DISABLE);
		return super.findUnique(queryRule);
	}
	
	/**
	 * 获取文件列表
	 * @param grounp 文件组，个人云盘、班级云盘、资源库或者其他云盘
	 * @param uname 用户名
	 * @param xpath 文件路径
	 * @param field 排序字段
	 * @param order 排序规则
	 * @return
	 */
	public List<UFile> queryByPath(String group,String uname,String xpath,String sort,String order){
		QueryRule queryRule = QueryRule.getInstance();
		queryRule.andEqual("fgroup", group);
		queryRule.andEqual("uname", uname);
		queryRule.andEqual("xpath", xpath);
		queryRule.andEqual("deleted", SyncConstants.DISABLE);
		return super.find(queryRule);
	}
	
	/**
	 * 修改某个文件的删除状态
	 * @param grounp
	 * @param uname
	 * @param xpath
	 * @param fname
	 * @return
	 */
	public boolean updateFotDelByPath(String group,String uname,String xpath){
		UFile f = new UFile(xpath);
		String sql = "update " + super.getTableName() + " set deleted = ? where xpath = ? and fname = ? and fgroup = ? and uname = ? and deleted = ?";
		List<Object> param = new ArrayList<Object>();
		param.add(SyncConstants.ENABLE);
//		param.add("" + f.getXpath() + f.getFname() + "%");
		param.add(f.getXpath());
		param.add(f.getFname());
		param.add(group);
		param.add(uname);
		param.add(SyncConstants.DISABLE);
		int result = super.jdbcTemplateWrite().update(sql, param.toArray());
		return result > 0;
	}
	
	public boolean updateForDelByPath(String group,String uname,String [] xpaths){
//		String sql = "update ufile set deleted = ? where xpath = ? and fname = ?";
//		int result = super.jdbcTemplateWrite().update(sql, SyncConstants.ENABLE,f.getXpath(),f.getFname());
//		return result > 0;
		return true;
	}
	
	public boolean updateForRenameByPath(String group,String uname,String fromPath,String toPath){
		UFile ff = new UFile(fromPath);
		UFile tf = new UFile(toPath);
		
		String sql = "update " + super.getTableName() + " set fname = ? where fname = ? and fgroup = ? and uname = ? and deleted = ?";
		List<Object> param = new ArrayList<Object>();
		param.add(tf.getFname());
		param.add(ff.getFname());
		param.add(group);
		param.add(uname);
		param.add(SyncConstants.DISABLE);
		int result = super.jdbcTemplateWrite().update(sql, param.toArray());
		
		String topath = ff.getXpath() + tf.getFname();
		int index = (ff.getXpath() + ff.getFname()).length() + 1;
		String usql = "update " + super.getTableName() + " set xpath = CONCAT(?,SUBSTRING(xpath,?)) where xpath like ? and fgroup = ? and uname = ? and deleted = ?";
		List<Object> uparam = new ArrayList<Object>();
		uparam.add(topath);
		uparam.add(index);
		uparam.add(ff.getXpath() + ff.getFname() + "%");
		uparam.add(group);
		uparam.add(uname);
		uparam.add(SyncConstants.DISABLE);
		super.jdbcTemplateWrite().update(usql, uparam.toArray());
		return result > 0;
	}
	
	public boolean deleteByPath(String grounp,String uname,String xpath){
		return false;
	}
	
	public boolean deleteByPath(String grounp,String uname,String [] xpaths){
		return false;
	}
	
}
