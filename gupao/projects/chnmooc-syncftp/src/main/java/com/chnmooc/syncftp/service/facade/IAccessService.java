/**
 * 操作权限接口：
 * <p>读，写权限的控制，
 * 速度限制，空间大小
 * <p>静态限制：随用户变化
 */
package com.chnmooc.syncftp.service.facade;

import java.io.*;
import java.util.List;

import com.chnmooc.common.ResultMsg;
import com.chnmooc.syncftp.model.UFile;

public interface IAccessService {
	/** 列目录内容 */
	public static final int OPER_LIST = 0x00000001;
	/** 下载文件 */
	public static final int OPER_DOWNLOAD = 0x00000002;
	/** 上传 */
	public static final int OPER_UPLOAD = 0x00000004;
	/** 新建目录 */
	public static final int OPER_MKDIR = 0x00000008;
	/** 修改文件名或目录名 */
	public static final int OPER_RENAME = 0x00000010;
	/** 删除空目录 */
	public static final int OPER_RMDIR = 0x00000020;
	/** 删除文件 */
	public static final int OPER_DELETE = 0x00000040;
	/**
	 * 切换根目录
	 * @param rootDir
	 */
	public abstract ResultMsg<Boolean> changeRoot(String rootDir);
	
	/**
	 * 切换用户根目录
	 * @param uname
	 * @return
	 */
	public abstract ResultMsg<Boolean> changeRoot(String rootDir,String id);

	/** 登录，返回true表示成功 */
	public abstract ResultMsg<Boolean> login(String rootDir,String userName, String userPwd);
	
	/**
	 * 判断文件是否存在
	 * @param strDir
	 * @return
	 */
	public abstract ResultMsg<Boolean> exists(String strDir);

	/** 列虚拟目录，虚拟文件详细信息 */
	public abstract ResultMsg<String> LISTV(String strDir, FilenameFilter fltr);

	/** 列虚拟目录名，虚拟文件名 */
	public abstract ResultMsg<String> NLSTV(String strDir, FilenameFilter fltr);
	
	/**
	 * 创建文件夹
	 * @param strDir
	 * @return
	 */
	public abstract ResultMsg<Boolean> createDir(String strDir);
	
	/**
	 * 获取文件列表
	 * @param strDir
	 * @return
	 */
	public abstract ResultMsg<List<UFile>> getFiles(String path);
	
	/**
	 * 获取某个文件
	 * @param fpath
	 * @return
	 */
	public abstract ResultMsg<UFile> getFileByPath(String fpath);
	
	/**
	 * 上传文件
	 * @param strPath
	 * @return
	 */
	public abstract ResultMsg<Boolean> createFile(String strPath,String objectId,File srcfile);
	
	/**
	 * 永久删除
	 * @param path
	 * @return
	 */
	public abstract ResultMsg<Boolean> removeForeverByPath(String path);
	
	
	/**
	 * 伪删除
	 * @param path
	 * @return
	 */
	public abstract ResultMsg<Boolean> removeByPath(String path);
	
	/**
	 * 重命名文件
	 * @param fromPath
	 * @param toPath
	 * @return
	 */
	public abstract ResultMsg<Boolean> renameByPath(String fromPath,String toPath);
	

	/** 返回该点是否是虚拟目录或虚拟文件 */
	public abstract ResultMsg<Boolean> isVirtualNode(String strPath);

	/** 获取目录或文件的绝对路径 */
	public abstract ResultMsg<String> getAbsolutePath(String strPath);
	
	/**  获取文件的真实路径 */
	public abstract ResultMsg<String> getOriginalPath(String objectId);

	/** 获取操作许可 */
	public abstract ResultMsg<Boolean> getAccessPermission(String strPath, int Oper);

	/** 上传传输速度限制（单位：字节） */
	public abstract ResultMsg<Long> getUploadSpeedLimit(String strPath);

	/** 下载传输速度限制（单位：字节） */
	public abstract ResultMsg<Long> getDownloadSpeedLimit(String strPath);

	/** 获取某可写虚拟目录允许空间大小 */
	public abstract ResultMsg<Long> getSpaceLimit(String strDir);

	/** 获取某可写虚拟目录剩余空间大小 */
	public abstract ResultMsg<Long> getSpaceFree(String strDir);

	/** 保存已使用空间大小 */
	public abstract ResultMsg<Boolean> setSpaceUsed(String strDir, long spaceSize);

	/** 空间使用变化（+表示占用，-表示释放） */
	public abstract ResultMsg<Boolean> setUseSpace(String strDir, long spaceSize);
}
