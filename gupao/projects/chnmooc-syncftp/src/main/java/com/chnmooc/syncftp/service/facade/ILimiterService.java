/**
 * 访问限制接口：
 * <p>控制在某种情况下拒绝登录，
 * 黑名单，线程数控制
 * <p>动态限制：与服务器情况有关
 */
package com.chnmooc.syncftp.service.facade;

import com.chnmooc.core.ftp.FtpServer;
import com.chnmooc.core.ftp.FtpThread;

public interface ILimiterService {
	/** 设定服务器实例 */
	public abstract void setServer(FtpServer server);

	/** 设定线程实例 */
	public abstract void setThread(FtpThread thread);

	/**
	 * 登录，决定是否允许登录
	 * 
	 * @return true表示允许并且登录成功
	 */
	public abstract boolean login(String userName, String userPwd, String strIP);

	/** 获取登录失败或被踢出的原因描述 */
	public abstract String getReason();

	/** 获取操作权限接口 */
	public abstract IAccessService getAccess();

	/** 关闭，做清理工作 */
	public void close();
}
