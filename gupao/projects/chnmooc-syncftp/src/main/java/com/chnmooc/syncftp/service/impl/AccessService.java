package com.chnmooc.syncftp.service.impl;

import java.io.File;
import java.io.FilenameFilter;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chnmooc.common.ResultMsg;
import com.chnmooc.common.config.SystemConfig;
import com.chnmooc.common.constants.ResultStatuCons;
import com.chnmooc.common.constants.SyncConstants;
import com.chnmooc.common.encrypt.MD5;
import com.chnmooc.core.dao.SequenceDao;
import com.chnmooc.syncftp.dao.MemberDao;
import com.chnmooc.syncftp.dao.UFileDao;
import com.chnmooc.syncftp.model.Member;
import com.chnmooc.syncftp.model.UFile;
import com.chnmooc.syncftp.service.facade.IAccessService;

@Service
public class AccessService implements IAccessService {
	private String uname;
	private String root = null;
	private String group = null;
	
	@Autowired private SequenceDao sequenceDao;
	
	@Autowired private MemberDao memberDao;
	
	@Autowired private UFileDao uFileDao;
	
	/**
	 * 列虚拟目录，虚拟文件
	 */
	@Override
	public ResultMsg<String> LISTV(String strDir, FilenameFilter fltr) {
		return new ResultMsg(ResultStatuCons.SUCCESS,null, "");
	}
	
	@Override
	public ResultMsg<String> NLSTV(String strDir, FilenameFilter fltr) {
		return new ResultMsg(ResultStatuCons.SUCCESS,null, "");
	}
	
	/**
	 * 创建文件夹
	 */
	@Override
	public ResultMsg<Boolean> createDir(String strDir) {
		UFile ufile = new UFile(strDir);
		pushRequired(ufile,null);
		if(uFileDao.queryExists(group, uname, strDir)){
			return new ResultMsg(ResultStatuCons.ERROR, "已经存在相同名字的文件夹",false);
		}
		boolean result = uFileDao.insertFile(ufile);
		return new ResultMsg(ResultStatuCons.SUCCESS, result);
	}
	
	/**
	 * 获得某个目录下的文件列表
	 */
	public ResultMsg<List<UFile>> getFiles(String path){
		List<UFile> result = uFileDao.queryByPath(group,uname, path,null,null);
		return new ResultMsg(ResultStatuCons.SUCCESS, result);
	}

	
	public ResultMsg<UFile> getFileByPath(String path){
		UFile result = uFileDao.queryByPath(group,uname,path);
		return new ResultMsg(ResultStatuCons.SUCCESS, result);
	}
	
	/**
	 * 上传文件
	 */
	public ResultMsg<Boolean> createFile(String strPath,String objectId,File srcfile){
		UFile ufile = new UFile(strPath);
		ufile.setObjectId(objectId);
		pushRequired(ufile,srcfile);
		if(uFileDao.queryExists(group, uname, strPath)){
			return new ResultMsg(ResultStatuCons.ERROR, "已经存在相同名字的文件",false);
		}
		boolean result = uFileDao.insertFile(ufile);
		return new ResultMsg(ResultStatuCons.SUCCESS, result);
	}
	
	/**
	 * 根据路径永久删除
	 */
	public ResultMsg<Boolean> removeForeverByPath(String path){
		//uFileDao.updateFotDelByPath(path, path, path);
		return null;
	}
	
	/**
	 * 根据路径做伪删除
	 */
	public ResultMsg<Boolean> removeByPath(String path){
		boolean flag = uFileDao.updateFotDelByPath(group, uname, path);
		return new ResultMsg<Boolean>(ResultStatuCons.SUCCESS, flag);
	}
	
	public ResultMsg<Boolean> renameByPath(String fromPath,String toPath){
		boolean flag = uFileDao.updateForRenameByPath(group, uname, fromPath, toPath);
		return new ResultMsg<Boolean>(ResultStatuCons.SUCCESS, flag);
	}
	
	/**
	 * 登录
	 */
	@Override
	public ResultMsg<Boolean> login(String rootDir,String userName, String userPwd) {
		try {
			userPwd = MD5.calcMD5(userPwd);
			Member m = memberDao.queryForLogin(userName, userPwd);
			if(m != null){
				changeRoot(rootDir);
				uname = m.getName();
				return new ResultMsg(ResultStatuCons.SUCCESS, true);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResultMsg(ResultStatuCons.SUCCESS, false);
	}

	/**
	 * 获取目录或文件的绝对路径，返回空表示不对应实际目录
	 */
	@Override
	public ResultMsg<String> getAbsolutePath(String strPath) {
		String path = root + "/" + uname.replaceAll("([\\@|\\.])", "/") + "/" + uname + "/" ;
		
		File file = new File(path);
		if(!file.exists()) file.mkdirs();
		return new ResultMsg(ResultStatuCons.SUCCESS,null, path + strPath);
	}
	
	public ResultMsg<String> getOriginalPath(String objectId) {
		String savepath = objectId.replaceAll("(.{3})", "$1/");
		return new ResultMsg(ResultStatuCons.SUCCESS,null, SystemConfig.filesDepot + "/" + savepath + "/" + objectId);
	}

	@Override
	public ResultMsg<Boolean> getAccessPermission(String strPath, int Oper) {
		return new ResultMsg(ResultStatuCons.SUCCESS, true);
	}

	@Override
	public ResultMsg<Long> getDownloadSpeedLimit(String strPath) {
		return new ResultMsg(ResultStatuCons.SUCCESS, 0L);
	}

	@Override
	public ResultMsg<Long> getSpaceFree(String strDir) {
		return new ResultMsg(ResultStatuCons.SUCCESS, 0L);
	}

	@Override
	public ResultMsg<Long> getSpaceLimit(String strDir) {
		return new ResultMsg(ResultStatuCons.SUCCESS, 0L);
	}

	@Override
	public ResultMsg<Long> getUploadSpeedLimit(String strPath) {
		return new ResultMsg(ResultStatuCons.SUCCESS, 0L);
	}

	@Override
	public ResultMsg<Boolean> isVirtualNode(String strPath) {
		return new ResultMsg(ResultStatuCons.SUCCESS, false);
	}

	@Override
	public ResultMsg<Boolean> setSpaceUsed(String strDir, long spaceSize) {
		return new ResultMsg(ResultStatuCons.SUCCESS, true);
	}

	@Override
	public ResultMsg<Boolean> setUseSpace(String strDir, long spaceSize) {
		return new ResultMsg(ResultStatuCons.SUCCESS, true);
	}

	/**
	 * 判断文件是否存在
	 */
	@Override
	public ResultMsg<Boolean> exists(String path) {
		UFile result = uFileDao.queryByPath(group,uname,path);
		return new ResultMsg(ResultStatuCons.SUCCESS, (null != result));
	}
	
	/**
	 * 切换根目录
	 */
	public ResultMsg<Boolean> changeRoot(String rootDir){
		this.root = rootDir;
		if(root.contains(SyncConstants.FILE_GROUP_USERS.toLowerCase())){
			group = SyncConstants.FILE_GROUP_USERS;
		}else if(root.contains(SyncConstants.FILE_GROUP_RESOURCE.toLowerCase())){
			group = SyncConstants.FILE_GROUP_RESOURCE;
		}else if(root.contains(SyncConstants.FILE_GROUP_CLASS.toLowerCase())){
			group = SyncConstants.FILE_GROUP_CLASS;
		}
		return new ResultMsg(ResultStatuCons.SUCCESS, true);
	}

	public ResultMsg<Boolean> changeRoot(String rootDir,String id){
		this.uname = id;
		return changeRoot(rootDir);
	}
	
	/**
	 * 设置必需的属性
	 * @param ufile
	 */
	private void pushRequired(UFile ufile,File file){
		String id = sequenceDao.getNextVal(UFile.class.getSimpleName());
		ufile.setId(id);
		ufile.setUname(uname);
		long curr = System.currentTimeMillis();
		ufile.setCreateTime(curr);
		ufile.setLastModified(curr);
		ufile.setFgroup(group);
		if(null != file && file.isFile()){
			ufile.setFsize(file.length());
			ufile.setLastModified(file.lastModified());
//			ufile.setFtype(SyncConstants.FILE_TYPE_FILE);
//			ufile.setFname(file.getName());
//			ufile.setExt(file.getName().substring(file.getName().lastIndexOf("\\.")));
		}
		
		
		UFile p = uFileDao.queryByPath(group, uname, ufile.getXpath());
		if(null != p){
			ufile.setPid(p.getId());
			ufile.setXcode(p.getXcode() + ufile.getPid() + "/");
		}else{
			ufile.setPid("0");
			ufile.setXcode("/");
		}
	}

	
}
