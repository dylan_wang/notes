package com.chnmooc.syncftp.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chnmooc.core.ftp.FtpThread;
import com.chnmooc.core.ftp.FtpServer;
import com.chnmooc.common.constants.SyncConstants;
import com.chnmooc.syncftp.service.facade.IAccessService;
import com.chnmooc.syncftp.service.facade.ILimiterService;

/**
 * 
 * @author Tanyongde
 *
 */
@Service
public class LimiterService implements ILimiterService {
	FtpServer theServer = null;
	FtpThread theThread = null;

	@Autowired private IAccessService  accessService;
	
//	// 操作权限
//	private IAccessService theAccess;

	/** 设定服务器实例 */
	public void setServer(FtpServer server) {
		theServer = server;
	}

	/** 设定线程实例 */
	public void setThread(FtpThread thread) {
		theThread = thread;
	}

	// 登录
	public boolean login(String userName, String userPwd, String strIP) {
//		if (accessService == null) {
//			accessService = new AccessService(theServer.getRootDir());
//		}
		String root = theServer.getRootDir() + SyncConstants.FILE_GROUP_USERS.toLowerCase();
		return accessService.login(root,userName, userPwd).getData();
	}

	// 获取原因
	public String getReason() {
		return "";
	}

	// 获取操作权限
	public IAccessService getAccess() {
		return accessService;
	}
	
	public void setAccess(IAccessService accessService) {
		this.accessService = accessService;
	}

	public void close() {
		//accessService = null;
	}

}
