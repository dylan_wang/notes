package com.chnmooc.syncftp.test;

import java.io.File;
import java.io.IOException;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ch.ethz.ssh2.Connection;
import ch.ethz.ssh2.SCPClient;
import ch.ethz.ssh2.Session;

import com.chnmooc.common.config.SystemConfig;
import com.chnmooc.syncftp.service.facade.IAccessService;
import com.twmacinta.util.MD5;



@ContextConfiguration(locations = {"classpath*:application-context.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class SyncFileTest {
	
	@Autowired IAccessService accessService;
	
	
	public static void main(String[] args) {
		try{

			String rootDir = SystemConfig.rootDir + "users_bak/";
			
			Connection conn = new Connection("221.204.194.21"); 
			conn.connect(); 
			boolean isAuthenticated = conn.authenticateWithPassword("root","][po';lk");
			if(isAuthenticated){
				SCPClient scp = new SCPClient(conn);
				scp.get("/home/ftphome/users/USERS.zip", rootDir);
				System.out.println("-============");
			}
			
    	}catch(Exception e){
    		e.printStackTrace();
    	}
	}
	
	
	@Test
	@Ignore
	public void file2DB(){
		try{

			String rootDir = SystemConfig.rootDir + "users_bak/";
			
			Connection conn = new Connection("221.204.194.21"); 
			conn.connect(); 
			boolean isAuthenticated = conn.authenticateWithPassword("root","][po';lk");
			if(isAuthenticated){
				SCPClient scp = new SCPClient(conn);
				getFiles(scp,conn,rootDir);
				conn.close();
			}
			
			
    	}catch(Exception e){
    		e.printStackTrace();
    	}
	}
	
	
	
	  /** 
     * 删除目录（文件夹）以及目录下的文件 
     * @param   dirPath 被删除目录的文件路径 
     * @return  目录删除成功返回true，否则返回false 
     */  
    private void getFiles(SCPClient scp,Connection conn,String dirPath) {  
        if (!dirPath.endsWith(File.separator)) {  
        	dirPath = dirPath + File.separator;  
        }  
        File dirFile = new File(dirPath);  
    
        for (File f : dirFile.listFiles()) {  
            //删除子文件  
            if (f.isFile()) {  
            	try {
            		if(!f.isHidden()){
            			//声明公共变量
						String objectId = MD5.asHex(MD5.getHash(f));
						String fname = f.getName();
						String fpath = f.getAbsolutePath();

						
						//重命名
						String savepath = objectId.replaceAll("(.{3})", "$1/") + "/";
						File renameFile = new File("G:/home/ftphome/" + savepath + objectId);
						File dir = new File("G:/home/ftphome/" + savepath);
						if(!dir.exists()){ dir.mkdirs(); }
						f.renameTo(renameFile);
						
						
						//发送到云盘机器
						System.out.println("开始发送" + fpath + "至" + SystemConfig.filesDepot + savepath);
						Session session = conn.openSession();
						session.execCommand("cd /&&mkdir -p " + SystemConfig.filesDepot + savepath);
						session.close();
						scp.put(renameFile.getAbsolutePath(), SystemConfig.filesDepot + savepath);
						
						
						//入库
						String xpath = fpath.replaceAll("G:\\\\home\\\\ftphome\\\\users_bak","").replaceAll("\\\\", "/");
						xpath = xpath.substring(1,xpath.lastIndexOf("/") + 1);
						String uname = xpath.substring(0,xpath.indexOf("/"));
						xpath = xpath.substring(xpath.indexOf("/"));
						
						accessService.changeRoot(SystemConfig.usersRootDir, uname);
						accessService.createFile(xpath + fname, objectId, f);
						
						
						//输出
						System.out.println("xpath=" + xpath + ",uname=" + uname + ",fname=" + fname + ",ftype=FILE,objectid=" + objectId);
            		}
				} catch (IOException e) {
					e.printStackTrace();
				}
            } //删除子目录  
            else {
            	String xpath = f.getAbsolutePath().replaceAll("G:\\\\home\\\\ftphome\\\\users_bak","").replaceAll("\\\\", "/") + "/";
				xpath = xpath.substring(1,xpath.lastIndexOf("/") + 1);
				String uname = xpath.substring(0,xpath.indexOf("/"));
				xpath = xpath.substring(xpath.indexOf("/"));
				if(!xpath.equals("/")){
				
					//在数据库中创建文件夹
					accessService.changeRoot(SystemConfig.usersRootDir, uname);
					accessService.createDir(xpath);
					
					//输出
					System.out.println("xpath=" + xpath + ",uname=" + uname + ",fname=" + f.getName() + ",ftype=DIR");
				}
				
                getFiles(scp,conn,f.getAbsolutePath());  
            }  
        }  
    }
}
