## 浅尝系列（二）--AOP原理演示

AOP 就是在目标方法执行前后添加指定方法。详细的解释我就不说了，百度一堆。

### 基础实现

通过JDK的源生方法实现。需要用到接口。

创建目标接口

```

public interface ISayHelloWorld {

	String say(String str);
}
```

创建目标接口实现

```

public class ManSayHelloWorld implements ISayHelloWorld{

	@Override
	public String say(String str) {
		System.out.println("hello word");
		return str;
	}

}
```

创建AOP切面
需要实现 InvocationHandler 接口

```

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class AOPHandle implements InvocationHandler{

	private Object obj;
	public AOPHandle(Object obj){
		this.obj=obj;
	}
	
	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		System.out.println("前置代理");
		Object ret=method.invoke(obj, args);
		System.out.println("后置代理");
		return ret;
	}

}
```

通过切面执行目标方法

```

import wang.yearn.demo.aop.born.AOPHandle;
import wang.yearn.demo.aop.born.ISayHelloWorld;
import wang.yearn.demo.aop.born.ManSayHelloWorld;


	@Test
	public void testBorn() {
		ManSayHelloWorld sayHelloWorld = new ManSayHelloWorld();
		AOPHandle handle = new AOPHandle(sayHelloWorld);
		ClassLoader cl=ManSayHelloWorld.class.getClassLoader();
		Class<?>[] cs=new Class[] {ISayHelloWorld.class};
		ISayHelloWorld i = (ISayHelloWorld) Proxy.newProxyInstance(cl, cs, handle);
		String ret=i.say("123");
		System.out.println(ret);
	}

```

查看执行结果

```

	前置代理
	hello word
	后置代理
	123
```

可以看出来在目标方法执行前后都使用了指定的方法

### cglib实现

使用cglib实现需要引入cglib.jar

使用cglib实现的时候不需要接口

pox.xml 配置

```

	<dependency>
	    <groupId>cglib</groupId>
	    <artifactId>cglib</artifactId>
	    <version>3.2.10</version>
	</dependency>
```

创建目标函数

```

public class SayHello {

	public String say(String str) {
		System.out.println("hello word");
		return str;
	}
}

```

创建AOP实现

需要实现 MethodInterceptor 接口

```

import java.lang.reflect.Method;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

public class CglibProxy implements MethodInterceptor{

	private Enhancer enhancer = new Enhancer();
	public Object getProxy(Class clazz){
	    enhancer.setSuperclass(clazz);
	    enhancer.setCallback(this);
	    return enhancer.create();
	}
	@Override
	public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
	    System.out.println("前置代理");
	    System.out.println(methodProxy.getClass().getName());
	    //通过代理类调用父类中的方法
	    Object result = methodProxy.invokeSuper(o, objects);
	
	    System.out.println("后置代理");
	    return result;
	}

}
```

创建测试方法

```

	@Test
	public void testCglit() {
		CglibProxy proxy = new CglibProxy();
		//通过生成子类的方式创建代理类
		SayHello proxyImp = (SayHello)proxy.getProxy(SayHello.class);
		proxyImp.say("123");
	}
```

查看结果

```

前置代理
net.sf.cglib.proxy.MethodProxy
hello word
后置代理
```

同样实现了目标方法前后的控制。

jdk和cglib的区别就在于一个需要接口一个不需要接口

### 增加配置

扩展一下，通过配置执行指定的拦截方法

为了方便就用cglib方式实现

创建切面接口

```

import java.lang.reflect.Method;

public interface Intercept {

	void before(Object obj, Method method, Object[] objects);
	
	void after(Object result);
}
```

创建关于切面的实现类

```

import java.lang.reflect.Method;

public class MyIntercept implements Intercept{

	@Override
	public void before(Object obj, Method method, Object[] objects) {
		System.out.println("前置代理");
		
	}

	@Override
	public void after(Object result) {
		System.out.println("后置代理");
		
	}

}
```

创建一个配置类的基础类

```

import java.util.HashMap;
import java.util.Map;

public abstract class ProxyConfig {

	public static Map<Class<?>, Intercept> pc=new HashMap<Class<?>, Intercept>();
	
	public abstract void pcConfig();
	
	public void add(Class<?> targe, Intercept intercept) {
		pc.put(targe, intercept);
	}
}
```

创建自己的配置类继承配置基础类

```

public class MyProxyConfig extends ProxyConfig{

	@Override
	public void pcConfig() {
		add(SayHello.class, new MyIntercept1());
		add(SayHello.class, new MyIntercept());
	}

}
```

创建AOP实现

```

import java.lang.reflect.Method;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

public class DynamicProxy implements MethodInterceptor {

	private Enhancer enhancer = new Enhancer();

	private Intercept ipt;

	public Object getProxy(Class clazz) {
		enhancer.setSuperclass(clazz);
		enhancer.setCallback(this);
		//根据配置方法取出切面类
		ipt = MyProxyConfig.pc.get(clazz);
		return enhancer.create();
	}

	@Override
	public Object intercept(Object obj, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {

		ipt.before(obj, method, objects);
		Object result = methodProxy.invokeSuper(obj, objects);
		ipt.after(result);
		return result;
	}

}
```

创建测试方法

```

	@Test
	public void dynamic() {
		DynamicProxy dp=new DynamicProxy();
		MyProxyConfig mpc=new MyProxyConfig();
		mpc.pcConfig();
		SayHello targe=(SayHello) dp.getProxy(SayHello.class);
		targe.say("123");
	}
```

查看结果

```

前置代理
hello word
后置代理
```

至此一个简单的AOP框架也就实现了，还有很多方面可以扩展，比如添加异常和返回的控制。多个切面的连续控制。

浅尝辄止！知道什么原理就好了！
