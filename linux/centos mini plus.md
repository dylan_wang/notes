### 配置网络
```
#配置网络
vi /etc/sysconfig/network-scripts/ifcfg-enp0s3
#修改ONBOOT=yes
#重启网络
service network restart



#下载wget
yum install wget -y

#备份原始源文件
mv /etc/yum.repos.d/CentOS-Base.repo /etc/yum.repos.d/CentOS-Base.repo.backup
cd /etc/yum.repos.d/
#下载新的源文件
wget http://mirrors.163.com/.help/CentOS6-Base-163.repo

#重建缓存
yum clean all
yum makecache

#更新软件包
yum -y update

#安装vim
yum install vim
//修改vim配置
vim ~/.vimrc
set encoding=utf-8 " 文件编码
set number " 显示行号
set tabstop=4 " tab宽度为4
set softtabstop=4 " 设置一次可以删除4个空格
set expandtab " tab转换为空格
set nowrap " 不自动换行
set showmatch " 显示括号配对情况
syntax on " 开启语法高亮

#安装vsftpd

#绑定固定ip
vim /etc/sysconfig/network-scripts/ifcfg-enp0s3
IPADDR=192.168.1.180
GATEWAY=192.168.1.1
DNS1=114.114.114.114
DNS2=8.8.8.8
:wq
service network restart

#修改时间
ll /etc/localtime
// 如果不为上海时区则执行
rm -rf /etc/localtime ln -s /etc/localtime /usr/share/zoneinfo/Asia/Shanghai

#修改计算机名称
echo $HOSTNAME
vi /etc/hostname

#开启指定端口
systemctl status firewalld  


#开启80端口


firewall-cmd --zone=public --add-port=80/tcp --permanent  


#开启3306端口


firewall-cmd --zone=public --add-port=3306/tcp --permanent  


#重启防火墙
firewall-cmd --reload

```
