#!/bin/bash
#cat命令
#将文件内容输出
#语法格式
#cat [-AbeEnstTuv] [--help] [--version] fileName
#-n 或 --number：由 1 开始对所有输出的行数编号。
#-b 或 --number-nonblank：和 -n 相似，只不过对于空白行不编号。
#-s 或 --squeeze-blank：当遇到有连续两行以上的空白行，就代换为一行的空白行。
#-v 或 --show-nonprinting：使用 ^ 和 M- 符号，除了 LFD 和 TAB 之外。
#-E 或 --show-ends : 在每行结束处显示 $。
#-T 或 --show-tabs: 将 TAB 字符显示为 ^I。
#-e : 等价于 -vE。
#-A, --show-all：等价于 -vET。
#-e：等价于"-vE"选项；
#-t：等价于"-vT"选项；
echo "cat -A"
cat -A log.log
echo "cat -b：显示行号，空白行不显示"
cat -b log.log
echo "cat -e"
cat -e log.log
echo "cat -E：每行结尾显示$"
cat -E log.log
echo "cat -n：显示行号"
cat -n log.log
echo "cat -s：合并多行空白"
cat -s log.log
echo "cat -t：等价于vT"
cat -t log.log
echo "cat -T：将TAB字符显示为^I"
cat -T log.log
echo "cat -u："
cat -u log.log
echo "cat -v"
cat -v log.log


#清空文档内容
#cat /dev/null > file
#制作镜像文件
#cat /dev/fd0 > outfile
#图片写到软盘
#cat imgfile > /dev/fd0

