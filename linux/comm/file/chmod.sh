#!/bin/bash
#chmod
#更改文件或目录使用全线
#语法
#chmod [-cfvR] [--help] [--version] mode file...
#
#    -c : 若该文件权限确实已经更改，才显示其更改动作
#    -f : 若该文件权限无法被更改也不要显示错误讯息
#    -v : 显示权限变更的详细资料
#    -R : 对目前目录下的所有文件与子目录进行相同的权限变更(即以递回的方式逐个变更)
#    --help : 显示辅助说明
#    --version : 显示版本
#
#mode语法
#[ugoa...][[+-=][rwxX]...][,...]

#    u 表示该文件的拥有者
#    g 表示与该文件的拥有者属于同一个群体(group)者
#    o 表示其他以外的人
#    a 表示这三者皆是
#
#    + 表示增加权限
#    - 表示取消权限
#    = 表示唯一设定权限
#
#    r 表示可读取
#    w 表示可写入
#    x 表示可执行
#    X 表示只有当该文件是个子目录或者该文件已经被设定过为可执行。
echo 设置读取权限
chmod -v ugo+r log2017.log
chmod -v a+r log2018.log
ls -l
echo 设置写入权限
chmod -v ug+w a-w log2017.log log2018.log
ls -l
#也可以通过数字设置权限
#语法：chmod abc file
#a:user=u   b:group=g   c:other=o
#r=4  w=2  x=1
echo 设置全部权限
chmod -v 777 log2017.log log2018.log
chmod -v a+rwx log2017.log log2018.log
ls -l


