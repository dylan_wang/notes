#!/bin/bash
#chown
#修改文件或目录的所有者
#只有root用户可以使用
#语法
#chown [-cfhvR] [--help] [--version] user[:group] file...
#-c : 显示更改的部分的信息
#-f : 忽略错误信息
#-h :修复符号链接
#-v : 显示详细的处理信息
#-R : 处理指定目录以及其子目录下的所有文件
#--help : 显示辅助说明
#--version : 显示版本
#user : 新的文件拥有者的使用者 ID
#group : 新的文件拥有者的使用者组(group)
echo 修改文件所有者
chown -v dylan:dylan log2017.log log2018.log
ls -l
#修改目录下的所有文件
#chown -R dylan:dylan *
