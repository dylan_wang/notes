#!/bin/bash
#cmp
#比较两个文件是否一直
#语法
#cmp [-clsv][-i <字符数目>][--help][第一个文件][第二个文件]
#    -c或--print-chars 　除了标明差异处的十进制字码之外，一并显示该字符所对应字符。
#    -i<字符数目>或--ignore-initial=<字符数目> 　指定一个数目。
#    -l或--verbose 　标示出所有不一样的地方。
#    -s或--quiet或--silent 　不显示错误信息。
#    -v或--version 　显示版本信息。
#    --help 　在线帮助。
echo 比较文件
cmp log2017.log log2018.log
#比较完全相同没有任何显示
cmp -c log2017.log log2018.log
cmp -l log2017.log log2018.log

