#!/bin/bash
#diffstat
#根据diff的结果统计差异量
#语法
#diff [-wV][-n <文件名长度>][-p <文件名长度>]
#-w 　指定输出时栏位的宽度。
#-V 　显示版本信息。
#-n<文件名长度> 　指定文件名长度，指定的长度必须大于或等于所有文件中最长的文件名。
#-p<文件名长度> 　与-n参数相同，但此处的<文件名长度>包括了文件的路径。
echo 比较并进行统计
diff log2017.log log2018.log | diffstat


