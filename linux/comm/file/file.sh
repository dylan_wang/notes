#!/bin/bash
#file
#查看文件类型
#语法
#file [-bcLvz][-f <名称文件>][-m <魔法数字文件>...][文件或目录...]

#-b 　列出辨识结果时，不显示文件名称。
#-c 　详细显示指令执行过程，便于排错或分析程序执行的情形。
#-L 　直接显示符号连接所指向的文件的类别。
#-v 　显示版本信息。
#-z 　尝试去解读压缩文件的内容。
#-f<名称文件> 　指定名称文件，其内容有一个或多个文件名称时，让file依序辨识这些文件，格式为每列一个文件名称。
#-m<魔法数字文件> 　指定魔法数字文件。
#   [文件或目录...] 要确定类型的文件列表，多个文件之间使用空格分开，可以使用shell通配符匹配多个文件。
echo 显示文件类型
file log2017.log
echo 不显示文件名
file -b log2017.log
echo 显示符号链接的文件类型
ls -l /var/mail
file /var/mail
file -L var/mail
file /var/spool/mail
file -L /var/spool/mail

