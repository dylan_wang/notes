#!/bin/bash

array1=(A B "C" D)
echo "第一个元素为: ${array1[0]}"
echo "第二个元素为: ${array1[1]}"
echo "第三个元素为: ${array1[2]}"
echo "第四个元素为: ${array1[3]}"
array2[0]=A
array2[1]=B
array2[3]=D
echo "数组元素为:${array2[*]}"
echo "数组元素为:${array2[@]}"
echo "数组元素个数为:${#array2[*]}"
echo "数组元素个数为:${#array2[@]}"
