#!/bin/bash
#流程控制

#if
#流程分支中不能为空类似存储过程
#语法：if then  else if
a=10
b=20
if [ $a==$b  ]
then
	echo "a==b"
else    
	echo "a!=b"
fi
#语法：if then elif then fi
if [ $a == $b ]
then
	echo "a 等于 b"
elif [ $a -gt $b ]
then
	echo "a 大于 b"
elif [ $a -lt $b ]
then
	echo "a 小于 b"
else
	echo "没有符合条件"
fi
#也可以写在一行
if [ $(ps -ef | grep -c "ssh") -gt 1 ]; then echo "true"; fi
#经常与test命令一起使用
num1=$[2*3]
num2=$[1+5]
if test $[num1] -eq $[num2]
then
	echo "两个数字相等"
else
	echo "两个数字不相等"
fi
#for
#语法for var in .... 
#do 
#[] 
#done
#写成一行for var in ...;do [;;] done;
for loop in 1 2 3 4 5
do
	echo "the value is:$loop"
done
#按顺序输出字符
for str in 'this is a string'
do
	echo $str
done

#while
#语法
#while conditino
#do
#	command
#done
int=1
while (($int<=5))
do
	echo $int
	#let:Bash let命令，执行一个或多个表达式，变量不用添加$
	let "int++"
done

echo "按下<CTRL+D>退出"
echo -n "输入你的名字："
while read FILM
do
	echo "$FILM 是个好人！"
done

#无限循环
#while :
#do
#	command
#done
#或者
#while true
#do
#	command
#done
#或者
#for ((;;))

#until
#与while正好相反
#语法until condition
#do
#	command
#done
a=0
until [ ! $a -lt 10 ]
do
	echo $a
	a=`expr $a + 1`
done

#case
#选择模式
#语法:case 值 in
#选择1)command
#;;
#选择2)command
#;;
#..............
#*)command  其他选择
#;;
#esac
echo '输入 1 到 4 之间的数字:'
echo '你输入的数字为:'
read aNum
case $aNum in
    1)  echo '你选择了 1'
    ;;
    2)  echo '你选择了 2'
    ;;
    3)  echo '你选择了 3'
    ;;
    4)  echo '你选择了 4'
    ;;
    *)  echo '你没有输入 1 到 4 之间的数字'
    ;;
esac
#break 结束循环
#continue 跳出当前循环
