#!/bin/sh
#显示字符串
echo hello
echo 'hello'
echo "hello"
echo \"hello\"
echo '"$name\"'
#显示变量
echo "请输入姓名"
read name
echo hello $name
echo 换行显示
echo -e "ok! \n"  #-e 开启转义
echo hello
echo 换行显示
echo "ok! \n"  #一定要有双引号
echo hello
echo 不换行显示
echo -e "ok! \c" # -e 开启转义 \c 不换行
echo hello
#输出到文本
echo hello > test.log
#显示命令结果
echo `date`
