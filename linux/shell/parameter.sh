#!/bin/bash

#执行方法：./parameter.sh  1 2 3
echo "Shell 传递参数实例！";
echo "执行的文件名：$0";
echo "第一个参数为：$1";
echo "第二个参数为：$2";
echo "第三个参数为：$3";
echo "传递参数个数：$#";
echo "脚本运行的当前进程ID：$$";
echo "后台运行的最后一个进程ID：$!";
echo "显示Shell使用的当前选项：$-";
echo "显示最后命令的退出状态，0是正常：$?";
echo "-- \$* 演示 ---"
for i in "$*"; do
  echo $i
done

echo "-- \$@ 演示 ---"
for i in "$@"; do
  echo $i
done

echo $*;
echo $@;
