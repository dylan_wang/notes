#!/bin/bash
#输入/输出重定向
#将$who命令的完整输出重定向到users
#执行之后看不见输出内容输出会保存到users
who > users
#输出重定向可以覆盖文件内容
echo 我是个好人 > users
cat users
#也可以在原来内容后追加输出
echo 我是个好人 >> users
#输入重定向
wc -l < users
#标准输入文件(stdin)：stdin的文件描述符为0，Unix程序默认从stdin读取数据
#标准输出文件(stdout)：stdout 的文件描述符为1，Unix程序默认向stdout输出数据
#标准错误文件(stderr)：stderr的文件描述符为2，Unix程序会向stderr流中写入错误信息
#重定向错误信息
#command 2 > file
#将输出和错误同时重定向
#command > file 2>&1
#输入输出分别重定向
#command < file1 >file2
#Here Document
#交互式输入重定向
#command << delimiter
#  document
#delimiter
#结尾的delimiter 一定要顶格写，前面不能有任何字符，后面也不能有任何字符，包括空格和 tab 缩进
wc -l << EOF
  欢迎来到
  禽兽空间
  我是个好人
EOF
#如果希望执行某个命令，但又不希望在屏幕上显示输出结果，那么可以将输出重定向到 /dev/null：
#command > /dev/null

