$ mvn -h

usage: mvn [options] [<goal(s)>] [<phase(s)>]

Options:
 -am,--also-make                        If project list is specified, also build projects required by the list
 -amd,--also-make-dependents            If project list is specified, also build projects that depend on projects on the list
 -B,--batch-mode                        Run in non-interactive (batch) mode (disables output color)
 -b,--builder <arg>                     The id of the build strategy to use
 -C,--strict-checksums                  Fail the build if checksums don't match
 -c,--lax-checksums                     Warn if checksums don't match
 -cpu,--check-plugin-updates            Ineffective, only kept for backward compatibility
 -D,--define <arg>                      Define a system property
 -e,--errors                            Produce execution error messages
 -emp,--encrypt-master-password <arg>   Encrypt master security password
 -ep,--encrypt-password <arg>           Encrypt server password
 -f,--file <arg>                        Force the use of an alternate POM file (or directory with pom.xml)
 -fae,--fail-at-end                     Only fail the build afterwards; allow all non-impacted builds to continue
 -ff,--fail-fast                        Stop at first failure in reactorized builds
 -fn,--fail-never                       NEVER fail the build, regardless of project result
 -gs,--global-settings <arg>            Alternate path for the global settings file
 -gt,--global-toolchains <arg>          Alternate path for the global toolchains file
 -h,--help                              Display help information
 -l,--log-file <arg>                    Log file where all build output will go (disables output color)
 -llr,--legacy-local-repository         Use Maven 2 Legacy Local Repository behaviour, ie no use of _remote.repositories. Can also be activated by using -Dmaven.legacyLocalRepo=true
 -N,--non-recursive                     Do not recurse into sub-projects
 -npr,--no-plugin-registry              Ineffective, only kept for backward compatibility
 -npu,--no-plugin-updates               Ineffective, only kept for backward compatibility
 -nsu,--no-snapshot-updates             Suppress SNAPSHOT updates
 -o,--offline                           Work offline
 -P,--activate-profiles <arg>           Comma-delimited list of profiles to activate
 -pl,--projects <arg>                   Comma-delimited list of specified reactor projects to build instead of all projects. A project can be specified by [groupId]:artifactId or by its relative path
 -q,--quiet                             Quiet output - only show errors
 -rf,--resume-from <arg>                Resume reactor from specified project
 -s,--settings <arg>                    Alternate path for the user settings file
 -t,--toolchains <arg>                  Alternate path for the user toolchains file
 -T,--threads <arg>                     Thread count, for instance 2.0C where C is core multiplied
 -U,--update-snapshots                  Forces a check for missing releases and updated snapshots on remote repositories
 -up,--update-plugins                   Ineffective, only kept for backward compatibility
 -v,--version                           Display version information
 -V,--show-version                      Display version information WITHOUT stopping build
 -X,--debug                             Produce execution debug output





-am，--如果指定了项目列表，也可以生成该列表所需的项目

-amd，--如果指定了项目列表，还将生成依赖于该列表上的项目的项目

-b，-batch模式在非交互（批处理）模式下运行（禁用输出颜色）

-b，-builder<arg>要使用的生成策略的ID

-c，-strict校验和如果校验和不匹配，则生成失败

-c，-lax校验和不匹配时发出警告

-cpu，--check插件更新无效，只保留向后兼容性

-d，--define<arg>定义系统属性

-e，-errors生成执行错误消息

-emp，--encrypt master password<arg>encrypt master security password

-ep，--encrypt password<arg>encrypt server password

-f，-file<arg>强制使用备用pom文件（或pom.xml目录）

-fae，--最后失败，只有在随后的生成失败；允许所有未受影响的生成继续

-ff，-—在反应生成中第一次失败时快速停止

-fn，--fail决不会使构建失败，无论项目结果如何

-gs，-global settings<arg>global settings文件的备用路径

-gt，--global toolchains<arg>global toolchains文件的备用路径

-h，-help显示帮助信息

-l，--log file<arg>log file其中所有生成输出将转到（禁用输出颜色）

-llr，-legacy本地存储库使用maven 2遗留本地存储库行为，即不使用远程存储库。也可以使用-dmaven.legacyocalrepo=true激活。

-n，-non-recursive不递归到子项目中

-npr，--没有插件注册表无效，只保留向后兼容性

-npu，--没有插件更新无效，只保留向后兼容性

-nsu，-no snapshot updates禁止快照更新

-o，--offline离线工作

-p，--activate profiles<arg>comma分隔的要激活的配置文件列表

-pl，--projects<arg>comma分隔的指定反应堆项目列表，而不是所有项目。项目可以由[groupid]：artifactid或其相对路径指定

-q，-quiet安静输出-仅显示错误

-rf，-resume from<arg>resume reactor from specified project（从指定项目恢复反应堆）

-s，-settings<arg>用户设置文件的备用路径

-t，--toolchains<arg>用户toolchains文件的备用路径

-t，-threads<arg>thread count，例如2.0c，其中c是core乘法

-u，--update snapshots强制检查远程存储库中丢失的版本和更新的快照

-向上，-更新插件无效，仅保持向后兼容性

-v，--版本显示版本信息

-v，--显示版本显示版本信息而不停止生成

-x，-debug生成执行调试输出 

