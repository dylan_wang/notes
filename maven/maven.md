# maven使用
## 下载安装
### 下载
下载地址:http://maven.apache.org/download.cgi

![下载说明](image/750D721E-0624-4C16-AD4B-9EA5D7F6289A.png)
```

$ wget -b http://mirror.bit.edu.cn/apache/maven/maven-3/3.6.0/binaries/apache-maven-3.6.0-bin.tar.gz
Continuing in background, pid 5791.
Output will be written to ‘wget-log’.
```
### 解压
`$tar -xzf apache-maven-3.6.0-bin.tar.gz`
### 移动
`$sudo mv apache-maven-3.6.0 /opt/`
### 配置环境变量
`$ vim ~/.bashrc`
```
export MAVEN_HOME=/opt/apache-maven-3.6.0
export PATH=${PATH}:${MAVEN_HOME}/bin
```
### 校验安装结果

```
$ mvn -v
Apache Maven 3.6.0 (97c98ec64a1fdfee7767ce5ffb20918da4f719f3; 2018-10-25T08:26:47+13:45)
Maven home: /opt/apache-maven-3.6.0
Java version: 1.8.0_191, vendor: Oracle Corporation, runtime: /opt/jdk1.8.0_191/jre
Default locale: en_US, platform encoding: UTF-8
OS name: "linux", version: "4.13.0-46-generic", arch: "amd64", family: "unix"
```
## 仓库
### 云仓库
修改配置文件：$mvn目录/conf/settings.xml

`vim /opt/apache-maven-3.6.0/conf/settings.xml`

在mirros节点添加远程仓库
```
<mirrors>
    <!-- mirror
     | Specifies a repository mirror site to use instead of a given repository. The repository that
     | this mirror serves has an ID that matches the mirrorOf element of this mirror. IDs are used
     | for inheritance and direct lookup purposes, and must be unique across the set of mirrors.
     |
    <mirror>
      <id>mirrorId</id>
      <mirrorOf>repositoryId</mirrorOf>
      <name>Human Readable Name for this Mirror.</name>
      <url>http://my.repository.com/repo/path</url>
    </mirror>
     -->
	<mirror>
      <id>alimaven</id>
      <name>aliyun maven</name>
      <url>http://maven.aliyun.com/nexus/content/groups/public/</url>
      <mirrorOf>central</mirrorOf>        
    </mirror>
  </mirrors>
```
修改项目中pom.xml文件添加相应的远程仓库
```
<repositories>  
        <repository>  
            <id>alimaven</id>  
            <name>aliyun maven</name>  
            <url>http://maven.aliyun.com/nexus/content/groups/public/</url>  
            <releases>  
                <enabled>true</enabled>  
            </releases>  
            <snapshots>  
                <enabled>false</enabled>  
            </snapshots>  
        </repository>  
</repositories>
```
### 本地仓库
默认位置：$用户目录/.m2/respository/
```
$ pwd
/home/dylan/.m2/repository
 $ ls
classworlds  commons-codec        commons-io    junit  org      xpp3
com          commons-collections  commons-lang  net    xmlpull

```
修改默认位置配置文件：$mvn目录/conf/settings.xml
```
<!-- localRepository
   | The path to the local repository maven will use to store artifacts.
   |
   | Default: ${user.home}/.m2/repository
  <localRepository>/path/to/local/repo</localRepository>
-->
```
默认是被注释掉的需要修改
`<localRepository>/opt/apache-maven-3.6.0/repo</localRepository>`
### 中央仓库
maven社区提供的网络仓库

不需配置！不可更改！

仓库地址：[http://search.maven.org/#browse](http://search.maven.org/#browse)

### 远程仓库

### 仓库读取流程
1. 读取本地仓库
2. 读取中央仓库
3. 检查是否配置远程仓库
    - 输出错误并从远程仓库读取
    - 输出错误
4. 输出错误

## 构建Java项目
### 项目构建语句
`mvn archetype:generate -DgroupId=com.encho.dic -DartifactId=dic-user -DarchetypeArtifactId=maven-archetype-quickstart -DinteractiveMode=false`

- -Dgroupid:组织(公司）名+项目名称
- -Darifactid:项目名-模块名
- -DarchetypeArtifactid:指定创建的项目类型
- -DinteractiveMode:是否使用交互模式

### 项目目录

## eclipse整合
需要m2eclipse插件

### 导入maven项目

elcipse使用自己的maven仓库如果想用本机安装的maven仓库需要配置

进入：Window>Preferences>Maven>Installations 添加本地maven目录

![导入](image/maven-05.png)

进入：User Settings 修改User Settings 的路径为本机配置

配置完成一定要点 Update Settings

![导入](image/maven-06.png)

打开菜单:File>import

![导入](image/maven-02.png)

选择 Existing Maven Projects 点击next

![导入](image/maven-03.png)

选择已有的maven项目点击Finish

导入的时候eclipse还是会下载一些jar包，而且经常卡死，如果发现卡在某个地方不动了不要犹豫，直接关掉重起。

重起之后之前导入的项目会不见需要重新导入。

目录结构

![导入](image/maven-04.png)

### 本地仓库导入jar包
有些jar包由于版权问题无法直接从maven下载，需要手动下载安装到maven本地库中
`mvn install:install-file -DgroupId=com.oracle -DartifactId=ojdbc14 -Dversion=10.2.0.1.0 -Dpackaging=jar -Dfile=/home/dylan/Desktop/ojdbc14.jar`
