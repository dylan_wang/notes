## pom配置
### pom引入jar
通过网址查找需要的jar包

[https://search.maven.org/search](https://search.maven.org/search)

如图查询dom4j

![jar包查询](image/maven-07.png);

点击Latest Version后面括号内的数字查看各个版本

![jar包查询](image/maven-08.png);

选择想要的版本点击行(要点击空白处否则又要查询了)

![jar包查询](image/maven-09.png);

复制右边的代码到项目的pom.xml中即可

```

  <dependencies>
    <dependency>
      <groupId>dom4j</groupId>
      <artifactId>dom4j</artifactId>
      <version>1.6</version>
    </dependency>
  </dependencies>
```

我是用的eclipse做的不需要其他操作直接自动引用，其他的方式不知道要不要更新或者重新构建

### 引入本地其他项目

引入本地其他项目就比较简单了例如将B引入到A只要将B的pom.xml信息添加到A项目即可

B项目pom.xml的开头部分

```

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <groupId>com.encho.dic</groupId>
  <artifactId>util</artifactId>
  <version>0.0.1-SNAPSHOT</version>
  <packaging>jar</packaging>

  <name>util</name>
  <url>http://maven.apache.org</url>
```

引入到A项目的pom.xml dependencies 节点中

```

  <dependencies>
    <dependency>
      <groupId>com.encho.dic</groupId>
      <artifactId>util</artifactId>
      <version>0.0.1-SNAPSHOT</version>
    </dependency>
  </dependencies>
```

