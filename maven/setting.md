### 修改项目默认编译环境

新建了一个项目把原来的代码考过来将然不让用 @Override 注解

百度了一下原来是因为编译环境默认为1.5了，只有1.6以上才支持注解

java -complier

在profiles节点中添加

具体细节以后有时间在研究
```

  <profiles>
   <profile>
    <id>jdk-1.8</id>

    <activation>
        <activeByDefault>true</activeByDefault>  
        <jdk>1.8</jdk>  
    </activation>
    <properties>
        <maven.compiler.source>1.8</maven.compiler.source>  
        <maven.compiler.target>1.8</maven.compiler.target>  
        <maven.compiler.compilerVersion>1.8</maven.compiler.compilerVersion>  
    </properties>
   </profile>
  </profiles>
```

如果项目已经生成右键配置java-complier即可
