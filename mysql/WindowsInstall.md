## windows下安装

设置环境变量

`Path=D:\tools\oracle\mysql-8.0.11-winx64\bin`


mysqld: can't create directory my.ini配置的路径和mysql文件路径不同


runas /user:Administrator cmd

mysqld --initialize --console

新建my.ini

```

[mysqld]
# 设置3306端口
port=3306
# 自定义设置mysql的安装目录，即解压mysql压缩包的目录
basedir=D:\\tools\\oracle\\mysql-8.0.11-winx64
# 自定义设置mysql数据库的数据存放目录
datadir=D:\\tools\\oracle\\mysql-8.0.11-winx64\\Data
# 允许最大连接数
max_connections=200
# 允许连接失败的次数，这是为了防止有人从该主机试图攻击数据库系统
max_connect_errors=10
# 服务端使用的字符集默认为UTF8
character-set-server=utf8
# 创建新表时将使用的默认存储引擎
default-storage-engine=INNODB
# 默认使用“mysql_native_password”插件认证
default_authentication_plugin=mysql_native_password
[mysql]
# 设置mysql客户端默认字符集
default-character-set=utf8
[client]
# 设置mysql客户端连接服务端时默认使用的端口和默认字符集
port=3306
default-character-set=utf8
```

这里要使用双斜杠，我的电脑上单斜杠会报错。不知道为什么，明明网上很多写单斜杠的。

mysqld: can't create directory my.ini配置的路径和mysql文件路径不同。

```

# 自定义设置mysql的安装目录，即解压mysql压缩包的目录
basedir=D:\\tools\\oracle\\mysql-8.0.11-winx64

# 自定义设置mysql数据库的数据存放目录
datadir=D:\\tools\\oracle\\mysql-8.0.11-winx64\\Data
```

执行`mysqld --initialize --console`命令

```


D:\tools\oracle\mysql-8.0.11-winx64>mysqld --initialize --console
2019-04-17T14:36:59.667310Z 0 [System] [MY-013169] [Server] D:\tools\oracle\mysq
l-8.0.11-winx64\bin\mysqld.exe (mysqld 8.0.11) initializing of server in progres
s as process 12252
2019-04-17T14:37:21.362551Z 5 [Note] [MY-010454] [Server] A temporary password i
s generated for root@localhost: *irqsu!:g9dW
2019-04-17T14:37:41.774718Z 0 [System] [MY-013170] [Server] D:\tools\oracle\mysq
l-8.0.11-winx64\bin\mysqld.exe (mysqld 8.0.11) initializing of server has comple
ted
```

这个是密码root@localhost: *irqsu!:g9dW

注册服务`mysqld --install mysql8`

启动服务`net start mysql8`

停止服务`net stop mysql8`

修改密码

`ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'Root_123';`