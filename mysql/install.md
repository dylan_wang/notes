## yum在线安装
### 在官网下载rmp包
```

$ ls
mysql-8.0.13-linux-glibc2.12-x86_64.tar.xz
mysql80-community-release-el7-1.noarch.rpm
```
### 安装rpm包
```

sudo rpm -Uvh mysql80-community-release-el7-1.noarch.rpm 
[sudo] dylan 的密码：
警告：mysql80-community-release-el7-1.noarch.rpm: 头V3 DSA/SHA1 Signature, 密钥 ID 5072e1f5: NOKEY
准备中...                          ################################# [100%]
正在升级/安装...
   1:mysql80-community-release-el7-1  ################################# [100%]
```
### 使用yum查看软件包
```

yum list mysql*
已加载插件：fastestmirror, langpacks
Loading mirror speeds from cached hostfile
mysql-connectors-community                               | 2.5 kB     00:00
mysql-tools-community                                    | 2.5 kB     00:00
mysql80-community                                        | 2.5 kB     00:00
(1/3): mysql80-community/x86_64/primary_db                 |  35 kB   00:02
(2/3): mysql-connectors-community/x86_64/primary_db        |  29 kB   00:05
(3/3): mysql-tools-community/x86_64/primary_db             |  48 kB   00:11
```
### 安装mysql
```

sudo yum install mysql-community-server
已加载插件：fastestmirror, langpacks
Loading mirror speeds from cached hostfile
正在解决依赖关系
--> 正在检查事务
---> 软件包 mysql-community-server.x86_64.0.8.0.13-1.el7 将被 安装
--> 正在处理依赖关系 mysql-community-common(x86-64) = 8.0.13-1.el7，它被软件包 mysql-community-server-8.0.13-1.el7.x86_64 需要
--> 正在处理依赖关系 mysql-community-client(x86-64) >= 8.0.0，它被软件包 mysql-community-server-8.0.13-1.el7.x86_64 需要
--> 正在检查事务
---> 软件包 mysql-community-client.x86_64.0.8.0.13-1.el7 将被 安装
--> 正在处理依赖关系 mysql-community-libs(x86-64) >= 8.0.0，它被软件包 mysql-community-client-8.0.13-1.el7.x86_64 需要
---> 软件包 mysql-community-common.x86_64.0.8.0.13-1.el7 将被 安装
--> 正在检查事务
---> 软件包 mariadb-libs.x86_64.1.5.5.60-1.el7_5 将被 取代
--> 正在处理依赖关系 libmysqlclient.so.18()(64bit)，它被软件包 2:postfix-2.10.1-7.el7.x86_64 需要
--> 正在处理依赖关系 libmysqlclient.so.18(libmysqlclient_18)(64bit)，它被软件包 2:postfix-2.10.1-7.el7.x86_64 需要
---> 软件包 mysql-community-libs.x86_64.0.8.0.13-1.el7 将被 舍弃
--> 正在检查事务
---> 软件包 mysql-community-libs-compat.x86_64.0.8.0.13-1.el7 将被 舍弃
--> 解决依赖关系完成

依赖关系解决

================================================================================
 Package                      架构    版本             源                  大小
================================================================================
正在安装:
 mysql-community-libs         x86_64  8.0.13-1.el7     mysql80-community  2.3 M
      替换  mariadb-libs.x86_64 1:5.5.60-1.el7_5
 mysql-community-libs-compat  x86_64  8.0.13-1.el7     mysql80-community  2.1 M
      替换  mariadb-libs.x86_64 1:5.5.60-1.el7_5
 mysql-community-server       x86_64  8.0.13-1.el7     mysql80-community  381 M
为依赖而安装:
 mysql-community-client       x86_64  8.0.13-1.el7     mysql80-community   26 M
 mysql-community-common       x86_64  8.0.13-1.el7     mysql80-community  554 k

事务概要
================================================================================
安装  3 软件包 (+2 依赖软件包)

总下载量：412 M
Is this ok [y/d/N]: 
Downloading packages:
警告：/var/cache/yum/x86_64/7/mysql80-community/packages/mysql-community-common-8.0.13-1.el7.x86_64.rpm: 头V3 DSA/SHA1 Signature, 密钥 ID 5072e1f5: NOKEY
mysql-community-common-8.0.13-1.el7.x86_64.rpm 的公钥尚未安装
(1/5): mysql-community-common-8.0.13-1.el7.x86_64.rpm      | 554 kB   00:08     
(2/5): mysql-community-libs-8.0.13-1.el7.x86_64.rpm        | 2.3 MB   00:01     
(3/5): mysql-community-libs-compat-8.0.13-1.el7.x86_64.rpm | 2.1 MB   00:01     
(4/5): mysql-community-client-8.0.13-1.el7.x86_64.rpm      |  26 MB   00:24     
(5/5): mysql-community-server-8.0.13-1.el7.x86_64.rpm      | 381 MB   02:36     
--------------------------------------------------------------------------------
总计                                               2.5 MB/s | 412 MB  02:47     
从 file:///etc/pki/rpm-gpg/RPM-GPG-KEY-mysql 检索密钥
导入 GPG key 0x5072E1F5:
 用户ID     : "MySQL Release Engineering <mysql-build@oss.oracle.com>"
 指纹       : a4a9 4068 76fc bd3c 4567 70c8 8c71 8d3b 5072 e1f5
 软件包     : mysql80-community-release-el7-1.noarch (installed)
 来自       : /etc/pki/rpm-gpg/RPM-GPG-KEY-mysql
是否继续？[y/N]：
Running transaction check
Running transaction test
Transaction test succeeded
Running transaction
警告：RPM 数据库已被非 yum 程序修改。
  正在安装    : mysql-community-common-8.0.13-1.el7.x86_64                  1/6 
  正在安装    : mysql-community-libs-8.0.13-1.el7.x86_64                    2/6 
  正在安装    : mysql-community-client-8.0.13-1.el7.x86_64                  3/6 
  正在安装    : mysql-community-server-8.0.13-1.el7.x86_64                  4/6 
  正在安装    : mysql-community-libs-compat-8.0.13-1.el7.x86_64             5/6 
  正在删除    : 1:mariadb-libs-5.5.60-1.el7_5.x86_64                        6/6 
  验证中      : mysql-community-libs-compat-8.0.13-1.el7.x86_64             1/6 
  验证中      : mysql-community-libs-8.0.13-1.el7.x86_64                    2/6 
  验证中      : mysql-community-common-8.0.13-1.el7.x86_64                  3/6 
  验证中      : mysql-community-server-8.0.13-1.el7.x86_64                  4/6 
  验证中      : mysql-community-client-8.0.13-1.el7.x86_64                  5/6 
  验证中      : 1:mariadb-libs-5.5.60-1.el7_5.x86_64                        6/6 

已安装:
  mysql-community-libs.x86_64 0:8.0.13-1.el7                                    
  mysql-community-libs-compat.x86_64 0:8.0.13-1.el7                             
  mysql-community-server.x86_64 0:8.0.13-1.el7                                  

作为依赖被安装:
  mysql-community-client.x86_64 0:8.0.13-1.el7                                  
  mysql-community-common.x86_64 0:8.0.13-1.el7                                  

替代:
  mariadb-libs.x86_64 1:5.5.60-1.el7_5                                          

完毕！
```

### 启动mysql服务
```

$ sudo service mysqld start
Redirecting to /bin/systemctl start mysqld.service
```

### 查看服务状态
虽然完全看不懂什么意思应该是没问题
```

$ sudo service mysqld status
[sudo] dylan 的密码：
Redirecting to /bin/systemctl status mysqld.service
● mysqld.service - MySQL Server
   Loaded: loaded (/usr/lib/systemd/system/mysqld.service; enabled; vendor preset: disabled)
   Active: active (running) since 一 2018-12-31 17:20:18 CST; 2min 31s ago
     Docs: man:mysqld(8)
           http://dev.mysql.com/doc/refman/en/using-systemd.html
  Process: 22668 ExecStartPre=/usr/bin/mysqld_pre_systemd (code=exited, status=0/SUCCESS)
 Main PID: 22775 (mysqld)
   Status: "SERVER_OPERATING"
    Tasks: 38
   CGroup: /system.slice/mysqld.service
           └─22775 /usr/sbin/mysqld

12月 31 17:19:58 localhost.localdomain systemd[1]: Starting MySQL Server...
12月 31 17:20:18 localhost.localdomain systemd[1]: Started MySQL Server.
```
### 查看root用户初始密码
```

sudo grep 'temporary password' /var/log/mysqld.log
2018-12-31T09:20:10.936952Z 5 [Note] [MY-010454] [Server] A temporary password is generated for root@localhost: UYsn>:-q6l4y
```
可以看出来密码是：UYsn>:-q6l4y。太复杂了

ps:不要尝试手打！！！！

### 登陆并修改密码
Mysql 在安装的时候会默认安装一个validate_password的插件用于保护密码安全（我觉得没啥用）

密码要求：至少包含一个大写字母、一个小写字母、一个数字和一个特殊字符，并且密码的总长度至少为8个字符

这对于我这种没事拿来玩玩的人来说太麻烦了，后面在学习学习怎么解决把

```

mysql -uroot -p
Enter password: 
Welcome to the MySQL monitor.  Commands end with ; or \g.
........
mysql> ALTER USER 'root'@'localhost' IDENTIFIED BY 'Root_123';
Query OK, 0 rows affected (0.05 sec)
```

到此Mysql算是安装完了，不过这个需要在有网络的条件下使用也有没有网也能安装的也有其他方法，后面再学

-----

## 离线安装
### 安装说明
1. 如果之前使用包管理器安装过Mysql需要将安装包彻底删除，包括配置文件(/etc/my.cnf或者/etc/mysql/)和数据文件。
2. Mysql依赖于libaio库，如果未在本地安装此库，则数据目录初始化和随后的服务器启动步骤将失败。
3. Mysql-5.7.19以后的版本依赖于libnuma库

### 简易步骤

```
#创建mysql用户组
shell> groupadd mysql
#创建用户 -s /bin/false mysql 禁止用户登陆
shell> useradd -r -g mysql -s /bin/false mysql
#移动到安装目录
shell> cd /usr/local
#解压
shell> tar xvf /path/to/mysql-VERSION-OS.tar.xz
#解压到
shell> ln -s full-path-to-mysql-VERSION-OS mysql
shell> cd mysql
shell> mkdir mysql-files
shell> chown mysql:mysql mysql-files
shell> chmod 750 mysql-files
shell> bin/mysqld --initialize --user=mysql
shell> bin/mysql_ssl_rsa_setup
shell> bin/mysqld_safe --user=mysql &
# Next command is optional
shell> cp support-files/mysql.server /etc/init.d/mysql.server
```

### 下载和安装

下载页面：[https://dev.mysql.com/downloads/mysql/](https://dev.mysql.com/downloads/mysql/)

```

$ wget https://cdn.mysql.com//Downloads/MySQL-8.0/mysql-8.0.13-linux-glibc2.12-x86_64.tar.xz
.......
$ ls
  mysql-8.0.13-linux-glibc2.12-x86_64.tar.xz
````

解压文件

`tar xf mysql-8.0.13-linux-glibc2.12-x86_64.tar.xz`

移动到其他文件夹

`$ mv mysql-8.0.13-linux-glibc2.12-x86_64 /opt/mysql`

配置环境变量(在~/.bashrc文件末尾追加)

`export PATH=$PATH:/opt/mysql/bin`

### 初始化数据库

创建mysql-files目录

`$ mkdir mysql-files`

初始化数据目录

- 包括包含初始mysql grant表的mysql数据库
- 确定如何允许用户连接到服务器
- 初始化是在首次安装是需要初始化，如果只是升级使用 mysql_upgrade
- 初始化不会覆盖原有信息所以不会影响已有的权限
```

$ bin/mysqld --initialize --user=dylan
2019-01-07T08:55:20.872194Z 0 [Warning] [MY-010139] [Server] Changed limits: max_open_files: 1024 (requested 8161)
2019-01-07T08:55:20.873119Z 0 [Warning] [MY-010142] [Server] Changed limits: table_open_cache: 431 (requested 4000)
2019-01-07T08:55:20.877325Z 0 [System] [MY-013169] [Server] /opt/mysql/bin/mysqld (mysqld 8.0.13) initializing of server in progress as process 28657
2019-01-07T08:55:31.169458Z 5 [Note] [MY-010454] [Server] A temporary password is generated for root@localhost: iidJ&DMu68qt
2019-01-07T08:55:35.936305Z 0 [System] [MY-013170] [Server] /opt/mysql/bin/mysqld (mysqld 8.0.13) initializing of server has completed
```

以上初始化使用系统默认配置，启动方式如下

`$ bin/mysqld_safe --user=dylan &`

### 添加配置文件

创建Mysql配置文件：my.cnf

`vim /opt/mysql/my.cnf`

在my.cnf中写入下面内容

```

[mysqld]
basedir=/opt/mysql
datadir=/opt/mysql/data
```

经配置文件引入mysql
```

$ bin/mysqld --defaults-file=/opt/mysql/my.cnf --initialize --user=dylan
2019-01-07T10:48:29.419824Z 0 [Warning] [MY-010139] [Server] Changed limits: max_open_files: 1024 (requested 8161)
2019-01-07T10:48:29.420375Z 0 [Warning] [MY-010142] [Server] Changed limits: table_open_cache: 431 (requested 4000)
2019-01-07T10:48:29.462477Z 0 [System] [MY-013169] [Server] /opt/mysql/bin/mysqld (mysqld 8.0.13) initializing of server in progress as process 11497
2019-01-07T10:48:36.463821Z 5 [Note] [MY-010454] [Server] A temporary password is generated for root@localhost: OvKtdpZqK2/J
2019-01-07T10:48:41.021849Z 0 [System] [MY-013170] [Server] /opt/mysql/bin/mysqld (mysqld 8.0.13) initializing of server has completed
```

### 登陆并修改密码

Mysql 在安装的时候会默认安装一个validate_password的插件用于保护密码安全（我觉得没啥用）

密码要求：至少包含一个大写字母、一个小写字母、一个数字和一个特殊字符，并且密码的总长度至少为8个字符

这对于我这种没事拿来玩玩的人来说太麻烦了，后面在学习学习怎么解决把

```

mysql -uroot -p
Enter password: 
Welcome to the MySQL monitor.  Commands end with ; or \g.
........
mysql> ALTER USER 'root'@'localhost' IDENTIFIED BY 'Root_123';
Query OK, 0 rows affected (0.05 sec)
```

