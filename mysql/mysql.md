>MySQL distributions on Unix and Unix-like system include a script named mysql.server, which starts the MySQL server using mysqld_safe. It can be used on systems such as Linux and Solaris that use System V-style run directories to start and stop system services. It is also used by the macOS Startup Item for MySQL. 

在类Unix和类Unix系统上的mysql分布包括一个名为mysql.server的脚本，它使用mysqld_safe启动mysql服务器。它可以用于Linux和Solaris等使用SystemV-Style运行目录启动和停止系统服务的系统。它也被用于mysql的macos启动项。

>mysql.server is the script name as used within the MySQL source tree. The installed name might be different; for example, mysqld or mysql. In the following discussion, adjust the name mysql.server as appropriate for your system. 

server是mysql源树中使用的脚本名。安装的名称可能不同；例如，mysqld或mysql。在下面的讨论中，根据您的系统调整名称mysql.server。 

>>For some Linux platforms, MySQL installation from RPM or Debian packages includes systemd support for managing MySQL server startup and shutdown. On these platforms, mysql.server and mysqld_safe are not installed because they are unnecessary. For more information, see Section 2.5.9, “Managing MySQL Server with systemd”. 

对于某些Linux平台，RPM或Debian软件包中的mysql安装包括用于管理mysql服务器启动和关闭的systemd支持。在这些平台上，没有安装mysql.server和mysqld_safe，因为它们是不必要的。有关更多信息，请参见第2.5.9节“使用systemd管理MySQL服务器”。

>To start or stop the server manually using the mysql.server script, invoke it from the command line with start or stop arguments: 

要使用mysql.server脚本手动启动或停止服务器，请使用start或stop参数从命令行调用该脚本： 

```

shell> mysql.server start
shell> mysql.server stop
```

>mysql.server changes location to the MySQL installation directory, then invokes mysqld_safe. To run the server as some specific user, add an appropriate user option to the [mysqld] group of the global /etc/my.cnf option file, as shown later in this section. (It is possible that you must edit mysql.server if you've installed a binary distribution of MySQL in a nonstandard location. Modify it to change location into the proper directory before it runs mysqld_safe. If you do this, your modified version of mysql.server may be overwritten if you upgrade MySQL in the future; make a copy of your edited version that you can reinstall.) 

server将位置更改为mysql安装目录，然后调用mysqld_safe。要以特定用户的身份运行服务器，请向global/etc/my.cnf选项文件的[mysqld]组添加适当的用户选项，如本节后面所示。（如果在非标准位置安装了MySQL的二进制分发版，则可能必须编辑mysql.server。在运行mysqld_safe之前，修改它以将位置更改为正确的目录。如果这样做，如果以后升级mysql，修改后的mysql.server版本可能会被覆盖；请复制编辑后的版本，以便重新安装。） 

>mysql.server stop stops the server by sending a signal to it. You can also stop the server manually by executing mysqladmin shutdown. 

server stop通过向服务器发送信号来停止服务器。也可以通过执行mysqladmin shutdown手动停止服务器。 

>To start and stop MySQL automatically on your server, you must add start and stop commands to the appropriate places in your /etc/rc* files: 

要在服务器上自动启动和停止MySQL，必须将启动和停止命令添加到/etc/rc*文件中的相应位置： 

>- If you use the Linux server RPM package (MySQL-server-VERSION.rpm), or a native Linux package installation, the mysql.server script may be installed in the /etc/init.d directory with the name mysqld or mysql. See Section 2.5.4, “Installing MySQL on Linux Using RPM Packages from Oracle”, for more information on the Linux RPM packages. 

- 如果使用Linux Server RPM包（mysql server version.rpm）或本机Linux包安装，mysql.server脚本可能安装在/etc/init.d目录中，名称为mysql d或mysql。有关Linux RPM包的更多信息，请参阅第2.5.4节“使用Oracle的RPM包在Linux上安装MySQL”。 

>- If you install MySQL from a source distribution or using a binary distribution format that does not install mysql.server automatically, you can install the script manually. It can be found in the support-files directory under the MySQL installation directory or in a MySQL source tree. Copy the script to the /etc/init.d directory with the name mysql and make it executable: 

- 如果从源分发版安装mysql，或者使用不自动安装mysql.server的二进制分发格式，则可以手动安装脚本。它可以在MySQL安装目录下的支持文件目录或MySQL源代码树中找到。将脚本复制到名为mysql的/etc/init.d目录，并使其可执行：

```

shell> cp mysql.server /etc/init.d/mysql
shell> chmod +x /etc/init.d/mysql
```

>After installing the script, the commands needed to activate it to run at system startup depend on your operating system. On Linux, you can use chkconfig: 

安装脚本后，在系统启动时激活它所需的命令取决于您的操作系统。在Linux上，可以使用chkconfig:

`shell> chkconfig --add mysql`

>On some Linux systems, the following command also seems to be necessary to fully enable the mysql script: 

在某些Linux系统上，完全启用mysql脚本似乎还需要以下命令：

`shell> chkconfig --level 345 mysql on`

>- On FreeBSD, startup scripts generally should go in /usr/local/etc/rc.d/. Install the mysql.server script as /usr/local/etc/rc.d/mysql.server.sh to enable automatic startup. The rc(8) manual page states that scripts in this directory are executed only if their base name matches the *.sh shell file name pattern. Any other files or directories present within the directory are silently ignored. 

- 在freebsd上，启动脚本通常应进入/usr/local/etc/rc.d/。安装mysql.server脚本为/usr/local/etc/rc.d/mysql.server.sh以启用自动启动。rc（8）手册页说明，只有当脚本的基名称与*.sh shell文件名模式匹配时，才会执行此目录中的脚本。目录中存在的任何其他文件或目录都将被静默忽略。 

>- As an alternative to the preceding setup, some operating systems also use /etc/rc.local or /etc/init.d/boot.local to start additional services on startup. To start up MySQL using this method, append a command like the one following to the appropriate startup file: 

- 作为上述设置的替代方案，一些操作系统还使用/etc/rc.local或/etc/init.d/boot.local在启动时启动其他服务。要使用此方法启动MySQL，请将如下命令附加到相应的启动文件中：

`/bin/sh -c 'cd /usr/local/mysql; ./bin/mysqld_safe --user=mysql &'`

>- For other systems, consult your operating system documentation to see how to install startup scripts. 

- 对于其他系统，请参阅操作系统文档以了解如何安装启动脚本。

