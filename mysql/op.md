create schema [数据库名称] default character set utf8 collate utf8_general_ci;

create schema JPress default character set utf8 collate utf8_general_ci;

create user '[用户名称]'@'%' identified by '[用户密码]';

create user 'JPress'@'%' identified by 'JPress_123';

grant select,insert,update,delete,create on [数据库名称].* to [用户名称];

grant select,insert,update,delete,create on JPress.* to JPress;

flush privileges; 

```

create schema JPress default character set utf8 collate utf8_general_ci;

create user 'JPress'@'%' identified by 'JPress_123';

grant select,insert,update,delete,create on JPress.* to JPress;

grant all privileges on JPress.* to JPress

flush privileges;
```

--查询用户
select user,host from mysql.user;

3.删除用户

--删除用户“test”
drop user test@localhost ;
--若创建的用户允许任何电脑登陆，删除用户如下
drop user test@'%';

4.更改密码

--方法1，密码实时更新；修改用户“test”的密码为“1122”
set password for test =password('1122');
--方法2，需要刷新；修改用户“test”的密码为“1234”
update  mysql.user set  password=password('1234')  where user='test'
--刷新
flush privileges;

5.用户分配权限

--授予用户test通过外网IP对数据库“testdb”的全部权限
grant all privileges on 'testdb'.* to 'test'@'%' identified by '1234';  

--刷新权限
flush privileges; 

--授予用户“test”通过外网IP对于该数据库“testdb”中表的创建、修改、删除权限,以及表数据的增删查改权限
grant create,alter,drop,select,insert,update,delete on testdb.* to test@'%';     

6.查看用户权限

--查看用户“test”
show grants for test;
